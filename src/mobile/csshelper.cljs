(ns mobile.csshelper)

(defn transition-css
  [transition]
  {:transition         transition
   :-webkit-transition transition
   :-moz-transition    transition
   :-o-transition      transition})
