(ns mobile.db)

(def default-db
  {:page      :annc-by-date
   :search    {:show-suggestion false
               :suggestions     []
               :input-val       nil
               :favorstocks     #{}}
   :flash-tip {:show false}})
