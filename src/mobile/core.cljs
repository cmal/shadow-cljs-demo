(ns mobile.core
  (:require [reagent.core :as r]
            [re-frame.core :as rf]
            [secretary.core :as secretary]
            [goog.events :as events]
            [goog.history.EventType :as HistoryEventType]
            [ajax.core :refer [GET POST]]
            [mobile.ajax :refer [load-interceptors!]]
            [mobile.events]
            [garden.core :refer [css]]
            [clojure.string :as str]
            [mobile.csshelper :refer [transition-css]])
  (:import goog.History))

(defn base "根据设计图基础尺寸计算rem"
  [px]
  (str (/ px 40 #_(/ 640 16)) "rem"))

(defn stockid2secucode ""
  [stockid]
  (str/join (take 6 stockid)))

(defn search-bar []
  (let [show-search-right-x (r/atom false)
        input-val (rf/subscribe [:search-input-val])]
    (fn []
      [:div.search-bar
       {:style {:height (base 80)}}
       [:div#stock-search
        {:style {:background "#fff"
                 :padding    (str (base 12) " " (base 18))
                 :height     (base 56)
                 :position   "relative"}}
        [:a {:href  "/"
             :style {:display     "block"
                     :float       "left"
                     :padding-top (base 6)}}
         [:img.logo {:src   "/common/img/logo.png"
                     :style {:height (base 44)
                             :width  (base (/ (* 44 375) 138))}}]]
        [:div.input
         {:style     {:height           (base 54)
                      :float            "left"
                      :margin-left      (base 16)
                      :width            (base 464)
                      :position         "relative"
                      :display          "block"
                      :border           "solid 1px #e6e6e6"
                      :border-radius    (base 5)
                      :background-color "#f2f3f3"}}
         [:div.icon-wrapper
          {:style {:position                  "absolute"
                   :display                   "inline"
                   :line-height               (base 54)
                   :color                     "#aaa"
                   :border                    0
                   :border-radius             (base 5)
                   :font-size                 (base 24)
                   :background                "#f2f3f3"
                   :padding-left              (base 16)
                   :border-top-left-radius    (base 8)
                   :border-bottom-left-radius (base 8)}}
          [:i.icon.icon-search]]
         [:input {:type        "text"
                  :value       @input-val
                  :name        "search-bar-input"
                  :placeholder "请输入您要查询的股票名称或代码"
                  :on-focus    (fn [e]
                                 (reset! show-search-right-x true)
                                 (rf/dispatch [:search-show-suggestion true]))
                  :on-blur     (fn [e]
                                 (js/setTimeout
                                  (reset! show-search-right-x false))
                                 (rf/dispatch [:search-show-suggestion false]))
                  :on-change   (fn [e]
                                 (when-let [val (->> e .-target .-value)]
                                   (rf/dispatch [:search-input-val val])
                                   (rf/dispatch [:search-keyword val])))
                  :style       {:background                "#f2f3f3"
                                :width                     "10rem"
                                :padding                   (str "0 0 0 " (base 54))
                                :border                    0
                                :border-radius             (base 5)
                                :outline                   "none"
                                :line-height               (base 54)
                                :font-size                 (base 24)
                                :vertical-align            "top"
                                :border-top-left-radius    (base 8)
                                :border-bottom-left-radius (base 8)}}]
         [:div.search-right-wrapper
          {:style {:position    "absolute"
                   :top         0
                   :right       0
                   :display     (if @show-search-right-x "block" "none")
                   :float       "right"
                   :line-height (base 54)
                   :color       "#aaa"
                   :font-size   (base 24)
                   :padding     (str "0 " (base 16))
                   :transform   (str "stranslateY(" (base (- 54 1)) ")")}
           :on-click (fn [e] (rf/dispatch [:search-clear-input]))}
          [:i.icon.icon-times]]]]])))


(defn icon-wrapper-add-fav
  [class-name]
  [:span.icon-wrapper
   {:style {:position "relative"
            :top      (base 7)}}
   [(keyword (str "i.icon." class-name))
    {:style {:font-size (base 38)}}]])

(defn suggestions
  []
  (let [show        (rf/subscribe [:search-show-suggestion])
        suggestions (rf/subscribe [:search-suggestions])
        _           (rf/dispatch [:search-load-favorstocks])
        favorstocks (rf/subscribe [:search-favorstocks])]
    (fn []
      [:div.suggestions
       {:style {:clear      "both"
                :display    (if @show "block" "none")
                :background "#fff"
                :padding    "0 0.9rem"
                :overflow   "auto"
                :position   "absolute"
                :top        "2rem"
                :width      "100%"
                :box-sizing "border-box"
                :z-index    "100"
                :box-shadow "0px 5px 5px 0px rgba(130,130,130,.25)"
                :border-top "1px solid #eee"}}
       (for [{:keys [stockid stockname]} @suggestions
             :let [secucode (stockid2secucode stockid)]]
         ^{:key stockid}
         [:div.suggestion-item
          [:span.suggestion-name
           {:style {:float "left"
                    :width "25%"}}
           stockname]
          [:span.suggestion-id
           {:style {:float        "left"
                    :padding-left ".6rem"}}
           secucode]
          [:span.add-wrapper
           {:style {:padding-left "1rem"
                    :right        0
                    :float        "right"}}
           [:span.add
            {:on-click (fn [e]
                         (rf/dispatch [:search-add-favorstock secucode]))
             :style {:display (if (contains? favorstocks secucode) "none" "inline")
                     :color   "#284a7d"}}
            "加自选"
            [icon-wrapper-add-fav "icon-add"]]
           [:span.added
            {:style {:display (if (contains? favorstocks secucode) "inline" "none")
                     :color   "#666"}}
            "已添加"
            [icon-wrapper-add-fav "icon-added"]]]])])))

(defn flash-tip
  [icon-name text-line-1 text-line-2]
  (let [show (rf/subscribe [:flash-tip-show])]
    (fn [icon-name text-line-1 text-line-2]
      [:div.flash-tip
       {:style (merge {:display          (if @show "block" "none")
                       :position         "absolute"
                       :top              "50%"
                       :left             "50%"
                       :background-color "rgba(51,51,51,.75)"
                       :width            (base 160)
                       :height           (base 160)
                       :border-radius    (base 16)
                       :transform        "translate(-50%,-50%)"
                       :color            "#fff"
                       :z-index          10000
                       :padding          0
                       :text-align       "center"
                       :opacity          (if @show 1 0)
                       }
                      (transition-css "opacity 1s ease-out"))}
       [:div.icon-wrapper
        {:style {:padding-top    (base 25)
                 :padding-bottom (base 15)
                 :font-size      (base 42)
                 :width          "100%"}}
        [(keyword (str "i.icon.icon-" icon-name))]]
       [:div.text-wrapper
        {:style {:font-size (base 24)
                 :width     "100%"}}
        [:div.text-line-1
         text-line-1]
        [:div.text-line-2
         text-line-2]]])))

(defn search-bar-component []
  [:div
   [search-bar]
   [suggestions]
   [flash-tip "icon-check-circle" "已添加" "至自选"]
   [flash-tip "icon-times" "添加自选" "失败"]])

(defn annc-by-date []
  [:div.annc-by-date
   [search-bar-component]])

(defn annc-by-stock []
  [:div.annc-by-stock
   [search-bar-component]])

(def pages
  {:annc-by-date  #'annc-by-date
   :annc-by-stock #'annc-by-stock})

(defn stylesheets
  []
  ;; use gardent to generate css string and wrap in a script tag
  ;; all script tag will be applied globally
  [:style (css
           [:div.suggestion-item
            {:position      "relative"
             :height        "1.5rem"
             :line-height   "1.5rem"
             :border-bottom "1px solid #eee"
             :font-size     ".7rem"}
            [:&:last-child
             {:border "none"}]]
           ;; add other [:div]s
           [:body {:margin 0}]
           )])

(defn page []
  [:div
   [stylesheets] ;; works only in html5
   [(pages @(rf/subscribe [:page]))]])

;; -------------------------
;; Routes
(secretary/set-config! :prefix "#")

(secretary/defroute "/annc-by-date" []
  (rf/dispatch [:set-active-page :annc-by-date]))

(secretary/defroute "/annc-by-stock" []
  (rf/dispatch [:set-active-page :annc-by-stock]))

;; -------------------------
;; History
;; must be called after routes have been defined
(defn hook-browser-navigation! []
  (doto (History.)
    (events/listen
     HistoryEventType/NAVIGATE
     (fn [event]
       (secretary/dispatch! (.-token event))))
    (.setEnabled true)))

;; -------------------------
;; Initialize app

(defn mount-components []
  (rf/clear-subscription-cache!)
  (r/render [#'page] (.getElementById js/document "app")))

(defn ^:export init! []
  (rf/dispatch-sync [:initialize-db])
  (load-interceptors!)
  (hook-browser-navigation!)
  (mount-components))

#_(defn ^:export add)
