(ns mobile.events
  (:require [mobile.db :as db]
            [day8.re-frame.http-fx]
            [ajax.core :as ajax]
            [re-frame.core :refer [dispatch reg-event-db reg-event-fx reg-sub path]]))

;;dispatchers
;; m.beta.joudou.com
(def hostname "http://localhost:6787")
(def env "beta") ;; or www

(reg-event-db
 :initialize-db
 (fn [_ _]
   db/default-db))

(reg-event-db
 :set-active-page
 (fn [db [_ page]]
   (assoc db :page page)))

(def search-interceptors
  [(path :search)])

(def flash-tip-interceptors
  [(path :flash-tip)])

(reg-event-db
 :search-show-suggestion
 search-interceptors
 (fn [search [_ show?]]
   (assoc search :show-suggestion show?)))

(reg-event-db
 :search-suggestions
 search-interceptors
 (fn [search [_ suggestions]]
   (assoc search :suggestions suggestions)))

(reg-event-db
 :search-input-val
 search-interceptors
 (fn [search [_ input-val]]
   (assoc search :input-val input-val)))

(reg-event-db
 :search-clear-input
 search-interceptors
 (fn [search _]
   (assoc search :input-val nil)))

(reg-event-fx
 :search-keyword
 (fn [_ [_ word]]
   {:http-xhrio {:method          :get
                 :uri             (str hostname "/p/" env "/stockinfogate/stock/namefinder?keyword=" word)
                 :response-format (ajax/json-response-format {:keywords? true})
                 :on-success      [:search-keyword-success]
                 :on-failure      [:search-keyword-failed]}}))

(reg-event-db
 :search-keyword-success
 search-interceptors
 (fn [search [_ result]]
   (assoc search :suggestions (:data result))))

(reg-event-db
 :search-keyword-failed
 search-interceptors
 (fn [search _]
   search))

(reg-event-fx
 :search-load-favorstocks
 (fn [_ _]
   {:http-xhrio {:method          :get
                 :uri             (str hostname "/p/" env "/stockinfogate/user/favorstock/list")
                 :response-format (ajax/json-response-format {:keywords? true})
                 :on-success      [:search-load-favorstocks-success]
                 :on-failure      [:search-load-favorstocks-failed]}}))

(reg-event-db
 :search-load-favorstocks-success
 search-interceptors
 (fn [search [_ result]]
   (assoc search :favorstocks (set (map :secucode (:data result))))))

(reg-event-db
 :search-load-favorstocks-failed
 search-interceptors
 (fn [search _]
   search))

(reg-event-fx
 :search-add-favorstock
 (fn [{:keys [db]} [_ secucode]]
   {:db       (do
                (println (:favorstocks db))
                (update-in db
                           [:search :favorstocks] conj secucode))
    :dispatch [:search-favorstock-add secucode]}))

(reg-event-fx
 :search-favorstock-add
 (fn [_ [_ secucode]]
   {:http-xhrio {:method          :post
                 :uri             (str hostname "/p/" env "/stockinfogate/user/favorstock/add")
                 ;;       :params {:here ["is" "a" "map"]} ;; this cannot be parsed by websvc
                 :body            (doto (js/FormData.)
                                    (.append "secucode" secucode))
                 :format          (ajax/json-request-format)
                 :response-format (ajax/json-response-format {:keywords? true})
                 :on-success      [:search-favorstock-add-success]
                 :on-failure      [:search-favorstock-add-failed]}}))

(reg-event-fx
 :search-favorstock-add-success
 search-interceptors
 (fn [search [_ result]]
   (if (:status result)
     {:db             search
      :dispatch-n     (list [:deregister-event-handler
                             [:flash-tip-success-add
                              :flash-tip-fail-add
                              :flash-tip-hide]]
                            (when (:data result)
                              [:flash-tip-success-add :favorstock-add-success]
                              [:search-load-favorstocks]))
      :dispatch-later [{:ms 200 :dispatch [:flash-tip-hide]}]
      }
     {:db       search
      :dispatch [:search-favorstock-add-failed]})))

(reg-event-fx
 :search-favorstock-add-failed
 search-interceptors
 (fn [search _]
   {:db             search
    :dispatch-n     (list [:deregister-event-handler
                           [:flash-tip-success-add
                            :flash-tip-fail-add
                            :flash-tip-hide]]
                          [:flash-tip-fail-add :favorstock-add-failed])
    :dispatch-later [{:ms 200 :dispatch [:flash-tip-hide]}]}))

(reg-event-db
 :flash-tip-success-add
 flash-tip-interceptors
 ;; 显示 tip success
 (fn [flash-tip [_ kw]]
   (assoc flash-tip
          :show true
          :kw kw)))
;; should define the data showed respond to specific keys

(reg-event-db
 :flash-tip-fail-add
 flash-tip-interceptors
 ;; 显示 tip failed
 (fn [flash-tip [_ kw]]
   (assoc flash-tip
          :show true
          :kw kw)))

(reg-event-db
 :flash-tip-hide
 flash-tip-interceptors
 (fn [flash-tip _]
   (assoc flash-tip :show false)))

;;subscriptions
(reg-sub
 :page
 (fn [db _]
   (:page db)))

(reg-sub
 :search
 (fn [db _]
   (:search db)))

(reg-sub
 :search-show-suggestion
 :<- [:search]
 (fn [search _]
   (and (:show-suggestion search)
        (not (empty? (:suggestions search))))))

(reg-sub
 :search-suggestions
 :<- [:search]
 (fn [search _]
   (:suggestions search)))

(reg-sub
 :search-input-val
 :<- [:search]
 (fn [search _]
   (:input-val search)))

(reg-sub
 :search-favorstocks
 :<- [:search]
 (fn [search _]
   (:favorstocks search)))

(reg-sub
 :flash-tip
 (fn [db _]
   (:flash-tip db)))

(reg-sub
 :flash-tip-show
 :<- [:flash-tip]
 (fn [flash-tip _]
   (:show flash-tip)))
