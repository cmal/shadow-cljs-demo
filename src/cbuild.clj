(ns cbuild
  (:require [shadow.build :as build]
            [shadow.build.targets.browser :as browser]
            [clojure.java.shell :refer [sh]]
            #_[doumi.css :refer [gen-css]]))

(defn call-rsync-doumi []
  (comment
    (println "GENERATING CSS ...")
    (println
     (sh "clj" "-m" "doumi.css"))
    (println "CSS GENERATED."))

  (println "RSYNCING ...")
  (println
   #_(sh "rsync" "-qzPr" "public/js/doumi.js" "test:~/apps/mobile/public/doumi/js/"
         :dir "/Users/yuzhao/gits/luminus-projects/shadow-cljs-demo")
   (sh "rsync" "-qzPr"
       "public/js/doumi.js"
       "test:~/apps/mobile/source/public/doumi/js/"
       :dir "/Users/yuzhao/gits/luminus-projects/shadow-cljs-demo"))
  (println
   #_(sh "rsync" "-qzPrt" "public/js/cljs-runtime/" "test:~/apps/mobile/public/js/cljs-runtime/"
         :dir "/Users/yuzhao/gits/luminus-projects/shadow-cljs-demo")
   (sh "rsync" "-qzPrt"
       "public/js/cljs-runtime/"
       "test:~/apps/mobile/source/public/js/cljs-runtime/"
       :dir "/Users/yuzhao/gits/luminus-projects/shadow-cljs-demo"))
  (println "Rsync DONE."))

(defn custom [{::build/keys [stage mode config] :as state}]
  (let [state (browser/process state)]
    (when (and (= :flush stage) (= :dev mode))
      (call-rsync-doumi))
    state))

;; stages:
;; :configure
;; :compile-prepare
;; :compile-finish
;; :optimize-prepare
;; :optimize-finish
;; :flush


(defn call-copy []
  (let [{:keys [exit out err]} (sh "cp public/js/*.js ../public/doumi/js")]
    (when-not (zero? exit)
      (println "error while copying: " err))))

(defn custom-release [{::build/keys [stage mode config] :as state}]
  (let [state (browser/process state)]
    (when (and (= :flush stage) (= :release mode))
      (call-copy))
    state))
