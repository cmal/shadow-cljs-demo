(ns cljsjs.react
  (:require #_[goog.object :as gobj]
            ["react" :as react]))

(js/goog.exportSymbol "React" react)
