(ns cljsjs.create-react-class
  (:require #_[goog.object :as gobj]
            ["create-react-class" :as create-react-class]))

(js/goog.exportSymbol "createReactClass" create-react-class)
