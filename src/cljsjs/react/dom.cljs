(ns cljsjs.react.dom
  (:require #_[goog.object :as gobj]
            ["react-dom" :as react-dom]))

(js/goog.exportSymbol "ReactDOM" react-dom)
