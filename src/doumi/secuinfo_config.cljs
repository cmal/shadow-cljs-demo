(ns doumi.secuinfo-config
  (:require [reagent.core :as r]
            [re-frame.core :as rf]
            [doumi.helper :refer [redirect-to]]
            [doumi.log :refer [logger-log]]))

(def secuinfo-nav-data
  ;; 这个数据结构被 secuinfo 和 indexinfo 共用
  ;; 修改的时候注意一下，有必要的话分离开
  [{:icon       "search"
    :text       "个股查询"
    :properties {:on-click #(rf/dispatch [:search-input-focus])}}
   {:icon       "question"
    :text       "帮助"
    :properties {:style    {:color "#ffab36"}
                 :on-click #(rf/dispatch [:secuinfo-show-helper-mask])}} ;; use deep-merge to merge
   {:icon       "share"
    :text       "分享"
    :properties {}}
   {:icon       "favorstock"
    :text       "自选股"
    :properties {:on-click #(redirect-to "/stock/self")}}]) ;; if properties is empty, must use {}

(def indexinfo-nav-data
  ;; 这个数据结构被 secuinfo 和 indexinfo 共用
  ;; 修改的时候注意一下，有必要的话分离开
  [{:icon       "search"
    :text       "个股查询"
    :properties {:on-click (fn [e]
                             (rf/dispatch [:search-input-focus]))}}
   {:icon       "question"
    :text       "帮助"
    :properties {:style    {:color "#ffab36"}
                 :on-click (fn [e]
                             (rf/dispatch [:indexinfo-show-helper-mask]))}} ;; use deep-merge to merge
   {:icon       "share"
    :text       "分享"
    :properties {}}
   {:icon       "favorstock"
    :text       "自选股"
    :properties {:on-click (fn [e]
                             (logger-log "stockIndex.favor")
                             (redirect-to "/stock/self"))}}])

(def secuinfo-noodles-data
  [{:name :tech
    :text "技术面"}
   {:name :fundamental
    :text "基本面"}
   {:name :capital
    :text "资金面"}
   {:name :msg
    :text "消息面"}])

(def indexinfo-noodles-data
  [{:name :tech
    :text "技术面"}
   {:name :fundamental
    :text "基本面"}
   {:name :capital
    :text "资金面"}
   {:name :ingredients
    :text "成分股"}])

(def headline-cnt
  {:fenshi      1
   :volume      2
   :zdf         4
   :fundamental 5
   :dividend    1})

(def line-chart-nav-data
  [{:k    :1year
    :name "一年"}
   {:k    :3year
    :name "三年"}
   {:k    :5year
    :name "五年"}
   {:k    :all
    :name "全部"}])

(def k-chart-nav-data
  [{:k    :fenshi
    :name "分时"}
   {:k    :day
    :name "日K"}
   {:k    :week
    :name "周K"}
   {:k    :month
    :name "月K"}])

(defn get-volume-color
  [cate]
  (case cate
    2  "#ff8000" ;; deep-orange
    1  "#ffab35" ;; light-orange
    -1 "#35a2ff" ;; light-blue
    -2 "#0066ff" ;; deep-blue
    "#666"))

(defn get-zdf-color
  [cate]
  (case cate
    2  "#ff1200" ;; deep-red
    1  "#fb6969" ;; light-red
    -1 "#6ee59b" ;; light-green
    -2 "#1dd99a" ;; deep-green
    "#666"))

(def secuinfo-mask-img-data
  ;; keys comming from secuinfo noodle active
  {:tech        "/wx/img/tech.jpg"
   :fundamental "/wx/img/fundamental.jpg"
   :capital     "/wx/img/capital.jpg"
   :msg         "/wx/img/msg.jpg"
   nil          "/wx/img/four-noodles.jpg"})

(def indexinfo-mask-img-data
  ;; keys comming from indexinfo noodle active
  {:tech        "/wx/img/tech.jpg"
   :fundamental "/wx/img/fundamental.jpg"
   :capital     "/wx/img/capital.jpg"
   :ingredients "/wx/img/four-noodles.jpg" ;; FIXME 暂用
   nil          "/wx/img/four-noodles.jpg"})

(def pay-urls
  {:season   "https://h5.youzan.com/v2/goods/1ycomou0e7dcp"
   :month    "https://h5.youzan.com/v2/goods/2fz1ktfbsocgp"
   :halfyear "https://h5.youzan.com/v2/goods/26xt4rmp64ukp"
   :year     "https://h5.youzan.com/v2/goods/35wq633humg9l"})

(def noodles-logger
  {:tech        "secuinfo.tech"
   :fundamental "secuinfo.fundamental"
   :capital     "secuinfo.capital"
   :msg         "secuinfo.news"})

(def tech-nav-logger
  {:volume "secuinfo.tech.volume"
   :zdf    "secuinfo.tech.price"})

(def fundamental-nav-logger
  {:pe  "secuinfo.fundamental.pe"
   :pb  "secuinfo.fundamental.pb"
   :ps  "secuinfo.fundamental.ps"
   :rgr "secuinfo.fundamental.nr"
   :egr "secuinfo.fundamental.ni"})

(def capital-nav-logger
  {:dividend "secuinfo.capital.rate"
   :bonus    "secuinfo.capital.bonus"
   :unlock   "secuinfo.capital.unlock"})

(def msg-nav-logger
  {:merger "secuinfo.news.merger"
   :pp     "secuinfo.news.private"
   :jl     "secuinfo.news.incentive"})

(def help-logger
  {:tech        "secuinfo.tech.help"
   :fundamental "secuinfo.fundamental.help"
   :capital     "secuinfo.capital.help"
   :msg         "secuinfo.news.help"})

(def switch-intervals-logger
  {:pe  "secuinfo.fundamental.peZoom"
   :pb  "secuinfo.fundamental.pbZoom"
   :ps  "secuinfo.fundamental.psZoom"
   :rgr "secuinfo.fundamental.nrZoom"
   :egr "secuinfo.fundamental.niZoom"})

(def switch-season-year-logger
  {:season {:egr "secuinfo.switch-to-season.egr"
            :rgr "secuinfo.switch-to-season.rgr"}
   :year   {:egr "secuinfo.switch-to-year.egr"
            :rgr "secuinfo.switch-to-year.rgr"}})


(def gr-name
  ;; (get-in gr-name [:egr :season])
  {:egr {:season "净利增速"
         :year   "年净利增速"}
   :rgr {:season "营收增速"
         :year   "年营收增速"}})

(def fenshi-num 242)

(def k-num 50)

(def zd-color
  {:zero "gray"
   :pos  "#f64843"
   :neg  "#5fd042"})

(def ma-colors
  ["#469cf3" "#ffab34" "#d13ca0"])

(def indexinfo-line-types
  ;; hack
  ;; [20150206 3075.91 13.31 1.81 1.26 2.38 27816855604832.42 23166119878615.11]
  [nil nil :pe :pb :ps :divirate :totalmv :freefloatmv])
