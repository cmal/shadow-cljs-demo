(ns doumi.helper
  (:require [clojure.string :as str]
            [cljs-time.core :as t]
            [cljs-time.coerce :as tc]
            [cljs-time.format :as tf]
            [cljs-time.local :as tl]
            [reagent.cookies :as cookie]
            [doumi.history :as hst]
            [doumi.config :refer [env]]))

(defn base "根据设计图基础尺寸计算rem"
  [px]
  (str (/ px 40 #_(/ 640 16)) "rem")  )

(defn base-n "根据设计图基础尺寸计算rem值，不含单位"
  [px]
  (/ px 40 #_(/ 640 16)))

(defn stockid2secucode ""
  [stockid]
  (str/join (take 6 stockid)))

(defn set-hash! [url]
  (set! (.-hash (.-location js/window))
        url))

(defn url-search [name]
  ;; name should be a string
  ;; returns a string
  ;; if not find, return nil
  (let [search (-> js/window .-location .-search)]
    (or (when-not (empty? search)
          (let [substr (str/join (drop 1 search))
                params (str/split search #"&")]
            (-> (into {} (map #(str/split % #"=") params))
                ;; must use get here, because name is a string
                (get name))))
        "")))

(defn get-ref []
  (url-search "ref"))

(defn get-unionid []
  ;; here can use keyword or string
  ;; because cookie/get will first do (name k)
  ;; if not find return nil
  (cookie/get "_WEIXIN_COOKIE_UNIONID" nil))

(defn get-pathname []
  (-> js/window .-location .-pathname))

(defn get-url []
  (-> js/window .-location .-href))

(defn get-logger-uid []
  (or (cookie/get "_UID_LOG" nil)
      (let [uid (str "jd" (.getTime (js/Date.)) (rand))]
        (cookie/set! "_UID_LOG" uid)
        uid)))

#_(defn zhuge-track [target data]
    (.track (.-zhuge js/window) target data))

(defn json-stringify [m]
  ;; convert a clojure map to a JSON string
  (.stringify js/JSON (clj->js m)))

(defn add-logger-info [data]
  (merge data
         {:userid   (get-unionid)
          :plt      "m"
          :pathname (get-pathname)
          :page     (get-url)}))

(defonce encode-uri js/encodeURIComponent)

;; deep-merge map
(defn- deep-merge* [& maps]
  (let [f (fn [old new]
            (if (and (map? old) (map? new))
              (merge-with deep-merge* old new)
              new))]
    (if (every? map? maps)
      (apply merge-with f maps)
      (last maps))))

(defn deep-merge [& maps]
  (let [maps (filter identity maps)]
    (assert (every? map? maps))
    (apply merge-with deep-merge* maps)))

(defn redirect-to [location]
  (doto hst/history
    (.setToken location))
  (-> js/window
      .-location
      (.replace location)))

(defn combine-base [& vals]
  (str/join " " (map base vals)))

(defn indexed [data]
  (map-indexed (fn [a b] [a b]) data))

(defn icon-key [name]
  (keyword (str "i.icon.icon-" name)))

(def num-name
  ["ling" "yi" "er" "san" "si"
   "wu" "liu" "qi" "ba" "jiu"])

(defn format-date-slash
  "with `date` of type number or string, length 6 or 8"
  [date]
  (let [date-str (str date)]
    (case (count date-str)
      6 (str (subs date-str 0 4) "/" (subs date-str 4))
      8 (str (subs date-str 0 4) "/" (subs date-str 4 6) "/" (subs date-str 6)))))

(def ymd-formatter
  (tf/formatter "yyyyMMdd"))

(def ymd-slash-formatter
  (tf/formatter "yyyy/MM/dd"))

(defn in-one-year?
  "ymd-slash is yyyy/mm/dd"
  [ymd-slash]
  (let [date (tf/parse ymd-slash-formatter ymd-slash)]
    (->> (t/today)
         (t/interval date)
         t/in-years
         zero?)))

(defn js-inyear?
  [n js-date]
  (->> (t/today)
       (t/interval (tc/from-date js-date))
       t/in-years
       (> n)))

(defn js-in1year?
  [js-date]
  (js-inyear? 1 js-date))

(defn js-in3year?
  [js-date]
  (js-inyear? 3 js-date))

(defn js-in5year?
  [js-date]
  (js-inyear? 5 js-date))

(defn js-fmt-date-yyyymmdd
  [js-date]
  (tf/unparse ymd-formatter (tc/from-date js-date)))

(defn yyyymmdd-in-n-year?
  [n yyyymmdd]
  (when yyyymmdd
    (let [yyyymmdd (if (string? yyyymmdd) yyyymmdd (str yyyymmdd))
          ymd      (tf/parse ymd-formatter yyyymmdd)]
      (if (t/before? ymd (t/today))
        (->> (t/today)
             (t/interval ymd)
             t/in-years
             (> n))
        (-> (t/today)
            (t/interval (t/today))
            (t/in-years)
            (<= n))))))

(defn yyyymmdd-in1year?
  [yyyymmdd]
  (yyyymmdd-in-n-year? 1 yyyymmdd))

(defn yyyymmdd-in3year?
  [yyyymmdd]
  (yyyymmdd-in-n-year? 3 yyyymmdd))

(defn yyyymmdd-in5year?
  [yyyymmdd]
  (yyyymmdd-in-n-year? 5 yyyymmdd))

(def local-now-ymd
  (js/Number (tf/unparse ymd-formatter (tl/local-now))))

(defn ymd-num-in-n-year?
  [n ymd]
  ;; ymd is yyyymmdd number
  (when-let [ymd-num (js/Number ymd)]
    (let [now-year    (quot local-now-ymd 10000)
          now-month   (-> local-now-ymd (quot 100) (rem 100))
          now-day     (-> local-now-ymd (rem 100))
          year-before (- now-year n)
          date-before (+ (* 1E4 year-before) (* 1E2 now-month) now-day)]
      (>= ymd-num date-before))))

(defn in-n-year?
  [n date formatter]
  (let [ymd (tf/parse formatter date)]
    (if (t/before? ymd (t/today))
      (->> (t/today)
           (t/interval ymd)
           t/in-years
           (> n))
      (-> (t/today)
          (t/interval (t/today))
          (t/in-years)
          (<= n)))))

(defn proxy-uri
  [uri]
  (str "/p/" env uri))

(defn get-stock-title
  [stockname stockid]
  (str stockname "（" (stockid2secucode stockid) "）"))

(defn format-delta-percent
  [percent]
  (if-not percent
    "--"
    (if (pos? percent)
      (str "+" percent "%")
      (str percent "%"))))

(defn format-number-chn
  [number fixed]
  (let [abs-num (Math/abs number)]
    (cond
      (>= abs-num 1E8) (str (-> number (/ 1E8) (.toFixed fixed)) "亿")
      (>= abs-num 1E4) (str (-> number (/ 1E4) (.toFixed fixed)) "万")
      :else            (-> number (.toFixed fixed)))))

(defn format-number-chn-precision
  [number precision]
  (let [abs-num (Math/abs number)]
    (cond
      (>= abs-num 1E12) (str (-> number (/ 1E12) js/Number (.toPrecision precision)) "万亿")
      (>= abs-num 1E11) (str (-> number (/ 1E11) js/Number (.toPrecision precision)) "千亿")
      (>= abs-num 1E7)  (str (-> number (/ 1E8) js/Number (.toPrecision precision)) "亿")
      (>= abs-num 1E3)  (str (-> number (/ 1E4) js/Number (.toPrecision precision)) "万")
      :else             (-> number js/Number (.toPrecision precision)))))

(defn format-number-chn-1
  [number]
  (format-number-chn-precision number 1))

(defn format-number-chn-2
  [number fixed]
  (let [abs-num (Math/abs number)]
    (cond
      (>= abs-num 1E12) (str (-> number (/ 1E12) (.toFixed fixed)) "万亿")
      (>= abs-num 1E8)  (str (-> number (/ 1E8) (.toFixed fixed)) "亿")
      (>= abs-num 1E4)  (str (-> number (/ 1E4) (.toFixed fixed)) "万"))))

(defn get-number-cardinality
  [number]
  (-> number
      Math/abs
      Math/log
      (/ (Math/log 10))
      (.toFixed 0) ;; round to int
      js/Number))

(defn log
  [text a]
  (doto a
    (prn text a)))

(defn median
  [v]
  (let [cnt (count v)
        mid (quot cnt 2)
        #_  (prn "@@@@@@@@@###" cnt mid)]
    (cond
      (odd? cnt)  (nth v mid)
      (even? cnt) (-> (+ (nth v (dec mid)) (nth v mid))
                      (/ 2)))))

(defn precision-round
  [num precision]
  (let [factor (Math/pow 10 precision)]
    (/ (Math/round (* num factor)) factor)))
