(ns doumi.weixin
  (:require [reagent.cookies :as cookie]
            [doumi.config :refer [env]]
            [doumi.log :refer [logger-log]]))

(defn get-weixin-cookie
  [name]
  (let [cookie-name (str "_WEIXIN_COOKIE_" (.toUpperCase name))]
    (cookie/get cookie-name)))

(defn get-info
  ([] {:openid     (get-weixin-cookie "openid")
       :nickname   (get-weixin-cookie "nickname")
       :headImgUrl (get-weixin-cookie "headimgurl")
       :unionid    (get-weixin-cookie "unionid")})
  ([name]
   (get-weixin-cookie name)))

(defonce appids
  {"www"  "wx44fc432bf0487e03"
   "beta" "wx9fbb3c69f897b050"})

(defn get-appid
  []
  (appids env))

(def js-api-list
  ["onMenuShareTimeline" "onMenuShareAppMessage"])

(defn weixin-init
  []
  (when (.-isWeixin js/_weixin)
    (.config js/wx
             (clj->js
              {
               ;; 开启调试模式,调用的所有api的返回值会在客户端alert出来，
               ;; 若要查看传入的参数，可以在pc端打开，参数信息会通过log打出，仅在pc端时才会打印。
               :debug     false
               ;; 必填，公众号的唯一标识
               :appId     (get-appid)
               ;; 必填，生成签名的时间戳
               :timestamp (.-timestamp js/_weixin)
               ;; 必填，生成签名的随机串
               :nonceStr  (.-noncestr js/_weixin)
               ;; 必填，签名，见附录1
               :signature (.-signature js/_weixin)
               ;; 必填，需要使用的JS接口列表，所有JS接口列表见附录2
               :jsApiList js-api-list}))))


(defn- get-share-link [url type]
  (str (-> js/window .-location .-origin)
       "/share?"
       "target=" (js/encodeURIComponent url)
       "&code=" (get-info "Unionid")
       "&type=" (or type 0)))

(defn- get-link [link type]
  (or link
      (get-share-link (-> js/window .-location .-href) type)))

(defn- get-img-url [img-url]
  (or img-url
      (str (-> js/window .-location .-origin)
           "/common/img/share.png")))

(defn set-share [{:keys [title desc img-url
                         type data-url link
                         timeline-logger
                         weixin-logger]}]
  (when (.-isWeixin js/_weixin)
    (.ready js/wx
            (clj->js
             (fn []
               (.onMenuShareTimeline ;; 分享到朋友圈
                js/wx
                (clj->js {;; 分享标题
                          :title   title
                          ;; 分享链接
                          :link    link
                          ;; 分享图标
                          :imgUrl  (get-img-url img-url)
                          ;; 用户确认分享后执行的回调函数
                          :success (fn []
                                     (js/alert "success")
                                     (logger-log timeline-logger))
                          ;; 用户取消分享后执行的回调函数
                          :cancel  (fn []
                                     (js/alert "cancel"))
                          }))
               (.onMenuShareAppMessage ;; 分享到好友
                js/wx
                (clj->js {;; 分享标题
                          :title   title
                          ;; 分享描述
                          :desc    (or desc "您的数据智囊")
                          ;; 分享链接
                          :link    (get-link link type)
                          ;; 分享图标
                          :imgUrl  (get-img-url img-url)
                          ;; 分享类型,music、video或link，不填默认为link
                          :type    (or type "")
                          ;; 如果type是music或video，则要提供数据链接，默认为空
                          :dataUrl (or data-url "")
                          ;; 用户确认分享后执行的回调函数
                          :success (fn []
                                     (js/alert "success")
                                     (logger-log weixin-logger))
                          ;; 用户取消分享后执行的回调函数
                          :cancel  (fn [] (js/alert "cancel"))
                          })))))))
