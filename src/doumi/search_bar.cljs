(ns doumi.search-bar
  (:require [reagent.core :as r]
            [re-frame.core :as rf]
            [doumi.doumi-config :refer [flash-tip-data]]
            [doumi.helper :refer [base icon-key
                                  stockid2secucode]]
            [doumi.routes :refer [secuinfo-url] :as routes]
            [doumi.csshelper :refer [transition-css]]
            [doumi.log :refer [logger-log]]
            [doumi.flash-tip :refer [flash-tip]]
            ))

(defn icon-wrapper-add-fav
  [name]
  [:span.icon-wrapper
   [(icon-key name)]])

(defn search-bar []
  (let [show-search-right-x (r/atom false)]
    (fn []
      [:div.search-bar
       {:style {:height (base 80)}}
       [:div#stock-search
        {:style {:background "#fff"
                 :padding    (str (base 12) " " (base 18))
                 :height     (base 56)
                 :position   "relative"}}
        [:a {:href  "/"
             :style {:display     "block"
                     :float       "left"
                     :padding-top (base 6)}}
         [:img.logo {:src   "/common/img/logo.png"
                     :style {:height (base 44)
                             :width  (base (/ (* 44 375) 138))}}]]
        [:div.input
         {:style {:height           (base 54)
                  :float            "left"
                  :margin-left      (base 16)
                  :width            (base 464)
                  :position         "relative"
                  :display          "block"
                  :border           "solid 1px #e6e6e6"
                  :border-radius    (base 5)
                  :background-color "#f2f3f3"}}
         [:div.icon-wrapper
          {:style {:position                  "absolute"
                   :display                   "inline"
                   :line-height               (base 52)
                   :color                     "#aaa"
                   :border                    0
                   :border-radius             (base 5)
                   :font-size                 (base 24)
                   :background                "#f2f3f3"
                   :padding-left              (base 16)
                   :border-top-left-radius    (base 8)
                   :border-bottom-left-radius (base 8)}}
          [:i.icon.icon-search]]
         [:input#search-bar-input
          {:type        "text"
           :name        "search-bar-input"
           :placeholder "请输入您要查询的股票名称或代码"
           :on-focus    (fn [e]
                          #_(logger-log "favorstock.search")
                          (reset! show-search-right-x true)
                          (rf/dispatch [:search-show-suggestion true])
                          (logger-log "secuinfo.search.focus"))
           :on-blur     (fn [e]
                          (reset! show-search-right-x false)
                          (rf/dispatch [:search-later-hide-suggestion]))
           :on-input    (fn [e]
                          (let [value (->> e .-target .-value)]
                            (when value
                              (rf/dispatch [:search-input-val value])
                              (when (pos? (count value))
                                (rf/dispatch [:search-keyword value])))))
           :style       {:background                "#f2f3f3"
                         :width                     (base 380)
                         :padding-left              (base 54)
                         :border                    0
                         :border-radius             (base 5)
                         :outline                   "none"
                         :height                    (base 42)
                         :line-height               (base 52)
                         :font-size                 (base 24)
                         :vertical-align            "top"
                         :border-top-left-radius    (base 8)
                         :border-bottom-left-radius (base 8)}}]
         [:div.search-right-wrapper
          {:style    {:position    "absolute"
                      :top         0
                      :right       0
                      :display     (if @show-search-right-x "block" "none")
                      :float       "right"
                      :line-height (base 52)
                      :color       "#aaa"
                      :font-size   (base 24)
                      :padding     (str "0 " (base 16))
                      :transform   (str "stranslateY(" (base (- 54 1)) ")")}
           :on-click (fn [e]
                       (rf/dispatch [:search-clear-input])
                       (reset! val ""))}
          [:i.icon.icon-times]]]]])))

(defn suggestions
  []
  (let [show        (rf/subscribe [:search-show-suggestion])
        suggestions (rf/subscribe [:search-suggestions])
        _           (rf/dispatch [:search-load-favorstocks])
        favorstocks (rf/subscribe [:search-favorstocks])]
    (fn []
      [:div.suggestions
       {:style {:clear      "both"
                :display    (if @show "block" "none")
                :background "#fff"
                :padding    "0 0.9rem"
                :overflow   "auto"
                :position   "absolute"
                :top        "2rem"
                :width      "100%"
                :box-sizing "border-box"
                :z-index    "100"
                :box-shadow "0px 5px 5px 0px rgba(130,130,130,.25)"
                :border-top "1px solid #eee"}}
       (doall
        (for [{:keys [stockid stockname]} @suggestions
              :let                        [secucode (stockid2secucode stockid)]]
          ^{:key stockid}
          [:div.suggestion-item
           {:on-click (fn [e]
                        (when (not= "I" (-> e .-target .-nodeName))
                          (logger-log "secuinfo.suggestion.jump")
                          (set! (-> js/window .-location .-href)
                                (routes/secuinfo-url stockid stockname))))}
           [:span.suggestion-name stockname]
           [:span.suggestion-id secucode]
           [:span.add-wrapper
            [:span.add
             {:style    {:display (if (contains? @favorstocks stockid) "none" "inline")}
              :on-click (fn [e]
                          (when (= "I" (-> e .-target .-nodeName))
                            (rf/dispatch [:search-add-favorstock secucode])))}
             "加自选"
             [icon-wrapper-add-fav "add"]]
            [:span.added
             {:style {:display (if (contains? @favorstocks stockid) "inline" "none")}}
             "已添加"
             [icon-wrapper-add-fav "checked"]]]]))])))

(defn search []
  [:div#search-bar
   [search-bar]
   [suggestions]
   [flash-tip]])
