(ns doumi.pos
  )

(defn scroll-top
  []
  (or (-> js/document .-body .-scrollTop)
      (-> js/window .-pageYOffset)
      (-> js/document .-documentElement .-scrollTop)))

(defn inner-height
  []
  (-> js/window .-innerHeight))

(defn client-height
  []
  (-> js/document .-body .-clientHeight))

(defn scroll-height
  []
  (-> js/document .-body .-scrollHeight))

(defn scroll-left
  []
  (or (.-pageXOffset js/window)
      (-> js/document .-documentElement .-scrollLeft)))

(defn get-element-left
  [element]
  (-> element
      .getBoundingClientRect
      .-left
      (+ (scroll-left))))

(def doc-element (-> js/document .-documentElement))

(defn doc-bounding-rect
  []
  (-> doc-element .getBoundingClientRect))

(defn doc-width
  []
  (-> (doc-bounding-rect) .-width))

(defn doc-height
  []
  (-> (doc-bounding-rect) .-height))

(defn get-current-viewport
  []
  (-> doc-element
      (.querySelector "meta[name=\"viewport\"]")))

(def root-font-size
  (/ doc-width 16))

(def dpr 1) ;; fix me

(def scale 1) ;; fix me

#_(def rem
    (* dpr root-font-size))

(defn rem-to-px
  [rem]
  (* rem root-font-size))

#_(defn document-height
    []
    (-> js/document
        .-documentElement
        .-clientHeight))

(def screen
  (-> js/window .-screen))

(def screen-height
  (- (-> screen .-height) 64))

(def screen-width
  (-> screen .-width))
