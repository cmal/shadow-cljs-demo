(ns doumi.db
  (:require [clojure.spec.alpha :as s]))

(s/def ::page #{:doumi :coupon :feedback :annc :secuinfo :indexinfo})

(s/def :search/show-suggestion boolean?)
(s/def :search/suggestions vector?)
(s/def :search/input-val string?)
(s/def :search/favorstocks (s/or :nil nil? :set set?))
(s/def ::search (s/keys :req-un [:search/show-suggestion
                                 :search/suggestions
                                 :search/input-val
                                 :search/favorstocks]))

(s/def :flash-tip/show boolean?)
(s/def :flash-tip/kw keyword?)
(s/def ::flash-tip (s/keys :req-un [:flash-tip/show
                                    :flash-tip/kw]))

(def feedback-keys #{:good :bad :confusing})
(s/def :feedback/good (s/or :nil nil?
                            :int int?))
(s/def :feedback/bad (s/or :nil nil?
                           :int int?))
;; how to check the three to be nil at the same time?
(s/def :feedback/confusing (s/or :nil nil?
                                 :int int?))
(s/def :feedback/current (s/or :nil nil? :kw feedback-keys))
(s/def :feedback/show-count boolean?)
(s/def :feedback-change/add-one (s/or :nil nil?
                                      :key feedback-keys))
(s/def :feedback-change/minus-one (s/or :nil nil?
                                        :key feedback-keys))
(s/def :feedback-change/done (s/or :nil nil?
                                   :bool boolean?))
(s/def :feedback/change (s/keys :req-un [:feedback-change/add-one
                                         :feedback-change/minus-one
                                         :feedback-change/done]))
(s/def ::feedback (s/keys :req-un [:feedback/good
                                   :feedback/bad
                                   :feedback/confusing
                                   :feedback/current
                                   :feedback/show-count
                                   :feedback/change]))

(s/def :doumi-doumi/doumi int?)
(s/def :doumi-doumi/log vector?)
(s/def :doumi/doumi (s/keys :req-un [:doumi-doumi/doumi
                                     :doumi-doumi/log]))
(s/def :doumi/userinfo map?)
(s/def :doumi-phone/codesent boolean?)
(s/def :doumi-phone/countdown (s/or :zero zero? :pos pos?))
(s/def :doumi-phone/verified boolean?)
(s/def :doumi/phone (s/keys :req-un [:doumi-phone/codesent
                                     :doumi-phone/countdown
                                     :doumi-phone/verified]))
(s/def :doumi/page #{:strategy :log :use})
(s/def :doumi-use/page #{:coupon :vip})
(s/def :doumi/use (s/keys :req-un [:doumi-use/page]))
(s/def :doumi-coupon/page #{:redeem :redeem-success})
(s/def :doumi-coupon-redeem/amount int?)
(s/def :doumi-coupon-redeem/result string?)
(s/def :doumi-coupon-redeem/disable boolean?)
(s/def :doumi-coupon/redeem (s/keys :req-un [:doumi-coupon-redeem/amount
                                             :doumi-coupon-redeem/result
                                             :doumi-coupon-redeem/disable]))
(s/def :doumi-coupon/active #{:10 :20 :50 :100 :500 :custom})
(s/def :doumi-coupon-custom/valid boolean?)
(s/def :doumi-coupon-custom/value int?)
(s/def :doumi-coupon/custom (s/keys :req-un [:doumi-coupon-custom/valid
                                             :doumi-coupon-custom/value]))
(s/def :doumi-coupon/data vector?)
(s/def :doumi/coupon (s/keys :req-un [:doumi-coupon/page
                                      :doumi-coupon/redeem
                                      :doumi-coupon/active
                                      :doumi-coupon/custom
                                      :doumi-coupon/data]))
(s/def :doumi-vip/page #{:redeem-doumi :redeem-rmb})
(s/def :doumi-vip-doumi/page #{:redeem :success})
(s/def :doumi-vip/doumi (s/keys :req-un [:doumi-vip-doumi/page]))
(def vip-types #{:season :month :year :halfyear})
(s/def :doumi-vip-active/doumi vip-types)
(s/def :doumi-vip-active/rmb vip-types)
(s/def :doumi-vip/active (s/keys :req-un [:doumi-vip-active/doumi
                                          :doumi-vip-active/rmb]))
(s/def :doumi-vip-rmb/page #{:redeem :success :failed})
(s/def :doumi-vip-rmb-show/help boolean?)
(s/def :doumi-vip-rmb-show/phone boolean?)
(s/def :doumi-vip-rmb/show (s/keys :req-un [:doumi-vip-rmb-show/help
                                            :doumi-vip-rmb-show/phone]))
(s/def :doumi-vip-redeem/disable boolean?)
(s/def :doumi-vip-redeem/result (s/or :nil nil?
                                      :string string?))
(s/def :doumi-vip/redeem (s/keys :req-un [:doumi-vip-redeem/disable
                                          :doumi-vip-redeem/result]))
(s/def :doumi/vip (s/keys :req-un [:doumi-vip/page
                                   :doumi-vip/doumi
                                   :doumi-vip/active
                                   :doumi-vip/rmb
                                   :doumi-vip/redeem]))
(s/def :doumi-style-pay-helper-qm/offsetLeft number?)
(s/def :doumi-style-pay-helper-qm/offsetWidth number?)
(s/def :doumi-style/pay-helper-qm
  (s/keys :req-un [:doumi-style-pay-helper-qm/offsetLeft
                   :doumi-style-pay-helper-qm/offsetWidth]))
(s/def :doumi/style (s/keys :req-un [:doumi-style/pay-helper-qm]))
(s/def :doumi/loading boolean?)
(s/def ::doumi (s/keys :req-un [:doumi/doumi
                                :doumi/userinfo
                                :doumi/phone
                                :doumi/page
                                :doumi/use
                                :doumi/coupon
                                :doumi/vip
                                :doumi/style
                                :doumi/loading]))

(s/def :annc/page #{:annc :performance :dividend :nextweek})

;; ==== begin annc/annc
(s/def :annc-annc/init boolean?)
(s/def :annc-annc/display-type #{:stock :date :all})
(s/def :annc-date/data vector?)
(s/def :annc-date/page (s/and pos? int?))
(s/def :annc-date/loading boolean?)
(s/def :annc-annc/date (s/keys :req-un [:annc-date/data
                                        :annc-date/page
                                        :annc-date/loading]))
(s/def :annc-stock/data vector?)
(s/def :annc-stock/loading boolean?)
(s/def :annc-annc/stock (s/keys :req-un [:annc-stock/data
                                         :annc-stock/loading]))
(s/def :annc-all/stockid string?)
(s/def :annc-all/stockname string?)
(s/def :annc-all/data vector?)
(s/def :annc-all/page (s/and int? pos?))
(s/def :annc-all/loading boolean?)
(s/def :annc-annc/all (s/keys :req-un [:annc-all/data
                                       :annc-all/stockid
                                       :annc-all/stockname
                                       :annc-all/page
                                       :annc-all/loading]))
(s/def :annc-annc/read set?)
(s/def :annc/annc (s/keys :req-un [:annc-annc/init
                                   :annc-annc/display-type
                                   :annc-annc/date
                                   :annc-annc/stock
                                   :annc-annc/all
                                   :annc-annc/read]))
;; ==== end annc/annc
(s/def :annc-performance/orderby #{:date :stock :stockall})
(s/def :annc-performance-date/lastdate (s/or :nil nil? :string string?))
(s/def :annc-performance-date/data vector?)
(s/def :annc-performance-date/lastcount (s/or :nil nil?
                                              :pos-int (s/and pos? int?)
                                              :zero zero?))
(s/def :annc-performance-date/loading boolean?)
(s/def :annc-performance/date (s/keys :req-un [:annc-performance-date/lastdate
                                               :annc-performance-date/data
                                               :annc-performance-date/lastcount
                                               :annc-performance-date/loading]))
(s/def :annc-performance-stock/data vector?)
(s/def :annc-performance-stock/loading boolean?)
(s/def :annc-performance/stock (s/keys :req-un [:annc-performance-stock/data]))
#_(s/def :annc-performance-stockall/stockid (s/or :nil nil? :string string?))
(s/def :annc-performance-stockall/data map?)
(s/def :annc-performance-stockall/loading boolean?)
(s/def :annc-performance/stockall (s/keys :req-un [;;:annc-performance-stockall/stockid
                                                   :annc-performance-stockall/data
                                                   :annc-performance-stockall/loading]))
(s/def :annc/performance (s/keys :req-un [:annc-performance/orderby
                                          :annc-performance/date
                                          :annc-performance/stock
                                          :annc-performance/stockall]))

(s/def :annc-dividend/lastdate (s/or :nil nil? :string string?))
(s/def :annc-dividend/data vector?)
(s/def :annc-dividend/lastcount (s/or :nil nil?
                                      :lte0 (s/and number?
                                                   (s/or
                                                    :pos-int (s/and pos? int?)
                                                    :zero zero?))))
(s/def :annc-dividend/loading boolean?)
(s/def :annc/dividend (s/keys :req-un [:annc-dividend/lastdate
                                       :annc-dividend/data
                                       :annc-dividend/lastcount
                                       :annc-dividend/loading]))
(s/def :annc-nextweek/title (s/or :nil nil? :string string?))
(s/def :annc-nextweek/data vector?)
(s/def :annc-nextweek/loading boolean?)
(s/def :annc/nextweek (s/keys :req-un [:annc-nextweek/title
                                       :annc-nextweek/data
                                       :annc-nextweek/loading]))

(s/def ::annc (s/keys :req-un [:annc/page
                               :annc/annc
                               :annc/performance
                               :annc/dividend
                               :annc/nextweek]))

#_(s/def :loader/show boolean?)
#_(s/def ::loader (s/keys :req-un [:loader/show]))

(s/def :secuinfo/stockname (s/or :nil nil? :string string?))
(s/def :secuinfo/stockid (s/or :nil nil? :string string?))
(s/def :secuinfo-headline/index (s/and number?
                                       (s/or
                                        :pos-int (s/and pos? int?)
                                        :zero zero?)))
(s/def :secuinfo-headline/cate #{:realtime :volume :zdf :fundamental :dividend})
(s/def :secuinfo/headline (s/keys :req-un [:secuinfo-headline/index
                                           :secuinfo-headline/cate]))
(s/def ::secuinfo (s/keys :req-un [:secuinfo/stockname
                                   :secuinfo/stockid]))

;; for use in check-spec-interceptor in events.cljs
(s/def ::db (s/keys :req-un [::page
                             ::search
                             ::flash-tip
                             ::feedback
                             ::doumi
                             ::annc
                             ;;                           ::loader
                             ::secuinfo
                             ]))

(def doumi-store
  {:doumi    {:doumi 0
              :log   []}
   :userinfo {}
   :phone    {:codesent  false
              :countdown 0
              :verified  false}
   :page     :strategy       ;; :strategy, :log, :use
   :use      {:page :coupon} ;; :coupon, :vip
   :coupon   {:page   :redeem ;; :redeem :redeem-success (no :redeem-failed)
              :redeem {:amount  0  ;; 兑换10元优惠券成功
                       :result  "" ;; youzan url
                       :disable false}
              :active :10     ;; :10 :20 :50 :100 :500 :custom
              ;; :custom should be local
              :custom {:valid false
                       :value 10}
              :data   []}
   :vip      {:page   :redeem-doumi ;; :redeem-doumi :redeem-rmb
              :doumi  {:page :redeem ;; :success
                       }
              :active {:doumi :season
                       :rmb   :season}
              :rmb    {:page :redeem ;; :success :failed
                       :show {:help  false
                              :phone false}}
              :redeem {:disable false
                       :result  nil ;; vip valid date
                       }}
   :style    {:pay-helper-qm {:offsetLeft  0
                              :offsetWidth 0}}
   :loading  false})

(def annc-store
  {:page        :annc ;; :performance :dividend :nextweek
   :annc        {:init         true
                 :display-type :stock ;; :date, :all
                 :date         {:data    []
                                :loading true
                                :page    1}
                 :stock        {:data    []
                                :loading true}
                 :all          {:data      []
                                :page      1
                                :stockid   ""
                                :stockname ""
                                :loading   true}
                 :read         #{}}
   :performance {:orderby  :date ;; :stock :stockall
                 :date     {:lastdate  nil
                            :data      []
                            :lastcount nil
                            :loading   true}
                 :stock    {:data    []
                            :loading true}
                 :stockall {;; :stockid nil
                            :data    {}
                            :loading true}}
   :dividend    {:lastdate  nil
                 :data      []
                 :lastcount nil
                 :loading   true}
   :nextweek    {:title   nil
                 ;; data: vector of map, each has :title and :data keys
                 :data    []
                 :loading true}})

(def secuinfo-store
  {:stockname nil
   :stockid   nil
   :noodle    {:active nil}
   :secondary {:tech        {:active nil} ;; nil :volume :zdf
               :fundamental {:active nil}
               :capital     {:active nil}
               :msg         {:active nil}}
   :headline  {:index 0
               :cate  :realtime}
   :values    {:fundamental {:pe  {}
                             :pb  {}
                             :ps  {}
                             :egr {}
                             :rgr {}}
               :capital     {:dividend nil
                             :bonus    nil
                             :unlock   nil}}
   :chart     {:active     :3year
               :interval   :season
               :cate       :k
               :ktype      :day
               :fuquantype :restoration_forward
               :ma-index   nil
               :data       {:pe        {:x        []
                                        :y        []
                                        :tool-tip []
                                        }
                            :pb        {:x []
                                        :y []
                                        }
                            :ps        {:x []
                                        :y []}
                            :egr       {:season
                                        {:x         []
                                         :y         []
                                         :tool-tip  []
                                         :mark-line []
                                         :mark-area []
                                         :bar-y     []}
                                        :year
                                        {:x         []
                                         :y         []
                                         :tool-tip  []
                                         :mark-line []
                                         :mark-area []
                                         :bar-y     []}}
                            :rgr       {:season
                                        {:x         []
                                         :y         []
                                         :tool-tip  []
                                         :mark-line []
                                         :mark-area []
                                         :bar-y     []}
                                        :year
                                        {:x         []
                                         :y         []
                                         :tool-tip  []
                                         :mark-line []
                                         :mark-area []
                                         :bar-y     []}}
                            :mark-line []
                            :mark-area []}
               :option     {:fenshi   {}
                            :k        {}
                            :pe       {}
                            :pb       {}
                            :ps       {}
                            :divirate {}}}
   :data      {:realtime {}
               :fenshi   []
               :k        {:day   {}
                          :week  {}
                          :month {}}
               :summary  {}
               :unlock   {}
               :tech     {}
               :scores   {}
               :annc     {}
               :rules    {}
               :verify   {} ;; vip verify phone
               :divirate []
               :userinfo {}}
   :mask      {:show    false
               :type    :img
               :content :main}})

(def indexinfo-store
  {:id              nil
   :name            nil
   :list            []
   :noodle          {:active nil}
   :secondary       {:tech        {:active nil} ;; nil :volume :zdf
                     :fundamental {:active nil}
                     :capital     {:active nil}
                     :ingredients {:active nil}}
   :headline        {:index 0
                     :cate  :realtime}
   :values          {:fundamental {:pe          {}
                                   :pb          {}
                                   :ps          {}
                                   :totalmv     {}
                                   :freefloatmv {}}
                     :capital     {:dividend nil
                                   ;; :bonus    nil
                                   :unlock   nil}
                     ;; :ingredients {}
                     :kpe         {:daype   nil
                                   #_       {:last50-pe    {:min nil
                                                            :mid nil
                                                            :max nil}
                                             :pes          nil
                                             ;; prices 用于显示kswitch
                                             :prices       {:min nil
                                                            :mid nil
                                                            :max nil}
                                             ;; fixed-prices 用于echarts
                                             :fixed-prices {:min nil
                                                            :mid nil
                                                            :max nil}}
                                   :weekpe  nil
                                   #_       {:last50-pe    {:min nil
                                                            :mid nil
                                                            :max nil}
                                             :pes          nil
                                             :prices       {:min nil
                                                            :mid nil
                                                            :max nil}
                                             :fixed-prices {:min nil
                                                            :mid nil
                                                            :max nil}}
                                   :monthpe nil
                                   #_       {:last50-pe    {:min nil
                                                            :mid nil
                                                            :max nil}
                                             :pes          nil
                                             :prices       {:min nil
                                                            :mid nil
                                                            :max nil}
                                             :fixed-prices {:min nil
                                                            :mid nil
                                                            :max nil}}}
                     }
   :chart           {;; pe pb ps totalmv freefloatmv divirate
                     :cate     :k
                     ;; ktype: fenshi day week month daype weekpe monthpe
                     :ktype    :day
                     :active   :3year
                     :show-y?  false
                     :kswitch  {:show  :MA ;; :MA or :PE
                                :index nil ;; 冗余，暂留
                                :MA    {:MA5  nil
                                        :MA20 nil
                                        :MA60 nil}
                                :PE    {:PEmax    nil
                                        :PEmid    nil
                                        :PEmin    nil
                                        :pricemax nil
                                        :pricemid nil
                                        :pricemin nil}}
                     :datazoom {:start nil
                                :end   nil}
                     :use-data {} ;; {:k {:day {:k nil :prices nil} ...}}
                     }
   :setting         {:show     false
                     :pe       nil
                     :changed? false ;; 当前设置有变动
                     :up       {:pe        ""
                                :valid?    true
                                :has-rule? false}
                     :down     {:pe        ""
                                :valid?    true
                                :has-rule? false}
                     }
   :show-indexlist? false
   :intro           nil ;; :setting-save :setting-choose :clock :switch-chart
   :data            {:cnt                  0   ;; is need realtime, update cnt in on-realtime
                     :realtime             {}
                     :ingredients          {}  ;; :count :component
                     :ingredients-realtime {}  ;; "stockid"->realtime
                     :summary              {}
                     :indexlist            []  ;; {:indexid .. :indexname ..}
                     :k                    {:day   {}
                                            :week  {}
                                            :month {}}
                     :line                 nil #_ {:date          []
                                                   :pe            []
                                                   :pb            []
                                                   :ps            []
                                                   :dividendratio []
                                                   :totalmv       []
                                                   :freefloatmv   []}
                     :fenshi               []
                     :reminder             {}
                     :unlocks              {}}
   :mask            {:show    false
                     :type    :img
                     :content :main}})

(def default-db
  {:page      :doumi ;; :doumi :coupon :feedback :annc
   :search    {:show-suggestion false
               :suggestions     []
               :input-val       ""
               :favorstocks     nil}
   :flash-tip {:show false
               :kw   :favorstock-add-failed}
   :feedback  {:good       nil
               :bad        nil
               :confusing  nil
               :current    nil
               :show-count false
               :change     {:add-one   nil
                            :minus-one nil
                            :done      nil}}
   :doumi     doumi-store
   :annc      annc-store
   ;;   :loader    {:show false}
   :secuinfo  secuinfo-store
   :indexinfo indexinfo-store
   :ui        {:bottom-nav {:y nil}
               :secuinfo
               {:secuinfo-main         {:y nil}
                :main-content          {:y nil}
                :secondary-nav         {:y nil}
                :chart-info-switch-bar {:height 0}
                ;;                :chart-x-axis-line {:height 35}
                }
               :indexinfo
               {:main-content          {:y nil}
                :secondary-nav         {:y nil}
                :indexinfo-nav         {:y nil}
                :chart-info-switch-bar {:height 0}
                ;;                :chart-x-axis-line {:height 35}
                }}
   :document  {:readystate nil}
   ;; :window    {:inner-width  nil
   ;;             :inner-height nil}
   :share-tip {:show false}
   })
