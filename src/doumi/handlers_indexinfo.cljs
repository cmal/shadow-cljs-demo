(ns doumi.handlers-indexinfo
  (:require [doumi.handlers-common :refer [check-spec-interceptor]]
            [re-frame.core :refer [reg-event-db after debug path]]))

(def indexinfo-interceptors
  [check-spec-interceptor
   (path [:indexinfo])
   (when ^boolean js/goog.DEBUG debug)])

(reg-event-db
 :indexinfo-on-realtime
 indexinfo-interceptors
 (fn [indexinfo _]
   (let [data                  (:data indexinfo)
         indicator             (get-in data [:summary :indicator])
         {:keys [newprice
                 lastclose
                 changeratio]} (:realtime data)
         ratio                 (some-> changeratio (* 0.01) inc)
         pe                    (get-in indicator [:pe :val])
         pb                    (get-in indicator [:pb :val])
         ps                    (get-in indicator [:ps :val])
         #_                    (prn pe pb ps)
         {:keys [totalmv
                 freefloatmv
                 dividendratio
                 liftedratio]} indicator]
     (if ratio
       (cond-> indexinfo
         pe            (assoc-in [:values :fundamental :pe :val]
                                 (* ratio pe))
         pb            (assoc-in [:values :fundamental :pb :val]
                                 (* ratio pb))
         ps            (assoc-in [:values :fundamental :ps :val]
                                 (* ratio ps))
         totalmv       (assoc-in [:values :fundamental :totalmv]
                                 (* ratio totalmv))
         freefloatmv   (assoc-in [:values :fundamental :freefloatmv]
                                 (* ratio freefloatmv))
         dividendratio (assoc-in [:values :capital :dividend]
                                 (/ dividendratio ratio))
         liftedratio   (assoc-in [:values :capital :unlock] liftedratio))
       indexinfo))))

