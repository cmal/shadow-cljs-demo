(ns doumi.echarts-options
  (:require-macros [hiccups.core :as hiccups :refer [html]])
  (:require
   [reagent.core :as r]
   [re-frame.core :as rf]
   [hiccups.runtime :as hiccupsrt]
   [clojure.string :as str]
   [doumi.pos :refer [screen-width]]
   [cljs-time.core :as t]
   #_[cljs-time.coerce :as tc]
   [cljs-time.format :as tf]
   [goog.string :as gstr]
   [doumi.secuinfo-config :refer [gr-name fenshi-num k-num ma-colors
                                  indexinfo-line-types]]
   [doumi.helper :refer [log base
                         in-n-year? ymd-slash-formatter ymd-formatter
                         format-number-chn format-number-chn-1
                         format-number-chn-precision
                         format-number-chn-2
                         get-number-cardinality format-date-slash
                         median]]))

(defn tooltip-positioner-1
  ;;  [^js pos ^js params ^js dom ^js rect ^js size]
  [^js pos]
  (clj->js
   (merge {"top" 16}
          (if (<= screen-width (* 2 (first (js->clj pos))))
            {"left" 0}
            {"right" 0}))))

(defn tooltip-positioner-2
  ;;  [^js pos ^js params ^js dom ^js rect ^js size]
  [^js pos]
  (clj->js
   (merge {"top" 0}
          (if (<= screen-width (* 2 (first (js->clj pos))))
            {"left" 0}
            {"right" 0}))))

(defn get-fenshi-color
  [newprice preprice]
  (condp #(%1 newprice %2) preprice
    = "#999"
    < "#a6e7b7"
    > "#ffa4a4"))

(defn get-fenshi-volume-data
  ([trading? use-data]
   (get-fenshi-volume-data trading? use-data false))
  ([trading? use-data index?]
   (if-not trading?
     (repeat fenshi-num {:value 0})
     (let [{:keys [newprice volume amount lastclose]} (first use-data)]
       (take fenshi-num
             (concat
              (loop [result    []
                     pre-price lastclose
                     rst       use-data]
                (if (empty? rst)
                  result
                  (let [item                             (first rst)
                        {:keys [volume amount newprice]} item
                        color                            (get-fenshi-color newprice pre-price)
                        item                             {:itemStyle {:normal {:color color}}
                                                          :value     (if index? amount volume)}]
                    (recur (conj result item)
                           newprice
                           (rest rst)))))
              (repeat {:value "NA"})))))))

(defn get-fenshi-newprice-data
  [use-data]
  (take fenshi-num
        (concat
         (map :newprice use-data)
         (repeat {}))))

(defn format-fenshi-x-time
  [index]
  (cond
    (< index 120) (gstr/format "%02d:%02d"
                               (+ 9 (quot (+ 30 index) 60))
                               (rem (+ 30 index) 60))
    (= index 120) "11:30/13:00"
    (> index 120) (gstr/format "%02d:%02d"
                               (+ 11 (quot (dec index) 60))
                               (rem (dec index) 60))))

(def fenshi-x-axis
  ;; 09:30--11:29,11:30/13:00,13:00--15:00
  (map format-fenshi-x-time (range fenshi-num)))

(defn get-fenshi-delta
  [use-data]
  (let [{:keys [lastclose]} (first use-data)]
    (apply max
           (map (fn [item]
                  (Math/abs (- (:newprice item) lastclose)))
                use-data))))

(defn get-line-option
  [cate
   tooltip-formatter
   xaxis-data
   xaxis-pointer-label-formatter
   yaxis-pointer-label-formatter
   yaxis-label-formatter
   series-data
   series-markline-data-yaxis
   series-markline-data-label-formatter]
  {:animation   false
   :title       {:show false}
   :grid        {:top    16
                 :bottom 0
                 :height "auto"
                 :left   "0%"
                 :right  "0%"}
   :tooltip     {:trigger            "axis"
                 :transitionDuration 0
                 :axisPointer        {:type      "cross"
                                      :z         120
                                      :lineStyle {:type "dotted"}
                                      :crosStyle {:type "dotted"}}
                 :extraCssText       (str "background:rgba(0,0,0,.6);"
                                          "border-radius:0;"
                                          "color:#eee;")
                 :formatter          tooltip-formatter
                 :position           tooltip-positioner-1}
   :axisPointer {:z 150}
   :xAxis       {:show        false
                 :type        "category"
                 :data        xaxis-data
                 :boundaryGap false
                 :splitArea   {:show false}
                 :splitLine   {:show false}
                 :axisLine    {:lineStyle {:color "#999"}}
                 :axisPointer {:show      true
                               :lineStyle {:type "dotted"}
                               :label     {:formatter xaxis-pointer-label-formatter}}}
   :yAxis       {:show        true
                 :type        "value"
                 :scale       true
                 :boundaryGap false
                 :splitNumber 3
                 :min         "dataMin"
                 :max         "dataMax"
                 :splitArea   {:show false}
                 :splitLine   {:lineStyle {:color "#ddd"}}
                 :axisLine    {:show      false
                               :lineStyle {:color "#999"}}
                 :axisPointer {:show      true
                               :lineStyle {:type "dotted"}
                               :label     {:formatter yaxis-pointer-label-formatter}}
                 :axisTick    {:show false}
                 :axisLabel   {:show          true
                               :margin        4
                               :inside        true
                               :formatter     yaxis-label-formatter
                               :showMinLabel  true
                               :showMaxLabel  true
                               :verticalAlign "bottom"
                               :color         "#999"
                               :fontSize      12}
                 :z           100}
   :series      [{:name      (name cate)
                  :type      "line"
                  :smooth    true
                  ;;:animation false
                  :areaStyle {:normal {:color "#e2e5eb"}}
                  :data      series-data
                  :lineStyle {:normal {:width 1}}
                  :color     ["#57657b"]
                  :markLine  {:silent    true
                              :symbol    "none"
                              :lineStyle {:color "#284a7d"}
                              :data
                              [{:name  "median"
                                :yAxis series-markline-data-yaxis
                                :label {:normal {:position  "middle"
                                                 :formatter series-markline-data-label-formatter}}}]}}]})

(defn get-line-series-data
  [use-data cate]
  ;; [20150206 3075.91 13.31 1.81 1.26 2.38 27816855604832.42 23166119878615.11]
  (let [index (.indexOf indexinfo-line-types cate)]
    (map (fn [item]
           (some-> item (get index) js/parseFloat (.toFixed 2)))
         use-data)))


(defn get-line-markline-data-yaxis
  [use-data cate]
  (let [index (.indexOf indexinfo-line-types cate)]
    (some->> use-data
             (map #(get % index))
             (filter identity)
             median)))

(defn get-line-option-wrapper
  [use-data cate cate-name formatter median-formatter]
  (let [tooltip-formatter             (fn [^js item]
                                        (let [{:strs [name value]} (first (js->clj item))]
                                          (str
                                           (html
                                            [:div.tooltip-main
                                             [:div.tooltip-row (some-> name format-date-slash)]
                                             [:div.tooltip-row
                                              [:div.tooltip-left (str cate-name ":") ]
                                              [:div.tooltip-right (some-> value formatter)]]]))))
        xaxis-data                    (map first use-data)
        xaxis-pointer-label-formatter (fn [^js value]
                                        (some-> value
                                                js->clj
                                                (get "value")
                                                format-date-slash))
        yaxis-label-formatter         (fn [^js value ^js index]
                                        (some-> value (.toFixed 2) formatter))
        yaxis-pointer-label-formatter (fn [item]
                                        (some-> item
                                                js->clj
                                                (get "value")
                                                (.toFixed 2)
                                                formatter))
        series-data                   (get-line-series-data use-data cate)
        series-markline-data-yaxis    (get-line-markline-data-yaxis use-data cate)]
    (get-line-option
     cate
     tooltip-formatter
     xaxis-data
     xaxis-pointer-label-formatter
     yaxis-pointer-label-formatter
     yaxis-label-formatter
     series-data
     series-markline-data-yaxis
     median-formatter)))

(defn get-divirate-option
  [use-data]
  (get-line-option-wrapper
   use-data
   :divirate
   "股息率"
   (fn [val] (str val "%"))
   "中值：{c}%"))

(def secuinfo-get-divirate-option get-divirate-option)
(def indexinfo-get-divirate-option get-divirate-option)

(defn indexinfo-get-pe-option
  [use-data]
  (get-line-option-wrapper
   use-data
   :pe
   "市盈率(PE)"
   identity
   (fn [^js params]
     (let [{:strs [value]} (js->clj params)]
       (str "中值：" (some-> value (.toFixed 2)))))))

(defn indexinfo-get-pb-option
  [use-data]
  (get-line-option-wrapper
   use-data
   :pb
   "市净率(PB)"
   identity
   (fn [^js params]
     (let [{:strs [value]} (js->clj params)]
       (str "中值：" (some-> value (.toFixed 2)))))))

(defn indexinfo-get-ps-option
  [use-data]
  (get-line-option-wrapper
   use-data
   :ps
   "市销率(PS)"
   identity
   (fn [^js params]
     (let [{:strs [value]} (js->clj params)]
       (str "中值：" (some-> value (.toFixed 2)))))))

(defn indexinfo-get-totalmv-option
  [use-data]
  (get-line-option-wrapper
   use-data
   :totalmv
   "总市值"
   (fn [num] (format-number-chn-precision num 2))
   (fn [^js params]
     (let [{:strs [value]} (js->clj params)]
       (str "中值：" (some-> value (format-number-chn-precision 3)))))))

(defn indexinfo-get-freefloatmv-option
  [use-data]
  (get-line-option-wrapper
   use-data
   :freefloatmv
   "自由流通市值"
   (fn [num] (format-number-chn-precision num 2))
   (fn [^js params]
     (let [{:strs [value]} (js->clj params)]
       (str "中值：" (some-> value (format-number-chn-precision 3)))))))

(defn indexinfo-get-fenshi-option
  [use-data]
  (let [tooltip-formatter (fn [^js params]
                            ;; http://echarts.baidu.com/option.html#tooltip.formatter
                            (let [{:strs [dataIndex]} (first (js->clj params))
                                  item                (get use-data dataIndex)
                                  data-date           (:date (first use-data))
                                  month               (subs data-date 5 7)
                                  day                 (subs data-date 8 10)
                                  time-str            (nth fenshi-x-axis dataIndex)
                                  time                (subs time-str 0 5)
                                  {:keys [newprice
                                          changeratio
                                          amount]}    item]
                              (html
                               [:div.tooltip-main
                                [:div.tooltip-row
                                 month "/" day " " time]
                                [:div.tooltip-row
                                 [:div.tooltip-left
                                  "点位:"]
                                 [:div.tooltip-right
                                  (or (some-> newprice (.toFixed 2)) "NA")]]
                                [:div.tooltip-row
                                 [:div.tooltip-left
                                  "涨跌幅:"]
                                 [:div.tooltip-right
                                  (or (some-> changeratio (.toFixed 2) (str "%")) "NA")]]
                                [:div.tooltip-row
                                 [:div.tooltip-left
                                  "成交额:"]
                                 [:div.tooltip-right
                                  (or (some-> amount (format-number-chn 2)) "NA")]]])))
        line-data         (map :newprice use-data)
        bar-data          (map :amount use-data)

        lastclose             (-> use-data first :lastclose)
        {:keys [delta
                changeratio]} (apply max-key (comp Math/abs :delta)
                                     (map (fn [{:keys [newprice changeratio]}]
                                            {:delta       (- newprice lastclose)
                                             :changeratio changeratio})
                                          use-data))
        delta-abs             (Math/abs delta)
        data-max              (-> lastclose (+ delta-abs))
        data-min              (-> lastclose (- delta-abs))]
    {:animation   false
     :title       {:show false}
     :grid        [{:top    0
                    :height "70%"
                    :left   "0%"
                    :right  "0%"}
                   {:bottom "0%"
                    :height "30%"
                    :left   "0%"
                    :right  "0%"}]
     :tooltip     {:trigger            "axis"
                   :transitionDuration 0
                   :axisPointer        {:type "line"
                                        ;; :lineStyle {:type "dotted"}
                                        ;; :crosStyle {:type "dotted"}
                                        }
                   :extraCssText       (str "background:rgba(0,0,0,.6);"
                                            "border-radius:0;"
                                            "color:#eee;"
                                            "font-size:" (base 16) ";"
                                            "padding-top:" (base 10) ";"
                                            "padding-left:" (base 11) ";")
                   :formatter          tooltip-formatter
                   :position           tooltip-positioner-2}
     :axisPointer {:link  {:xAxisIndex "all"}
                   :label {:backgroundColor "#777"}
                   :z     150}
     :xAxis       [{:gridIndex   0
                    :show        false
                    :type        "category"
                    :data        fenshi-x-axis
                    :boundaryGap false
                    :splitArea   {:show false}
                    :splitLine   {:show false}
                    :axisLine    {:lineStyle {:color "#999"}}
                    :axisPointer {:show      true
                                  :lineStyle {:type "solid"}
                                  :label     {:show false}
                                  }}
                   {:gridIndex   1
                    :show        false
                    :type        "category"
                    :data        fenshi-x-axis
                    :boundaryGap false
                    :splitArea   {:show false}
                    :splitLine   {:show false}
                    :axisLine    {:lineStyle {:color "#999"}}
                    :axisPointer {:show      true
                                  :lineStyle {:type "solid"}
                                  ;;:label     {:formatter xaxis-pointer-label-formatter}
                                  }}
                   ]
     :yAxis       [{:gridIndex   0
                    :show        true
                    :type        "value"
                    :scale       true
                    :boundaryGap false
                    :min         data-min
                    :max         data-max
                    :splitArea   {:show false}
                    :splitLine   {:show false}
                    :axisLine    {:show      false
                                  :lineStyle {:color "#999"}}
                    :axisPointer {:show      false
                                  :lineStyle {:type "dotted"}
                                  ;;:label     {:formatter yaxis-pointer-label-formatter}
                                  }
                    :axisTick    {:show false}
                    ;; :axisLabel   {:show          false
                    ;;               :margin        4
                    ;;               :inside        true
                    ;;               ;;:formatter     yaxis-label-formatter
                    ;;               :showMinLabel  true
                    ;;               :showMaxLabel  true
                    ;;               :verticalAlign "bottom"
                    ;;               :color         "#999"
                    ;;               :fontSize      12}
                    :z           100}
                   {:gridIndex   1
                    :show        true
                    :position    "left"
                    :type        "value"
                    :scale       false
                    :boundaryGap false
                    :splitLine   {:show false}
                    :axisLine    {:show false}
                    :axisPointer {:show      true
                                  :lineStyle {:type "solid"}}
                    :axisTick    {:show false}
                    :axisLabel   {:show false}}
                   {:gridIndex   1
                    :show        false
                    :position    "right"
                    :type        "value"
                    :scale       true
                    :boundaryGap false
                    :splitLine   {:show false}
                    :axisPointer {:show      true
                                  :lineStyle {:type "solid"}}
                    :axisLine    {:show false}
                    :axisTick    {:show false}
                    :axisLabel   {:show false}}
                   ]
     :series      [{:name      "price"
                    :type      "line"
                    :smooth    true
                    ;;:animation false
                    :areaStyle {:normal {:color "#e2e5eb"}}
                    :data      line-data
                    :lineStyle {:normal {:color "#284a7d"
                                         :type  "solid"
                                         :width 1}}
                    :color     ["#57657b"]}
                   {:name       "volume"
                    :type       "bar"
                    :xAxisIndex 1
                    :yAxisIndex 1
                    :gridIndex  1
                    :data       (get-fenshi-volume-data true use-data)}]}))


(defn fill-up-k
  [data fill]
  (if (>= (count data) k-num)
    data
    (take k-num
          (concat data
                  (repeat fill)))))

(defn indexinfo-get-k-option
  [use-data]
  (let [{:keys [start end k]} use-data
        {:keys [ma_info origin turnover]} k
        k-max                             (or (apply max (map #(get % 3) k)) 9999)
        k-min                             (or (apply min (map #(get % 4) k)) 0)
        k-red                             "#fa5c5c"
        k-green                           "#65cd6b"
        ma-data                           (map (fn [item] (-> item (get 7))) origin)
        x-axis-data                       (map first origin)
        cnt                               (count origin)
        _                                 (rf/dispatch [:indexinfo-set-active-k-item (dec cnt)])
        tooltip-formatter                 (fn [^js params]
                                            (let [[series-0]          (js->clj params)
                                                  {:strs [dataIndex]} series-0
                                                  _                   (rf/dispatch [:indexinfo-set-active-k-item dataIndex])
                                                  [date close
                                                   open high
                                                   low change changeratio
                                                   mas lastclose pe mv ffmv] (get origin dataIndex)
                                                  [ma5 ma20 ma60]     mas
                                                  [_ _ amount to]     (get turnover dataIndex)]
                                              (html
                                               [:div.tooltip-main
                                                [:div.tooltip-row (some-> date format-date-slash)]
                                                [:div.tooltip-row
                                                 [:div.tooltip-left "开盘价："]
                                                 [:div.tooltip-right (or (some-> open (.toFixed 2)) "NA")]]
                                                [:div.tooltip-row
                                                 [:div.tooltip-left "最高价："]
                                                 [:div.tooltip-right (or (some-> high (.toFixed 2)) "NA")]]
                                                [:div.tooltip-row
                                                 [:div.tooltip-left "最低价："]
                                                 [:div.tooltip-right (or (some-> low (.toFixed 2)) "NA")]]
                                                [:div.tooltip-row
                                                 [:div.tooltip-left "收盘价："]
                                                 [:div.tooltip-right (or (some-> close (.toFixed 2)) "NA")]]
                                                [:div.tooltip-row
                                                 [:div.tooltip-left "涨跌幅："]
                                                 [:div.tooltip-right (or (some-> changeratio (.toFixed 2) (str "%")) "NA")]]
                                                [:div#hl-row.tooltip-row
                                                 [:div.tooltip-left "市盈率："]
                                                 [:div.tooltip-right (or (some-> pe (.toFixed 1)) "NA")]]
                                                [:div.tooltip-row
                                                 [:div.tooltip-left "总市值："]
                                                 [:div.tooltip-right (or (some-> mv (format-number-chn-2 2)) "NA")]]
                                                [:div.tooltip-row
                                                 [:div.tooltip-left "自由流通："]
                                                 [:div.tooltip-right (or (some-> ffmv (format-number-chn-2 2)) "NA")]]
                                                [:div.tooltip-row
                                                 [:div.tooltip-left "成交额："]
                                                 [:div.tooltip-right (or (some-> amount (format-number-chn-2 2)) "NA")]]
                                                [:div.tooltip-row
                                                 [:div.tooltip-left "换手率："]
                                                 [:div.tooltip-right (or (some-> to (.toFixed 1) (str "%")) "NA")]]])))
        start-new                             (if (> cnt k-num)
                                            (* (/ (- cnt k-num) cnt) 100)
                                            0)
        end-new                               100
        start-index                       (Math/round (* cnt start 0.01))
        end-index                         (max 0 (dec (Math/round (* cnt end 0.01))))
        _                                 (rf/dispatch [:indexinfo-datazoom-set-start-end
                                                        start end start-index end-index])]
    {:animation false
     :grid      [{:top    0
                  :left   0
                  :right  0
                  :height "100%"}
                 {:left   0
                  :right  0
                  :bottom 0
                  :height "25%"}]
     :dataZoom  [{:type "slider"
                  :show false
                  :xAxisIndex [0 1]
                  :start      start
                  :end        end}
                 {:type       "inside"
                   :xAxisIndex [0 1]}]
     :tooltip   {:trigger      "axis"
                 :axisPointer  {:type "shadow"}
                 :formatter    tooltip-formatter
                 :position     tooltip-positioner-2
                 :extraCssText (str "background:rgba(0,0,0,.6);"
                                    "border-radius:0;"
                                    "color:#eee;")}
     :xAxis     [{:type        "category"
                  :scale       true
                  :boundaryGap true
                  :axisLine    {:show false}
                  :axisLabel   {:show false}
                  :axisTick    {:show false}
                  :axisPointer {:show true}
                  :data        (fill-up-k x-axis-data nil)
                  }
                 {:stype       "category"
                  :gridIndex   1
                  :scale       true
                  :boundaryGap true
                  :axisPointer {:show false}
                  :axisLine    {:show false}
                  :axisTick    {:show false}
                  :axisLabel   {:show false}
                  :data        (fill-up-k x-axis-data nil)}
                 ]
     :yAxis     [{:scale     true
                  :axisLabel {:show false}
                  :axisLine  {:show false}
                  :axisTick  {:show false}
                  :splitLine {:show false}}
                 {:scale     false
                  :gridIndex 1
                  :axisLabel {:show false}
                  :axisLine  {:show false}
                  :axisTick  {:show false}
                  :splitLine {:show false}}]
     :series    [{:name        "k"
                  :type        "k"
                  :barMaxWidth 10
                  :itemStyle   {:normal {:color        k-red
                                         :color0       k-green
                                         :borderWidth  1
                                         :borderColor  k-red
                                         :borderColor0 k-green}}
                  :markPoint {:symbol "pin"
                              :symbolSize 0
                              :data   [{:type "max"}
                                       {:type "min"}]}
                  :data        (fill-up-k (map (fn [item]
                                                 [(get item 2)
                                                  (second item)
                                                  (get item 4)
                                                  (get item 3)]) origin)
                                          [])}
                 {:name      (first ma_info)
                  :type      "line"
                  :data      (fill-up-k (map first ma-data) nil)
                  :symbol    "none"
                  :itemStyle {:normal
                              {:color     (first ma-colors)
                               :lineStyle {:width 1}}}}
                 {:name      (second ma_info)
                  :type      "line"
                  :data      (fill-up-k (map second ma-data) nil)
                  :symbol    "none"
                  :itemStyle {:normal
                              {:color     (second ma-colors)
                               :lineStyle {:width 1}}}}
                 {:name      (get ma_info 2)
                  :type      "line"
                  :data      (fill-up-k (map #(get % 2) ma-data) nil)
                  :symbol    "none"
                  :itemStyle {:normal
                              {:color     (get ma-colors 2)
                               :lineStyle {:width 1}}}}
                 {:name       "amount"
                  :type       "bar"
                  :xAxisIndex 1
                  :yAxisIndex 1
                  :gridIndex  1
                  :data       (fill-up-k (map #(get % 2) turnover) nil)
                  :itemStyle  {:normal
                               {:color "rgba(0,0,0,.2)"}}}]}))


(defn indexinfo-get-kpe-option
  [use-data]
  (let [pe-info                   ["pe-low" "pe-mid" "pe-high"]
        {:keys [k prices]} use-data
        {:keys [origin turnover]} k
        {prices-min :min prices-mid :mid prices-max :max} prices
        k-red                     "#fa5c5c"
        k-green                   "#65cd6b"
        x-axis-data               (map first origin)
        cnt                       (count origin)
        _                         (rf/dispatch [:indexinfo-set-active-k-item (dec cnt)])
        tooltip-formatter         (fn [^js params]
                                    (let [{:strs [dataIndex]}        (first (js->clj params))
                                          _                          (rf/dispatch [:indexinfo-set-active-k-item dataIndex])
                                          [date close
                                           open high
                                           low change changeratio
                                           mas lastclose pe mv ffmv] (get origin dataIndex)
                                          [_ _ amount to]            (get turnover dataIndex)]
                                      (html
                                       [:div.tooltip-main
                                        [:div.tooltip-row (some-> date format-date-slash)]
                                        [:div.tooltip-row
                                         [:div.tooltip-left "开盘价："]
                                         [:div.tooltip-right (or (some-> open (.toFixed 2)) "NA")]]
                                        [:div.tooltip-row
                                         [:div.tooltip-left "最高价："]
                                         [:div.tooltip-right (or (some-> high (.toFixed 2)) "NA")]]
                                        [:div.tooltip-row
                                         [:div.tooltip-left "最低价："]
                                         [:div.tooltip-right (or (some-> low (.toFixed 2)) "NA")]]
                                        [:div.tooltip-row
                                         [:div.tooltip-left "收盘价："]
                                         [:div.tooltip-right (or (some-> close (.toFixed 2)) "NA")]]
                                        [:div.tooltip-row
                                         [:div.tooltip-left "涨跌幅："]
                                         [:div.tooltip-right (or (some-> changeratio (.toFixed 2) (str "%")) "NA")]]
                                        [:div#hl-row.tooltip-row
                                         [:div.tooltip-left "市盈率："]
                                         [:div.tooltip-right (or (some-> pe (.toFixed 1)) "NA")]]
                                        [:div.tooltip-row
                                         [:div.tooltip-left "总市值："]
                                         [:div.tooltip-right (or (some-> mv (format-number-chn-2 2)) "NA")]]
                                        [:div.tooltip-row
                                         [:div.tooltip-left "自由流通："]
                                         [:div.tooltip-right (or (some-> ffmv (format-number-chn-2 2)) "NA")]]
                                        [:div.tooltip-row
                                         [:div.tooltip-left "成交额："]
                                         [:div.tooltip-right (or (some-> amount (format-number-chn-2 2)) "NA")]]
                                        [:div.tooltip-row
                                         [:div.tooltip-left "换手率："]
                                         [:div.tooltip-right (or (some-> to (.toFixed 1) (str "%")) "NA")]]])))
        start                     (if (> cnt k-num)
                                    (* (/ (- cnt k-num) cnt) 100)
                                    0)
        end                       100
        start-index               (Math/round (* cnt start 0.01))
        end-index                 (max 0 (dec (Math/round (* cnt end 0.01))))
        _                         (rf/dispatch [:indexinfo-datazoom-set-start-end
                                                start end start-index end-index])]
    {:animation false
     :grid      [{:top    0
                  :left   0
                  :right  0
                  :height "100%"}
                 {:left   0
                  :right  0
                  :bottom 0
                  :height "25%"}]
     :dataZoom  [{:type "slider"
                  :show false
                  :xAxisIndex [0 1]
                  :start      start
                  :end        end}
                 {:type       "inside"
                   :xAxisIndex [0 1]}]
     :tooltip   {:trigger      "axis"
                 :axisPointer  {:type "shadow"}
                 :formatter    tooltip-formatter
                 :position     tooltip-positioner-2
                 :extraCssText (str "background:rgba(0,0,0,.6);"
                                    "border-radius:0;"
                                    "color:#eee;")}
     :xAxis     [{:type        "category"
                  :scale       true
                  :boundaryGap true
                  :axisLine    {:show false}
                  :axisLabel   {:show false}
                  :axisTick    {:show false}
                  :axisPointer {:show true}
                  :data        (fill-up-k x-axis-data nil)}
                 {:stype       "category"
                  :gridIndex   1
                  :scale       true
                  :boundaryGap true
                  :axisPointer {:show false}
                  :axisLine    {:show false}
                  :axisTick    {:show false}
                  :axisLabel   {:show false}
                  :data        (fill-up-k x-axis-data nil)}]
     :yAxis     [{:scale     true
                  :axisLabel {:show false}
                  :axisLine  {:show false}
                  :axisTick  {:show false}
                  :splitLine {:show false}}
                 {:scale     false
                  :gridIndex 1
                  :axisLabel {:show false}
                  :axisLine  {:show false}
                  :axisTick  {:show false}
                  :splitLine {:show false}}]
     :series    [{:name        "k"
                  :type        "k"
                  :barMaxWidth 10
                  :itemStyle   {:normal {:color        k-red
                                         :color0       k-green
                                         :borderWidth  1
                                         :borderColor  k-red
                                         :borderColor0 k-green}}
                  :markPoint {:symbol "pin"
                              :symbolSize 0
                              :data [{:type "max"}
                                     {:type "min"}]}
                  :data        (fill-up-k (map (fn [item]
                                                 [(get item 2)
                                                  (second item)
                                                  (get item 4)
                                                  (get item 3)]) origin)
                                          [])}
                 {:name      (first pe-info)
                  :type      "line"
                  :data      (fill-up-k prices-min nil)
                  :symbol    "none"
                  :itemStyle {:normal
                              {:color     (first ma-colors)
                               :lineStyle {:width 1}}}}
                 {:name      (second pe-info)
                  :type      "line"
                  :data      (fill-up-k prices-mid nil)
                  :symbol    "none"
                  :itemStyle {:normal
                              {:color     (second ma-colors)
                               :lineStyle {:width 1}}}}
                 {:name      (get pe-info 2)
                  :type      "line"
                  :data      (fill-up-k prices-max nil)
                  :symbol    "none"
                  :itemStyle {:normal
                              {:color     (get ma-colors 2)
                               :lineStyle {:width 1}}}}
                 {:name       "amount"
                  :type       "bar"
                  :xAxisIndex 1
                  :yAxisIndex 1
                  :gridIndex  1
                  :data       (fill-up-k (map #(get % 2) turnover) nil)
                  :itemStyle  {:normal
                               {:color "rgba(0,0,0,.2)"}}}]}))

(def indexinfo-get-kday-option
  indexinfo-get-k-option)

(def indexinfo-get-kweek-option
  indexinfo-get-k-option)

(def indexinfo-get-kmonth-option
  indexinfo-get-k-option)

(def indexinfo-get-kdaype-option
  indexinfo-get-kpe-option)

(def indexinfo-get-kweekpe-option
  indexinfo-get-kpe-option)

(def indexinfo-get-kmonthpe-option
  indexinfo-get-kpe-option)

(defn indexinfo-get-option-fn
  [cate]
  (case cate
    :pe          indexinfo-get-pe-option
    :pb          indexinfo-get-pb-option
    :ps          indexinfo-get-ps-option
    :totalmv     indexinfo-get-totalmv-option
    :freefloatmv indexinfo-get-freefloatmv-option
    :divirate    indexinfo-get-divirate-option
    :fenshi      indexinfo-get-fenshi-option
    :kday        indexinfo-get-kday-option
    :kweek       indexinfo-get-kweek-option
    :kmonth      indexinfo-get-kmonth-option
    :kdaype      indexinfo-get-kdaype-option
    :kweekpe     indexinfo-get-kweekpe-option
    :kmonthpe    indexinfo-get-kmonthpe-option))

(defn get-fenshi-y-axis
  [trading? use-data]
  (let [{:keys [newprice lastclose]} (first use-data)
        lastclose                    (if trading? lastclose newprice)
        delta                        (get-fenshi-delta use-data)]
    {:type        "value"
     :boundaryGap false
     :axisLine    {:show false}
     :axisTick    {:show false}
     :splitLine   {:lineStyle {:color "#f0f0f0"
                               :type  "dotted"}}
     :splitNumber 1
     :axisLabel   {:show false}
     :min         (- lastclose delta)
     :max         (+ lastclose delta)
     :interval    delta}))

(defn get-fenshi-option
  [trading? use-data]
  (let [line-data         (get-fenshi-newprice-data use-data)
        bar-data          (get-fenshi-volume-data trading? use-data)
        y-axis            (get-fenshi-y-axis trading? use-data)
        tooltip-formatter (fn [^js item]
                            (let [{:strs [dataIndex value name]} (first (clj->js item))
                                  #_                             (prn "@@@dataIndex@@@" dataIndex)
                                  volume                         (dataIndex "value")]
                              (html [:div
                                     [:span.fenshi-0
                                      (if name
                                        (subs name 0 (str/index-of name "/"))
                                        "--:--")]
                                     [:br]
                                     [:span.fenshi-1 "股价："]
                                     [:span.fenshi-2
                                      (or
                                       (some-> value (.toFixed 2))
                                       "NA")]
                                     [:br]
                                     [:span.fenshi-1 "成交量："]
                                     [:span.fenshi-2
                                      (or volume "0")]])))
        position-fn       (fn [^js pos ^js params ^js dom ^js rect ^js size]
                            (clj->js
                             (merge {"top" 10}
                                    (if (<= screen-width (* 2 (first (js->clj pos))))
                                      {"left" 30}
                                      {"right" 30}))))]
    {:grid    {:left 20 :right 20 :top 0 :bottom 1}
     :color   ["rgba(0,0,0,0)" "#4b93f0"]
     :tooltip {:trigger   "axis"
               :show      true
               :formatter tooltip-formatter
               :position  position-fn}
     :xAxis   [{:type        "category"
                :boundaryGap false
                :data        fenshi-x-axis
                :axisLine    {:lineStyle {:color "#f0f0f0"
                                          :type  "dotted"}}
                :splitLine   {:show      true
                              :lineStyle {:color "#f0f0f0"}}
                :axisLabel   {:interval #(zero? (rem % 60))}}]
     :yAxis   y-axis
     :series  [{:type       "line"
                :showSymbol false
                :animation  false
                :smooth     true
                :data       line-data
                :lineStyle  {:normal {:color "#4b93f0"
                                      :type  (if trading? "solid" "dotted")
                                      :width 1}}
                }]}))

(defn get-fenshi-bar-option
  [trading? use-data]
  (let [series-data (get-fenshi-volume-data trading? use-data)]
    {:grid    {:left 20 :right 20 :top 0 :bottom 20}
     :tooltip {:trigger "axis" :show false}
     :xAxis   [{:type        "category"
                :boundaryGap false
                :data        fenshi-x-axis
                :axisLine    {:lineStyle {:color "#f0f0f0"
                                          :type  "dotted"}}
                :splitLine   {:show      true
                              :lineStyle {:color "#f0f0f0"}}
                :axisTick    {:show true}
                :axisLabel   {:show      true ;; fenshi here is true
                              :textStyle {:color "#666"}
                              :interval  #(if (and (not (zero? %)) (zero? (rem % 13)))
                                            (if (<= % 121)
                                              (zero? (rem % 60))
                                              (zero? (mod (dec %) 60))))}}]
     :yAxis   [{:type        "value"
                :boundaryGap false
                :axisLine    {:show false}
                :axisTick    {:show false}
                :splitLine   {:show false}
                :axisLabel   {:show false}
                :min         0}]
     :series  [{:type      "bar"
                :animation false
                :data      series-data}]}))

(defn update-k-data-with-realtime
  [ktype fuquantype k-data realtime]
  (let [real-date                   (->> (-> (:date realtime) (subs 0 10))
                                         (tf/parse (tf/formatter "yyyy-MM-dd")))
        use-k                       (-> k-data ktype)
        last-date                   (->> (-> use-k fuquantype last first .toString)
                                         (tf/parse (tf/formatter "yyyyMMdd")))
        update-last?                (case ktype
                                      :day   (= real-date last-date)
                                      :week  (= (t/week-number-of-year real-date) (t/week-number-of-year last-date))
                                      :month (= (t/month real-date) (t/month last-date)))
        ;; NOTE 这个地方改写 indexinfo 的时候需要注意，realtime不一样
        rt-date                     (->> real-date (tf/unparse (tf/formatter "yyyyMMdd")) js/Number)
        {rt-turnover    :turnover
         rt-volume      :volume
         rt-amount      :amount
         ;; rt-changeratio :changeratio
         rt-fuquanprice fuquantype} realtime
        {rt-newprice  :newprice
         rt-open      :open
         rt-high      :high
         rt-low       :low
         rt-lastclose :lastclose}   rt-fuquanprice
        #_                          (prn rt-turnover rt-volume
                                         rt-amount #_rt-changeratio
                                         rt-fuquanprice
                                         rt-newprice
                                         rt-open
                                         rt-high
                                         rt-low
                                         rt-lastclose)]
    {:ma-info  (:ma_info use-k)
     :turnover (let [turnover (:turnover use-k)]
                 (if update-last?
                   (update turnover (dec (count turnover))
                           (fn [[date volume amount turnover]]
                             [date
                              (+ volume rt-volume)
                              (+ amount rt-amount)
                              (+ turnover rt-turnover)]))
                   (conj turnover [rt-date rt-volume rt-amount rt-turnover])))
     :k        (let [k            (fuquantype use-k)
                     last-item    (last k)
                     last60-items (take-last 60 k)
                     get-MAs      (fn [rt-newprice]
                                    (let [last-closeprices (map second last60-items)
                                          last-4  (take-last 4 last-closeprices)
                                          last-19 (take-last 19 last-closeprices)
                                          last-59 (take-last 59 last-closeprices)]
                                      [(-> (apply + last-4)
                                           (+ rt-newprice)
                                           (/ (inc (count last-4))))
                                       (-> (apply + last-19)
                                           (+ rt-newprice)
                                           (/ (inc (count last-19))))
                                       (-> (apply + last-59)
                                           (+ rt-newprice)
                                           (/ (inc (count last-59))))]))]
                 (if update-last?
                   (update k (dec (count k))
                           (fn [item]
                             ;;                             (prn "update")
                             [rt-date
                              rt-newprice
                              (get item 2)
                              (max rt-high (get item 3))
                              (min rt-low (get item 4))
                              (-> (- rt-newprice (second item))
                                  (.toFixed 2)
                                  js/Number)
                              (-> (- rt-newprice (second item))
                                  (/ (second item))
                                  (* 100)
                                  (.toFixed 2)
                                  js/Number)
                              (get-MAs rt-newprice)
                              rt-lastclose]))
                   (conj k [rt-date
                            rt-newprice
                            rt-open
                            rt-high
                            rt-low
                            (-> (- rt-newprice (second last-item))
                                (.toFixed 2)
                                js/Number)
                            (-> (- rt-newprice (second last-item))
                                (/ (second last-item))
                                (* 100)
                                (.toFixed 2)
                                js/Number)
                            (get-MAs rt-newprice)
                            rt-lastclose])))}))

(defn update-indexinfo-k-data-with-realtime
  ;; k-data 为{:origin ...}
  [k-data ktype realtime]
  (let [real-date    (->> (-> (:date realtime) (subs 0 10))
                          (tf/parse (tf/formatter "yyyy-MM-dd")))
        use-k        k-data
        last-date    (->> (-> use-k :origin last first .toString)
                          (tf/parse (tf/formatter "yyyyMMdd")))
        update-last? (case ktype
                       :day     (= real-date last-date)
                       :daype   (= real-date last-date)
                       :week    (= (t/week-number-of-year real-date) (t/week-number-of-year last-date))
                       :weekpe  (= (t/week-number-of-year real-date) (t/week-number-of-year last-date))
                       :month   (= (t/month real-date) (t/month last-date))
                       :monthpe (= (t/month real-date) (t/month last-date)))
        rt-date      (->> real-date (tf/unparse (tf/formatter "yyyyMMdd")) js/Number)
        {rt-turnover    :turnover
         rt-volume      :volume
         rt-amount      :amount
         rt-changeratio :changeratio
         rt-newprice    :newprice
         rt-open        :open
         rt-high        :high
         rt-low         :low
         rt-lastclose   :lastclose
         }           realtime]
    {:ma-info  (:ma_info use-k)
     :turnover (let [turnover (:turnover use-k)]
                 (if update-last?
                   (update turnover (dec (count turnover))
                           (fn [[date volume amount turnover]]
                             [date
                              (+ volume rt-volume)
                              (+ amount rt-amount)
                              (+ turnover rt-turnover)]))
                   (conj turnover [rt-date rt-volume rt-amount rt-turnover])))
     :k        (let [k            (:origin use-k)
                     last-item    (last k)
                     last60-items (take-last 60 k)
                     get-MAs      (fn [rt-newprice]
                                    (let [last-closeprices (map second last60-items)
                                          last-4           (take-last 4 last-closeprices)
                                          last-19          (take-last 19 last-closeprices)
                                          last-59          (take-last 59 last-closeprices)]
                                      [(-> (apply + last-4)
                                           (+ rt-newprice)
                                           (/ (inc (count last-4))))
                                       (-> (apply + last-19)
                                           (+ rt-newprice)
                                           (/ (inc (count last-19))))
                                       (-> (apply + last-59)
                                           (+ rt-newprice)
                                           (/ (inc (count last-59))))]))]
                 (if update-last?
                   (update k (dec (count k))
                           (fn [item]
                             (let [close (second item)
                                   change (- rt-newprice close)
                                   changeratio (* 0.01 (get item 6))]
                              [rt-date
                               rt-newprice
                               (get item 2)
                               (max rt-high (get item 3))
                               (min rt-low (get item 4))
                               (-> change
                                   (.toFixed 2)
                                   js/Number)
                               (-> change
                                   (/ close)
                                   (* 100)
                                   (.toFixed 2)
                                   js/Number)
                               (get-MAs rt-newprice)
                               rt-lastclose
                               ;; pe
                               (* (get item 9) (inc changeratio))
                               ;; totalmv
                               (* (get item 10) (inc changeratio))
                               ;; freefloatmv
                               (* (get item 11) (inc changeratio))
                               ])))
                   (conj k (let [close (second last-item)
                                 change (- rt-newprice (second last-item))
                                 changeratio (* 0.01 (get last-item 6))]
                            [rt-date
                             rt-newprice
                             rt-open
                             rt-high
                             rt-low
                             (-> change
                                 (.toFixed 2)
                                 js/Number)
                             (-> change
                                 (/ close)
                                 (* 100)
                                 (.toFixed 2)
                                 js/Number)
                             (get-MAs rt-newprice)
                             rt-lastclose
                             (* (get last-item 9) (inc changeratio))
                             (* (get last-item 10) (inc changeratio))
                             (* (get last-item 11) (inc changeratio))]))))}))

(defn get-k-x-axis-data
  [ktype k]
  (take k-num
        (concat
         (map first k)
         (let [last-date (first (last k))
               year      (quot last-date 1E4)
               month     (rem (quot last-date 1E2) 1E2)
               day       (rem last-date 1E2)
               period-f  ({:day   t/days
                           :week  t/weeks
                           :month t/months} ktype)]
           (map
            #(js/parseInt
              (tf/unparse (tf/formatter "yyyyMMdd")
                          (t/plus (t/date-time year month day) (period-f %))))
            (drop 1 (range)))))))

(defn get-k-bar-data
  [use-data]
  (let [{:keys [k turnover]} use-data]
    (map (fn [item]
           {:value     (second item)
            :itemStyle {:normal {:color "gray"}}})
         turnover)))

(defn get-k-bar-option
  [use-data axis-data]
  (let [series-data (get-k-bar-data use-data)]
    {:grid    {:left 20 :right 20 :top 0 :bottom 20}
     :tooltip {:trigger "axis" :show false}
     :xAxis   [{:type        "category"
                :boundaryGap false
                :data        axis-data
                :axisLine    {:lineStyle {:color "#f0f0f0"
                                          :type  "dotted"}}
                :splitLine   {:show      true
                              :lineStyle {:color "#f0f0f0"}}
                :axisTick    {:show true}
                :axisLabel   {:show      true ;; fenshi here is true
                              :textStyle {:color "#666"}
                              :interval  #(if (and (not (zero? %)) (zero? (rem % 13)))
                                            (if (<= % 121)
                                              (zero? (rem % 60))
                                              (zero? (mod (dec %) 60))))}}]
     :yAxis   [{:type        "value"
                :boundaryGap false
                :axisLine    {:show false}
                :axisTick    {:show false}
                :splitLine   {:show false}
                :axisLabel   {:show false}
                :min         0}]
     :series  [{:type      "bar"
                :animation false
                :data      series-data}]}))

(defn get-k-option
  [use-data x-axis-data]
  (let [{:keys [ma-info k turnover]} use-data ;; turnover?
        ma-color                     ["#469cf3", "#ffab34", "#d13ca0"]
        k-max                        (if (empty? use-data) 9999 (apply max (map #(get % 3) use-data)))
        k-min                        (if (empty? use-data) 0 (apply max (map #(get % 4) use-data)))
        red                          "#fa5c5c"
        green                        "#65cd6b"
        ma-data                      (map (fn [item] (-> item (get 7))) k)
        k-series                     [{:name        ""
                                       :type        "k"
                                       :barMaxWidth 10
                                       :itemStyle   {:normal {:color        red
                                                              :color0       green
                                                              :borderWidth  1
                                                              :borderColor  red
                                                              :borderColor0 green}}
                                       :data        (map (fn [item]
                                                           [(get item 2)
                                                            (second item)
                                                            (get item 4)
                                                            (get item 3)]) k)}
                                      {:name      (first ma-info)
                                       :type      "line"
                                       :data      (map first ma-data)
                                       :symbol    "none"
                                       :itemStyle {:normal
                                                   {:color     (first ma-color)
                                                    :lineStyle {:width 1}}}}
                                      {:name      (second ma-info)
                                       :type      "line"
                                       :data      (map second ma-data)
                                       :symbol    "none"
                                       :itemStyle {:normal
                                                   {:color     (second ma-color)
                                                    :lineStyle {:width 1}}}}
                                      {:name      (get ma-info 2)
                                       :type      "line"
                                       :data      (map #(get % 2) ma-data)
                                       :symbol    "none"
                                       :itemStyle {:normal
                                                   {:color     (get ma-color 2)
                                                    :lineStyle {:width 1}}}}]]
    {:animation false
     :grid      {:left   20
                 :right  20
                 :top    0
                 :bottom 5}
     :tooltip   {:trigger   "axis"
                 :show      true
                 :formatter (fn [^js param]
                              (let [item      (first (js->clj param))
                                    time      (get item "name")
                                    index     (get item "dataIndex")
                                    data-item (get use-data index)]
                                (when data-item
                                  (rf/dispatch [:secuinfo-set-active-ma-index index])
                                  (html [:div
                                         [:span.title time]
                                         [:div
                                          [:span.title "开盘价："]
                                          [:span.price (-> (get data-item 2) (.toFixed 2))]]
                                         [:div
                                          [:span.title "最高价："]
                                          [:span.price (-> (get data-item 3) (.toFixed 2))]]
                                         [:div
                                          [:span.title "最低价："]
                                          [:span.price (-> (get data-item 4) (.toFixed 2))]
                                          ]
                                         [:div
                                          [:span.title "收盘价："]
                                          [:span.price (-> (get data-item 1) (.toFixed 2))]]
                                         [:div
                                          [:span.title "涨跌额："]
                                          [:span.price (-> (get data-item 5) (.toFixed 2))]]
                                         [:div
                                          [:span.title "涨跌幅："]
                                          [:span.price (str (-> (get data-item 6) (.toFixed 2)) "%")]]]))))
                 :position  (fn [^js pos ^js params ^js dom ^js rect ^js size]
                              (clj->js
                               {:top         10
                                (keyword
                                 (if (<= (.-clientWidth js/window)
                                         (* 2 (first (js->clj pos))))
                                   "left"
                                   "right")) 30}))}
     :dataZoom  {:type      "inside"
                 :axisIndex [0,1]
                 :start     40
                 :end       70
                 :top       30
                 :height    20}
     :xAxis     {:type        "category"
                 :boundaryGap false
                 :data        x-axis-data
                 :axisLine    {:lineStyle {:color "#f0f0f0"
                                           :type  "dotted"}}
                 :splitLine   {:show      true
                               :lineStyle {:color "#f0f0f0"}}
                 :axisTick    {:show false}
                 :axisLabel   {:interval (fn [^js index]
                                           (and (not (zero? index))
                                                (zero? (rem index 13))))}}
     :yAxis     {:type        "value"
                 :boundaryGap false
                 :offset      -15
                 :max         k-max
                 :min         k-min
                 :axisLine    {:show false}
                 :axisTick    {:show false}
                 :splitLine   {:lineStyle {:color "#f0f0f0"
                                           :type  "dotted"}}
                 :splitNumber 1
                 :axisLabel   {:show      false
                               :textStyle {:color "#686868"}}}
     :series    k-series})
  )

(defn get-pe-option
  [{:keys [x y tool-tip active mark-line mark-area]}]
  ;; use drop-while to get the list
  ;; and use (- (count full) (count part)) to get the index
  ;; and use (drop index another-full) to get another list

  ;; active :1year :3year :5year :all
  (let [use-x        (drop-while (fn [item]
                                   (case active
                                     :1year (in-n-year? 1 item ymd-slash-formatter)
                                     :3year (in-n-year? 3 item ymd-slash-formatter)
                                     :5year (in-n-year? 5 item ymd-slash-formatter)
                                     :all   true))
                                 x)
        cnt          (- (count x) (count use-x))
        use-y        (drop cnt y)
        use-tool-tip (drop cnt tool-tip)]
    {:title   {:show false}
     :grid    {:top 10}
     :left    "4%"
     :right   "10%"
     :tooltip {:trigger     "axis"
               :axisPointer {:type "line"}
               :formatter   (fn [^js item]
                              (clj->js
                               (let [clj-item (js->clj item)]
                                 (html
                                  [:div
                                   [:div (-> clj-item first (get "name"))]
                                   [:div
                                    "PE: "
                                    (or
                                     (-> clj-item first (get "dataIndex") use-tool-tip)
                                     "-")]]))))}
     :xAxis   {:type      "category"
               :data      use-x
               :splitArea {:show false}
               :axisLine  {:show      false
                           :lineStyle {:color "#999"}}
               :axisTick  {:show false}
               :axisLabel {:textStyle {:fontSize 9}}}
     :yAxis   {:type      "value"
               :scale     true
               :min       0
               :splitArea {:show true}
               :splitLine {:show false}
               :axisLine  {:lineStyle {:color "#999"}}
               :axisTick  {:show false}
               :axisLabel {:show      false
                           :textStyle {:fontSize 9}}}
     :series  [{:name     ""
                :type     "line"
                :data     use-y
                :markLine {:silent    true
                           :symbol    "none"
                           :data      mark-line
                           :label     {:normal {:position  "end"
                                                :formatter (fn [^js item] (-> item js->clj (get "name")))
                                                :textStyle {:fontSize 9}}}
                           :lineStyle {:normal {:color "#999"
                                                :type  "dotted"}}}
                :markArea {:silent    true
                           :itemStyle {:normal {:color       "rgba(101,205,107,.2)"
                                                :borderWidth 0
                                                :borderType  "dashed"}}
                           :data      mark-area}
                :color    ["#ffab36"]}]}))


(defn get-pb-option
  [{:keys [x y active]}]
  (let [use-x (drop-while (fn [item]
                            (case active
                              :1year (not (in-n-year? 1 item ymd-slash-formatter))
                              :3year (not (in-n-year? 3 item ymd-slash-formatter))
                              :5year (not (in-n-year? 5 item ymd-slash-formatter))
                              :all   true))
                          x)
        cnt   (- (count x) (count use-x))
        use-y (drop cnt y)]
    {:title   {:show false}
     :grid    {:top    10
               :height "auto"
               :left   "10%"
               :right  "4%"}
     :tooltip {:trigger     "axis"
               :axisPointer {:type "line"}
               :formatter   (fn [^js item]
                              (clj->js
                               (let [clj-item (js->clj item)]
                                 (html
                                  [:div
                                   [:div (-> clj-item first (get "name"))]
                                   [:div "PB: " (or (-> clj-item first (get "value")) "")]]))))}
     :xAxis   {:type      "category"
               :data      use-x
               :splitArea {:show false}
               :splitLine {:show false}
               :axisLine  {:lineStyle {:color "#999"}}
               :axisLabel {:textStyle {:fontSize 9}}}
     :yAxis   {:type      "value"
               :scale     true
               :splitArea {:show false}
               :splitLine {:show false}
               :axisLine  {:lineStyle {:color "#999"}}
               :axisTick  {:show true}
               :axisLabel {:show      true
                           :textStyle {:fontSize 9}}}
     :series  [{:name  ""
                :type  "line"
                :data  use-y
                :color ["#ffab36"]}]}))

(defn get-ps-option
  [{:keys [x y active]}]
  (let [use-x (drop-while (fn [item]
                            (case active
                              :1year (not (in-n-year? 1 item ymd-slash-formatter))
                              :3year (not (in-n-year? 3 item ymd-slash-formatter))
                              :5year (not (in-n-year? 5 item ymd-slash-formatter))
                              :all   false))
                          x)
        cnt   (- (count x) (count use-x))
        use-y (drop cnt y)]
    {:title   {:show false}
     :grid    {:top    10
               :height "80%"
               :left   "10%"
               :right  "4%"}
     :tooltip {:trigger     "axis"
               :axisPointer {:type "line"}
               :formatter   (fn [^js item]
                              (clj->js
                               (let [clj-item (js->clj item)]
                                 (html
                                  [:div
                                   [:div (-> clj-item first (get "name"))]
                                   [:div "PS: " (str (or (-> clj-item first (get "value")) "") "%")]]))))}
     :xAxis   {:type      "category"
               :data      use-x
               :splitArea {:show false}
               :splitLine {:show false}
               :axisLine  {:lineStyle {:color "#999"}}
               :axisLabel {:textStyle {:fontSize 9}}}
     :yAxis   {:type      "value"
               :scale     true
               :splitArea {:show false}
               :splitLine {:show false}
               :axisLine  {:lineStyle {:color "#999"}}
               :axisTick  {:show true}
               :axisLabel {:show      true
                           :textStyle {:fontSize 9}}}
     :series  [{:name  ""
                :type  "line"
                :data  use-y
                :color ["#ffab36"]}]}))

(defn season-number
  [month]
  (case month
    "03" "1"
    "06" "2"
    "09" "3"
    "12" "4"))

(defn get-season
  [yyyymm]
  (str (subs yyyymm 0 4)
       "Q"
       (season-number (subs yyyymm 4))))

(defn get-gr-option
  [{:keys [x y tool-tip mark-line mark-area bar-y season-year cate active]}]
  (let [use-x (drop-while (fn [item]
                            (case active
                              :1year (not (in-n-year? 1 item ymd-formatter))
                              :3year (not (in-n-year? 3 item ymd-formatter))
                              :5year (not (in-n-year? 5 item ymd-formatter))
                              :all   false))
                          x)
        cnt   (- (count x) (count use-x))
        use-y (drop cnt y)]
    {:title   {:show false}
     :grid    {:top    10
               :left   "4%"
               :right  "10%"
               :height "75%"}
     :tooltip {:trigger     "axis"
               :axisPointer {:type "line"}
               :formatter   (fn [^js item]
                              (let [clj-item (js->clj item)]
                                (html
                                 (let [name  (-> clj-item first (get "name"))
                                       index (-> clj-item first (get "dataIndex"))
                                       title (case season-year
                                               :season (get-season name)
                                               :year   (let [month (-> x last (subs 6))]
                                                         (if (and (not= "12" month)
                                                                  (== index (dec (count use-x))))
                                                           (str clj-item
                                                                "Q"
                                                                (season-number month)
                                                                "LTM")
                                                           clj-item)))]
                                   [:div
                                    [:div title]
                                    [:div
                                     (get-in gr-name [cate season-year])
                                     ":"
                                     (str (or (get tool-tip index) "-") "%")]
                                    [:div
                                     (case cate
                                       :egr "净利"
                                       :rgr "营收")
                                     ":"
                                     (format-number-chn (get bar-y index) 1)]]))))}
     :xAxis   {:type      "category"
               :data      use-x
               :splitArea {:show false}
               :splitLine {:show false}
               :axisLine  {:show      false
                           :lineStyle {:color "#999"}}
               :axisTick  {:show false}
               :axisLabel {:textStyle {:fontSize 9}
                           :formatter (fn [^js item]
                                        (get-season item))}}
     :yAxis   {:type      "value"
               :scale     true
               :min       0
               :splitArea {:show false}
               :splitLine {:show false}
               :axisLine  {:lineStyle {:color "#999"}}
               :axisTick  {:show false}
               :axisLabel {:show      false
                           :textStyle {:fontSize 9}}}
     :series  [{:name     ""
                :type     "line"
                :data     use-y
                :markLine {:silent    true
                           :symbol    "none"
                           :data      mark-line
                           :label     {:normal {:position  "end"
                                                :formatter (fn [^js item]
                                                             ;; TODO first?
                                                             (-> item js->clj (get "name")))
                                                :textStyle {:fontSize 9}}}
                           :lineStyle {:normal {:color "#999"
                                                :type  "dotted"}}}
                :markArea {:silent    false
                           :itemStyle {:normal {:color       "rgba(100,100,100,.2)"
                                                :borderWidth 0
                                                :borderType  "dashed"}}
                           :data      mark-area}
                :color    ["#ffab36"]}]}))

(defn get-markline-data-range
  [y-min y-max]
  (let [start      (min 0 y-min)
        end        (max 0 y-max)
        max-cardin (max (get-number-cardinality start)
                        (get-number-cardinality end))]
    ;; if need to filter, do filter
    (map (fn [num]
           {:name  (format-number-chn-1 num)
            :yAxis num})
         (map #(* % (Math/pow 10 max-cardin)) (range -10 11)))))

(defn get-gr-bar-option
  [{:keys [x y]}]
  {:grid    {:y     "0%"
             :y2    "10%"
             :left  "4%"
             :right "10%"}
   :tooltip {:trigger "axis"
             :show    false}
   :xAxis   {:type        "category"
             :show        true
             :boundaryGap true
             :axisTick    {:show  false
                           :onGap false}
             :splitLine   {:show false}
             :data        x
             :axisLine    {:show      true
                           :lineStyle {:color "#999"
                                       :type  "dotted"}}
             :axisLabel   {:show false}}
   :yAxis   {:type        "value"
             :scale       false
             :boundaryGap [0.01 0.01]
             :axisLine    {:show      (not (zero? (count y)))
                           :lineStyle {:color "#999"}}
             :splitNumber 5
             :axisLabel   {:show      false
                           :formatter (fn [item]
                                        (format-number-chn item 1))}
             :axisTick    {:show false}
             :splitLine   {:show false}}
   :series  {:name          {:egr "净利润"
                             :rgr "营收"}
             :type          "bar"
             :barMinHeight  5
             :data          y
             :symbol        "none"
             :showSymbol    true
             :showAllSymbol false
             :itemStyle     {:normal   {:color "rgba(255,171,54,.7)"}
                             :emphasis {:label {:position  "top"
                                                :show      false
                                                :formatter (fn [^js item]
                                                             (-> item js->clj (get "data") (format-number-chn 1)))
                                                :textStyle {:color "#666"}}}}
             :markLine      {:silent    true
                             :symbol    "none"
                             :data      (get-markline-data-range (apply min y) (apply max y))
                             :label     {:normal {:position  "end"
                                                  :formatter (fn [^js item] (-> item js->clj (get "name")))
                                                  :textStyle {:fontSize 9}}}
                             :lineStyle {:normal {:color "#999"
                                                  :type  "dotted"}}}}})


(defn update-secuinfo-chart-data
  [secuinfo k data] ;; k is :egr or :rgr
  (let [{:keys [q y]} data]
    (-> secuinfo
        (assoc-in [:chart :data k]
                  {:season (let [{:keys [data conf ltzero]} q]
                             {:x         (map (fn [item]
                                                (some->> item first format-date-slash))
                                              data)
                              :y         (map #(get % 3) data)
                              :tool-tip  (map #(get % 2) data)
                              :mark-line (map (fn [item]
                                                {:name  (second item)
                                                 :yAxis (first item)})
                                              conf)
                              :mark-area [{:yAxis 0}
                                          {:yAxis (first ltzero)}]
                              :bar-y     (map (fn [item]
                                                (or (first item) 0))
                                              data)})
                   :year   (let [{:keys [data conf ltzero]} y]
                             {:x         (map (fn [item]
                                                (-> item first (subs 0 4)))
                                              data)
                              :y         (map #(get % 3) data)
                              :tool-tip  (map #(get % 2) data)
                              :mark-line (map (fn [item]
                                                {:name  (second item)
                                                 :yAxis (first item)})
                                              conf)
                              :mark-area [{:yAxis 0}
                                          {:yAxis (first ltzero)}]
                              :bar-y     (map (fn [item]
                                                (or (first item) 0))
                                              data)})}))))
