(ns doumi.events
  (:require
   [clojure.string :as str]
   [doumi.db :as db]
   #_[doumi.tracer :refer [stop-tracer
                           add-trace print-report]]
   [cljs-time.core :as t]
   [cljs-time.format :as tf]
   [day8.re-frame.http-fx]
   [ajax.core :as ajax]
   [re-frame.core :refer [dispatch subscribe reg-fx
                          reg-event-db reg-event-fx
                          reg-sub path after
                          debug]]
   [clojure.spec.alpha :as s]
   [doumi.config :refer [env]]
   [doumi.feedback-config :refer [feedbacks get-kw-by-judgement]]
   [doumi.doumi-config :refer [vip-price]]
   [doumi.helper :refer [log redirect-to in-one-year?
                         yyyymmdd-in1year? yyyymmdd-in3year? yyyymmdd-in5year?
                         ymd-num-in-n-year? proxy-uri format-date-slash]]
   [doumi.secuinfo-config :refer [fenshi-num]]
   [doumi.echarts-options :refer [secuinfo-get-divirate-option
                                  #_indexinfo-get-option-fn
                                  get-fenshi-option
                                  get-fenshi-bar-option
                                  get-k-option
                                  get-k-bar-option
                                  update-k-data-with-realtime
                                  update-indexinfo-k-data-with-realtime
                                  get-k-x-axis-data
                                  update-secuinfo-chart-data]]
   [doumi.handlers-common :refer [check-spec-interceptor]]
   [doumi.handlers-indexinfo :refer [indexinfo-interceptors]]
#_   ["./es6.js" :as es6]
   ))

;; (es6/someAsyncFn "/index.html")

;; (es6/someAsyncFn (js/fetch "/index.html"))

;; (js/console.log "es6" (es6/foo "es6"))



;; Interceptors

(def search-interceptors
  [check-spec-interceptor
   (path :search)
   (when ^boolean js/goog.DEBUG debug)])

(def flash-tip-interceptors
  [check-spec-interceptor
   (path :flash-tip)
   (when ^boolean js/goog.DEBUG debug)])

(def feedback-interceptors
  [check-spec-interceptor
   (path :feedback)
   (when ^boolean js/goog.DEBUG debug)])

#_(def bottom-nav-interceptors
    [(path :bottom-nav)])

(def doumi-interceptors
  [check-spec-interceptor
   (path :doumi)
   (when ^boolean js/goog.DEBUG debug)])

(def annc-interceptors
  [check-spec-interceptor
   (path :annc)
   (when ^boolean js/goog.DEBUG debug)])

(def annc-annc-interceptors
  [check-spec-interceptor
   (path [:annc :annc])
   (when ^boolean js/goog.DEBUG debug)])

(def annc-performance-interceptors
  [check-spec-interceptor
   (path [:annc :performance])
   (when ^boolean js/goog.DEBUG debug)])

(def annc-dividend-interceptors
  [check-spec-interceptor
   (path [:annc :dividend])
   (when ^boolean js/goog.DEBUG debug)])

(def annc-nextweek-interceptors
  [check-spec-interceptor
   (path [:annc :nextweek])
   (when ^boolean js/goog.DEBUG debug)])

(def secuinfo-interceptors
  [check-spec-interceptor
   (path [:secuinfo])
   (when ^boolean js/goog.DEBUG debug)])

;;dispatchers
(defn doumi-vip-default
  [doumi]
  (if (>= doumi 109) :season :month))

(reg-event-db
 :initialize-db
 (fn [_ _]
   db/default-db))

(reg-event-db
 :clear-secuinfo-store
 secuinfo-interceptors
 (fn [secuinfo _]
   db/secuinfo-store))

(reg-event-db
 :clear-indexinfo-store
 indexinfo-interceptors
 (fn [indexinfo _]
   db/indexinfo-store))

(reg-event-db
 :set-active-page
 (fn [db [_ page]]
   (assoc db :page page)))

(reg-event-db
 :search-show-suggestion
 search-interceptors
 (fn [search [_ show?]]
   (assoc search :show-suggestion show?)))

(reg-event-fx
 :search-later-hide-suggestion
 (fn [_ _]
   {:dispatch-later [{:ms 100 :dispatch [:search-show-suggestion false]}]}))

(reg-event-db
 :search-suggestions
 search-interceptors
 (fn [search [_ suggestions]]
   (assoc search :suggestions suggestions)))

(reg-event-db
 :search-input-val
 search-interceptors
 (fn [search [_ input-val]]
   (assoc search :input-val input-val)))

(reg-event-db
 :search-clear-input
 search-interceptors
 (fn [search _]
   (assoc search :input-val "")))

(reg-event-fx
 :search-keyword
 (fn [_ [_ word]]
   {:http-xhrio {:method          :get
                 :uri             (str "/p/" env "/stockinfogate/stock/namefinder?keyword=" word)
                 :response-format (ajax/json-response-format {:keywords? true})
                 :on-success      [:search-keyword-success]
                 :on-failure      [:search-keyword-failed]}}))

(reg-event-db
 :search-keyword-success
 search-interceptors
 (fn [search [_ result]]
   (assoc search :suggestions (:data result))))

(reg-event-db
 :search-keyword-failed
 search-interceptors
 (fn [search _]
   search))

(reg-event-fx
 :search-load-favorstocks
 (fn [_ _]
   {:http-xhrio {:method          :get
                 :uri             (str "/p/" env "/stockinfogate/user/favorstock/list/mobile")
                 :response-format (ajax/json-response-format {:keywords? true})
                 :on-success      [:search-load-favorstocks-success]
                 :on-failure      [:search-load-favorstocks-failed]}}))

(reg-event-db
 :search-load-favorstocks-success
 search-interceptors
 (fn [search [_ result]]
   (assoc search :favorstocks (set (map :secucode (:data result))))))

(reg-event-db
 :search-load-favorstocks-failed
 search-interceptors
 (fn [search _]
   search))

(reg-event-fx
 :search-add-favorstock
 (fn [{:keys [db]} [_ secucode]]
   {:db       (update-in db
                         [:search :favorstocks] conj secucode)
    :dispatch [:search-favorstock-add secucode]}))

(reg-event-fx
 :search-favorstock-add
 (fn [_ [_ secucode]]
   {:http-xhrio {:method          :post
                 :uri             (str "/p/" env "/stockinfogate/user/favorstock/add")
                 ;;       :params {:here ["is" "a" "map"]} ;; this cannot be parsed by websvc
                 :body            (doto (js/FormData.)
                                    (.append "secucode" secucode))
                 :format          (ajax/json-request-format)
                 :response-format (ajax/json-response-format {:keywords? true})
                 :on-success      [:search-favorstock-add-success]
                 :on-failure      [:search-favorstock-add-failed]}}))

(reg-event-fx
 :search-input-focus
 (fn [_ _]
   (-> js/document
       (.getElementById "search-bar-input")
       .focus)
   {}))

(reg-event-db
 :flash-tip-success-add
 flash-tip-interceptors
 ;; 显示 tip success
 (fn [flash-tip [_ kw]]
   (assoc flash-tip
          :show true
          :kw kw)))
;; should define the data showed respond to specific keys

(reg-event-db
 :flash-tip-fail-add
 flash-tip-interceptors
 ;; 显示 tip failed
 (fn [flash-tip [_ kw]]
   (assoc flash-tip
          :show true
          :kw kw)))

(reg-event-db
 :flash-tip-hide
 flash-tip-interceptors
 (fn [flash-tip _]
   (assoc flash-tip :show false)))

(reg-event-fx
 :search-favorstock-add-success
 (fn [_ [_ result]]
   (if (:status result)
     {:dispatch-n     (list
                       [:flash-tip-success-add :favorstock-add-success] ;; FIXME
                       [:search-load-favorstocks])
      :dispatch-later [{:ms 2000 :dispatch [:flash-tip-hide]}]
      }
     {:dispatch [:search-favorstock-add-failed]})))

(reg-event-fx
 :search-favorstock-add-failed
 (fn [_ _]
   {:dispatch-n     (list [:flash-tip-fail-add :favorstock-add-failed]) ;; FIXME
    :dispatch-later [{:ms 2000 :dispatch [:flash-tip-hide]}]}))

(reg-event-fx
 :flash-tip-doumi-sent-to-term
 (fn [{:keys [db]} _]
   (let [{:keys [flash-tip]} db]
     {:db             (-> db
                          (assoc-in [:flash-tip :show] true)
                          (assoc-in [:flash-tip :kw] :doumi-sent-to-term))
      :dispatch-later [{:ms 2000 :dispatch [:flash-tip-hide]}]})))

(reg-event-fx
 :feedback-get-data
 (fn [_ _]
   {:http-xhrio {:method          :get
                 :uri             (str "/p/" env "/stockinfogate/feedbackjudgementday?from_url=www.joudou.com/usercenter")
                 :response-format (ajax/json-response-format {:keywords? true})
                 :on-success      [:feedback-get-data-success false]
                 :on-failure      [:xhrio-null-failed]}}))

(reg-event-db
 :feedback-get-data-success
 feedback-interceptors
 (fn [feedback [_ click? result]]
   (let [{:keys [cnt1 cnt2 cnt3 judgement]} (:data result)
         current                            (:current feedback)
         kw                                 (get-kw-by-judgement judgement)]
     (-> feedback
         (assoc
          :good cnt1
          :bad cnt2
          :confusing cnt3
          :current kw
          :show-count (not (zero? judgement))
          :change {:add-one   (when click? kw)
                   :minus-one (when click? current)
                   :done      false})))))

(reg-event-db
 :feedback-change-done
 feedback-interceptors
 (fn [feedback _]
   (assoc-in feedback [:change :done] true)) )

#_(reg-event-db ;; do nothing
   :feedback-get-data-failed
   feedback-interceptors
   (fn [feedback _]
     feedback))

(reg-event-fx
 :feedback-on-click
 (fn [{:keys [db]} [_ kw]]
   (when (not= kw (get-in db [:feedback :current]))
     {:dispatch [:feedback-do-add kw]})))

(reg-event-fx
 :feedback-do-add
 (fn [_ [_ kw]]
   {:http-xhrio {:method          :post
                 :uri             (str "/p/" env "/stockinfogate/feedbackjudgementday/add")
                 :format          (ajax/json-request-format)
                 :body            (doto (js/FormData.)
                                    (.append "judgement" (kw feedbacks))
                                    (.append "from_url" "www.joudou.com/usercenter"))
                 :response-format (ajax/json-response-format {:keywords? true})
                 :on-success      [:feedback-do-add-success kw]
                 :on-failure      [:xhrio-null-failed]}}))

(reg-event-fx
 :feedback-do-add-success
 (fn [{:keys [db]} [_ kw result]]
   {:dispatch [:feedback-get-data-success true result]}))

(reg-event-fx
 :doumi-get-doumi
 (fn [_ _]
   {:http-xhrio {:method          :get
                 :uri             (str "/p/" env "/stockinfogate/doumi?page=0")
                 :response-format (ajax/json-response-format {:keywords? true})
                 :on-success      [:doumi-get-data-success]
                 :on-failure      [:xhrio-null-failed]}}))

(reg-event-db
 :doumi-get-data-success
 doumi-interceptors
 (fn [doumi [_ {:keys [data]}]]
   (-> doumi
       (assoc :doumi data)
       (assoc-in [:vip :active :doumi] (doumi-vip-default (:doumi data))))))

(reg-event-fx
 :doumi-get-userinfo
 (fn [_ _]
   {:http-xhrio {:method          :get
                 :uri             (str "/p/" env "/stockinfogate/userinfo")
                 :response-format (ajax/json-response-format {:keywords? true})
                 :on-success      [:doumi-get-userinfo-success]
                 :on-failure      [:xhrio-null-failed]}}))

(reg-event-db
 :doumi-get-userinfo-success
 doumi-interceptors
 (fn [doumi [_ {:keys [data]}]]
   (assoc doumi :userinfo data)))

(reg-event-db
 :doumi-set-page
 doumi-interceptors
 (fn [doumi [_ page]]
   (assoc doumi :page page)))

(reg-event-db
 :doumi-set-use-page
 doumi-interceptors
 (fn [doumi [_ page]]
   (assoc-in doumi [:use :page] page)))

(reg-event-db
 :doumi-coupon-set-active
 doumi-interceptors
 (fn [doumi [_ active]]
   (assoc-in doumi [:coupon :active] active)))

(reg-event-db
 :doumi-coupon-set-custom-value
 doumi-interceptors
 (fn [doumi [_ value]]
   (assoc-in doumi [:coupon :custom :value] value)))

#_(reg-event-db
   :doumi-coupon-set-redeemed
   doumi-interceptors
   (fn [doumi [_ redeemed]]
     (assoc-in doumi [:coupon :redeemed] redeemed)))

(reg-event-db
 :doumi-coupon-set-page
 doumi-interceptors
 (fn [doumi [_ page]]
   (assoc-in doumi [:coupon :page] page)))

(reg-event-fx
 :doumi-redeem-coupon
 (fn [{:keys [db]} _]
   (let [coupon (get-in db [:doumi :coupon])
         active (get coupon :active)
         amount (if (= active :custom)
                  (get-in coupon [:custom :value])
                  (js/parseInt (name active)))]
     {:db         (-> db
                      (assoc-in [:doumi :coupon :redeem :disable] true)
                      (assoc-in [:doumi :loading] true))
      :http-xhrio {:method          :post
                   :uri             (str "/p/" env "/stockinfogate/doumi/promocard")
                   :body            (doto (js/FormData.)
                                      (.append "doumi" amount))
                   :format          (ajax/json-request-format)
                   :response-format (ajax/json-response-format {:keywords? true})
                   :on-success      [:doumi-redeem-coupon-result amount]
                   :on-failure      [:xhrio-null-failed]}})))

(reg-event-fx
 :doumi-redeem-coupon-result
 (fn [{:keys [db]} [_ amount {:keys [status data]}]]
   (let [{:keys [doumi result message]} data]
     (if-not status
       (do
         (js/alert (str message))
         {:db (-> db
                  (assoc-in [:doumi :doumi :doumi] doumi)
                  (assoc-in [:doumi :coupon :redeem :disable] false)
                  (assoc-in [:doumi :vip :active :doumi] (doumi-vip-default doumi))
                  (assoc-in [:doumi :loading] false))})
       {:db (-> db
                (assoc-in [:doumi :doumi :doumi] doumi)
                (assoc-in [:doumi :coupon :redeem :amount] amount)
                (assoc-in [:doumi :coupon :redeem :result] result)
                (assoc-in [:doumi :coupon :page] :redeem-success)
                (assoc-in [:doumi :coupon :redeem :disable] false)
                (assoc-in [:doumi :vip :active :doumi] (doumi-vip-default doumi))
                (assoc-in [:doumi :loading] false))}))))

#_(reg-event-db
   :doumi-redeem-coupon-success
   doumi-interceptors
   (fn [doumi [_ ]]
     (-> doumi
         (assoc-in [:doumi :doumi] ))))

(reg-event-db
 :doumi-use-coupon-custom-valid
 doumi-interceptors
 (fn [doumi [_ valid?]]
   (assoc-in doumi [:coupon :custom :valid] valid?)))


(reg-event-db
 :doumi-vip-set-page
 doumi-interceptors
 (fn [doumi [_ page]]
   (assoc-in doumi [:vip :page] page)))

(reg-event-db
 :doumi-use-vip-set-doumi-page
 doumi-interceptors
 (fn [doumi [_ page]]
   (assoc-in doumi [:vip :doumi :page] page)))

(reg-event-db
 :doumi-use-vip-set-rmb-page
 doumi-interceptors
 (fn [doumi [_ page]]
   (assoc-in doumi [:vip :rmb :page] page)))

(reg-event-db
 :doumi-use-vip-set-show-help
 doumi-interceptors
 (fn [doumi [_ show?]]
   (assoc-in doumi [:vip :rmb :show :help] show?)))

(reg-event-db
 :doumi-use-vip-set-show-phone
 doumi-interceptors
 (fn [doumi [_ show?]]
   (assoc-in doumi [:vip :rmb :show :phone] show?)))

(reg-event-db
 :doumi-use-vip-redeem-set-valid-date
 doumi-interceptors
 (fn [doumi [_ result]]
   (assoc-in doumi [:vip :redeem :result] result)))

(reg-event-fx
 :doumi-redeem-vip-doumi
 (fn [{:keys [db]} [_ active]]
   (let [amount (vip-price active)]
     {:db         (-> db
                      (assoc-in [:doumi :vip :redeem :disable] true)
                      (assoc-in [:doumi :loading] true))
      :http-xhrio {:method          :post
                   :uri             (str "/p/" env "/stockinfogate/doumi/stockvip")
                   :body            (doto (js/FormData.)
                                      (.append "doumi" amount))
                   :format          (ajax/json-request-format)
                   :response-format (ajax/json-response-format {:keywords? true})
                   :on-success      [:doumi-redeem-vip-success]
                   :on-failure      [:xhrio-null-failed]}})))

(reg-event-db
 :doumi-redeem-vip-success
 doumi-interceptors
 (fn [db-doumi [_ {:keys [status data]}]]
   (let [{:keys [doumi result]} data]
     (-> db-doumi
         (assoc-in [:vip :redeem :result] result)
         (assoc-in [:doumi :doumi] doumi)
         (assoc-in [:vip :redeem :disable] false)
         (assoc-in [:vip :doumi :page] :success)
         (assoc :loading false)))))

(reg-event-fx
 :doumi-get-coupon
 (fn [{:keys [db]} _]
   {:http-xhrio {:method          :get
                 :uri             (str "/p/" env "/stockinfogate/doumi/promocard?page=0")
                 :format          (ajax/json-request-format)
                 :response-format (ajax/json-response-format {:keywords? true})
                 :on-success      [:doumi-get-coupon-success]
                 :on-failure      [:xhrio-null-failed]}
    :db         (-> db (assoc-in [:doumi :loading] true))}))

(reg-event-db
 :doumi-get-coupon-success
 doumi-interceptors
 (fn [doumi [_ {:keys [status data]}]]
   (let [{:keys [counts cards]} data]
     (-> doumi
         (assoc-in [:coupon :data] cards)
         (assoc :loading false)))))

(reg-event-db
 :doumi-vip-set-active-doumi
 doumi-interceptors
 (fn [doumi [_ t]]
   (assoc-in doumi [:vip :active :doumi] t)))

(reg-event-db
 :doumi-vip-set-active-rmb
 doumi-interceptors
 (fn [doumi [_ t]]
   (assoc-in doumi [:vip :active :rmb] t)))

(reg-event-db
 :doumi-style-set-pay-helper-qm-offsetleft
 doumi-interceptors
 (fn [doumi [_ offsetleft]]
   (assoc-in doumi [:style :pay-helper-qm :offsetLeft] offsetleft)))

(reg-event-db
 :doumi-style-set-pay-helper-qm-offsetwidth
 doumi-interceptors
 (fn [doumi [_ offsetwidth]]
   (assoc-in doumi [:style :pay-helper-qm :offsetWidth] offsetwidth)))

(reg-event-fx
 :doumi-verify-redeem-rmb-vip
 (fn [{:keys [db]} _]
   (if-let [phone (get-in db [:doumi :userinfo :phone])]
     {:dispatch [:doumi-get-rmb-vip-verify-result phone]}
     {:dispatch [:doumi-use-vip-set-show-phone true]})))

(reg-event-fx
 :doumi-get-rmb-vip-verify-result
 (fn [{:keys [db]} [_ phone]]
   {:db         (-> db (assoc-in [:doumi :loading] true))
    :http-xhrio {:method          :post
                 :uri             (str "/p/" env "/stockinfogate/user/recharge-verify")
                 :body            (doto (js/FormData.)
                                    (.append "phone" phone))
                 :format          (ajax/json-request-format)
                 :response-format (ajax/json-response-format {:keywords? true})
                 :on-success      [:doumi-rmb-vip-verify-response]
                 :on-failure      [:xhrio-null-failed]}}))

(reg-event-db
 :doumi-rmb-vip-verify-response
 doumi-interceptors
 (fn [doumi [_ {:keys [status data]}]]
   (cond-> doumi
     status (assoc-in [:redeem :result] (:result data))
     true   (assoc-in [:vip :rm :page] (if status :success :failed))
     true   (assoc :loading false))
   #_(-> (if status
           (-> doumi
               (assoc-in [:redeem :result] (:result data))
               (assoc-in [:vip :rmb :page] :success))
           (-> doumi
               (assoc-in [:vip :rmb :page] :failed)))
         (assoc :loading false))))

(reg-event-fx
 :doumi-phone-verify
 (fn [{:keys [db]} [_ phone]]
   {:db         (-> db (assoc-in [:doumi :loading] true))
    :http-xhrio {:method          :post
                 :uri             (str "/p/" env "/stockinfogate/user/phone/verifycode")
                 :body            (doto (js/FormData.)
                                    (.append "phone" phone))
                 :format          (ajax/json-request-format)
                 :response-format (ajax/json-response-format {:keywords? true})
                 :on-success      [:doumi-phone-verify-success]
                 :on-failure      [:xhrio-null-failed]}}))

(reg-event-fx
 :doumi-phone-verify-success
 (fn [{:keys [db]} [_ {:keys [status]}]]
   (-> {:db (-> (if status
                  (-> db
                      (assoc-in [:doumi :phone :codesent] true)
                      (assoc-in [:doumi :phone :countdown] 60))
                  (do
                    (js/alert "获取验证码失败，请检查手机号或稍后重试")
                    db))
                (assoc-in [:doumi :loading] false))}
       (merge
        (when status
          {:dispatch [:doumi-phone-verify-countdown]})))))

(reg-event-fx
 :doumi-phone-verify-countdown
 (fn [{:keys [db]} _]
   (when-not (zero? (get-in db [:doumi :phone :countdown]))
     {:db             (update-in db [:doumi :phone :countdown] dec)
      :dispatch-later [{:ms 1000 :dispatch [:doumi-phone-verify-countdown]}]})))

(reg-event-fx
 :doumi-update-user-phone
 (fn [{:keys [db]} [_ phone code]]
   {:http-xhrio {:method          :put
                 :uri             (str "/p/" env "/stockinfogate/user/phone")
                 :body            (doto (js/FormData.)
                                    (.append "phone" phone)
                                    (.append "code" code))
                 :format          (ajax/json-request-format)
                 :response-format (ajax/json-response-format {:keywords? true})
                 :on-success      [:doumi-update-user-phone-response]
                 :on-failure      [:xhrio-null-failed]}}))

(reg-event-fx
 :doumi-update-user-phone-response
 (fn [{:keys [db]} [_ result]]
   (let [{:keys [status result phone]} result]
     {:db       (if (and status result)
                  (-> db
                      (assoc-in [:doumi :userinfo :phone] phone)
                      (assoc-in [:doumi :phone :verified] true)
                      (assoc-in [:doumi :vip :rmb :show :phone] false))
                  (-> db
                      (assoc-in [:doumi :phone :verified] false)))
      :dispatch [:doumi-get-rmb-vip-verify-result phone]})))

(reg-event-fx
 :xhrio-null-success
 (fn [_ _]
   {}))

(reg-event-fx
 :xhrio-null-failed
 (fn [_ _]
   {}))

(reg-event-fx
 :doumi-recommend-user
 (fn [_ _]
   {:http-xhrio {:method          :post
                 :uri             (str "/p/" env "/jdweixin/reply/usercard")
                 :format          (ajax/json-request-format)
                 :response-format (ajax/json-response-format {:keywords? true})
                 :on-success      [:xhrio-null-success]
                 :on-failure      [:xhrio-null-failed]}}))

(reg-event-fx
 :doumi-find-bug
 (fn [_ _]
   {:http-xhrio {:method          :post
                 :uri             (str "/p/" env "/jdweixin/reply/debug")
                 :format          (ajax/json-request-format)
                 :response-format (ajax/json-response-format {:keywords? true})
                 :on-success      [:xhrio-null-success]
                 :on-failure      [:xhrio-null-failed]}}))

;; annc
(reg-event-db
 :set-annc-active-page
 (fn [db [_ page]]
   (assoc-in db [:annc :page] page)))

;; annc-annc
(reg-event-db
 :annc-annc-init
 annc-annc-interceptors
 (fn [annc _]
   (assoc annc :init false)))

(reg-event-fx
 :annc-set-display-type
 (fn [{:keys [db]} [_ t]]
   {:db (-> db
            (assoc-in [:annc :annc :display-type] t))
    ;;    :dispatch [:loader-set-show false]
    }))

(reg-event-fx
 :annc-toggle-display-type
 (fn [{:keys [db]} [_ t]]
   {:db (let [t (if (= :stock (get-in db [:annc :annc :display-type]))
                  :date
                  :stock)]
          (-> db
              (assoc-in [:annc :annc :display-type] t)))
    ;;    :dispatch [:loader-set-show false]
    }))

(reg-event-fx
 :annc-get-date-data
 (fn [{:keys [db]} _]
   (let [page (get-in db [:annc :annc :date :page])]
     {:db         (-> db
                      (assoc-in [:annc :annc :date :page] (inc page))
                      (assoc-in [:annc :annc :date :loading] true))
      :http-xhrio {:method          :get
                   :uri             (str "/p/" env "/stockinfogate/stock-announcement-list?page=" page)
                   :format          (ajax/json-request-format)
                   :response-format (ajax/json-response-format {:keywords? true})
                   :on-success      [:annc-get-date-data-success]
                   :on-failure      [:xhrio-null-failed]}})))

(reg-event-fx
 :annc-get-date-data-success
 (fn [{:keys [db]} [_ {:keys [status page data]}]]
   (->
    (if (empty? data)
      {:db       db
       :dispatch [:annc-get-date-data]}
      {:db (-> db
               (update-in [:annc :annc :date :data]
                          #(vec (sort-by :date > (concat % data))))
               (update-in [:annc :annc :read]
                          into (->> data
                                    (map :announcements)
                                    (apply concat)
                                    (filter :unread)
                                    (map :id))))})
    (update :db (fn [db]
                  (if (= page (get-in db [:annc :annc :date :page]))
                    identity
                    (-> db (assoc-in [:annc :annc :date :loading] false))))))))

(reg-event-fx
 :annc-get-stock-data
 (fn [{:keys [db]} _]
   {:http-xhrio {:method          :get
                 :uri             (str "/p/" env "/stockinfogate/stock-announcement-list-group-by-stock")
                 :format          (ajax/json-request-format)
                 :response-format (ajax/json-response-format {:keywords? true})
                 :on-success      [:annc-get-stock-data-success]
                 :on-failure      [:xhrio-null-failed]}
    :db         (-> db (assoc-in [:annc :annc :stock :loading] true))}))

(reg-event-db
 :annc-get-stock-data-success
 annc-interceptors
 (fn [annc [_ {:keys [status data]}]]
   (-> annc
       (assoc-in [:annc :stock :data] data)
       (update-in [:annc :read] into (->> data
                                          (map :announcements)
                                          (apply concat)
                                          (filter :unread)
                                          (map :id)))
       (assoc-in [:annc :stock :loading] false))))

(reg-event-db
 :annc-set-all-stockid
 annc-interceptors
 (fn [annc [_ stockid]]
   (-> annc (assoc-in [:annc :all :stockid] stockid))))

(reg-event-db
 :annc-set-all-stockname
 annc-interceptors
 (fn [annc [_ stockname]]
   (-> annc (assoc-in [:annc :all :stockname] stockname))))

(reg-event-fx
 :annc-show-all
 (fn [{:keys [db]} [_ stockid stockname]]
   (let [old-stockid (-> db (get-in [:annc :annc :all :stockid]))
         same?       (= stockid old-stockid)
         #_          (prn same?)]
     {:dispatch-n (list
                   [:annc-set-all-stockid stockid]
                   [:annc-set-all-stockname stockname]
                   [:annc-set-display-type :all]
                   [:annc-get-all-data same?])})))

(reg-event-fx
 :annc-get-all-data
 (fn [{:keys [db]} [_ same?]]
   (let [{:keys [page stockid]} (get-in db [:annc :annc :all])]
     {:db         (-> db (assoc-in [:annc :annc :all :loading] true))
      :http-xhrio {:method          :get
                   :uri             (str "/p/" env "/stockinfogate/stock-announcement/" stockid)
                   :params          {:page page}
                   :format          (ajax/json-request-format)
                   :response-format (ajax/json-response-format {:keywords? true})
                   :on-success      [:annc-get-all-data-success same?]
                   :on-failure      [:xhrio-null-failed]}})))

(reg-event-fx
 :annc-get-all-data-success
 (fn [{:keys [db]} [_ same? {:keys [status data]}]]
   {:db (-> (if-not same?
              (-> db
                  (assoc-in [:annc :annc :all :page] 1)
                  (assoc-in [:annc :annc :all :data] data))
              (-> db
                  (update-in db [:annc :annc :all :page] inc)
                  (update-in [:annc :annc :all :data] #(vec (sort-by :infopubldate > (concat % data))))
                  (update-in [:annc :annc :read] into (->> data
                                                           (filter :unread)
                                                           (map :id)))))
            (assoc-in [:annc :annc :all :loading] false))
    ;;    :dispatch-n (list [:loader-set-show false])

    }))

(reg-event-fx
 :annc-item-clicked
 (fn [annc [_ id unread]]
   (if unread
     {:dispatch [:annc-mark-read id]}
     {:dispatch [:annc-redirect-to id]})))

(reg-event-fx
 :annc-mark-read
 (fn [{:keys [db]} [_ id]]
   {:db         (-> db
                    (update-in [:annc :annc :read] conj id)
                    (assoc-in [:annc :annc :mark-read] false))
    :http-xhrio {:method          :post
                 :uri             (str "/p/" env "/stockinfogate/stock-announcement/mark-read")
                 :body            (doto (js/FormData.)
                                    (.append "annc_id" id))
                 :format          (ajax/json-request-format)
                 :response-format (ajax/json-response-format {:keywords? true})
                 :on-success      [:annc-mark-read-success id]
                 :on-failure      [:xhrio-null-failed]}}))

(reg-event-fx
 :annc-mark-read-success
 (fn [{:keys [db]} [_ id {:keys [status]}]]
   {:db       (assoc-in db [:annc :annc :mark-read] true)
    :dispatch [:annc-redirect-to id]}))

(reg-event-fx
 :annc-redirect-to
 (fn [_ [_ id]]
   (redirect-to (str "/pdf/" id ".html"))
   {}))

#_(reg-event-db
   :annc-performance-init
   annc-performance-interceptors
   (fn [performance _]
     (assoc performance :init false)))

(reg-event-db
 :annc-performance-set-orderby
 annc-performance-interceptors
 (fn [performance [_ orderby]]
   (assoc performance :orderby orderby)))

(reg-event-fx
 :annc-performance-get-data
 (fn [{:keys [db]} [_ orderby & [stockid]]]
   (if (zero? (get-in db [:annc :performance :date :lastcount]))
     {}
     (merge
      {:http-xhrio {:method          :get
                    :uri             (str "/p/" env "/stockinfogate/favornews/income")
                    :params          (merge
                                      {:orderby orderby}
                                      (case orderby
                                        :stockall {:stockid stockid}
                                        :date     (when-let [lastdate (get-in db [:annc :performance :date :lastdate])]
                                                    {:lastdate lastdate})
                                        nil))
                    :format          (ajax/json-request-format)
                    :response-format (ajax/json-response-format {:keywords? true})
                    :on-success      [:annc-performance-get-data-success orderby]
                    :on-failure      [:xhrio-null-failed]}
       :db         (-> db (assoc-in [:annc :performance orderby :loading] true))}))))

(reg-event-fx
 :annc-performance-get-data-success
 (fn [{:keys [db]} [_ orderby {:keys [status data]}]]
   (merge
    {:db
     (->
      (case orderby
        :date     (-> db
                      (update-in [:annc :performance :date :data] into data)
                      (assoc-in [:annc :performance :date :lastdate] (some-> data last :title))
                      (assoc-in [:annc :performance :date :lastcount] (count data)))
        :stock    (-> db
                      (assoc-in [:annc :performance :stock :data] data))
        :stockall (-> db
                      (assoc-in [:annc :performance :stockall :data] data)))
      (assoc-in [:annc :performance orderby :loading] false))})))

(reg-event-fx
 :dividend-get-data
 (fn [{:keys [db]} _]
   (if (zero? (get-in db [:annc :dividend :lastcount]))
     {}
     (merge
      {:http-xhrio (merge {:method          :get
                           :uri             (str "/p/" env "/stockinfogate/favornews/dividend")
                           :format          (ajax/json-request-format)
                           :response-format (ajax/json-response-format {:keywords? true})
                           :on-success      [:dividend-get-data-success]
                           :on-failure      [:xhrio-null-failed]}
                          (when-let [lastdate (get-in db [:annc :dividend :lastdate])]
                            {:params {:lastdate lastdate}}))
       :db         (-> db (assoc-in [:annc :dividend :loading] true))}))))

(reg-event-db
 :dividend-get-data-success
 annc-dividend-interceptors
 (fn [dividend [_ {:keys [status data]}]]
   (-> dividend
       (assoc :lastdate (:title (last data)))
       (update :data into data)
       (assoc :lastcount (count data))
       (assoc :loading false))))

(reg-event-fx
 :nextweek-get-data
 (fn [{:keys [db]} _]
   {:http-xhrio (merge {:method          :get
                        :uri             (str "/p/" env "/stockinfogate/favornews/nextweek")
                        :format          (ajax/json-request-format)
                        :response-format (ajax/json-response-format {:keywords? true})
                        :on-success      [:nextweek-get-data-success]
                        :on-failure      [:xhrio-null-failed]})
    :db         (-> db (assoc-in [:annc :nextweek :loading] true))}))

(reg-event-db
 :nextweek-get-data-success
 annc-nextweek-interceptors
 (fn [nextweek [_ {:keys [status data]}]]
   (-> nextweek
       (assoc :title (:title data)
              :data (:data data)
              :loading false))))

;;subscriptions
(reg-sub
 :page
 (fn [db _]
   (:page db)))

(reg-sub
 :sub-search
 (fn [db _]
   (:search db)))

(reg-sub
 :search
 (fn [query-v _]
   (subscribe [:sub-search]))
 (fn [search query-v _]
   search))

(reg-sub
 :sub-feedback
 (fn [db _]
   (:feedback db)))

(reg-sub
 :feedback
 (fn [query-v _]
   (subscribe [:sub-feedback]))
 (fn [feedback query-v _]
   feedback))

(reg-sub
 :sub-doumi
 (fn [db _]
   (:doumi db)))

(reg-sub
 :doumi
 (fn [query-v _]
   (subscribe [:sub-doumi]))
 (fn [doumi query-v _]
   doumi))

(reg-sub
 :sub-annc
 (fn [db _]
   (:annc db)))

(reg-sub
 :annc
 (fn [query-v _]
   (subscribe [:sub-annc]))
 (fn [annc query-v _]
   annc))

(reg-sub
 :sub-annc-annc
 (fn [db _]
   (get-in db [:annc :annc])))

(reg-sub
 :annc-annc
 (fn [query-v _]
   (subscribe [:sub-annc-annc]))
 (fn [annc query-v _]
   annc))

(reg-sub
 :sub-annc-performance
 (fn [db _]
   (get-in db [:annc :performance])))

(reg-sub
 :annc-performance
 (fn [query-v _]
   (subscribe [:sub-annc-performance]))
 (fn [performance query-v _]
   performance))

(reg-sub
 :sub-annc-dividend
 (fn [db _]
   (get-in db [:annc :dividend])))

(reg-sub
 :annc-dividend
 (fn [query-v _]
   (subscribe [:sub-annc-dividend]))
 (fn [dividend query-v _]
   dividend))

(reg-sub
 :sub-annc-nextweek
 (fn [db _]
   (get-in db [:annc :nextweek])))

(reg-sub
 :annc-nextweek
 (fn [query-v _]
   (subscribe [:sub-annc-nextweek]))
 (fn [nextweek query-v _]
   nextweek))

(reg-sub
 :search-show-suggestion
 :<- [:search]
 (fn [search _]
   (and (:show-suggestion search)
        (not (empty? (:suggestions search))))))

(reg-sub
 :search-suggestions
 :<- [:search]
 (fn [search _]
   (:suggestions search)))

(reg-sub
 :search-input-val
 :<- [:search]
 (fn [search _]
   (:input-val search)))

(reg-sub
 :search-favorstocks
 :<- [:search]
 (fn [search _]
   (:favorstocks search)))

(reg-sub
 :flash-tip
 (fn [db _]
   (:flash-tip db)))

(reg-sub
 :flash-tip-show
 :<- [:flash-tip]
 (fn [flash-tip _]
   (:show flash-tip)))

(reg-sub
 :flash-tip-kw
 :<- [:flash-tip]
 (fn [flash-tip _]
   (:kw flash-tip)))

(reg-sub
 :feedback-good
 :<- [:feedback]
 (fn [feedback _]
   (:good feedback)))

(reg-sub
 :feedback-bad
 :<- [:feedback]
 (fn [feedback _]
   (:bad feedback)))

(reg-sub
 :feedback-confusing
 :<- [:feedback]
 (fn [feedback _]
   (:confusing feedback)))

(reg-sub
 :feedback-current
 :<- [:feedback]
 (fn [feedback _]
   (:current feedback)))

(reg-sub
 :feedback-show-count
 :<- [:feedback]
 (fn [feedback _]
   (:show-count feedback)))

(reg-sub
 :feedback-change
 :<- [:feedback]
 (fn [feedback _]
   (:change feedback)))

(reg-sub
 :feedback-change-done
 :<- [:feedback]
 (fn [feedback -]
   (get-in feedback [:change :done])))

(reg-sub
 :doumi-page
 :<- [:doumi]
 (fn [doumi _]
   (:page doumi)))

(reg-sub
 :doumi-headimgurl
 :<- [:doumi]
 (fn [doumi _]
   (get-in doumi [:userinfo :headimgurl])))

(reg-sub
 :doumi-username
 :<- [:doumi]
 (fn [doumi _]
   (get-in doumi [:userinfo :username])))

(reg-sub
 :doumi-phone
 :<- [:doumi]
 (fn [doumi _]
   (get-in doumi [:userinfo :phone])))

(reg-sub
 :doumi-doumi
 :<- [:doumi]
 (fn [doumi _]
   (get-in doumi [:doumi :doumi])))

(reg-sub
 :doumi-log
 :<- [:doumi]
 (fn [doumi _]
   (get-in doumi [:doumi :log])))

(reg-sub
 :doumi-use-page
 :<- [:doumi]
 (fn [doumi _]
   (get-in doumi [:use :page])))

(reg-sub
 :doumi-coupon-active
 :<- [:doumi]
 (fn [doumi _]
   (get-in doumi [:coupon :active])))

(reg-sub
 :doumi-coupon-redeem-amount
 :<- [:doumi]
 (fn [doumi _]
   (get-in doumi [:coupon :redeem :amount])))

(reg-sub ;; youzan url
 :doumi-coupon-redeem-result
 :<- [:doumi]
 (fn [doumi _]
   (get-in doumi [:coupon :redeem :result])))

(reg-sub
 :doumi-coupon-redeem-disable
 :<- [:doumi]
 (fn [doumi _]
   (get-in doumi [:coupon :redeem :disable])))

(reg-sub
 :doumi-use-coupon-page
 :<- [:doumi]
 (fn [doumi _]
   (get-in doumi [:coupon :page])))

(reg-sub
 :doumi-use-coupon-custom-valid
 :<- [:doumi]
 (fn [doumi _]
   (get-in doumi [:coupon :custom :valid])))

(reg-sub
 :doumi-use-coupon-custom-value
 :<- [:doumi]
 (fn [doumi _]
   (get-in doumi [:coupon :custom :value])))

(reg-sub
 :doumi-use-vip-page
 :<- [:doumi]
 (fn [doumi _]
   (get-in doumi [:vip :page])))

(reg-sub
 :doumi-use-vip-doumi-page
 :<- [:doumi]
 (fn [doumi _]
   (get-in doumi [:vip :doumi :page])))

(reg-sub
 :doumi-use-vip-rmb-page
 :<- [:doumi]
 (fn [doumi _]
   (get-in doumi [:vip :rmb :page])))

(reg-sub
 :doumi-use-vip-rmb-show-help
 :<- [:doumi]
 (fn [doumi _]
   (get-in doumi [:vip :rmb :show :help])))

(reg-sub
 :doumi-use-vip-rmb-show-phone
 :<- [:doumi]
 (fn [doumi _]
   (get-in doumi [:vip :rmb :show :phone])))

(reg-sub
 :doumi-use-vip-redeem-valid-date
 :<- [:doumi]
 (fn [doumi _]
   (get-in doumi [:vip :redeem :result])))

(reg-sub
 :doumi-coupon
 :<- [:doumi]
 (fn [doumi _]
   (get-in doumi [:coupon :data])))

(reg-sub
 :doumi-vip-active-doumi
 :<- [:doumi]
 (fn [doumi _]
   (get-in doumi [:vip :active :doumi])))

(reg-sub
 :doumi-vip-active-rmb
 :<- [:doumi]
 (fn [doumi _]
   (get-in doumi [:vip :active :rmb])))

(reg-sub
 :doumi-style-pay-helper-qm-offsetleft
 :<- [:doumi]
 (fn [doumi _]
   (get-in doumi [:style :pay-helper-qm :offsetLeft])))

(reg-sub
 :doumi-style-pay-helper-qm-offsetwidth
 :<- [:doumi]
 (fn [doumi _]
   (get-in doumi [:style :pay-helper-qm :offsetWidth])))

(reg-sub
 :doumi-vip-redeem-disable
 :<- [:doumi]
 (fn [doumi _]
   (get-in doumi [:vip :redeem :disable])))

(reg-sub
 :doumi-phone-verified
 :<- [:doumi]
 (fn [doumi _]
   (get-in doumi [:phone :verified])))

(reg-sub
 :doumi-phone-countdown
 :<- [:doumi]
 (fn [doumi _]
   (get-in doumi [:phone :countdown])))

(reg-sub
 :doumi-loading
 :<- [:doumi]
 (fn [doumi _]
   (:loading doumi)))

;; annc
(reg-sub
 :annc-active-page
 :<- [:annc]
 (fn [annc _]
   (:page annc)))

;; annc-annc
(reg-sub
 :annc-annc-init
 :<- [:annc-annc]
 (fn [annc _]
   (:init annc)))

(reg-sub
 :annc-display-type
 :<- [:annc-annc]
 (fn [annc _]
   (:display-type annc)))

(reg-sub
 :annc-date-data
 :<- [:annc-annc]
 (fn [annc _]
   (get-in annc [:date :data])))

(reg-sub
 :annc-date-loading
 :<- [:annc-annc]
 (fn [annc _]
   (get-in annc [:date :loading])))

(reg-sub
 :annc-stock-data
 :<- [:annc-annc]
 (fn [annc _]
   (get-in annc [:stock :data])))

(reg-sub
 :annc-stock-loading
 :<- [:annc-annc]
 (fn [annc _]
   (get-in annc [:stock :loading])))

(reg-sub
 :annc-all-data
 :<- [:annc-annc]
 (fn [annc _]
   (get-in annc [:all :data])))

(reg-sub
 :annc-all-loading
 :<- [:annc-annc]
 (fn [annc _]
   (get-in annc [:all :loading])))

(reg-sub
 :annc-all-stockname
 :<- [:annc-annc]
 (fn [annc _]
   (get-in annc [:all :stockname])))

(reg-sub
 :annc-all-stockid
 :<- [:annc-annc]
 (fn [annc _]
   (get-in annc [:all :stockid])))

(reg-sub
 :annc-all-favorstock?
 (fn [db _]
   (contains? (get-in db [:search :favorstocks])
              (get-in db [:annc :annc :all :stockid]))))

;; annc-performance
#_(reg-sub
   :annc-performance-init
   :<- [:annc-performance]
   (fn [performance _]
     (:init performance)))

(reg-sub
 :annc-performance-orderby
 :<- [:annc-performance]
 (fn [performance _]
   (:orderby performance)))

;; (reg-sub
;;  :annc-performance-date-lastdate
;;  :<- [:annc-performance]
;;  (fn [performance _]
;;    (get-in performance [:date :lastdate])))

(reg-sub
 :annc-performance-date-data
 :<- [:annc-performance]
 (fn [performance _]
   (get-in performance [:date :data])))

(reg-sub
 :annc-performance-date-lastcount
 :<- [:annc-performance]
 (fn [performance _]
   (get-in performance [:date :lastcount])))

(reg-sub
 :annc-performance-date-loading
 :<- [:annc-performance]
 (fn [performance _]
   (get-in performance [:date :loading])))

(reg-sub
 :annc-performance-stock-data
 :<- [:annc-performance]
 (fn [performance _]
   (get-in performance [:stock :data])))

(reg-sub
 :annc-performance-stock-loading
 :<- [:annc-performance]
 (fn [performance _]
   (get-in performance [:stock :loading])))

#_(reg-sub
   :annc-performance-stockall-stockid
   :<- [:annc-performance]
   (fn [performance _]
     (get-in performance [:stockall :data :stockid])))

(reg-sub
 :annc-performance-stockall-data
 :<- [:annc-performance]
 (fn [performance _]
   (get-in performance [:stockall :data])))

(reg-sub
 :annc-performance-stockall-loading
 :<- [:annc-performance]
 (fn [performance _]
   (get-in performance [:stockall :loading])))
;; end annc-performance

;; annc-dividend
(reg-sub
 :dividend-data
 :<- [:annc-dividend]
 (fn [dividend _]
   (:data dividend)))

(reg-sub
 :dividend-lastcount
 :<- [:annc-dividend]
 (fn [dividend _]
   (:lastcount dividend)))

(reg-sub
 :dividend-loading
 :<- [:annc-dividend]
 (fn [dividend _]
   (:loading dividend)))
;; end annc-dividend

;; annc-nextweek
(reg-sub
 :nextweek-title
 :<- [:annc-nextweek]
 (fn [nextweek _]
   (:title nextweek)))

(reg-sub
 :nextweek-data
 :<- [:annc-nextweek]
 (fn [nextweek _]
   (:data nextweek)))

(reg-sub
 :nextweek-loading
 :<- [:annc-nextweek]
 (fn [nextweek _]
   (:loading nextweek)))
;; end annc-nextweek

(reg-sub
 :annc-read
 :<- [:annc-annc]
 (fn [annc _]
   (get annc :read)))

#_(reg-sub
   :loader-show
   (fn [db _]
     (get-in db [:loader :show])))

;; secuinfo

;; handlers
(reg-event-db
 :secuinfo-swiper-set-default
 secuinfo-interceptors
 (fn [secuinfo _]
   secuinfo ;; TODO
   ))

(reg-event-db
 :secuinfo-minus-one-done
 secuinfo-interceptors
 (fn [secuinfo _]
   secuinfo
   )
 )

(reg-event-db
 :secuinfo-set-stockid-stockname
 secuinfo-interceptors
 (fn [secuinfo [_ stockid stockname]]
   (assoc secuinfo
          :stockid stockid
          :stockname stockname)))

(reg-event-db
 :secuinfo-set-active-page
 secuinfo-interceptors
 (fn [secuinfo [_ jump]]
   secuinfo ;; TODO
   ))

(reg-event-fx
 :secuinfo-clear-get-realtime-info-timer
 (fn [_ _]
   {:clear-timer {:id "secuinfo-get-realtime-info-timer"}}))

(reg-event-fx
 :secuinfo-get-realtime-info-timer
 (fn [_ _]
   {:set-timer {:id       "secuinfo-get-realtime-info-timer"
                :interval 6000
                :event    [:secuinfo-get-realtime-info]}}))

(reg-event-fx
 :secuinfo-get-realtime-info
 (fn [{:keys [db]} _]
   (if-let [stockid (get-in db [:secuinfo :stockid])]
     (let [fenshi (get-in db [:secuinfo :data :fenshi])]
       (if (not= fenshi-num (count fenshi))
         {:http-xhrio {:method          :get
                       :uri             (str "/p/" env "/stockinfogate/stock/realtimeinfo")
                       :params          {:stockids stockid}
                       :format          (ajax/json-request-format)
                       :response-format (ajax/json-response-format {:keywords? true})
                       :on-success      [:secuinfo-get-realtime-info-success stockid]
                       :on-failure      [:xhrio-null-failed]}}
         {}))
     {})))

(reg-event-fx
 :secuinfo-get-realtime-info-success
 (fn [{:keys [db]} [_ stockid {:keys [status data]}]]
   (if (and status (= stockid (get-in db [:secuinfo :stockid])))
     {:db       (assoc-in db [:secuinfo :data :realtime] (first data))
      :dispatch [:secuinfo-on-realtime]}
     {})))

(reg-event-fx
 :secuinfo-clear-get-fenshi-info-timer
 (fn [_ _]
   {:clear-timer {:id "secuinfo-get-fenshi-info-timer"}}))

(reg-event-fx
 :secuinfo-get-fenshi-info-timer
 (fn [_ _]
   {:set-timer {:id       "secuinfo-get-fenshi-info-timer"
                :interval 6000
                :event    [:secuinfo-get-fenshi-info]}}))

(reg-event-fx
 :secuinfo-get-fenshi-info
 (fn [{:keys [db]} _]
   (if-let [stockid (get-in db [:secuinfo :stockid])]
     (let [fenshi (get-in db [:secuinfo :data :fenshi])]
       (if (not= fenshi-num (count fenshi))
         {:http-xhrio {:method          :get
                       :uri             (str "/p/" env "/stockinfogate/stock/fenshitu")
                       :params          {:stockid stockid}
                       :format          (ajax/json-request-format)
                       :response-format (ajax/json-response-format {:keywords? true})
                       :on-success      [:secuinfo-get-fenshi-info-success stockid]
                       :on-failure      [:xhrio-null-failed]}}
         {}))
     {})))

(reg-event-db
 :secuinfo-get-fenshi-info-success
 secuinfo-interceptors
 (fn [secuinfo [_ stockid {:keys [status data]}]]
   (cond-> secuinfo
     (and status
          (= stockid (:stockid secuinfo))) (assoc-in [:data :fenshi] data))))

(reg-event-fx
 :secuinfo-get-k-day-info
 (fn [{:keys [db]} _]
   (if-let [stockid (get-in db [:secuinfo :stockid])]
     {:http-xhrio {:method          :get
                   :uri             (str "/p/" env "/stockinfogate/stock/klinedata")
                   :params          {:stockid stockid :period "D"}
                   :format          (ajax/json-request-format)
                   :response-format (ajax/json-response-format {:keywords? true})
                   :on-success      [:secuinfo-get-k-day-info-success]
                   :on-failure      [:xhrio-null-failed]}}
     {})))

(reg-event-db
 :secuinfo-get-k-day-info-success
 secuinfo-interceptors
 (fn [secuinfo [_ {:keys [status data]}]]
   (cond-> secuinfo
     status (assoc-in [:data :k :day] data))))

(reg-event-fx
 :secuinfo-get-k-week-info
 (fn [{:keys [db]} _]
   (if-let [stockid (get-in db [:secuinfo :stockid])]
     {:http-xhrio {:method          :get
                   :uri             (str "/p/" env "/stockinfogate/stock/klinedata")
                   :params          {:stockid stockid :period "W"}
                   :format          (ajax/json-request-format)
                   :response-format (ajax/json-response-format {:keywords? true})
                   :on-success      [:secuinfo-get-k-week-info-success]
                   :on-failure      [:xhrio-null-failed]}})))

(reg-event-db
 :secuinfo-get-k-week-info-success
 secuinfo-interceptors
 (fn [secuinfo [_ {:keys [status data]}]]
   (cond-> secuinfo
     status (assoc-in [:data :k :week] data))))

(reg-event-fx
 :secuinfo-get-k-month-info
 (fn [{:keys [db]} _]
   (if-let [stockid (get-in db [:secuinfo :stockid])]
     {:http-xhrio {:method          :get
                   :uri             (str "/p/" env "/stockinfogate/stock/klinedata")
                   :params          {:stockid stockid :period "M"}
                   :format          (ajax/json-request-format)
                   :response-format (ajax/json-response-format)
                   :on-success      [:secuinfo-get-k-month-info-success]
                   :on-failure      [:xhrio-null-failed]}})))

(reg-event-db
 :secuinfo-get-k-month-info-success
 secuinfo-interceptors
 (fn [secuinfo [_ {:keys [status data]}]]
   (cond-> secuinfo
     status (assoc-in [:data :k :month] data))))


(reg-event-fx
 :secuinfo-get-summary-info
 (fn [{:keys [db]} _]
   (if-let [stockid (get-in db [:secuinfo :stockid])]
     {:http-xhrio {:method          :get
                   :uri             (str "/p/" env "/stockinfogate/commonapi")
                   :params          {:name "summary" :secucode stockid}
                   :format          (ajax/json-request-format)
                   :response-format (ajax/json-response-format {:keywords? true})
                   :on-success      [:secuinfo-get-summary-info-success]
                   :on-failure      [:xhrio-null-failed]}}
     {})))

(reg-event-fx
 :secuinfo-get-summary-info-success
 (fn [{:keys [db]} [_ {:keys [status data]}]]
   (if status
     {:dispatch [:secuinfo-on-summary]
      :db       (assoc-in db [:secuinfo :data :summary] data)}
     {})))

(reg-event-fx
 :secuinfo-get-divirate-info
 (fn [{:keys [db]} _]
   (if-let [stockid (get-in db [:secuinfo :stockid])]
     {:http-xhrio {:method          :get
                   :uri             (str "/p/" env "/stockinfogate/commonapi")
                   :params          {:name "divirate_history" :secucode stockid}
                   :format          (ajax/json-request-format)
                   :response-format (ajax/json-response-format {:keywords? true})
                   :on-success      [:secuinfo-get-divirate-info-success]
                   :on-failure      [:xhrio-null-failed]}}
     {})))

;; d3 version
#_(reg-event-fx
   :secuinfo-get-divirate-info-success
   (fn [{:keys [db]} [_ {:keys [status data]}]]
     (if status
       {;;:dispatch [:secuinfo-on-divirate]
        :db (assoc-in db [:secuinfo :data :divirate]
                      (map #(-> %
                                (update :day parse-time)
                                (update :divirate (comp (partial * 0.01) js/parseFloat))) data))}
       {})))

(reg-event-fx
 :secuinfo-get-divirate-info-success
 (fn [{:keys [db]} [_ {:keys [status data]}]]
   (if status
     {:db (assoc-in db [:secuinfo :data :divirate] data)}
     {})))

#_(reg-event-db
   :secuinfo-on-divirate
   secuinfo-interceptors
   (fn [secuinfo _]
     secuinfo))


(reg-event-fx
 :secuinfo-get-unlock-info
 (fn [{:keys [db]} _]
   (if-let [stockid (get-in db [:secuinfo :stockid])]
     {:http-xhrio {:method          :get
                   :uri             (str "/p/" env "/stockinfogate/stock/unlockinfo/m/" stockid)
                   :format          (ajax/json-request-format)
                   :response-format (ajax/json-response-format {:keywords? true})
                   :on-success      [:secuinfo-get-unlock-info-success]
                   :on-failure      [:xhrio-null-failed]}}
     {})))

(reg-event-fx
 :secuinfo-get-unlock-info-success
 (fn [{:keys [db]} [_ {:keys [status data]}]]
   (if status
     {:db       (assoc-in db [:secuinfo :data :unlock] data)
      :dispatch [:secuinfo-on-unlock]}
     {})))

(reg-event-fx
 :secuinfo-get-tech-info
 (fn [{:keys [db]} _]
   (if-let [stockid (get-in db [:secuinfo :stockid])]
     {:http-xhrio {:method          :get
                   :uri             (str "/p/" env "/stockinfogate/stock/tech-info/" stockid)
                   :format          (ajax/json-request-format)
                   :response-format (ajax/json-response-format {:keywords? true})
                   :on-success      [:secuinfo-get-tech-info-success]
                   :on-failure      [:xhrio-null-failed]
                   }}
     {})))

(reg-event-db
 :secuinfo-get-tech-info-success
 secuinfo-interceptors
 (fn [secuinfo [_ {:keys [status data]}]]
   (cond-> secuinfo
     status (assoc-in [:data :tech] (:tech data)))))

(reg-event-fx
 :secuinfo-get-user-info
 (fn [{:keys [db]} _]
   {:http-xhrio {:method          :get
                 :uri             (proxy-uri "/stockinfogate/get-userinfo")
                 :format          (ajax/json-request-format)
                 :response-format (ajax/json-response-format {:keywords? true})
                 :on-success      [:secuinfo-get-user-info-success]
                 :on-failure      [:xhrio-null-failed]
                 }}))

(reg-event-db
 :secuinfo-get-user-info-success
 secuinfo-interceptors
 (fn [secuinfo [_ {:keys [status data]}]]
   (cond-> secuinfo
     status (assoc-in [:data :userinfo] data))))

(reg-event-fx
 :secuinfo-get-scores-info
 (fn [{:keys [db]} _]
   (if-let [stockid (get-in db [:secuinfo :stockid])]
     {:http-xhrio {:method          :get
                   :uri             (str "/p/" env "/stockinfogate/user/stock/scores/" stockid)
                   :format          (ajax/json-request-format)
                   :response-format (ajax/json-response-format {:keywords? true})
                   :on-success      [:secuinfo-get-scores-info-success]
                   :on-failure      [:xhrio-null-failed]}}
     {})))

(reg-event-db
 :secuinfo-get-scores-info-success
 secuinfo-interceptors
 (fn [secuinfo [_ {:keys [status data]}]]
   (cond-> secuinfo
     status (assoc-in [:data :scores] data))))

(reg-event-fx
 :secuinfo-get-annc-info
 (fn [{:keys [db]} _]
   (if-let [stockid (get-in db [:secuinfo :stockid])]
     {:http-xhrio {:method          :get
                   :uri             (str "/p/" env "/stockinfogate/stock/" stockid "/announcements")
                   :format          (ajax/json-request-format)
                   :response-format (ajax/json-response-format {:keywords? true})
                   :on-success      [:secuinfo-get-annc-info-success]
                   :on-failure      [:xhrio-null-failed]
                   }}
     {})))

(reg-event-db
 :secuinfo-get-annc-info-success
 secuinfo-interceptors
 (fn [secuinfo [_ {:keys [status data]}]]
   (cond-> secuinfo
     status (assoc-in [:data :annc] data))))

(reg-event-fx
 :secuinfo-get-rules-info
 (fn [{:keys [db]} _]
   (if-let [stockid (get-in db [:secuinfo :stockid])]
     {:http-xhrio {:method          :get
                   :uri             (str "/p/" env "/stockinfogate/user/favorstock/has-user-defined-rule")
                   :format          (ajax/json-request-format)
                   :response-format (ajax/json-response-format {:keywords? true})
                   :on-success      [:secuinfo-get-rules-info-success]
                   :on-failure      [:xhrio-null-failed]}}
     {})))

(reg-event-db
 :secuinfo-get-rules-info-success
 secuinfo-interceptors
 (fn [secuinfo [_ {:keys [status data]}]]
   (cond-> secuinfo
     status (assoc-in [:data :rules] data))))

(defonce timers (atom {}))

(reg-fx
 :set-timer
 (fn [{:keys [id interval event]}]
   (when-not (get @timers id)
     (swap! timers assoc id (js/setInterval #(dispatch event) interval)))))

;; name "headline-swiper-timer"
;; inverval 2000
;; event [:secuinfo-headline-update-index]

(reg-fx
 :clear-timer
 (fn [{:keys [id]}]
   (js/clearInterval (get @timers id))
   (swap! timers dissoc id)))

(reg-event-fx
 :secuinfo-set-headline-swiper-timer
 (fn [_ _]
   {:set-timer {:id       "secuinfo-headline-swiper-timer"
                :interval 2000
                :event    [:secuinfo-headline-update-index]}}))

(reg-event-fx
 :secuinfo-clear-headline-swiper-timer
 (fn [_ _]
   {:clear-timer {:id "secuinfo-headline-swiper-timer"}}))

(reg-event-fx
 :secuinfo-set-active-noodle
 (fn [{:keys [db]} [_ active]]
   {:db       (cond-> db
                true                    (assoc-in [:secuinfo :noodle :active] active)
                ;;                  true                 (assoc-in [:secuinfo :secondary :tech :active] nil)
                true                    (assoc-in [:secuinfo :headline :cate]
                                                  ({:tech        :realtime
                                                    :fundamental :fundamental} active))
                true                    (assoc-in [:secuinfo :headline :index] 0)
                true                    (assoc-in [:secuinfo :chart :active] :3year)
                true                    (assoc-in [:secuinfo :chart :interval] :season)
                (= :tech active)        (assoc-in [:secuinfo :secondary :tech :active] nil)
                (= :tech active)        (assoc-in [:secuinfo :chart :cate] :k)
                (= :tech active)        (assoc-in [:secuinfo :chart :ktype]
                                                  (if (-> db
                                                          (get-in [:secuinfo :data :fenshi])
                                                          count
                                                          (= fenshi-num))
                                                    :day
                                                    :fenshi))
                (= :fundamental active) (assoc-in [:secuinfo :secondary :fundamental :active] nil)
                (= :fundamental active) (assoc-in [:secuinfo :chart :cate] :pe)
                (= :capital active)     (assoc-in [:secuinfo :secondary :capital :active] nil)
                (= :msg active)         (assoc-in [:secuinfo :secondary :msg :active] nil))
    :dispatch [:secuinfo-clear-headline-swiper-timer]}))

(reg-event-fx
 :secuinfo-set-tech-active
 (fn [{:keys [db]} [_ active]]
   {:db         (-> db
                    (assoc-in [:secuinfo :secondary :tech :active] active)
                    (assoc-in [:secuinfo :headline :cate] active)
                    (assoc-in [:secuinfo :headline :index] 0))
    :dispatch-n (list [:secuinfo-clear-headline-swiper-timer]
                      [:secuinfo-set-headline-swiper-timer])}))

(reg-event-fx
 :secuinfo-set-fundamental-active
 (fn [{:keys [db]} [_ active]]
   {:db         (-> db
                    (assoc-in [:secuinfo :secondary :fundamental :active] active)
                    (assoc-in [:secuinfo :headline :cate] :fundamental)
                    (assoc-in [:secuinfo :headline :index]
                              (let [index (.indexOf [:pe :pb :ps :rgr :egr] active)]
                                (if (= -1 index) 0 index)))
                    (assoc-in [:secuinfo :chart :active] :3year)
                    (assoc-in [:secuinfo :chart :interval] :season))
    :dispatch-n (list [:secuinfo-clear-headline-swiper-timer]
                      [:secuinfo-secondary-set-chart active])}))

(reg-event-fx
 :secuinfo-set-capital-active
 (fn [{:keys [db]} [_ active]]
   {:db         (-> db
                    (assoc-in [:secuinfo :secondary :capital :active] active)
                    (assoc-in [:secuinfo :headline :cate] (when (= active :dividend) :dividend))
                    (assoc-in [:secuinfo :headline :index] 0)
                    (assoc-in [:secuinfo :chart :active] :3year)
                    (assoc-in [:secuinfo :chart :interval] :season))
    :dispatch-n (concat (list [:secuinfo-clear-headline-swiper-timer])
                        (when (= :divirate active)
                          (list [:secuinfo-secondary-set-chart])))}))

(reg-event-fx
 :secuinfo-set-msg-active
 (fn [{:keys [db]} [_ active]]
   {:db       (-> db
                  (assoc-in [:secuinfo :secondary :msg :active] active)
                  (assoc-in [:secuinfo :headline :cate] nil)
                  (assoc-in [:secuinfo :headline :index] 0))
    :dispatch [:secuinfo-clear-headline-swiper-timer]}))

(reg-event-db
 :secuinfo-headline-set-current-index
 secuinfo-interceptors
 (fn [secuinfo [_ index]]
   (assoc-in secuinfo [:headline :index] index)))

(reg-event-db
 :secuinfo-headline-update-index
 secuinfo-interceptors
 (fn [secuinfo [_ index]]
   (update-in secuinfo [:headline :index] inc)))

(reg-event-db
 :secuinfo-headline-set-cate
 secuinfo-interceptors
 (fn [secuinfo [_ cate]]
   (assoc-in secuinfo [:headline :cate] cate)))

(reg-event-fx
 :secuinfo-chart-set-active
 (fn [{:keys [db]} [_ active]]
   {:db       (-> db
                  (assoc-in [:secuinfo :chart :active] active))
    ;; if not using echarts, should use :secuinfo-update-chart-data
    :dispatch [:secuinfo-update-chart-option]}))


#_(reg-event-db
 :indexinfo-update-chart-data-by-active
 indexinfo-interceptors
 (fn [indexinfo [_ active]]
   (let [{:keys [cate active ktype]} (:chart indexinfo)
         data (get-in indexinfo [:data :line])]
     (-> indexinfo
         (assoc-in [:chart :use-data :line] (case active
                                              :all data
                                              (indexinfo-cut-data data active)))))))

(reg-event-db
 :indexinfo-chart-set-active
 indexinfo-interceptors
 (fn [indexinfo [_ active]]
   (-> indexinfo
       (assoc-in [:chart :active] active))))

(reg-event-fx
 :indexinfo-chart-set-ktype
 (fn [{:keys [db]} [_ ktype]]
   #_(prn "indexinfo-chart set ktype")
   #_(add-trace " :indexinfo-chart-set-ktype")
   (merge
    {:db       (-> db (assoc-in [:indexinfo :chart :ktype] ktype))}
    (when (not= :fenshi ktype)
      {:dispatch-n (list [:indexinfo-update-chart-data-by-ktype ktype]
                         [:indexinfo-datazoom-clear-start-end])}))))

(reg-event-fx
 :secuinfo-on-realtime
 (fn [{:keys [db]} _]
   {:db       (-> db
                  (assoc :secuinfo
                         (let [secuinfo       (get-in db [:secuinfo])
                               rt-origin      (get-in secuinfo [:data :realtime :origin])
                               summary        (get-in secuinfo [:data :summary])
                               state          (:state summary)
                               price          (or (:newprice rt-origin)
                                                  (:lastclose rt-origin))
                               total-shares   (get-in state [:shares :totalshares])
                               mv             (when price (some-> total-shares (* price)))
                               ni-ltm         (get-in state [:ni-ltm :v])
                               nr-ltm         (get-in state [:nr-ltm :v])
                               asset          (get-in state [:asset :v])
                               dividend-total (get-in state [:dividend-total])
                               no-fore        (get-in summary [:fundamental :no-forecasts])
                               nr             (:nr no-fore)
                               ni             (:ni no-fore)]
                           (-> secuinfo
                               (assoc-in [:values :fundamental]
                                         {:pe  {:value      (some-> mv (/ ni-ltm))
                                                :gt_history (some-> no-fore :pe :gt_history)}
                                          :pb  {:value      (some-> mv (/ asset))
                                                :gt_history (some-> no-fore :pb :gt_history)}
                                          :ps  {:value (some-> mv (/ nr-ltm))}
                                          :egr {:year   (some-> ni :quarter (subs 0 4))
                                                :season (some-> ni :quarter (subs 4 6) (quot 3))
                                                :value  (some-> ni :val (/ 1E8))
                                                :rate   (some-> ni :quarter_rate)}
                                          :rgr {:year   (some-> nr :quarter (subs 0 4))
                                                :season (some-> nr :quarter (subs 4 6) (quot 3))
                                                :value  (some-> nr :val (/ 1E8))
                                                :rate   (some-> nr :quarter_rate)}
                                          })
                               (assoc-in [:values :capital :dividend] (some->> mv (/ dividend-total) (* 100)))))))
    :dispatch [:secuinfo-update-chart-option]}))

(reg-event-fx
 :secuinfo-on-summary
 (fn [{:keys [db]} _]
   (let [history    (get-in db [:secuinfo :data :summary :finance :dividend :history])
         song-zhuan (->> history
                         (filter #(in-one-year? (:exdividate %)))
                         (map :title)
                         (map #(re-seq #"(\d+)(送([\d\.]+))?(转([\d\.]+))?(派([\d\.]+))?" %))
                         (map (fn [[[_ _ _ song _ zhuan _ pai]]]
                                (when (or song zhuan)
                                  (+ (if song (js/parseFloat song) 0)
                                     (if zhuan (js/parseFloat zhuan) 0)))))
                         (filter identity)
                         first)]
     {:db       (assoc-in db [:secuinfo :values :capital :bonus]
                          (if song-zhuan (str "10送转" song-zhuan) "--"))
      :dispatch [:secuinfo-on-realtime]})))

(reg-event-db
 :secuinfo-on-unlock
 secuinfo-interceptors
 (fn [secuinfo _]
   (-> secuinfo
       (assoc-in [:values :capital :unlock]
                 (some-> (get-in secuinfo [:data :unlock :half_year_newmarketable_ratio])
                         (* 100))))))

(reg-event-fx
 :secuinfo-secondary-set-chart
 (fn [{:keys [db]} [_ cate]]
   {:db       (-> db
                  (assoc-in [:secuinfo :chart :active] :3year)
                  (assoc-in [:secuinfo :chart :cate] cate))
    :dispatch [:secuinfo-update-chart-option]}))

(reg-event-db
 :secuinfo-update-chart-option
 secuinfo-interceptors
 (fn [secuinfo _]
   (let [{:keys [cate active interval ktype option]} (:chart secuinfo)]
     (-> secuinfo
         (assoc-in [:chart :option cate]
                   (case cate
                     :fenshi   (let [fenshi   (get-in secuinfo [:data :fenshi])
                                     realtime (:realtime secuinfo)]
                                 (cond-> (:fenshi option)
                                   (and (not (empty? fenshi))
                                        (not (empty? realtime))) ((fn [old-option]
                                                                    (let [use-data fenshi
                                                                          trading? (not (zero?
                                                                                         (:tradestatus realtime)))]
                                                                      {:line (get-fenshi-option trading? use-data)
                                                                       :bar  (get-fenshi-bar-option trading? use-data)})))))
                     :k        (let [k-data     (get-in secuinfo [:data :k])
                                     realtime   (get-in secuinfo [:data :realtime])
                                     fuquantype (get-in secuinfo [:chart :fuquantype])]
                                 (cond-> (:k option)
                                   (and (not (empty? realtime))
                                        (not (empty? k-data))
                                        fuquantype) ((fn [old-option]
                                                       (let [use-data  (update-k-data-with-realtime
                                                                        ktype fuquantype k-data realtime)
                                                             axis-data (get-k-x-axis-data ktype (:k use-data))]
                                                         {:use-data    use-data
                                                          :cancelstick (get-k-option use-data axis-data)
                                                          :bar         (get-k-bar-option use-data axis-data)})))))
                     :pe       {}
                     :pb       {}
                     :ps       {}
                     :egr      {:line {}
                                :bar  {}}
                     :rgr      {:line {}
                                :bar  {}}
                     :divirate (let [divirate (get-in secuinfo [:data :divirate])]
                                 (cond-> (:divirate option)
                                   (not (empty? divirate)) ((fn [old-option]
                                                              (let [#_       (.log js/console "divirate" (first divirate))
                                                                    use-data (case active
                                                                               :1year (->> divirate (filter #(yyyymmdd-in1year? (:day %))))
                                                                               :3year (->> divirate (filter #(yyyymmdd-in3year? (:day %))))
                                                                               :5year (->> divirate (filter #(yyyymmdd-in5year? (:day %))))
                                                                               :all   divirate)]
                                                                #_(.log js/console "use-data" use-data)
                                                                #_(.log js/console "option" (get-divirate-option use-data))
                                                                (secuinfo-get-divirate-option use-data))))))))))))

;; indexinfo
(reg-event-db
 :indexinfo-set-id-name
 indexinfo-interceptors
 (fn [indexinfo [_ id name]]
   (assoc indexinfo :id id :name name)))

(reg-event-fx
 :indexinfo-clear-get-realtime-timer
 (fn [_ _]
   {:clear-timer {:id "indexinfo-get-realtime-timer"}}))

(reg-event-fx
 :indexinfo-get-realtime-timer
 (fn [_ _]
   {:set-timer {:id       "indexinfo-get-realtime-timer"
                :interval 6000
                :event    [:indexinfo-get-realtime]}}))

(reg-event-fx
 :indexinfo-get-realtime
 (fn [{:keys [db]} _]
   (if-let [indexid (get-in db [:indexinfo :id])]
     (let [fenshi (get-in db [:indexinfo :data :fenshi])]
       (if (not= fenshi-num (count fenshi))
         {:http-xhrio {:method          :get
                       :uri             (proxy-uri "/stockinfogate/indexqt/realtimeinfo")
                       :params          {:indexids indexid}
                       :format          (ajax/json-request-format)
                       :response-format (ajax/json-response-format {:keywords? true})
                       :on-success      [:indexinfo-get-realtime-success indexid]
                       :on-failure      [:xhrio-null-failed]
                       }}
         {})))))

(reg-event-fx
 :indexinfo-get-realtime-success
 (fn [{:keys [db]} [_ indexid {:keys [status data]}]]
   (if (and status (= indexid (get-in db [:indexinfo :id])))
     {:db       (assoc-in db [:indexinfo :data :realtime] (first data))
      :dispatch [:indexinfo-update-chart-data-by-realtime indexid data]}
     {})))

(reg-sub
 :indexinfo-sixmonth-unlock-ratio
 :<- [:indexinfo]
 (fn [indexinfo _]
   (get-in indexinfo [:values :capital :unlock])))

(reg-event-db
 :indexinfo-on-summary
 indexinfo-interceptors
 (fn [indexinfo _]
   (-> indexinfo
       (assoc-in [:values :fundamental]
                 (select-keys (get-in indexinfo [:data :summary :indicator])
                              [:pe :pb :ps :totalmv :freefloatmv]))
       (assoc-in [:values :capital :dividend]
                 (get-in indexinfo [:data :summary :indicator :dividendratio])))))

(reg-event-fx
 :indexinfo-get-summary-info
 (fn [{:keys [db]} _]
   (if-let [indexid (get-in db [:indexinfo :id])]
    {:http-xhrio {:method          :get
                  :uri             (proxy-uri "/securities/stockindex/summary")
                  :params          {:indexid indexid}
                  :format          (ajax/json-request-format)
                  :response-format (ajax/json-response-format {:keywords? true})
                  :on-success      [:indexinfo-get-summary-info-success indexid]
                  :on-failure      [:xhrio-null-failed]}}
    {})))

(reg-event-fx
 :indexinfo-get-summary-info-success
 (fn [{:keys [db]} [_ indexid {:keys [status data]}]]
   (if (and status (= indexid (get-in db [:indexinfo :id])))
     {:db       (-> db (assoc-in [:indexinfo :data :summary] data))
      :dispatch [:indexinfo-on-summary]}
     {})))

(reg-event-fx
 :indexinfo-get-kline-info-d
 (fn [{:keys [db]} _]
   (let [indexid (get-in db [:indexinfo :id])]
     {:http-xhrio {:method          :get
                   :uri             (proxy-uri "/securities/stockindex/kline")
                   :params          {:indexid indexid
                                     :period  "D"}
                   :format          (ajax/json-request-format)
                   :response-format (ajax/json-response-format {:keywords? true})
                   :on-success      [:indexinfo-get-kline-info-d-success indexid]
                   :on-failure      [:xhrio-null-failed]}})))


(defn get-values-kpe
  [data]
  (let [origin     (:origin data)
        last-50    (take-last 50 origin)
        max-high   (apply max (map #(get % 3) last-50))
        min-low    (apply min (map #(get % 4) last-50))
        pe-max-50  (apply max (map #(get % 9) last-50))
        pe-min-50  (apply min (map #(get % 9) last-50))
        pe-mid-50  (/ (+ pe-max-50 pe-min-50) 2)
        pe-max     (apply max (map #(get % 9) origin))
        pe-min     (apply min (map #(get % 9) origin))
        pe-mid     (/ (+ pe-max pe-min) 2)
        prices-fn  (fn [base]
                     (vec (map (fn [item]
                                 (* (/ (second item) (get item 9)) base))
                               origin)))
        prices-max (prices-fn pe-max-50)
        prices-min (prices-fn pe-min-50)
        prices-mid (prices-fn pe-mid-50)
        ;; fix-fn           (fn [price-item]
        ;;                    (when (and (>= price-item min-low)
        ;;                               (<= price-item max-high))
        ;;                      price-item))
        ;; fixed-prices-min (map fix-fn prices-min)
        ;; fixed-prices-max (map fix-fn prices-max)
        ;; fixed-prices-mid (map fix-fn prices-mid)
        ]
    {:last50-pe
     {:max pe-max-50
      :min pe-min-50
      :mid pe-mid-50}
     :pes    (vec (map #(get % 9) origin))
     :prices {:min prices-min
              :max prices-max
              :mid prices-mid}
     ;; :fixed-prices {:min prices-min
     ;;                :max prices-max
     ;;                :mid prices-mid}
     }))

(reg-event-db
 :indexinfo-get-kline-info-d-success
 indexinfo-interceptors
 (fn [indexinfo [_ indexid {:keys [status data]}]]
   (let [valid? (and status (= indexid (:id indexinfo)))]
     (if valid?
       (-> indexinfo
           (assoc-in [:data :k :day] data)
           (assoc-in [:chart :use-data :k :day] data)
           #_(assoc-in [:chart :use-data :k :daype] data)
           (update-in [:data :cnt] inc)
           (assoc-in [:values :kpe :daype] (get-values-kpe data)))
      indexinfo))))

(reg-event-fx
 :indexinfo-get-kline-info-w
 (fn [{:keys [db]} _]
   (let [indexid (get-in db [:indexinfo :id])]
     {:http-xhrio {:method          :get
                   :uri             (proxy-uri "/securities/stockindex/kline")
                   :params          {:indexid indexid
                                     :period  "W"}
                   :format          (ajax/json-request-format)
                   :response-format (ajax/json-response-format {:keywords? true})
                   :on-success      [:indexinfo-get-kline-info-w-success indexid]
                   :on-failure      [:xhrio-null-failed]}})))

(reg-event-db
 :indexinfo-get-kline-info-w-success
 indexinfo-interceptors
 (fn [indexinfo [_ indexid {:keys [status data]}]]
   (let [valid? (and status (= indexid (:id indexinfo)))]
     (if valid?
       (-> indexinfo
           (assoc-in [:data :k :week] data)
           (assoc-in [:chart :use-data :k :week] data)
           #_(assoc-in [:chart :use-data :k :weekpe] data)
           (update-in [:data :cnt] inc)
           (assoc-in [:values :kpe :weekpe] (get-values-kpe data)))
      indexinfo))))

(reg-event-fx
 :indexinfo-get-kline-info-m
 (fn [{:keys [db]} _]
   (let [indexid (get-in db [:indexinfo :id])]
     {:http-xhrio {:method          :get
                   :uri             (proxy-uri "/securities/stockindex/kline")
                   :params          {:indexid indexid
                                     :period  "M"}
                   :format          (ajax/json-request-format)
                   :response-format (ajax/json-response-format {:keywords? true})
                   :on-success      [:indexinfo-get-kline-info-m-success indexid]
                   :on-failure      [:xhrio-null-failed]}})))

(reg-event-db
 :indexinfo-get-kline-info-m-success
 indexinfo-interceptors
 (fn [indexinfo [_ indexid {:keys [status data]}]]
   (let [valid? (and status (= indexid (:id indexinfo)))]
     (if valid?
      (-> indexinfo
          (assoc-in [:data :k :month] data)
          (assoc-in [:chart :use-data :k :month] data)
          #_(assoc-in [:chart :use-data :k :monthpe] data)
          (update-in [:data :cnt] inc)
          (assoc-in [:values :kpe :monthpe] (get-values-kpe data)))
      indexinfo))))

(reg-event-fx
 :indexinfo-clear-get-fenshi-info-timer
 (fn [_ _]
   {:clear-timer {:id "indexinfo-get-fenshi-info-timer"}}))

(reg-event-fx
 :indexinfo-get-fenshi-info-timer
 (fn [_ _]
   {:set-timer {:id       "indexinfo-get-fenshi-info-timer"
                :interval 6000
                :event    [:indexinfo-get-fenshi-info]}}))

(reg-event-fx
 :indexinfo-get-fenshi-info
 (fn [{:keys [db]} _]
   (let [indexid (get-in db [:indexinfo :id])
         fenshi (get-in db [:indexinfo :data :fenshi])]
     (if (not= fenshi-num (count fenshi))
       {:http-xhrio {:method          :get
                     :uri             (proxy-uri "/securities/stockindex/fenshitu")
                     :params          {:indexid indexid}
                     :format          (ajax/json-request-format)
                     :response-format (ajax/json-response-format {:keywords? true})
                     :on-success      [:indexinfo-get-fenshi-info-success indexid]
                     :on-failure      [:xhrio-null-failed]}}
       {}))))

(reg-event-db
 :indexinfo-get-fenshi-info-success
 indexinfo-interceptors
 (fn [indexinfo [_ indexid {:keys [status data]}]]
   (let [valid? (and status (= indexid (:id indexinfo)))]
     (cond-> indexinfo
       valid? (assoc-in [:data :fenshi] data)
       ;;       valid? (assoc-in [:chart :use-data :k :fenshi] data)
       valid? (update-in [:data :cnt] inc)
       ))))

(reg-event-fx
 :indexinfo-get-line-info
 (fn [{:keys [db]} _]
   (let [indexid (get-in db [:indexinfo :id])]
     {:http-xhrio {:method          :get
                   :uri             (proxy-uri "/securities/stockindex/line")
                   :params          {:indexid   indexid
                                     :indicator "pe,pb,ps,dividendratio,totalmv,freefloatmv"}
                   :format          (ajax/json-request-format)
                   :response-format (ajax/json-response-format {:keywords? true})
                   :on-success      [:indexinfo-get-line-info-success indexid]
                   :on-failure      [:xhrio-null-failed]}})))


;; TODO move this to charts.cljs
;; FIXME
(def filter-fns
  {:1year (partial ymd-num-in-n-year? 1)
   :3year (partial ymd-num-in-n-year? 3)
   :5year (partial ymd-num-in-n-year? 5)})

(defn indexinfo-cut-data
  [data active]
  #_(add-trace "before cut-data-by-years")
  (let [pred  (filter-fns active)
        start (max 0 (- (count data) (* 255 (case active
                                              :1year 1
                                              :3year 3
                                              :5year 5))))
        lst (loop [i   start]
              (let [lst (drop i data)]
                #_(prn (count lst))
                (if (or (pred (ffirst lst)) (empty? lst))
                  lst
                  (recur (inc i)))))]
    #_(add-trace "after cut-data-by-years")
    lst))

(reg-event-db
 :indexinfo-get-line-info-success
 indexinfo-interceptors
 (fn [indexinfo [_ indexid {:keys [status data]}]]
   (let [valid? (and status (= indexid (:id indexinfo)))
         active (get-in indexinfo [:chart :active])]
     (cond-> indexinfo
       valid? (assoc-in [:data :line] data)
       valid? (assoc-in [:chart :use-data :line]
                        {:1year (indexinfo-cut-data data :1year)
                         :3year (indexinfo-cut-data data :3year)
                         :5year (indexinfo-cut-data data :5year)
                         :all   data})
       valid? (update-in [:data :cnt] inc)))))

(reg-event-fx
 :indexinfo-get-reminder
 (fn [{:keys [db]} _]
   (let [indexid (get-in db [:indexinfo :id])]
     {:http-xhrio {:method          :get
                   :uri             (proxy-uri "/stockinfogate/stockindex/reminder")
                   :params          {:indexid   indexid
                                     :indicator "pe"}
                   :format          (ajax/json-request-format)
                   :response-format (ajax/json-response-format {:keywords? true})
                   :on-success      [:indexinfo-get-reminder-success indexid]
                   :on-failure      [:xhrio-null-failed]}})))

(reg-event-db
 :indexinfo-get-reminder-success
 indexinfo-interceptors
 (fn [indexinfo [_ indexid {:keys [status data]}]]
   (if-let [valid? (and status (= indexid (:id indexinfo)))]
     (let [{:keys [top bottom]} data]
       (cond-> indexinfo
         true           (assoc-in [:data :reminder] data)
         true           (assoc-in [:setting :up :pe] top)
         true           (assoc-in [:setting :down :pe] bottom)
         (some? top)    (assoc-in [:setting :up :has-rule?] true)
         (some? bottom) (assoc-in [:setting :down :has-rule?] true)))
     indexinfo)))

(reg-event-fx
 :indexinfo-set-reminder
 (fn [{:keys [db]} [_ indexid top bottom]]
   {:http-xhrio {:method          :post
                 :uri             (proxy-uri "/stockinfogate/stockindex/reminder")
                 :body            (doto (js/FormData.)
                                    (.append "indexid" indexid)
                                    (.append "indicator" "pe")
                                    (.append "top" top)
                                    (.append "bottom" bottom))
                 :format          (ajax/json-request-format)
                 :response-format (ajax/json-response-format {:keywords? true})
                 :on-success      [:indexinfo-set-reminder-success]
                 :on-failure      [:xhrio-null-failed]}}))

(reg-event-fx
 :indexinfo-set-reminder-success
 (fn [{:keys [db]} [_ {:keys [status]}]]
   (if status
     {:db             (-> db
                          (assoc-in [:flash-tip :show] true)
                          (assoc-in [:flash-tip :kw] :indexinfo-reminder-set))
      :dispatch-later [{:ms 2000 :dispatch [:flash-tip-hide]}]}
     {})))

(reg-event-fx
 :indexinfo-set-active-noodle
 (fn [{:keys [db]} [_ active]]
   {:db       (cond-> db
                true                    (assoc-in [:indexinfo :noodle :active] active)
                ;; true                 (assoc-in [:indexnfo :secondary :tech :active] nil)
                true                    (assoc-in [:indexinfo :headline :cate]
                                                  ({:tech        :realtime
                                                    :fundamental :fundamental} active))
                true                    (assoc-in [:indexinfo :headline :index] 0)
                true                    (assoc-in [:indexinfo :chart :active] :3year)
                true                    (assoc-in [:indexinfo :chart :interval] :season)
                (= :tech active)        (assoc-in [:indexinfo :secondary :tech :active] nil)
                (= :tech active)        (assoc-in [:indexinfo :chart :cate] :k)
                (= :tech active)        (assoc-in [:indexinfo :chart :ktype]
                                                  (if (-> db (get-in [:indexinfo :data :fenshi])
                                                          count (= fenshi-num))
                                                    :day :fenshi))
                (= :fundamental active) (assoc-in [:indexinfo :secondary :fundamental :active] nil)
                (= :fundamental active) (assoc-in [:indexinfo :chart :cate] :pe)
                (= :capital active)     (assoc-in [:indexinfo :secondary :capital :active] :divirate)
                (= :capital active)     (assoc-in [:indexinfo :chart :cate] :divirate)
                (= :capital active)     (assoc-in [:indexinfo :chart :active] :3year)
                (= :capital active)     (assoc-in [:indexinfo :headline :cate] :dividend)
                (= :capital active)     (assoc-in [:indexinfo :headline :active] 0)
                (= :ingredients active) (assoc-in [:indexinfo :secondary :msg :active] nil)
                (= :ingredients active) (assoc-in [:indexinfo :headline :cate] :ingredients))
    :dispatch [:indexinfo-clear-headline-swiper-timer]}))

(reg-event-fx
 :indexinfo-set-fundamental-active
 (fn [{:keys [db]} [_ active]]
   {:db         (-> db
                    (assoc-in [:indexinfo :secondary :fundamental :active] active)
                    (assoc-in [:indexinfo :headline :cate] :fundamental)
                    (assoc-in [:indexinfo :headline :index]
                              (let [index (.indexOf [:pe :pb :ps :totalmv :freefloatmv] active)]
                                (if (= -1 index) 0 index)))
                    (assoc-in [:indexinfo :chart :active] :3year)
                    ;; TODO 有没有season/year 切换了？
                    #_(assoc-in [:indexinfo :chart :interval] :season)
                    )
    :dispatch-n (list [:indexinfo-clear-headline-swiper-timer]
                      [:indexinfo-secondary-set-chart active])}))

(reg-event-fx
 :indexinfo-set-tech-active
 (fn [{:keys [db]} [_ active]]
   {:db         (-> db
                    (assoc-in [:indexinfo :secondary :tech :active] active)
                    (assoc-in [:indexinfo :headline :cate] active)
                    (assoc-in [:indexinfo :headline :index] 0))
    :dispatch-n (list [:indexinfo-clear-headline-swiper-timer]
                      [:indexinfo-set-headline-swiper-timer])}))

(reg-event-fx
 :indexinfo-set-capital-active
 (fn [{:keys [db]} [_ active]]
   {:db         (-> db
                    (assoc-in [:indexinfo :secondary :capital :active] active)
                    (assoc-in [:indexinfo :headline :cate] (when (= active :dividend) :dividend))
                    (assoc-in [:indexinfo :headline :index] 0)
                    (assoc-in [:indexinfo :chart :active] :3year)
                    (assoc-in [:indexinfo :chart :interval] :season))
    :dispatch-n (concat (list [:indexinfo-clear-headline-swiper-timer])
                        (when (= active :divirate)
                          (list [:indexinfo-secondary-set-chart active])))}))

(reg-event-db
 :indexinfo-headline-update-index
 indexinfo-interceptors
 (fn [indexinfo [_ index]]
   (update-in indexinfo [:headline :index] inc)))

(reg-event-fx
 :indexinfo-set-headline-swiper-timer
 (fn [_ _]
   {:set-timer {:id       "indexinfo-headline-swiper-timer"
                :interval 2000
                :event    [:indexinfo-headline-update-index]}}))

(reg-event-fx
 :indexinfo-clear-headline-swiper-timer
 (fn [_ _]
   {:clear-timer {:id "indexinfo-headline-swiper-timer"}}))

(reg-event-db
 :indexinfo-secondary-set-chart
 indexinfo-interceptors
 (fn [indexinfo [_ cate]]
   (-> indexinfo
       (assoc-in [:chart :active] :3year)
       (assoc-in [:chart :cate] cate))))

(reg-event-db
 :indexinfo-update-cnt
 indexinfo-interceptors
 (fn [indexinfo _]
   (-> indexinfo
       (update-in [:data :cnt] inc))))

(reg-sub
 :indexinfo-data-cnt
 :<- [:indexinfo-data]
 (fn [indexinfo _]
   (:cnt indexinfo)))

(reg-event-db
 :indexinfo-update-chart-data-by-realtime
 indexinfo-interceptors
 (fn [indexinfo [_ indexid realtime]]
   (let [day       (get-in indexinfo [:chart :use-data :k :day])
         week      (get-in indexinfo [:chart :use-data :k :week])
         month     (get-in indexinfo [:chart :use-data :k :month])
         new-day   (some-> day (update-indexinfo-k-data-with-realtime :day realtime))
         new-week  (some-> week (update-indexinfo-k-data-with-realtime :week realtime))
         new-month (some-> month (update-indexinfo-k-data-with-realtime :month realtime))]
     (cond-> indexinfo
       true  (update-in [:data :cnt] inc)
       day   (assoc-in [:use-data :k :day] new-day)
       day   (assoc-in [:values :kpe :daype] (get-values-kpe new-day))
       week  (assoc-in [:use-data :k :week] new-week)
       week  (assoc-in [:values :kpe :weekpe] (get-values-kpe new-week))
       month (assoc-in [:use-data :k :month] new-month)
       month (assoc-in [:values :kpe :monthpe] (get-values-kpe new-month))))))

(reg-event-db
 :indexinfo-update-chart-data-by-ktype
 indexinfo-interceptors
 (fn [indexinfo [_ ktype]]
   (let [{:keys [cate active ktype]} (:chart indexinfo)]
     (-> indexinfo
         (assoc-in [:chart :use-data :k ktype]
                   (get-in indexinfo [:data :k (case ktype
                                                 :daype   :day
                                                 :weekpe  :week
                                                 :monthpe :month
                                                 ktype)]))))))

(reg-event-db
 :secuinfo-mask-set-type
 secuinfo-interceptors
 (fn [secuinfo [_ t]]
   (assoc-in secuinfo [:mask :type] t)))

(reg-event-db
 :secuinfo-mask-set-content
 secuinfo-interceptors
 (fn [secuinfo [_ content]]
   (assoc-in secuinfo [:mask :content] content)))

(reg-event-db
 :secuinfo-mask-show ;; set mask to show
 secuinfo-interceptors
 (fn [secuinfo _]
   (assoc-in secuinfo [:mask :show] true)))

(reg-event-db
 :secuinfo-mask-hide ;; set mask to hide
 secuinfo-interceptors
 (fn [secuinfo [_ t]]
   (assoc-in secuinfo [:mask :show] false)))

(reg-event-db
 :indexinfo-mask-set-type
 indexinfo-interceptors
 (fn [indexinfo [_ t]]
   (assoc-in indexinfo [:mask :type] t)))

(reg-event-db
 :indexinfo-mask-set-content
 indexinfo-interceptors
 (fn [indexinfo [_ content]]
   (assoc-in indexinfo [:mask :content] content)))

(reg-event-db
 :indexinfo-mask-show ;; set mask to show
 indexinfo-interceptors
 (fn [indexinfo _]
   (assoc-in indexinfo [:mask :show] true)))

(reg-event-db
 :indexinfo-mask-hide ;; set mask to hide
 indexinfo-interceptors
 (fn [indexinfo [_ t]]
   (assoc-in indexinfo [:mask :show] false)))

(reg-event-fx
 :secuinfo-show-helper-mask
 (fn [{:keys [db]} _]
   {:db         db
    :dispatch-n (list [:secuinfo-mask-set-type :img]
                      [:secuinfo-mask-set-content (get-in db [:secuinfo :noodle :active])]
                      [:secuinfo-mask-show])}))

(reg-event-fx
 :secuinfo-show-pay-mask
 (fn [{:keys [db]} _]
   {:db         db
    :dispatch-n (list [:secuinfo-mask-set-type :div]
                      [:secuinfo-mask-show])}))

(reg-event-fx
 :indexinfo-show-helper-mask
 (fn [{:keys [db]} _]
   {:db         db
    :dispatch-n (list [:indexinfo-mask-set-type :img]
                      [:indexinfo-mask-set-content (get-in db [:indexinfo :noodle :active])]
                      [:indexinfo-mask-show])}))

(reg-event-db
 :share-tip-show
 (fn [db _]
   (assoc-in db [:share-tip :show] true)))

(reg-event-db
 :share-tip-hide
 (fn [db _]
   (assoc-in db [:share-tip :show] false)))

(reg-event-db
 :indexinfo-show-setting
 indexinfo-interceptors
 (fn [indexinfo _]
   (-> indexinfo
       (assoc-in [:setting :show] true)
       (assoc-in [:setting :pe]
                 (*
                  (get-in indexinfo [:data :summary :indicator :pe :val])
                  (get-in indexinfo [:data :realtime :changeratio])))
       (assoc-in [:setting :changed?] false))))

#_(reg-event-db
   :indexinfo-clear-setting
   indexinfo-interceptors
   (fn [indexinfo _]
     ;; TODO clear setting page data
     indexinfo))

(reg-event-db
 :indexinfo-setting-clear-pe-up
 indexinfo-interceptors
 (fn [indexinfo _]
   (-> indexinfo
       (assoc-in [:setting :up] {:pe        ""
                                 :valid?    true
                                 :has-rule? false})
       (assoc-in [:setting :changed?] true))))

(reg-event-db
 :indexinfo-setting-clear-pe-down
 indexinfo-interceptors
 (fn [indexinfo _]
   (-> indexinfo
       (assoc-in [:setting :down] {:pe        ""
                                   :valid?    true
                                   :has-rule? false})
       (assoc-in [:setting :changed?] true))))

(reg-event-db
 :indexinfo-setting-set-pe-up
 indexinfo-interceptors
 (fn [indexinfo [_ pe]]
   (let [valid? (> (js/Number pe)
                   (get-in indexinfo [:setting :pe]))]
     (-> indexinfo
         (assoc-in [:setting :up]
                   {:pe        pe
                    :valid?    valid?
                    :has-rule? valid?})
         (assoc-in [:setting :changed?] true)))))

(reg-event-db
 :indexinfo-setting-cancel
 indexinfo-interceptors
 (fn [indexinfo _]
   (let [data                 (get-in indexinfo [:data :reminder])
         {:keys [top bottom]} data]
     (-> indexinfo
         (assoc-in [:setting :show] false)
         (assoc-in [:setting :up]
                   {:pe        top
                    :valid?    true
                    :has-rule? (some? top)})
         (assoc-in [:setting :down]
                   {:pe        bottom
                    :valid?    true
                    :has-rule? (some? bottom)})))))

(reg-event-fx
 :indexinfo-setting-save
 (fn [{:keys [db]} _]
   (let [indexid           (get-in db [:indexinfo :id])
         setting           (get-in db [:indexinfo :setting])
         {:keys [up down]} setting
         {up-valid? :valid?
          up-pe     :pe}   up
         {down-valid? :valid?
          down-pe     :pe} down]
     {:dispatch       [:indexinfo-set-reminder
                       indexid
                       (when up-valid? up-pe)
                       (when down-valid? down-pe)]
      :db             (assoc-in db [:indexinfo :setting :show] false)
      :dispatch-later [{:ms 0 :dispatch [:indexinfo-get-reminder]}]})))

(reg-event-db
 :indexinfo-setting-set-pe-down
 indexinfo-interceptors
 (fn [indexinfo [_ pe]]
   (let [valid? (< (js/Number pe)
                   (get-in indexinfo [:setting :pe]))]
     (-> indexinfo
         (assoc-in [:setting :down]
                   {:pe        pe
                    :valid?    valid?
                    :has-rule? valid?})
         (assoc-in [:setting :changed?] true)))))

(reg-event-fx
 :indexinfo-setting-set-up-pe-btn
 (fn [_ [_ pe]]
   {:dispatch-n (list [:indexinfo-setting-clear-pe-up]
                      [:indexinfo-setting-set-pe-up pe])}))

(reg-event-fx
 :indexinfo-setting-set-down-pe-btn
 (fn [_ [_ pe]]
   {:dispatch-n (list [:indexinfo-setting-clear-pe-down]
                      [:indexinfo-setting-set-pe-down pe])}))

(reg-event-db
 :secuinfo-set-active-ma-index
 secuinfo-interceptors
 (fn [secuinfo [_ index]]
   (assoc-in secuinfo [:chart :ma-index] index)))

(reg-event-fx
 :secuinfo-verify-phone
 (fn [{:keys [db]} [_ number]]
   {:http-xhrio {:method          :post
                 :uri             (proxy-uri "/stockinfogate/user/recharge-verify")
                 :params          {:phone number}
                 :format          (ajax/json-request-format)
                 :response-format (ajax/json-response-format {:keywords? true})
                 :on-success      [:secuinfo-get-vip-info-success]
                 :on-failure      [:xhrio-null-failed]
                 }}))

(reg-event-fx
 :secuinfo-get-vip-info-success
 (fn [{:keys [db]} [_ response]]
   (merge
    {:db (assoc-in db [:secuinfo :data :verify] response)}
    (when (:status response)
      {:dispatch [:secuinfo-get-scores-info]}))))


(reg-event-fx
 :secuinfo-get-pepbps-history-info
 (fn [{:keys [db]} _]
   (if-let [stockid (get-in db [:secuinfo :stockid])]
     {:http-xhrio {:method          :get
                   :uri             (proxy-uri "/stockinfogate/commonapi")
                   :params          {:name     "pepbps_history"
                                     :secucode stockid}
                   :format          (ajax/json-request-format)
                   :response-format (ajax/json-response-format {:keywords? true})
                   :on-success      [:secuinfo-get-pepbps-history-info-success]
                   :on-failure      [:xhrio-null-failed]}}
     {})))

(reg-event-db
 :secuinfo-get-pepbps-history-info-success
 secuinfo-interceptors
 (fn [secuinfo [_ {:keys [status data]}]]
   (if status
     (let [{:keys [data conf ltzero]} data
           [pepb-x pe-y pe-tooltip
            pb-y ps-x ps-y]           (loop [pepb-x     []
                                             pe-y       []
                                             pe-tooltip []
                                             pb-y       []
                                             ps-x       []
                                             ps-y       []
                                             pepbps     data]
                                        (let [item    (first data)
                                              date    (format-date-slash (first item))
                                              pe      (get item 2)
                                              tooltip (get item 1)
                                              pb      (get item 3)
                                              ps      (get item 4)
                                              #_      (.log js/console "@@@@@@@" (count pepb-x) (count pepbps))]
                                          (if (empty? pepbps)
                                            [pepb-x pe-y pe-tooltip
                                             pb-y ps-x ps-y]
                                            (recur (conj pepb-x date)
                                                   (conj pe-y pe)
                                                   (conj pe-tooltip tooltip)
                                                   (conj pb-y pb)
                                                   (if ps (conj ps-x date) ps-x)
                                                   (if ps (conj ps-y ps) ps-y)
                                                   (drop 1 pepbps)))))]
       (-> secuinfo
           (assoc-in [:chart :data]
                     {:pe
                      {:x        pepb-x
                       :y        pe-y
                       :tool-tip pe-tooltip}
                      :pb
                      {:x pepb-x
                       :y pb-y}
                      :ps
                      {:x ps-x
                       :y ps-y}
                      :mark-line (map (fn [item]
                                        {:name  (second item)
                                         :yAxis (first item)})
                                      conf)
                      :mark-area [{:yAxis (first ltzero)}
                                  {:yAxis (second ltzero)}]})))
     secuinfo)))

(reg-event-fx
 :secuinfo-get-earning-history-info
 (fn [{:keys [db]} _]
   (if-let [stockid (get-in db [:secuinfo :stockid])]
     {:http-xhrio {:method          :get
                   :uri             (proxy-uri "/stockinfogate/commonapi")
                   :params          {:name      "earning_history"
                                     :logarithm true
                                     :secucode  stockid}
                   :format          (ajax/json-request-format)
                   :response-format (ajax/json-response-format {:keywords? true})
                   :on-success      [:secuinfo-get-earning-history-info-success]
                   :on-failure      [:xhrio-null-failed]}}
     {})))

(reg-event-db
 :secuinfo-get-earning-history-info-success
 secuinfo-interceptors
 (fn [secuinfo [_ {:keys [status data]}]]
   (cond-> secuinfo
     status (update-secuinfo-chart-data :egr data))))

(reg-event-fx
 :secuinfo-get-revenue-history-info
 (fn [{:keys [db]} _]
   (if-let [stockid (get-in db [:secuinfo :stockid])]
     {:http-xhrio {:method          :get
                   :uri             (proxy-uri "/stockinfogate/commonapi")
                   :params          {:name      "revenue_history"
                                     :logarithm true
                                     :secucode  stockid}
                   :format          (ajax/json-request-format)
                   :response-format (ajax/json-response-format {:keywords? true})
                   :on-success      [:secuinfo-get-revenue-history-info-success]
                   :on-failure      [:xhrio-null-failed]}})))

(reg-event-db
 :secuinfo-get-revenue-history-info-success
 secuinfo-interceptors
 (fn [secuinfo [_ {:keys [status data]}]]
   (cond-> secuinfo
     status (update-secuinfo-chart-data :rgr data))))

(reg-event-fx
 :indexinfo-get-ingredients
 (fn [{:keys [db]} _]
   {:http-xhrio {:method          :get
                 :uri             (proxy-uri "/securities/stockindex/component")
                 :params          {:indexid (get-in db [:indexinfo :id])}
                 :format          (ajax/json-request-format)
                 :response-format (ajax/json-response-format {:keywords? true})
                 :on-success      [:indexinfo-get-ingredients-success]
                 :on-failure      [:xhrio-null-failed]}}))

(reg-event-db
 :indexinfo-get-ingredients-success
 indexinfo-interceptors
 (fn [indexinfo [_ {:keys [status data]}]]
   (cond-> indexinfo
     status (assoc-in [:data :ingredients] data))))

(reg-event-fx
 :indexinfo-get-ingredients-realtime-info
 (fn [{:keys [db]} _]
   (let [stockids (map :stockid (get-in db [:indexinfo :data :ingredients :component]))]
     (if-not (empty? stockids)
       {:dispatch [:indexinfo-get-stocks-realtime-info stockids]}
       {}))))

(reg-event-fx
 :indexinfo-get-stocks-realtime-info
 (fn [{:keys [db]} [_ stockids]]
   (let [stockids-str (->> stockids (str/join ","))]
     (merge
      {:http-xhrio {:method          :get
                    :uri             (proxy-uri "/stockinfogate/stock/realtimeinfo")
                    :params          {:stockids stockids-str}
                    :format          (ajax/json-request-format)
                    :response-format (ajax/json-response-format {:keywords? true})
                    :on-success      [:indexinfo-get-stocks-realtime-info-success stockids]
                    :on-failure      [:xhrio-null-failed]}}
      (let [hour (.getHours (js/Date.))]
        (when (and (>= hour 9) (<= hour 15))
          {:dispatch-later [{:ms       6000
                             :dispatch [:indexinfo-get-stocks-realtime-info stockids]}]}))))))

(reg-event-db
 :indexinfo-get-stocks-realtime-info-success
 indexinfo-interceptors
 (fn [indexinfo [_ stockids {:keys [status data]}]]
   (-> indexinfo
       (assoc-in [:ingredients-realtime]
                 (into {} (map (fn [a b] [a b]) stockids data))))))

(reg-event-fx
 :indexinfo-get-index-list
 (fn [{:keys [db]} _]
   (if (empty? (get-in db [:indexinfo :data :indexlist]))
     {:http-xhrio {:method          :get
                   :uri             (proxy-uri "/securities/stockindex/list")
                   :format          (ajax/json-request-format)
                   :response-format (ajax/json-response-format {:keywords? true})
                   :on-success      [:indexinfo-get-index-list-success]
                   :on-failure      [:xhrio-null-failed]}}
     {})))

(reg-event-db
 :indexinfo-get-index-list-success
 indexinfo-interceptors
 (fn [indexinfo [_ {:keys [status data]}]]
   (-> indexinfo
       (assoc-in [:data :indexlist] data))))

(reg-event-db
 :indexinfo-show-indexlist
 indexinfo-interceptors
 (fn [indexinfo [_ show?]]
   (-> indexinfo
       (assoc :show-indexlist? show?))))

(reg-event-db
 :indexinfo-set-intro
 indexinfo-interceptors
 (fn [indexinfo [_ intro]]
   (-> indexinfo
       (assoc :intro intro))))

(reg-event-db
 :indexinfo-show-intro
 indexinfo-interceptors
 (fn [indexinfo _]
   (-> indexinfo
       (assoc :intro :switch-chart))))

(reg-event-fx
 :indexinfo-get-unlocks-info
 (fn [{:keys [db]} _]
   {:http-xhrio {:method          :get
                 :uri             (proxy-uri "/stockinfogate/stockindex/unlocks")
                 :params          {:indexid (get-in db [:indexinfo :id])}
                 :format          (ajax/json-request-format)
                 :response-format (ajax/json-response-format {:keywords? true})
                 :on-success      [:indexinfo-get-unlocks-info-success]
                 :on-failure      [:xhrio-null-failed]}}))

(reg-event-db
 :indexinfo-get-unlocks-info-success
 indexinfo-interceptors
 (fn [indexinfo [_ {:keys [status data]}]]
   (-> indexinfo
       (assoc-in [:data :unlocks] data))))

(reg-event-db
 :secuinfo-set-secuinfo-nav-y
 (fn [db [_ val]]
   (assoc-in db [:ui :secuinfo :secuinfo-nav :y] val)))

(reg-event-db
 :indexinfo-set-indexinfo-nav-y
 (fn [db [_ val]]
   (assoc-in db [:ui :indexinfo :indexinfo-nav :y] val)))

(reg-event-db
 :secuinfo-set-main-content-y
 (fn [db [_ val]]
   (assoc-in db [:ui :secuinfo :main-content :y] val)))

(reg-event-db
 :indexinfo-set-main-content-y
 (fn [db [_ val]]
   (assoc-in db [:ui :indexinfo :main-content :y] val)))

(reg-event-db
 :secuinfo-set-secuinfo-main-y
 (fn [db [_ val]]
   (assoc-in db [:ui :secuinfo :secuinfo-main :y] val)))

(reg-event-db
 :indexinfo-set-indexinfo-main-y
 (fn [db [_ val]]
   (assoc-in db [:ui :indexinfo :indexinfo-main :y] val)))

(reg-event-db
 :secuinfo-set-secondary-nav-y
 (fn [db [_ val]]
   (assoc-in db [:ui :secuinfo :secondary-nav :y] val)))

(reg-event-db
 :indexinfo-set-secondary-nav-y
 (fn [db [_ val]]
   (assoc-in db [:ui :indexinfo :secondary-nav :y] val)))

(reg-event-db
 :secuinfo-set-chart-info-switch-bar-height
 (fn [db [_ height]]
   (assoc-in db [:ui :secuinfo :chart-info-switch-bar :height] height)))

(reg-event-db
 :indexinfo-set-chart-info-switch-bar-height
 (fn [db [_ height]]
   (assoc-in db [:ui :indexinfo :chart-info-switch-bar :height] height)))

#_(reg-event-db
   :secuinfo-set-chart-x-axis-line-height
   (fn [db [_ val]]
     (assoc-in db [:ui :secuinfo :chart-x-axis-line :height] val)))

#_(reg-event-db
   :indexinfo-set-chart-x-axis-line-height
   (fn [db [_ val]]
     (assoc-in db [:ui :indexinfo :chart-x-axis-line :height] val)))

(reg-event-db
 :secuinfo-set-chart-nav-height
 (fn [db [_ val]]
   (assoc-in db [:ui :secuinfo :chart-nav :height] val)))

(reg-event-db
 :indexinfo-set-chart-nav-height
 (fn [db [_ val]]
   (assoc-in db [:ui :indexinfo :chart-nav :height] val)))

(reg-event-db
 :bottom-nav-set-y
 (fn [db [_ val]]
   (assoc-in db [:ui :bottom-nav :y] val)))




;; subs
(reg-sub
 :secuinfo
 (fn [db _]
   (:secuinfo db)))

(reg-sub
 :secuinfo-stockname
 :<- [:secuinfo]
 (fn [secuinfo _]
   (:stockname secuinfo)))

(reg-sub
 :secuinfo-stockid
 :<- [:secuinfo]
 (fn [secuinfo _]
   (:stockid secuinfo)))

(reg-sub
 :secuinfo-data
 :<- [:secuinfo]
 (fn [secuinfo _]
   (:data secuinfo)))

(reg-sub
 :secuinfo-data-realtime
 :<- [:secuinfo-data]
 (fn [data _]
   (:realtime data)))

(reg-sub
 :secuinfo-current-price
 :<- [:secuinfo-data-realtime]
 (fn [realtime _]
   (some-> realtime
           :origin
           :newprice
           (.toFixed 2))))

(reg-sub
 :secuinfo-delta-percent
 :<- [:secuinfo-data-realtime]
 (fn [realtime _]
   (some-> realtime
           :changeratio
           (.toFixed 2))))

(reg-sub
 :secuinfo-data-scores
 :<- [:secuinfo-data]
 (fn [data _]
   (:scores data)))

(reg-sub
 :secuinfo-stock-stars
 :<- [:secuinfo-data-scores]
 (fn [scores _]
   (:stars scores)))

(reg-sub
 :secuinfo-is-vip
 :<- [:secuinfo-data-scores]
 (fn [scores _]
   (not (zero? (:is-vip scores)))))

(reg-sub
 :secuinfo-secu-balance
 :<- [:secuinfo-data-scores]
 (fn [scores _]
   (:secu_balance scores)))

(reg-sub
 :secuinfo-vip-available-days
 :<- [:secuinfo-data-scores]
 (fn [scores _]
   (:available_days scores)))

(reg-sub
 :secuinfo-noodle
 :<- [:secuinfo]
 (fn [secuinfo _]
   (:noodle secuinfo)))

(reg-sub
 :secuinfo-active-noodle
 :<- [:secuinfo-noodle]
 (fn [noodle _]
   (:active noodle)))

(reg-sub
 :secuinfo-scores
 :<- [:secuinfo-data-scores]
 (fn [scores _]
   (-> scores
       (select-keys [:tech :fundamental :capital :msg]))))

(reg-sub
 :secuinfo-data-tech
 :<- [:secuinfo-data]
 (fn [data _]
   (:tech data)))

(reg-sub
 :secuinfo-secondary-nav-tech
 :<- [:secuinfo-data-tech]
 (fn [tech _]
   (merge
    (let [[text delta color]
          (get-in tech [:volume_variation :tab_volume])]
      {:volume {:text  text
                :delta delta
                :color color}})
    (let [[text delta color]
          (get-in tech [:zdf :tab_price])]
      {:zdf {:text  text
             :delta delta
             :color color}})
    (let [[text delta]
          (get-in tech [:new_high :tab_price])]
      {:newhigh {:text  text
                 :delta delta}}))))

(reg-sub
 :secuinfo-secondary-nav-fundamental
 :<- [:secuinfo]
 (fn [secuinfo _]
   ))

(reg-sub
 :secuinfo-secondary-nav-capital
 :<- [:secuinfo]
 (fn [secuinfo _]

   )
 )

(reg-sub
 :secuinfo-secondary-nav-msg
 :<- [:secuinfo]
 (fn [secuinfo _]
   )
 )

(reg-sub
 :secuinfo-secondary
 :<- [:secuinfo]
 (fn [secuinfo _]
   (:secondary secuinfo)))

(reg-sub
 :secuinfo-secondary-tech
 :<- [:secuinfo-secondary]
 (fn [secondary _]
   (:tech secondary)))

(reg-sub
 :secuinfo-secondary-fundamental
 :<- [:secuinfo-secondary]
 (fn [secondary _]
   (:fundamental secondary)))

(reg-sub
 :secuinfo-secondary-capital
 :<- [:secuinfo-secondary]
 (fn [secondary _]
   (:capital secondary)))

(reg-sub
 :secuinfo-secondary-msg
 :<- [:secuinfo-secondary]
 (fn [secondary _]
   (:msg secondary)))

(reg-sub
 :secuinfo-tech-active
 :<- [:secuinfo-secondary-tech]
 (fn [tech _]
   (:active tech)))

(reg-sub
 :secuinfo-fundamental-active
 :<- [:secuinfo-secondary-fundamental]
 (fn [fundamental _]
   (:active fundamental)))

(reg-sub
 :secuinfo-capital-active
 :<- [:secuinfo-secondary-capital]
 (fn [capital _]
   (:active capital)))

(reg-sub
 :secuinfo-msg-active
 :<- [:secuinfo-secondary-msg]
 (fn [secondary _]
   (:active secondary)))

(reg-sub
 :secuinfo-headline
 :<- [:secuinfo]
 (fn [secuinfo _]
   (:headline secuinfo)))

(reg-sub
 :secuinfo-headline-cate
 :<- [:secuinfo-headline]
 (fn [headline _]
   (:cate headline)))

(reg-sub
 :secuinfo-headline-current-index
 :<- [:secuinfo-headline]
 (fn [headline _]
   (:index headline)))

(reg-sub
 :secuinfo-data-summary
 :<- [:secuinfo-data]
 (fn [data _]
   (:summary data)))

(reg-sub
 :secuinfo-shares-total
 :<- [:secuinfo-data-summary]
 (fn [summary _]
   (get-in summary [:state :shares :totalshares])))

(reg-sub
 :secuinfo-headline-realtime
 :<- [:secuinfo-data-realtime]
 (fn [realtime _]
   {:open     (when (:tradestatus realtime) (get-in realtime [:origin :open]))
    :turnover (when (:tradestatus realtime) (:turnover realtime))
    :mv       (:market_value realtime)}))

(reg-sub
 :secuinfo-headline-tech-volume
 :<- [:secuinfo-data-tech]
 (fn [tech _]
   (:volume_variation tech)))

(reg-sub
 :secuinfo-headline-tech-zdf
 :<- [:secuinfo-data-tech]
 (fn [tech _]
   (:zdf tech)))

(reg-sub
 :secuinfo-headline-tech-newhigh
 :<- [:secuinfo-data-tech]
 (fn [tech _]
   (:new_high tech)))

(reg-sub
 :secuinfo-values
 :<- [:secuinfo]
 (fn [secuinfo _]
   (:values secuinfo)))

(reg-sub
 :secuinfo-fundamental-values
 :<- [:secuinfo-values]
 (fn [values _]
   (:fundamental values)))

(reg-sub
 :secuinfo-capital-values
 :<- [:secuinfo-values]
 (fn [values _]
   (:capital values)))

(reg-sub
 :secuinfo-annc-data
 :<- [:secuinfo-data]
 (fn [data _]
   (:annc data)))

(reg-sub
 :secuinfo-unlock-data
 :<- [:secuinfo-data]
 (fn [data _]
   (:unlock data)))

(reg-sub
 :secuinfo-chart
 :<- [:secuinfo]
 (fn [secuinfo _]
   (:chart secuinfo)))

(reg-sub
 :secuinfo-chart-active
 :<- [:secuinfo-chart]
 (fn [chart _]
   (:active chart)))

(reg-sub
 :indexinfo
 (fn [db _]
   (:indexinfo db)))

(reg-sub
 :indexinfo-chart
 :<- [:indexinfo]
 (fn [indexinfo _]
   (:chart indexinfo)))

(reg-sub
 :indexinfo-chart-active
 :<- [:indexinfo-chart]
 (fn [chart _]
   (:active chart)))

(reg-sub
 :secuinfo-divirate-data
 :<- [:secuinfo-data]
 (fn [data _]
   (:divirate data)))

(reg-sub
 :secuinfo-chart-option
 :<- [:secuinfo-chart]
 (fn [chart _]
   (:option chart)))

(reg-sub
 :secuinfo-fenshi-chart-option
 :<- [:secuinfo-chart-option]
 (fn [option _]
   (:fenshi option)))

(reg-sub
 :secuinfo-divirate-chart-option
 :<- [:secuinfo-chart-option]
 (fn [option _]
   (:divirate option)))

(reg-sub
 :indexinfo-chart-use-data
 :<- [:indexinfo-chart]
 (fn [chart _]
   (:use-data chart)))

(reg-sub
 :indexinfo-k-chart-use-data
 :<- [:indexinfo-chart-use-data]
 (fn [use-data _]
   (:k use-data)))

(reg-sub
 :indexinfo-fenshi-chart-use-data
 :<- [:indexinfo-data]
 (fn [data _]
   (:fenshi data)))

(reg-sub
 :indexinfo-kpe-values
 :<- [:indexinfo-values]
 (fn [values _]
   (:kpe values)))

(reg-sub
 :indexinfo-kpe-values-day
 :<- [:indexinfo-kpe-values]
 (fn [kpe _]
   ;; BUG FIXME
   ;; (prn "indexinfo-kpe-values update")
   (:daype kpe)))

(reg-sub
 :indexinfo-kpe-values-week
 :<- [:indexinfo-kpe-values]
 (fn [kpe _]
   (:weekpe kpe)))

(reg-sub
 :indexinfo-kpe-values-month
 :<- [:indexinfo-kpe-values]
 (fn [kpe _]
   (:monthpe kpe)))

(reg-sub
 :indexinfo-kday-chart-use-data
 :<- [:indexinfo-k-chart-use-data]
 (fn [k _]
   {:k (:day k)}))

(reg-sub
 :indexinfo-kweek-chart-use-data
 :<- [:indexinfo-k-chart-use-data]
 (fn [k _]
   {:k (:week k)}))

(reg-sub
 :indexinfo-kmonth-chart-use-data
 :<- [:indexinfo-k-chart-use-data]
 (fn [k _]
   {:k (:month k)}))

(reg-sub
 :indexinfo-kdaype-chart-use-data
 :<- [:indexinfo-k-chart-use-data]
 :<- [:indexinfo-kpe-values-day]
 (fn [[k day] _]
   {:k (:day k)
    :prices #_(:fixed-prices day)
    (:prices day)}))

(reg-sub
 :indexinfo-kweekpe-chart-use-data
 :<- [:indexinfo-k-chart-use-data]
 :<- [:indexinfo-kpe-values-week]
 (fn [[k week] _]
   {:k (:week k)
    :prices #_(:fixed-prices week)
    (:prices week)}))

(reg-sub
 :indexinfo-kmonthpe-chart-use-data
 :<- [:indexinfo-k-chart-use-data]
 :<- [:indexinfo-kpe-values-month]
 (fn [[k month]]
   {:k (:month k)
    :prices #_(:fixed-prices month)
    (:prices month)}))

(reg-sub
 :indexinfo-line-chart-use-data
 :<- [:indexinfo-chart-use-data]
 (fn [use-data _]
   (:line use-data)))

(reg-sub
 :indexinfo-data
 :<- [:indexinfo]
 (fn [indexinfo _]
   (:data indexinfo)))

(reg-sub
 :indexinfo-data-fenshi
 :<- [:indexinfo-data]
 (fn [data _]
   (:fenshi data)))

;; NOTE: returns different from set value
(reg-sub
 :indexinfo-chart-ktype
 :<- [:indexinfo-chart]
 :<- [:indexinfo-chart-kswitch-show]
 (fn [[chart kswitch-show] _]
   (case kswitch-show
     :MA (:ktype chart)
     :PE (let [ktype-name (-> chart :ktype name)]
           (if (or (str/ends-with? ktype-name "pe")
                   (= ktype-name "fenshi"))
             (-> ktype-name keyword)
             (-> ktype-name (str "pe") keyword))))))

(reg-sub
 :indexinfo-data-k
 :<- [:indexinfo-data]
 (fn [data _]
   (:k data)))

(reg-sub
 :indexinfo-name
 :<- [:indexinfo]
 (fn [indexinfo _]
   (:name indexinfo)))

(reg-sub
 :indexinfo-id
 :<- [:indexinfo]
 (fn [indexinfo _]
   (:id indexinfo)))

(reg-sub
 :indexinfo-data-realtime
 :<- [:indexinfo-data]
 (fn [data _]
   (:realtime data)))

(reg-sub
 :indexinfo-current-price
 :<- [:indexinfo-data-realtime]
 (fn [realtime _]
   (:newprice realtime)))

(reg-sub
 :indexinfo-delta-percent
 :<- [:indexinfo-data-realtime]
 (fn [{:keys [newprice lastclose]} _]
   (when (and newprice lastclose)
     (-> newprice
         (- lastclose)
         (/ lastclose)
         (* 100)
         (.toFixed 2)))))

(reg-sub
 :indexinfo-data-reminder
 :<- [:indexinfo-data]
 (fn [data _]
   (:reminder data)))

(reg-sub
 :indexinfo-has-reminder?
 :<- [:indexinfo-data-reminder]
 (fn [{:keys [top bottom]} _]
   (or top bottom)))

(reg-sub
 :indexinfo-data-summary
 :<- [:indexinfo-data]
 (fn [data _]
   (:summary data)))

(reg-sub
 :indexinfo-comment
 :<- [:indexinfo-data-summary]
 (fn [summary _]
   (get-in summary [:commoninfo :description])))

(reg-sub
 :indexinfo-noodle
 :<- [:indexinfo]
 (fn [indexinfo _]
   (:noodle indexinfo)))

(reg-sub
 :indexinfo-active-noodle
 :<- [:indexinfo-noodle]
 (fn [noodle _]
   (:active noodle)))

(reg-sub
 :indexinfo-values
 :<- [:indexinfo]
 (fn [indexinfo _]
   (:values indexinfo)))

(reg-sub
 :indexinfo-fundamental-values
 :<- [:indexinfo-values]
 (fn [values _]
   (:fundamental values)))

(reg-sub
 :indexinfo-secondary
 :<- [:indexinfo]
 (fn [indexinfo _]
   (:secondary indexinfo)))

(reg-sub
 :indexinfo-fundamental-active
 :<- [:indexinfo-secondary]
 (fn [secondary _]
   (get-in secondary [:fundamental :active])))

(reg-sub
 :indexinfo-capital-active
 :<- [:indexinfo-secondary]
 (fn [secondary _]
   (get-in secondary [:capital :active])))

(reg-sub
 :indexinfo-capital-values
 :<- [:indexinfo-values]
 (fn [values _]
   (:capital values)))

(reg-sub
 :indexinfo-chart-cate
 :<- [:indexinfo-chart]
 (fn [chart _]
   (:cate chart)))

(reg-sub
 :indexinfo-headline-realtime
 :<- [:indexinfo-data-realtime]
 :<- [:indexinfo-data-summary]
 (fn [[rt summary] _]
   {:open     (:open rt)
    :turnover (:turnover rt)
    :mv       (* (get-in summary [:indicator :totalmv])
                 (inc (* 0.01 (:changeratio rt))))}))

(reg-sub
 :indexinfo-data-summary-tech
 :<- [:indexinfo-data-summary]
 (fn [summary _]
   (:tech summary)))

(reg-sub
 :indexinfo-headline-tech-volume
 :<- [:indexinfo-data-summary-tech]
 (fn [tech _]
   (:volume_variation tech)))

(reg-sub
 :indexinfo-headline-tech-zdf
 :<- [:indexinfo-data-summary-tech]
 (fn [tech _]
   (:zdf tech)))

(reg-sub
 :indexinfo-headline-tech-newhigh
 :<- [:indexinfo-data-summary-tech]
 (fn [tech _]
   (:new_high tech)))

(reg-sub
 :indexinfo-secondary-nav-tech
 :<- [:indexinfo-data-summary-tech]
 (fn [tech _]
   (merge
    (let [[text delta color]
          (get-in tech [:volume_variation :tab_volume])]
      {:volume {:text  text
                :delta delta
                :color color}})
    (let [[text delta color]
          (get-in tech [:zdf :tab_price])]
      {:zdf {:text  text
             :delta delta
             :color color}})
    (let [[text delta]
          (get-in tech [:new_high :tab_price])]
      {:newhigh {:text  text
                 :delta delta}}))))

(reg-sub
 :indexinfo-secondary-tech
 :<- [:indexinfo-secondary]
 (fn [secondary _]
   (:tech secondary)))

(reg-sub
 :indexinfo-tech-active
 :<- [:indexinfo-secondary-tech]
 (fn [tech _]
   (:active tech)))

(reg-sub
 :indexinfo-headline
 :<- [:indexinfo]
 (fn [indexinfo _]
   (:headline indexinfo)))

(reg-sub
 :indexinfo-headline-cate
 :<- [:indexinfo-headline]
 (fn [headline _]
   (:cate headline)))

(reg-sub
 :indexinfo-headline-current-index
 :<- [:indexinfo-headline]
 (fn [headline _]
   (:index headline)))

(reg-sub
 :secuinfo-mask
 :<- [:secuinfo]
 (fn [secuinfo _]
   (:mask secuinfo)))

(reg-sub
 :secuinfo-mask-show
 :<- [:secuinfo-mask]
 (fn [mask _]
   (:show mask)))

(reg-sub
 :secuinfo-mask-type
 :<- [:secuinfo-mask]
 (fn [mask _]
   (:type mask)))

(reg-sub
 :secuinfo-mask-content
 :<- [:secuinfo-mask]
 (fn [mask _]
   (:content mask)))

(reg-sub
 :indexinfo-mask
 :<- [:indexinfo]
 (fn [indexinfo _]
   (:mask indexinfo)))

(reg-sub
 :indexinfo-mask-show
 :<- [:indexinfo-mask]
 (fn [mask _]
   (:show mask)))

(reg-sub
 :indexinfo-mask-type
 :<- [:indexinfo-mask]
 (fn [mask _]
   (:type mask)))

(reg-sub
 :indexinfo-mask-content
 :<- [:indexinfo-mask]
 (fn [mask _]
   (:content mask)))

(reg-sub
 :share-tip
 (fn [db _]
   (:share-tip db)))

(reg-sub
 :share-tip-show
 :<- [:share-tip]
 (fn [share-tip _]
   (:show share-tip)))

(reg-sub
 :indexinfo-setting
 :<- [:indexinfo]
 (fn [indexinfo _]
   (:setting indexinfo)))

(reg-sub
 :indexinfo-setting-show
 :<- [:indexinfo-setting]
 (fn [setting _]
   (:show setting)))

(reg-sub
 :indexinfo-current-pe
 :<- [:indexinfo-setting]
 (fn [setting _]
   (:pe setting)))

(reg-sub
 :indexinfo-setting-changed?
 :<- [:indexinfo-setting]
 (fn [setting _]
   (:changed? setting)))

(reg-sub
 :indexinfo-history-pe
 :<- [:indexinfo-data-summary]
 (fn [summary _]
   (->
    (get-in summary [:indicator :pe])
    (select-keys [:max :min :med]))))

(reg-sub
 :indexinfo-setting-up
 :<- [:indexinfo-setting]
 (fn [setting _]
   (:up setting)))

(reg-sub
 :indexinfo-setting-down
 :<- [:indexinfo-setting]
 (fn [setting _]
   (:down setting)))

(reg-sub
 :indexinfo-setting-up-pe
 :<- [:indexinfo-setting-up]
 (fn [up _]
   (:pe up)))

(reg-sub
 :indexinfo-setting-down-pe
 :<- [:indexinfo-setting-down]
 (fn [down _]
   (:pe down)))

(reg-sub
 :indexinfo-setting-up-valid?
 :<- [:indexinfo-setting-up]
 (fn [up _]
   (:valid? up)))

(reg-sub
 :indexinfo-setting-down-valid?
 :<- [:indexinfo-setting-down]
 (fn [down _]
   (:valid? down)))

(reg-sub
 :indexinfo-setting-up-has-rule?
 :<- [:indexinfo-setting-up]
 (fn [up _]
   (:has-rule? up)))

(reg-sub
 :indexinfo-setting-down-has-rule?
 :<- [:indexinfo-setting-down]
 (fn [down _]
   (:has-rule? down)))

(reg-sub
 :secuinfo-chart-MAs
 :<- [:secuinfo-chart]
 :<- [:secuinfo-chart-option]
 (fn [[chart option] _]
   (let [index (:ma-index chart)
         cate  (:cate chart)]
     (some-> option
             (get-in [cate :k :use-data])
             (get index)
             (get 7)))))

(reg-sub
 :secuinfo-fuquantype
 :<- [:secuinfo-chart]
 (fn [chart _]
   (:fuquantype chart)))

(reg-sub
 :secuinfo-ktype
 :<- [:secuinfo-chart]
 (fn [chart _]
   (:ktype chart)))

(reg-sub
 :secuinfo-data-verify
 :<- [:secuinfo-data]
 (fn [data _]
   (:verify data)))

(reg-sub
 :secuinfo-verify-status
 :<- [:secuinfo-data-verify]
 (fn [verify _]
   (:status verify)))

(reg-sub
 :indexinfo-ingredients
 :<- [:indexinfo-data]
 (fn [data _]
   (:ingredients data)))

(reg-sub
 :indexinfo-ingredients-realtime
 :<- [:indexinfo]
 (fn [indexinfo _]
   (:ingredients-realtime indexinfo)))

(reg-sub
 :indexinfo-index-list
 :<- [:indexinfo-data]
 (fn [data _]
   (:indexlist data)))

(reg-sub
 :indexinfo-show-indexlist?
 :<- [:indexinfo]
 (fn [indexinfo _]
   (:show-indexlist? indexinfo)))

(reg-sub
 :indexinfo-intro
 :<- [:indexinfo]
 (fn [indexinfo _]
   (:intro indexinfo)))

(reg-sub
 :indexinfo-unlocks-info
 :<- [:indexinfo-data]
 (fn [data _]
   (:unlocks data)))

(reg-sub
 :ui
 (fn [db _]
   (:ui db)))

(reg-sub
 :ui-secuinfo
 :<- [:ui]
 (fn [ui _]
   (:secuinfo ui)))

(reg-sub
 :ui-indexinfo
 :<- [:ui]
 (fn [ui _]
   (:indexinfo ui)))

(reg-sub
 :secuinfo-secondary-nav-y
 :<- [:ui-secuinfo]
 (fn [ui-secuinfo _]
   (get-in ui-secuinfo [:secondary-nav :y])))

(reg-sub
 :indexinfo-secondary-nav-y
 :<- [:ui-indexinfo]
 (fn [ui-indexinfo _]
   (get-in ui-indexinfo [:secondary-nav :y])))

(reg-sub
 :secuinfo-secuinfo-nav-y
 :<- [:ui-secuinfo]
 (fn [ui-secuinfo _]
   (get-in ui-secuinfo [:secuinfo-nav :y])))

(reg-sub
 :indexinfo-indexinfo-nav-y
 :<- [:ui-indexinfo]
 (fn [ui-indexinfo _]
   (get-in ui-indexinfo [:indexinfo-nav :y])))

(reg-sub
 :secuinfo-main-height
 :<- [:ui-secuinfo]
 (fn [ui-secuinfo _]
   (- (get-in ui-secuinfo [:secondary-nav :y])
      (get-in ui-secuinfo [:secuinfo-main :y]))))

(reg-sub
 :indexinfo-main-height
 :<- [:ui-indexinfo]
 (fn [ui-indexinfo _]
   (- (get-in ui-indexinfo [:secondary-nav :y])
      (get-in ui-indexinfo [:indexinfo-main :y]))))

(reg-sub
 :secuinfo-main-content-height
 :<- [:ui-secuinfo]
 (fn [ui-secuinfo _]
   (- (get-in ui-secuinfo [:secondary-nav :y])
      (get-in ui-secuinfo [:main-content :y]))))

(reg-sub
 :indexinfo-main-content-height
 :<- [:ui-indexinfo]
 :<- [:indexinfo-active-noodle]
 (fn [[ui-indexinfo active] _]
   (if (= :ingredients active)
     (- (get-in ui-indexinfo [:indexinfo-nav :y])
        (get-in ui-indexinfo [:main-content :y]))
     (- (get-in ui-indexinfo [:secondary-nav :y])
        (get-in ui-indexinfo [:main-content :y])))))

(reg-sub
 :secuinfo-chart-info-switch-bar-height
 :<- [:ui-secuinfo]
 (fn [ui-secuinfo _]
   (get-in ui-secuinfo [:chart-info-switch-bar :height])))

(reg-sub
 :indexinfo-chart-info-switch-bar-height
 :<- [:ui-indexinfo]
 (fn [ui-indexinfo _]
   (get-in ui-indexinfo [:chart-info-switch-bar :height])))

(reg-sub
 :secuinfo-chart-height
 :<- [:ui-secuinfo]
 (fn [ui-secuinfo _]
   (- (get-in ui-secuinfo [:secondary-nav :y])
      (get-in ui-secuinfo [:main-content :y])
      (get-in ui-secuinfo [:chart-nav :height])
      (get-in ui-secuinfo [:chart-info-switch-bar :height])
      (* 0.875 js/rem))))

(reg-sub
 :indexinfo-chart-height
 :<- [:ui-indexinfo]
 (fn [ui-indexinfo _]
   (let [secondary-nav-y              (get-in ui-indexinfo [:secondary-nav :y])
         main-content-y               (get-in ui-indexinfo [:main-content :y])
         chart-nav-height             (get-in ui-indexinfo [:chart-nav :height])
         chart-info-switch-bar-height (get-in ui-indexinfo [:chart-info-switch-bar :height])]
     (when (and secondary-nav-y
                main-content-y
                chart-nav-height
                chart-info-switch-bar-height)
       ;; debug
       (doto
           (- secondary-nav-y
              main-content-y
              chart-nav-height
              chart-info-switch-bar-height
              (* 0.875 js/rem))
         #_(prn "@@@@@@calculating indexinfo-chart-height"
                secondary-nav-y
                main-content-y
                chart-nav-height
                chart-info-switch-bar-height))))))

(reg-sub
 :bottom-nav-y
 :<- [:ui]
 (fn [ui _]
   (get-in ui [:bottom-nav :y])))

(reg-sub
 :indexinfo-chart-kswitch
 :<- [:indexinfo-chart]
 (fn [chart _]
   (:kswitch chart)))

(reg-sub
 :indexinfo-chart-kswitch-show
 :<- [:indexinfo-chart-kswitch]
 (fn [kswitch _]
   (:show kswitch)))

(reg-sub
 :indexinfo-chart-kswitch-MAs
 :<- [:indexinfo-chart-kswitch]
 (fn [kswitch _]
   (:MA kswitch)))

(reg-sub
 :indexinfo-chart-kswitch-PEs
 :<- [:indexinfo-chart-kswitch]
 (fn [kswitch _]
   (:PE kswitch)))

(reg-event-db
 :indexinfo-chart-toggle-kswitch
 indexinfo-interceptors
 (fn [indexinfo _]
   (let [show               (get-in indexinfo [:chart :kswitch :show])
         ktype              (get-in indexinfo [:chart :ktype])
         [ma-5 ma-20 ma-60] (-> (get-in indexinfo [:chart :use-data :k ktype :origin])
                                last (get 7))
         ]
     (-> indexinfo
         (assoc-in [:chart :kswitch :show] (case show
                                             :MA :PE
                                             :PE :MA))
         (update-in [:chart :ktype]
                    (fn switch-pe [k]
                      (let [name-k (name k)]
                        (if (str/ends-with? name-k "pe")
                          (keyword (str/replace name-k #"pe" ""))
                          (keyword (str name-k "pe"))))))
         (assoc-in [:chart :datazoom] nil)))))

(reg-event-db
 :indexinfo-datazoom-set-start-end
 indexinfo-interceptors
 (fn [indexinfo [_ start end start-index end-index]]
   (assoc-in indexinfo [:chart :datazoom]
             {:start       start
              :end         end
              :start-index start-index
              :end-index   end-index})))

(reg-event-db
 :indexinfo-datazoom-clear-start-end
 indexinfo-interceptors
 (fn [indexinfo _]
   (assoc indexinfo [:chart :datazoom] nil)))

(reg-sub
 :indexinfo-chart-datazoom
 :<- [:indexinfo-chart]
 (fn [chart _]
   (:datazoom chart)))

(reg-event-db
 :indexinfo-set-active-k-item
 indexinfo-interceptors
 (fn [indexinfo [_ index]]
   (let [ktype (get-in indexinfo [:chart :ktype])
         show  (get-in indexinfo [:chart :kswitch :show])]
     (cond-> indexinfo
       true         (assoc-in [:chart :kswitch :index] index)
       (= show :MA) (assoc-in [:chart :kswitch :MA]
                              (let [[ma-5 ma-20 ma-60]
                                    (get-in indexinfo [:chart :use-data :k ktype :origin index 7])]
                                {:MA5  ma-5
                                 :MA20 ma-20
                                 :MA60 ma-60}))
       (= show :PE) (assoc-in [:chart :kswitch :PE]
                              (let [ktype (get-in indexinfo [:chart :ktype])
                                    ktype-data (get-in indexinfo [:values :kpe ktype])
                                    ;; 加实时价格时需判断是否插入了实时价格，
                                    ;; 如果插入了实时价格，index 会不同
                                    index index]
                               {:PEmax    (get-in ktype-data [:last50-pe :max])
                                :PEmid    (get-in ktype-data [:last50-pe :mid])
                                :PEmin    (get-in ktype-data [:last50-pe :min])
                                :pricemax (-> ktype-data (get-in [:prices :max]) (get index))
                                :pricemid (-> ktype-data (get-in [:prices :mid]) (get index))
                                :pricemin (-> ktype-data (get-in [:prices :min]) (get index))}))))))

(reg-sub
 :indexinfo-active-k-item
 :<- [:indexinfo-chart-kswitch]
 (fn [kswitch _]
   (:index kswitch)))

(reg-event-db
 :indexinfo-chart-show-yaxis
 indexinfo-interceptors
 (fn [indexinfo [_ show?]]
   (assoc-in indexinfo [:chart :show-y?] show?)))

(reg-sub
 :indexinfo-chart-show-yaxis
 :<- [:indexinfo-chart]
 (fn [chart _]
   (:show-y? chart)))
