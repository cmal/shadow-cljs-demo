(ns doumi.coupon
  (:require [reagent.core :as r]
            [re-frame.core :as rf]
            [doumi.routes :as routes]
            [doumi.log :refer [logger-log]]
            [doumi.helper :refer [base redirect-to set-hash! indexed]]
            [doumi.search-bar :refer [search]]
            [doumi.bottomnav :refer [bottom-nav]]))

(defn coupon-component []
  (let [coupons (rf/subscribe [:doumi-coupon])
        loading (rf/subscribe [:doumi-loading])]
    (r/create-class
     {:display-name "coupon-component"
      :component-will-mount
      (fn []
        (rf/dispatch [:doumi-get-coupon]))
      :reagent-render
      (fn []
        [:div
         {:style {:text-align       "center"
                  :background-color "#fff"}}
         [:div
          {:style {:margin-top     (base 12)
                   :padding-top    (base 30)
                   :padding-bottom (base 25)
                   :font-size      (base 26)
                   :color          "#222"}}
          "我的优惠券"]
         [:div
          {:style {:font-size      (base 24)
                   :color          "#444"
                   :padding-bottom (base 30)}}
          "兑换的优惠券可用于九斗所有付费产品"]
         (when-not @loading
           (if (empty? @coupons)
             [:div
              {:style {:color       "#444"
                       :font-size   (base 24)
                       :padding-top (base 120)}}
              [:img
               {:src   "/wx/img/doumi-blank.png"
                :style {:height (base 230)
                        :width  (base 259)}}]
              [:div
               {:style {:padding-top (base 30)}}
               "抱歉，你还未兑换过优惠券哦~"]
              [:div
               {:style {:padding-top    (base 20)
                        :padding-bottom (base 80)}}
               "点击 "
               [:span
                {:style    {:color           "#284a7d"
                            :text-decoration "underline"}
                 :on-click (fn [e]
                             (do
                               (set-hash! (routes/doumi-doumi-path))
                               (rf/dispatch [:doumi-set-page :use])
                               (rf/dispatch [:doumi-set-use-page :coupon])))}
                "兑换优惠券"]]]
             [:div.coupon-table
              [:div.coupon-table-header
               {:style {:border-top    "1px solid #e2e2e2"
                        :border-bottom "1px solid #e2e2e2"
                        :font-size     (base 24)
                        :height        (base 58)
                        :line-height   (base 58)}}
               [:span
                {:style {:margin-left (base 30)
                         :text-align  "left"
                         :float       "left"
                         :width       (base 150)}}
                "日期"]
               [:span
                {:style {:text-align "right"
                         :width      (base 260)}}
                "兑换金额"]
               [:span
                {:style {:margin-right (base 30)
                         :text-align   "right"
                         :float        "right"}}
                "优惠券"]]
              [:div.coupon-table-content
               (doall
                (for [[index coupon] (indexed @coupons)
                      :let           [{:keys [date doumi promocard]} coupon]]
                  ^{:key index}
                  [:div.coupon-table-row
                   [:span.date date]
                   [:span.doumi doumi]
                   [:a.look-into
                    {:href promocard}
                    "查看"]]))]
              [:a.coupon-table-footer
               [:span
                {:on-click (fn [e]
                             (logger-log "dm.useMyCoupon")
                             (redirect-to "https://shop192168.youzan.com/v2/coupons?kdt_id=18381785&reft=1508899196019&spm=at18381785&sf=wx_sm&from=singlemessage&isappinstalled=0&redirect_count=2"))}
                "使用优惠券"]
               [:span.return
                {:on-click (fn []
                             (set-hash! (routes/doumi-doumi-path))
                             (rf/dispatch [:doumi-set-page :strategy]))}
                "返回我的斗米"]]]))])})))

(defn coupon []
  [:div
   [search]
   [coupon-component]
   [bottom-nav "我的"]])
