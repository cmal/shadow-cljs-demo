(ns doumi.ua
  (:require [clojure.string :as str]))

(defonce ua (.-userAgent js/navigator))

(defonce inWechat
  ;;是否在微信内打开
  (str/includes? ua "micromessenger"))

(defonce inQQ
  ;;是否在QQ内打开
  (str/includes? ua "YYB_D QQ"))

(defonce isUC
  ;;是否是uc浏览器
  (str/includes? ua "ucbrowser"))

(defonce isQQ
  ;;是否是QQ浏览器
  (str/includes? ua "qqbrowser"))

(defonce isMI
  ;;是否是小米浏览器
  (str/includes? ua "miuibrowser"))

(defonce isAndroid
  (str/includes? ua "Android"))

(defonce isIphone
  (str/includes? ua "iPhone"))

(defonce isSB
  (or inWechat inQQ isQQ isUC isMI))

;; 微信和 QQ 会伪装成 chrome 和 safari
(defonce isChrome
  (and (not isSB)
       (str/includes? ua "chrome")))

(defonce isSafari
  (and (not isSB)
       isIphone
       (str/includes? ua "safari")))
