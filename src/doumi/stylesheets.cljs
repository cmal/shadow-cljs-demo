(ns doumi.stylesheets
  (:require [clojure.string :as str]
            [garden.core :refer [css]]
            [garden.selectors :as gs]
            [garden.stylesheet :refer [at-keyframes]]
            [doumi.helper :refer [base combine-base]]
            ))

(gs/defpseudoelement placeholder)

(defn gen-stylesheets
  []
  (css
   {:vendors ["moz" "webkit"]}
   [:body {:-webkit-tap-highlight-color "rgba(0, 0, 0, 0)"}]
   [:div.suggestion-item
    {:position      "relative"
     :height        "1.5rem"
     :line-height   "1.5rem"
     :border-bottom "1px solid #eee"
     :font-size     ".7rem"}
    [:&:last-child
     {:border "none"}]]
   [:body {:margin     0
           :background "#f3f2f2"}
    :#app {:padding-bottom (base 80)}]
   [:div.doumi-strategy
    {:background-color "#fff"
     :padding-bottom   (base 40)
     :margin-top       (base 12)
     :color            "#444"}
    [(gs/& (gs/nth-child 1))
     {:padding-top (base 10)}]
    [:div.title
     {:font-size      (base 24)
      :padding-top    (base 5)
      :padding-left   (base 20)
      :padding-bottom (base 5)}
     [:a
      {:text-decoration "underline"
       :color           "#284a7d"}]]
    [:div.desc
     {:font-size     (base 22)
      :padding-left  (base 40)
      :padding-right (base 20)}
     [:i {:font-size  (base 28)
          :position   "absolute"
          :margin-top (base 2)}]
     [:img {:border "1px solid #dbdbdb"
            :width  (base 410)
            :height (base 71)}]]
    [:div.desc-line
     [:i {:font-size  (base 28)
          :position   "absolute"
          :margin-top (base 2)}]]]
   [:div.phone-panel-main
    ;; if (gs/defpseudoelement placeholder)
    ;; you can (css [:input [(gs/& placeholder) {}]])
    ;; or (css [(gs/input placeholder) {:width "32px"}])
    ;; or do not defpseudoelement,
    ;; just ["input::placeholder" {...}]
    [(gs/input placeholder)
     {:color        "#999"
      :font-size    (base 24)
      :padding-left (base 16)}]]
   [:div.log-item
    {:height        (base 41)
     :padding-top   (base 4)
     :position      "relative"
     :border-bottom "1px solid #eee"
     :font-size     (base 24)}
    [:span.date
     {:margin-left (base 20)}]
    [:span.content
     {:position    "absolute"
      :left        (base 160)
      :max-width   (base 260)
      :white-space "nowrap"
      :overflow    "hidden"}]
    [:span.wami
     {:position   "absolute"
      :left       (base 420)
      :width      (base 52)
      :text-align "right"}]
    [:span.sami
     {:position   "absolute"
      :left       (base 492)
      :width      (base 52)
      :text-align "right"}]
    [:span.balance
     {:position   "absolute"
      :left       (base 564)
      :width      (base 52)
      :text-align "right"}]]
   [:div.coupon-table-content
    {:font-size (base 22)}
    [:div.coupon-table-row
     {:height        (base 45)
      :line-height   (base 45)
      :border-bottom "1px solid #e2e2e2"}
     [:span.date
      {:margin-left (base 30)
       :text-align  "left"
       :float       "left"
       :width       (base 150)}]
     [:span.doumi
      {:text-align "right"
       :width      (base 260)}]
     [:a.look-into
      {:margin-right    (base 30)
       :text-align      "right"
       :float           "right"
       :color           "#284a7d"
       :text-decoration "underline"}]]]
   [:a.coupon-table-footer
    {:display         "block"
     :color           "#284a7d"
     :font-size       (base 24)
     :text-decoration "underline"
     :padding-top     (base 90)
     :padding-bottom  (base 80)}
    [:span.return
     {:margin-left (base 160)}]]
   [:div.suggestions
    [:div.suggestion-item
     [:span.suggestion-name
      {:float "left"
       :width "25%"}]
     [:span.suggestion-id
      {:float        "left"
       :padding-left ".6rem"}]
     [:span.add-wrapper
      {:padding-left "1rem"
       :right        0
       :float        "right"}
      [:span.add
       {:color "#284a7d"}]
      [:span.added
       {:color "#666"}]
      [:span.icon-wrapper
       {:position "relative"
        :top      (base 7)}]
      [:i {:font-size (base 38)}]]]]
   [:div.group-main
    [:div.annc-date-item
     {:padding       (str/join " " [(base 12) (base 18)])
      :position      "relative"
      :border-bottom "1px solid #f2f3f3"}
     [:div.text-line
      {:line-height ".8rem"
       :padding     "0 0 0 1rem"
       :height      "1.6rem"
       :font-size   ".6rem"}]]]
   [:div.all-main
    {:background    "#fff"
     :padding       ".3rem .45rem"
     :height        (base 64)
     :position      "relative"
     :border-bottom "1px solid #f2f3f3"}
    [:div.text-line
     {:line-height (base 32)
      :padding     "0 0 0 1rem"
      :height      (base 64)}]]
   [:div.annc-main
    [:i
     {:position  "absolute"
      :left      ".45rem"
      :top       ".3rem"
      :font-size ".8rem"}]]
   [:div.annc-unread-status
    {:position      "absolute"
     :bottom        ".3rem"
     :right         ".9rem"
     :background    "#999"
     :border-radius "2px"
     :font-size     "12px"
     :padding       "1px 2px"
     :color         "#f2f3f3"
     :margin-left   "2%"}]
   (at-keyframes
    "loader"
    [:to
     {:background-position "-600px 0"}])
   [:div.divid-group
    {:background-color "#fff"
     :margin-top       (base 18)}]
   [:div.divid-item
    {:padding     (combine-base 12 18)
     :line-height (base 32)
     :height      (base 64)
     :font-size   (base 24)
     :color       "#666"
     :position    "relative"}]
   [:div.divid-title
    {:padding-left (base 40)}]
   [:div.divid-item
    {:border-top "1px solid #f2f3f3"}
    [[(gs/& (gs/nth-child "2"))
      {:border-top 0}]]
    [:i.icon-dividend
     {:position  "absolute"
      :left      (base 18)
      :top       (base 12)
      :font-size (base 32)}]]
   [:div.perf-group
    {:background-color "#fff"
     :margin-bottom    (base 18)}]
   [:div.perf-item
    {:padding       (combine-base 12 18)
     :line-height   (base 32)
     :height        (base 64)
     :font-size     (base 24)
     :color         "#666"
     :position      "relative"
     :border-bottom "1px solid #f2f3f3"}
    [:div
     {:padding-left (base 40)
      :color        "#666"}]]
   [:div.group-header
    {:color         "#284a7d"
     :line-height   (base 24)
     :font-size     (base 24)
     :padding       (combine-base 12 18 10)
     :border-bottom "1px solid #f2f3f3"}]
   [:div.perf-group
    [:i
     {:position  "absolute"
      :left      (base 18)
      :top       (base 12)
      :font-size (base 32)
      :color     "#284a7d"}]]
   [:div.annc-nav-bar-item
    {:height (base 62)
     :width  (base 160)
     :float  "left"}
    [:div
     {:height      (base 60)
      :line-height (base 60)}]
    [:div.annc-nav-decoration
     {:top    (base 60)
      :height (base 2)}]]
   [:div.nextweek-group
    {:background-color "#fff"
     :margin-bottom    (base 18)}]
   [:div.nextweek-item
    {:border-top "1px solid #f2f3f3"
     :color      "#666"
     :position   "relative"
     :padding    (combine-base 12 18)
     :font-size  (base 24)}
    [[(gs/& (gs/nth-child "2"))
      {:border-top 0}]]
    [:i
     {:position  "absolute"
      :left      (base 18)
      :top       (base 12)
      :font-size (base 32)}]
    [:div
     {:height       (base 64)
      :line-height  (base 32)
      :padding-left (base 40)}]]
   [:span.detail
    {:float    "right"
     :color    "#284a7d"
     :position "absolute"
     :right    (base 18)
     :bottom   (base 12)}]
   [:div.headline-item
    {:height      "1.5rem"
     :line-height "1.5rem"}
    [:span
     {:padding-left ".5rem"}]]
   [:div.secondary-nav
    [:div.volume {:width "50%"}]
    [:div.zdf {:width "50%"}]
    [:div.pe {:width "20%"}]
    [:div.pb {:width "20%"}]
    [:div.ps {:width "20%"}]
    [:div.rgr {:width "20%"}]
    [:div.egr {:width "20%"}]
    [:div.totalmv {:width "20%"}]
    [:div.freefloatmv {:width "20%"}]
    [:div.dividend {:width "33.33%"}]
    [:div.bonus {:width "33.33%"}]
    [:div.dividend-index {:width "50%"}]
    [:div.unlock-index {:width "50%"}]
    [:div.unlock {:width "33.33%"}]
    [:div.secondary-item
     {:float       "left"
      :padding-top (base 8)}
     [:div.decoration
      {:position "absolute"
       :height   (base 4)}]
     [:div.text
      {:height      ".8rem"
       :line-height ".8rem"}]
     [:div.value
      {:height      ".9rem"
       :line-height ".9rem"}]
     [:div.one-line-text
      {:padding-top    ".4rem"
       :height         ".8rem"
       :padding-bottom ".4rem"}]]]
   [:div.no-content
    {:text-align "center"}
    [:img
     {:width "60%"}]]
   [:div.row-container
    {:color       "#333"
     :line-height (base 60)
     :font-size   (base 24)
     :position    "relative"
     :z-index     -1}
    [:div.row
     {:padding-left  ".5rem"
      :padding-right ".5rem"
      :height        (base 60)}
     [(gs/& (gs/first-child))
      {:padding-top ".25rem"}]
     [(gs/& (gs/last-child))
      {:padding-bottom ".25rem"}]
     [:a {:color   "inherit"
          :display "block"}]
     [:span.percent
      {:float "right"}]
     [:span.date
      {:display "block"
       :float   "left"
       :width   "30%"}]
     [:span.title
      {:display       "block"
       :float         "left"
       :width         "58%"
       :overflow      "hidden"
       :text-overflow "ellipsis"
       :white-space   "nowrap"}]
     [:span.unreadstatus
      {:background    "#999"
       :border-radius "2px"
       :font-size     "12px"
       :padding       "1px 2px"
       :color         "#f2f3f3"
       :margin-left   "2%"}]]]
   [:div.chart-nav
    {:height           "1.5rem"
     :font-size        ".8rem"
     :background-color "#f7f7f7"}
    [:div.type-item
     {:display     "block"
      :float       "left"
      :width       "25%"
      :height      "1.5rem"
      :line-height "1.5rem"
      :color       "#4b4b4b"
      :font-size   (base 24)
      :text-align  "center"}]]
   [:div#dividend-tip
    {:background-color "#666"
     :border-radius    "4px"
     :padding          "4px"
     :box-shadow       "4px 2px 2px #777"}]
   [:div.ingredients-group
    {:height        "1.5rem"
     :background    "#f3f3f3"
     :line-height   "1.5rem"
     :font-size     ".65rem"
     :border-bottom "1px solid #eaeaea"}]
   [:div.ingredients-item-0
    {:float      "left"
     :width      "7%"
     :text-align "right"}]
   [:div.ingredients-item-1
    {:float      "left"
     :width      "23%"
     :text-align "center"}]
   [:div.ingredients-item-2
    {:float      "left"
     :width      "25%"
     :text-align "center"}]
   [:div.ingredients-item-3
    {:float "left"
     :width "25%"}]
   [:div.ingredients-item-4
    {:float "left"
     :width "20%"}]
   [:div.indexinfo-setting
    [:div.current
     {:height       "1.5rem"
      :line-height  "1.5rem"
      :padding-left ".5rem"
      :font-size    ".6rem"
      :color        "#666"}
     [:span
      {:margin-right (base 32)}]]
    [:div.history
     {:background    "#fff"
      :height        "1.5rem"
      :line-height   "1.5rem"
      :padding-left  ".5rem"
      :font-size     ".6rem"
      :color         "#999"
      :border-bottom "1px solid #eee"}
     [:span
      {:margin-right (base 25)}]]
    [:div.toggle-btns
     {:float         "right"
      :padding-right ".8rem"
      :padding-top   ".5rem"}]
    [:div.setting-group
     {:font-size     ".6rem"
      :background    "#fff"
      :margin-bottom ".2rem"}
     [:div.setting-line
      {:height        "1.8rem"
       :line-height   "1.8rem"
       :border-bottom "1px solid #eee"
       :padding-left  ".5rem"}
      [:div.desc
       {:width      "5rem"
        :float      "left"
        :text-align "right"}]
      [:div.multi
       {:float "left"}]
      [:div.select-btns
       {:float       "left"
        :padding-top ".35rem"}
       [:div.select-btn
        {:border        "1px solid #aaa"
         :border-radius "2px"
         :text-align    "center"
         :width         (base 69)
         :height        (base 40)
         :line-height   (base 38)
         :font-size     (base 20)
         :margin-right  (base 20)
         :float         "left"}]]
      [:div.input
       {:float "left"
        :width (base 264)}
       [:input
        {:border-radius "4px"
         :width         (base 240)
         :padding-left  (base 12)
         :height        (base 50)
         :line-height   (base 48)
         :font-size     (base 24)}]]]]]
   [:div.inner
    {:padding "0 .25rem .25rem"}]
   [:div.pay-dialog
    {:position         "relative"
     :background-color "#fff"
     :padding-top      (base 20)
     :color            "#333"
     :font-size        (base 20)
     :z-index          200
     :text-align       "left"}
    [:div.account-info
     {:color         "#fb6969"
      :font-size     (base 24)
      :padding-left  (base 10)
      :padding-right (base 20)}]
    [:div.logo
     {:padding-left  (base 20)
      :padding-right (base 20)
      :height        (base 28)}
     [:img
      {:height (base 28)
       :width  (base 74)}]]
    [:div.counts-desc
     {:padding-left  (base 20)
      :padding-right (base 20)
      :margin-top    (base 18)}]
    [:div.share
     {:padding-left  (base 20)
      :padding-right (base 20)
      :margin-top    (base 12)}
     [:span.share-page-icon
      {:font-size (base 32)
       :color     "#0aba17"}]
     [:img {:height (base 32)}]]
    [:div.kai-tong
     {:margin-top       (base 12)
      :padding-left     (base 20)
      :padding-right    (base 20)
      :padding-bottom   (base 25)
      :background-color "#efefef"}
     [:div.text
      {:padding-top (base 20)}]
     [:a {:color "#333"}]
     [:div.kt-item
      {:position      "relative"
       :margin-top    (base 11)
       :height        (base 62)
       :width         (base 436)
       :font-size     (base 18)
       :border        "1px solid #ffab36"
       :border-radius "4px"}
      [:div#season
       {:background-color "#ffe9cb"}]
      [:div.decorate
       {:position "absolute"
        :top      "-1px"
        :left     "-1px"}
       [:img
        {:border-top-left-radius "4px"
         :height                 (base 30)
         :width                  (base 30)}]]
      [:div.left
       {:position "relative"
        :float    "left"
        :width    (base 344)}
       [:div.name
        {:float        "left"
         :padding-left (base 26)
         :padding-top  (base 16)}]
       [:div.price
        {:float         "right"
         :color         "#fa5c5c"
         :padding-right (base 10)
         :padding-top   (base 16)}]]
      [:div.right
       {:float       "left"
        :border-left "1px solid #ffab36"
        :margin-left "-1px"
        :margin-top  (base 6)}
       [:div.button
        {:background-color "#ffab36"
         :border-radius    "2px"
         :height           (base 32)
         :width            (base 72)
         :margin           (combine-base 8 10)
         :padding-top      (base 2)
         :color            "#fff"
         :text-align       "center"}]]]]
    [:div.tel-form
     {:padding-left  (base 20)
      :padding-right (base 20)
      :margin-top    (base 12)}
     [:div.tel-input
      {:margin-top (base 20)}
      [:input
       {:border "2px solid #ffab36"}]
      [:div.button-submit
       {:margin           (str (base 18) " auto 0")
        :height           (base 46)
        :width            (base 130)
        :background-color "#ffab36"
        :color            "#fff"
        :padding-top      (base 16)
        :border-radius    "2px"
        :text-align       "center"
        :font-size        (base 24)}]]
     [:div.tel-check
      {:margin-top     (base 10)
       :height         (base 32)
       :font-size      (base 24)
       :color          "#333"
       :text-align     "center"
       :padding-bottom (base 10)}]]]
   [:div.indexinfo-intro
    {:height         "100%"
     :width          "100%"
     :position       "fixed"
     :top            0
     :left           0
     :pointer-events "none"
     :color          "#fff"
     :z-index        200}
    [:div.mask-search
     {:height         "2.25rem"
      :pointer-events "auto"
      :background     "rgba(0,0,0,.6)"}]]
   [:div.indexlist
    [:div.indexlist-item
     {:background    "#fff"
      :height        (base 50)
      :width         (base 350)
      :padding       (combine-base 0 12)
      :line-height   (base 50)
      :font-size     (base 26)
      :color         "#222"
      :border-bottom "1px solid #eee"}
     [:div.index-name
      {:width (base 120)
       :float "left"}]
     [:div.index-id
      {:padding-left (base 20)
       :float        "left"}]]]
   [:div.tooltip-main
    {:font-size (base 18)
     :width     (base 210)
     :color     "rgba(255,255,255,.8)"}]
   [:div.tooltip-row
    {:padding-bottom (base 10)
     :height         (base 20)
     :line-height    (base 20)}]
   [:div.tooltip-left
    {:float "left"
     :width "50%"}]
   [:div.tooltip-right
    {:float "left"
     :width "50%"}]
   [:div#hl-row
    {:color "#fff"}]
   [:div.chart-legend
    {:margin-left   (base 8)
     :margin-right  (base 4)
     :margin-top    (base 12)
     :float         "left"
     :width         (base 16)
     :height        (base 16)
     :border-radius "50%"}]
   [:div.chart-legend-text
    {:float "left"}]))

(defn stylesheets
  []
  ;; use garden to generate css string and wrap in a script tag
  ;; all script tag will be applied globally
  [:style (gen-stylesheets)])
