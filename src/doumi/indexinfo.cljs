(ns doumi.indexinfo
  (:require
   #_[rid3.core :as rid3]
   [reagent.core :as r]
   [re-frame.core :as rf]
   [clojure.string :as str]
   [doumi.search-bar :refer [search]]
   [doumi.log :refer [logger-log]]
   [doumi.helper :refer [stockid2secucode icon-key num-name
                         base base-n combine-base indexed format-date-slash
                         get-stock-title format-delta-percent
                         set-hash! precision-round redirect-to
                         format-number-chn-2]]
   [doumi.secuinfo-config :refer [indexinfo-noodles-data headline-cnt
                                  k-chart-nav-data line-chart-nav-data
                                  get-volume-color get-zdf-color
                                  indexinfo-nav-data indexinfo-mask-img-data
                                  zd-color ma-colors indexinfo-line-types]]
   [doumi.pos :refer [screen screen-height doc-width]]
   [doumi.weixin :as wx]
   [doumi.util :refer [deep-merge]]
   [doumi.common :refer [no-content secondary-separater
                         secondary-decoration secondary-background-color
                         share-tip toggle-btn content-separator
                         msg-capital-content-rows chart-legend]]
   [doumi.charts :refer [indexinfo-pe-chart
                         indexinfo-pb-chart
                         indexinfo-ps-chart
                         indexinfo-totalmv-chart
                         indexinfo-freefloatmv-chart
                         indexinfo-dividend-chart
                         indexinfo-fenshi-chart
                         indexinfo-k-day-chart
                         indexinfo-k-week-chart
                         indexinfo-k-month-chart
                         indexinfo-k-daype-chart
                         indexinfo-k-weekpe-chart
                         indexinfo-k-monthpe-chart]]
   [doumi.routes :as routes]))


(defn index-title
  []
  (let [name          (rf/subscribe [:indexinfo-name])
        id            (rf/subscribe [:indexinfo-id])
        current-price (rf/subscribe [:indexinfo-current-price])
        delta-percent (rf/subscribe [:indexinfo-delta-percent])
        has-reminder? (rf/subscribe [:indexinfo-has-reminder?])
        intro         (rf/subscribe [:indexinfo-intro])
        indexlist     (rf/subscribe [:indexinfo-index-list])]
    (fn []
      [:div#stock-title.stock-title
       {:style {:position         "relative"
                :font-size        ".65rem"
                :background-color "#fff"
                :padding-left     ".5rem"
                :padding-right    ".5rem"
                :margin-top       ".3rem"
                :border-bottom    "1px solid #e2e2e2"
                :height           (base 58)
                :line-height      (base 58)}}
       [:span.stock-name
        {:style    {:display "inline-block"}
         :on-click (fn [e]
                     (rf/dispatch [:indexinfo-show-indexlist true])
                     (logger-log "stockIndex.dropdown"))}
        (get-stock-title @name @id)
        (when-not (empty? @indexlist)
          [:span
           {:style {:display   "inline-block"
                    :transform "rotate(-90deg)"}}
           [:i.icon.icon-angle-left]])]
       [:span.current-price
        {:style {:margin-left "1rem"
                 :color       (cond
                                (pos? @delta-percent) (:pos zd-color)
                                (neg? @delta-percent) (:neg zd-color)
                                :else                 (:zero zd-color))}}
        [:span.price (or (some-> @current-price (.toFixed 2)) "--")]
        [:span.percent
         {:style {:margin-left "1rem"}}
         (format-delta-percent @delta-percent)]]
       [:span.indexinfo-setting
        {:style    {:float      "right"
                    :color      (if @has-reminder? "#666" "#284a7d")
                    :margin-top ".1rem"}
         :on-click (fn [e]
                     (rf/dispatch [:indexinfo-show-setting])
                     (logger-log "stockIndex.remindPe")
                     (when (= :clock @intro)
                       (rf/dispatch [:indexinfo-set-intro :setting-choose])
                       (logger-log "stockIndex.boot.clock")
                       #_(set-hash!
                          (routes/indexinfo-url
                           {:id    @id
                            :name  @name
                            :intro "setting-choose"})))
                     (logger-log "stockIndex.remindPe"))}
        [(icon-key "clock-indexinfo")
         {:style {:font-size (base 28)
                  :color     (if @has-reminder? "#ffab35" "#284a7d")}}]]])))

(defn index-comment
  ;; 一句话信息
  []
  (let [comment (rf/subscribe [:indexinfo-comment])]
    [:div.stock-stars
     {:style {:position         "relative"
              :padding-left     ".5rem"
              :height           (base 58)
              :background-color "#fff"}}
     [:div.stars
      {:style
       {:float       "left"
        :font-size   (base 24)
        :line-height (base 58)}}
      [:span @comment]]]))

(defn indexinfo-noodles
  []
  (let [active-noodle (rf/subscribe [:indexinfo-active-noodle])]
    (fn []
      [:div.noodles
       {:style {:position         "relative"
                :text-align       "center"
                :font-size        ".65rem"
                :color            "#284a7d"
                :background-color "#fff"
                :margin-top       ".3rem"
                :height           "2.4rem"}}
       (doall
        (for [[index item] (indexed indexinfo-noodles-data)
              :let         [noodle (get indexinfo-noodles-data index)]]
          ^{:key index}
          [:div
           (when-not (zero? index)
             [:div.noodle-separator
              {:style {:position   "absolute"
                       :width      1
                       :height     "2.4rem"
                       :top        0
                       :left       (str (* 25 index) "%")
                       :background "#efefef"
                       :z-index    1}}])
           [:div.noodle-item
            {:style    {:position   "relative"
                        :width      "25%"
                        :display    "inline-block"
                        :float      "left"
                        :height     "2.4rem"
                        :background (if (= @active-noodle (:name noodle))
                                      "#f2f3f3"
                                      "#fff")}
             :on-click (fn []
                         (rf/dispatch [:indexinfo-set-active-noodle (:name noodle)])
                         (logger-log (str "stockIndex.noodle." (name (:name noodle)))))}
            [:div.noodle-decoration
             {:style {:position   "relative"
                      :height     ".1rem"
                      :background (if (= @active-noodle (:name noodle))
                                    "#ffab35"
                                    "#fff")}}]
            [:div.noodle-name
             {:style {:padding-top ".6rem"}}
             (:text noodle)]]]))])))

(defn indexinfo-secondary-tech
  []
  (let [tech-info   (rf/subscribe [:indexinfo-secondary-nav-tech])
        tech-active (rf/subscribe [:indexinfo-tech-active])]
    (fn []
      (let [{:keys [volume zdf newhigh]} @tech-info]
        [:div.secondary-tech
         [:div.volume.secondary-item
          {:style    {:background (secondary-background-color (= @tech-active :volume))
                      :color      (get-volume-color (:color volume))}
           :on-click (fn [e]
                       (rf/dispatch [:indexinfo-set-tech-active :volume])
                       (logger-log "stockIndex.tech.volume"))}
          [:div.text
           (:text volume)]
          [:div.value
           (:delta volume)]
          [secondary-decoration 50 (= @tech-active :volume)]]
         [secondary-separater 50]
         [:div.zdf.secondary-item
          {:style    {:background  (secondary-background-color (= @tech-active :zdf))
                      :color       (if (:text newhigh) "#666" (get-zdf-color (:color zdf)))
                      :font-weight (if (:text newhigh) "bold" "normal")}
           :on-click (fn [e]
                       (rf/dispatch [:indexinfo-set-tech-active :zdf])
                       (logger-log "stockIndex.tech.zdf"))}
          [:div.text
           (:text zdf)]
          [:div.value
           (:delta zdf)]
          [secondary-decoration 50 (= @tech-active :zdf)]]]))))

(defn indexinfo-secondary-fundamental
  []
  (let [active      (rf/subscribe [:indexinfo-fundamental-active])
        fundamental (rf/subscribe [:indexinfo-fundamental-values])]
    (fn []
      (let [{:keys [pe pb ps totalmv freefloatmv]} @fundamental]
        [:div.secondary-fundamental
         [:div.pe.secondary-item
          {:style    {:background (secondary-background-color (= @active :pe))}
           :on-click (fn [e]
                       (rf/dispatch [:indexinfo-set-fundamental-active :pe])
                       (logger-log "stockIndex.fundamental.pe"))}
          [:div.text "PE"]
          [:div.value (or (some-> pe :val (.toFixed 1)) "--")]
          [secondary-decoration 20 (= @active :pe)]]
         [secondary-separater 20]
         [:div.pb.secondary-item
          {:style    {:background (secondary-background-color (= @active :pb))}
           :on-click (fn [e]
                       (rf/dispatch [:indexinfo-set-fundamental-active :pb])
                       (logger-log "stockIndex.fundamental.pb"))}
          [:div.text "PB"]
          [:div.value (or (some-> pb :val (.toFixed 1)) "--")]
          [secondary-decoration 20 (= @active :pb)]]
         [secondary-separater 40]
         [:div.ps.secondary-item
          {:style    {:background (secondary-background-color (= @active :ps))}
           :on-click (fn [e]
                       (rf/dispatch [:indexinfo-set-fundamental-active :ps])
                       (logger-log "stockIndex.fundamental.ps"))}
          [:div.text "PS"]
          [:div.value (or (some-> ps :val (.toFixed 1)) "--")]
          [secondary-decoration 20 (= @active :ps)]]
         [secondary-separater 60]
         [:div.totalmv.secondary-item
          {:style    {:background (secondary-background-color (= @active :totalmv))}
           :on-click (fn [e]
                       (rf/dispatch [:indexinfo-set-fundamental-active :totalmv])
                       (logger-log "stockIndex.fundamental.totalmv"))}
          [:div.text "总市值"]
          [:div.value (str (or (some-> totalmv (.toFixed 1)) "--") "万亿")]
          [secondary-decoration 20 (= @active :totalmv)]]
         [secondary-separater 80]
         [:div.freefloatmv.secondary-item
          {:style    {:background (secondary-background-color (= @active :freefloatmv))}
           :on-click (fn [e]
                       (rf/dispatch [:indexinfo-set-fundamental-active :freefloatmv])
                       (logger-log "stockIndex.fundamental.freefloatmv"))}
          [:div.text "自由流通"]
          [:div.value (str (or (some-> freefloatmv (.toFixed 1)) "--") "万亿")]
          [secondary-decoration 20 (= @active :freefloatmv)]]]))))

(defn indexinfo-secondary-capital
  []
  (let [active  (rf/subscribe [:indexinfo-capital-active])
        capital (rf/subscribe [:indexinfo-capital-values])]
    (fn []
      (let [{:keys [dividend unlock]} @capital]
        [:div.secondary-capital
         [:div.dividend-index.secondary-item
          {:style    {:background (secondary-background-color (= @active :dividend))}
           :on-click (fn [e]
                       (rf/dispatch [:indexinfo-set-capital-active :dividend])
                       (rf/dispatch [:indexinfo-secondary-set-chart :divirate])
                       (logger-log "stockIndex.capital.divirate"))}
          [:div.text "股息率"]
          [:div.value (or (some-> dividend (.toFixed 2) (str "%")) "--")]
          [secondary-decoration 50 (= @active :dividend)]]
         [secondary-separater 50]
         [:div.unlock-index.secondary-item
          {:style    {:background (secondary-background-color (= @active :unlock))}
           :on-click (fn [e]
                       (rf/dispatch [:indexinfo-set-capital-active :unlock])
                       (logger-log "stockIndex.capital.unlock"))}
          [:div.text "6个月解禁"]
          [:div.value (or (some-> unlock (.toFixed 2) (str "%")) "--")]
          [secondary-decoration 50 (= @active :unlock)]]]))))

(defn indexinfo-ingredients-nav
  []
  [:div.indexinfo-ingredients-nav
   {:style {:height        "1.5rem"
            :width         "100"
            :line-height   "1.5rem"
            :font-size     ".65rem"
            :color         "#284a7d"
            :border-bottom "1px solid #eaeaea"
            :text-align    "center"}}
   [:div.ingredients-item-0
    {:style {:color "#f3f3f3"}}
    "."]
   [:div.ingredients-item-1 "名称"]
   [:div.ingredients-item-2 "当前价"]
   [:div.ingredients-item-3 "当日涨跌幅"]
   [:div.ingredients-item-4 "权重"]])

(defn indexinfo-ingredients-list
  [ingredients]
  (let [realtimes (rf/subscribe [:indexinfo-ingredients-realtime])
        height    (rf/subscribe [:indexinfo-main-content-height])]
    (fn []
      [:div.indexinfo-ingredients-list
       {:style {:height                     @height
                :overflow-y                 "scroll"
                :-webkit-overflow-scrolling "touch"}}
       (doall
        (for [[index item] (indexed (:component @ingredients))
              :let         [{:keys [stockname stockid tradestatus newprice changeratio weight]} item
                            suspend? (or (zero? tradestatus) (nil? newprice) (zero? newprice))
                            rt-info (get @realtimes stockid)
                            rt-newprice (get-in rt-info [:origin :newprice])
                            changeratio (or (:changeratio rt-info) changeratio)
                            price (or rt-newprice newprice)
                            secucode (stockid2secucode stockid)
                            ratio (str
                                   (when (some-> changeratio pos?) "+")
                                   (some-> changeratio (.toFixed 2) (str "%")))
                            zd-key (cond
                                     suspend?          :zero
                                     (> changeratio 0) :pos
                                     (< changeratio 0) :neg
                                     (= changeratio 0) :zero)]]
          ^{:key index}
          [:div.ingredients-group
           {:on-click (fn [e]
                        (logger-log "stockIndex.ingredients.main")
                        (redirect-to (routes/secuinfo-url stockid stockname)))}
           [:div.ingredients-item-0
            (inc index)]
           [:div.ingredients-item-1
            [:div.ingredients-stockname
             {:style {:height      ".9rem"
                      :line-height ".9rem"
                      :color       "#777"
                      :font-size   ".7rem"}}
             stockname]
            [:div.ingredients-id
             {:style {:height      ".6rem"
                      :line-height ".6rem"
                      :color       "#ccc"
                      :font-size   ".4rem"}}
             secucode]]
           [:div.ingredients-item-2
            [:div.ingredients-price
             {:style {:width      "70%"
                      :text-align "right"
                      :color      (zd-color zd-key)}}
             (or (some-> price (.toFixed 2))
                 "--")]]
           [:div.ingredients-item-3
            [:div {:style {:height ".1rem"}}]
            [:div.ingredients-changeratio
             {:style {:border-radius    4
                      :background-color (zd-color zd-key)
                      :color            "#fff"
                      :width            "75%"
                      :margin-left      "10%"
                      :padding-right    ".2rem"
                      :height           "1.3rem"
                      :line-height      "1.3rem"
                      :text-align       "right"}}
             (if suspend?
               "停牌"
               (str
                (when (some-> changeratio pos?) "+")
                (or (some-> changeratio (.toFixed 2) (str "%")) "--")))]]
           [:div.ingredients-item-4
            [:div.ingredients-weight
             {:style {:padding-right ".8rem"
                      :text-align    "right"}}
             (or (some-> weight (.toFixed 2) (str "%"))
                 "--")]]]))])))

(defn indexinfo-secondary-ingredients
  []
  [:div])

(defn indexinfo-secondary-nav
  []
  (let [active-noodle   (rf/subscribe [:indexinfo-active-noodle])
        update-ui       (fn [el]
                          (rf/dispatch [:indexinfo-set-secondary-nav-y
                                        (-> el r/dom-node .getBoundingClientRect .-top)]))
        indexinfo-nav-y (rf/subscribe [:indexinfo-indexinfo-nav-y])]
    (r/create-class
     {:display-name         "indexinfo-nav"
      :component-did-mount  update-ui
      :component-did-update update-ui
      :reagent-render
      (fn []
        [:div.secondary-nav
         {:style {;;:display     "block"
                  :position    "fixed"
                  :top         (some-> @indexinfo-nav-y
                                       (/ js/rem)
                                       (- 2)
                                       (str "rem"))
                  :height      (case @active-noodle
                                 :ingredients "0"
                                 "2rem")
                  :background  "#fff"
                  :width       "100%"
                  :text-align  "center"
                  :line-height (base 24)
                  :font-size   (base 24)
                  :color       "#666"}}
         (case @active-noodle
           :fundamental [indexinfo-secondary-fundamental]
           :capital     [indexinfo-secondary-capital]
           :ingredients [indexinfo-secondary-ingredients]
           [indexinfo-secondary-tech])])})))

(defn indexinfo-ingredients-main
  []
  (let [ingredients (rf/subscribe [:indexinfo-ingredients])]
    (fn []
      [indexinfo-ingredients-list ingredients])))

(defn indexinfo-chart-x-axis-line
  []
  (let [data                  (rf/subscribe [:indexinfo-chart-use-data])
        active                (rf/subscribe [:indexinfo-chart-active])
        cate                  (rf/subscribe [:indexinfo-chart-cate])
        ktype                 (rf/subscribe [:indexinfo-chart-ktype])
        indexinfo-main-height (rf/subscribe [:indexinfo-main-height])
        datazoom              (rf/subscribe [:indexinfo-chart-datazoom])
        kday-data             (rf/subscribe [:indexinfo-kday-chart-use-data])
        kweek-data            (rf/subscribe [:indexinfo-kweek-chart-use-data])
        kmonth-data           (rf/subscribe [:indexinfo-kmonth-chart-use-data])]
    (fn []
      (let [category                        @cate
            category-data                   (when (contains? (set indexinfo-line-types) category)
                                              (get-in @data [:line @active]))
            mid-index                       (quot (count category-data) 2)
            {:keys [start-index end-index]} @datazoom
            middle                          (when (and start-index end-index)
                                              (Math/round (/ (+ start-index end-index) 2)))
            {:keys [left middle right]}     (case category
                                              :k (condp contains? @ktype
                                                   #{:fenshi}         {:left   "9:30"
                                                                       :middle "11:30/13:00"
                                                                       :right  "15:00"}
                                                   #{:day :daype}     {:left   (some->> (get-in @kday-data [:k :origin start-index 0])
                                                                                        format-date-slash)
                                                                       :middle (some->> (get-in @kday-data [:k :origin middle 0])
                                                                                        format-date-slash)
                                                                       :right  (some->> (get-in @kday-data [:k :origin end-index 0])
                                                                                        format-date-slash)}
                                                   #{:week :weekpe}   {:left   (some->> (get-in @kweek-data [:k :origin start-index 0])
                                                                                        format-date-slash)
                                                                       :middle (some->> (get-in @kweek-data [:k :origin middle 0])
                                                                                        format-date-slash)
                                                                       :right  (some->> (get-in @kweek-data [:k :origin end-index 0])
                                                                                        format-date-slash)}
                                                   #{:month :monthpe} {:left   (some->> (get-in @kmonth-data [:k :origin start-index 0])
                                                                                        format-date-slash)
                                                                       :middle (some->> (get-in @kmonth-data [:k :origin middle 0])
                                                                                        format-date-slash)
                                                                       :right  (some->> (get-in @kmonth-data [:k :origin end-index 0])
                                                                                        format-date-slash)})
                                              {:left   (some-> category-data
                                                               ffirst
                                                               format-date-slash)
                                               :middle (some-> category-data
                                                               (nth mid-index)
                                                               first
                                                               format-date-slash)
                                               :right  (some-> category-data
                                                               last
                                                               first
                                                               format-date-slash)})]
        [:div.chart-x-axis-line
         {:style {:position    "absolute"
                  :top         (- @indexinfo-main-height (* 0.875 js/rem))
                  :height      ".875rem" #_ (base 35)
                  :line-height ".875rem"
                  :width       "100%"
                  :font-size   (base 20)
                  :color       "#999"
                  :background  "transparent"}}
         [:div.x-axis-left
          {:style {:width      "33.333%"
                   :text-align "left"
                   :float      "left"}}
          [:span
           {:style {:padding-left "4px"}}
           left]]
         [:div.x-axis-middle
          {:style {:width      "33.333%"
                   :text-align "center"
                   :float      "left"}}
          middle]
         [:div.x-axis-right
          {:style {:width      "33.333%"
                   :text-align "right"
                   :float      "left"}}
          [:span
           {:style {:padding-right "4px"}}
           right]]]))))

(defn indexinfo-chart-info-switch-toggler
  [k]
  (let [intro (rf/subscribe [:indexinfo-intro])]
    [:div
     {:style    {:float         "right"
                 :margin-right  (base 20)
                 :margin-top    (base 1)
                 :width         (base 104)
                 :height        (base 36)
                 :border        "1px solid #284a7d"
                 :border-radius "4px"
                 :color         "#284a7d"
                 :line-height   (base 36)}
      :on-click (fn [e]
                  (rf/dispatch [:indexinfo-chart-toggle-kswitch])
                  (when (= :switch-chart @intro)
                    (rf/dispatch [:indexinfo-set-intro :clock])
                    (logger-log "stockIndex.boot.switch-chart"))
                  (logger-log (case k
                                :MA "stockIndex.kswitch.PE"
                                :PE "stockIndex.kswitch.MA")))}
     [:span
      {:style {:margin-left (base 8)}}
      (case k
        :MA "均线"
        :PE "估值线")]
     [:span
      {:style {:margin-right (base 8)
               :float        "right"}}
      [:i.icon.icon-transfer]]]))

(defn indexinfo-chart-info-switch-bar
  []
  (let [cate  (rf/subscribe [:indexinfo-chart-cate])
        ktype (rf/subscribe [:indexinfo-chart-ktype])
        show  (rf/subscribe [:indexinfo-chart-kswitch-show])
        mas   (rf/subscribe [:indexinfo-chart-kswitch-MAs])
        pes   (rf/subscribe [:indexinfo-chart-kswitch-PEs])]
    (fn []
      [:div
       {:style {:height        (base 40)
                :display       (if (and (= @cate :k)
                                        (not= @ktype :fenshi))
                                 "block" "none")
                :line-height   (base 40)
                :font-size     (base 20)
                :color         "#666"
                :border-bottom "1px solid #ddd"}}
       (when (not= :fenshi @ktype)
         (case @show
           :MA (let [{ma-5  :MA5
                      ma-20 :MA20
                      ma-60 :MA60} @mas]
                 [:div
                  [chart-legend (get ma-colors 0)]
                  [:div.chart-legend-text
                   #_{:style {:margin-left (base 8)
                              :color       (get ma-colors 0)}}
                   "MA5:"
                   (or (some-> ma-5 (.toFixed 2)) "NA")]
                  [chart-legend (get ma-colors 1)]
                  [:div.chart-legend-text
                   #_{:style {:margin-left (base 8)
                              :color       (get ma-colors 1)}}
                   "MA20:"
                   (or (some-> ma-20 (.toFixed 2)) "NA")]
                  [chart-legend (get ma-colors 2)]
                  [:div.chart-legend-text
                   #_{:style {:margin-left (base 8)
                              :color       (get ma-colors 2)}}
                   "MA60:"
                   (or (some-> ma-60 (.toFixed 2)) "NA")]
                  [indexinfo-chart-info-switch-toggler :MA]])
           :PE (let [{pe-max    :PEmax
                      pe-mid    :PEmid
                      pe-min    :PEmin
                      price-max :pricemax
                      price-mid :pricemid
                      price-min :pricemin} @pes
                     #_                    (prn "@pes" pes pe-max pe-mid pe-min price-max price-mid price-min)]
                 [:div
                  [chart-legend (get ma-colors 0)]
                  [:div.chart-legend-text
                   #_{:style {:margin-left (base 8)
                              :color       (get ma-colors 0)}}
                   "PE"
                   (some-> pe-min (.toFixed 1))
                   ":"
                   (some-> price-min (.toFixed 0))]
                  [chart-legend (get ma-colors 1)]
                  [:div.chart-legend-text
                   #_{:style {:margin-left (base 8)
                              :color       (get ma-colors 1)}}
                   "PE"
                   (some-> pe-mid (.toFixed 1))
                   ":"
                   (some-> price-mid (.toFixed 0))]
                  [chart-legend (get ma-colors 2)]
                  [:div.chart-legend-text
                   #_{:style {:margin-left (base 8)
                              :color       (get ma-colors 2)}}
                   "PE"
                   (some-> pe-max (.toFixed 1))
                   ":"
                   (some-> price-max (.toFixed 0))]
                  [indexinfo-chart-info-switch-toggler :PE]])))])))

(defn indexinfo-chart-nav
  []
  (let [active (rf/subscribe [:indexinfo-chart-active])
        cate   (rf/subscribe [:indexinfo-chart-cate])
        ktype  (rf/subscribe [:indexinfo-chart-ktype])]
    (r/create-class
     {:display-name        "indexinfo-chart-nav"
      :component-did-mount (fn [el]
                             (rf/dispatch [:indexinfo-set-chart-nav-height
                                           (-> el
                                               r/dom-node
                                               .getBoundingClientRect
                                               .-height)]))
      :reagent-render
      (fn []
        [:div.chart-nav
         (doall
          (for [item (case @cate
                       :k k-chart-nav-data
                       line-chart-nav-data)
                :let [{k :k text :name} item
                      active? (case @cate
                                :k (str/includes? (name @ktype) (name k))
                                (= k @active))
                      #_ (prn "active?" active? @cate @ktype @active k)]]
            ^{:key (name k)}
            [:div.type-item
             {:on-click (fn [e]
                          (case @cate
                            :k (rf/dispatch [:indexinfo-chart-set-ktype k])
                            (rf/dispatch [:indexinfo-chart-set-active k]))
                          (logger-log
                           (case @cate
                             :k (case @ktype
                                  :fenshi "stockIndex.fenshitu"
                                  (str "stockIndex.k." (name @ktype)))
                             (str "stockIndex.zoom." (name @cate) "." (name @active)))))
              :style    (merge
                         {:border-bottom (str "1px solid " (if active? "#fdaa43" "#ddd"))}
                         (when active? {:color "#fdaa43"}))}
             text]))])})))

(defn axis-mask-style
  [top height]
  {:style {:position       "absolute"
           :background     "rgba(255,0,0,0)"
           :top            top
           :left           0
           :height         height
           :width          "100%"
           :font-size      ".5rem"
           :pointer-events "none"
           :color          "#999"
           :border-bottom  "1px solid #ddd"
           :z-index        120}})

(defn axis-middle-line-style
  [height]
  {:style {:position   "absolute"
           :background "#ddd"
           :top        (/ height 2)
           :left       0
           :width      "100%"
           :height     1
           :z-index    0}})

(defn axis-mask-left-axis
  [height top middle bottom]
  [:div.left-axis
   {:style {:float        "left"
            :padding-left 4}}
   [:div.left-axis-top
    {:style {:position "relative"
             :height   (/ height 2)}}
    [:div.top-price
     {:style {:position    "absolute"
              :top         0
              :line-height "15px"
              :height      15}}
     top]
    [:div.middle-price
     {:style {:position    "absolute"
              :bottom      0
              :line-height "15px"}}
     middle]]
   [:div.left-axis-bottom
    {:style {:height (/ height 2)}}
    [:div.bottom-price
     {:style {:position    "absolute"
              :bottom      0
              :line-height "15px"}}
     bottom]]])

(defn axis-mask-right-axis
  [height top middle bottom]
  [:div.right-axis
   {:style {:float         "right"
            :text-align    "right"
            :width         "30%"
            :padding-right 4}}
   [:div.right-axis-top
    {:style {:position "relative"
             :height   (/ height 2)}}
    [:div.top-percent
     {:style {:position    "absolute"
              :width       "100%"
              :top         0
              :height      15
              :line-height "15px"}}
     top]
    [:div.middle-percent
     {:style {:position    "absolute"
              :width       "100%"
              :bottom      0
              :line-height "15px"}}
     middle]]
   [:div.right-axis-bottom
    {:style {:position "relative"
             :height   (/ height 2)}}
    [:div.bottom-percent
     {:style {:position    "absolute"
              :width       "100%"
              :bottom      0
              :line-height "15px"}}
     bottom]]])

(defn indexinfo-fenshi-axis-mask
  []
  (let [indexinfo-main-height (rf/subscribe [:indexinfo-main-height])
        fenshi                (rf/subscribe [:indexinfo-data-fenshi])
        realtime              (rf/subscribe [:indexinfo-data-realtime])]
    (fn []
      (let [height                (-> @indexinfo-main-height
                                      (- (* 3.875 js/rem))
                                      (* 0.7))
            lastclose                  (-> @realtime :lastclose)
            {:keys [delta
                    changeratio]} (apply max-key (comp Math/abs :delta)
                                         (map (fn [{:keys [newprice changeratio]}]
                                                {:delta       (- newprice lastclose)
                                                 :changeratio changeratio})
                                              @fenshi))
            delta-abs             (Math/abs delta)
            changeratio-abs       (Math/abs changeratio)]
        [:div.axis-mask
         (axis-mask-style "3rem" height)
         [:div.middle-line
          (axis-middle-line-style height)]
         [axis-mask-left-axis
          height
          (-> lastclose (+ delta-abs) (.toFixed 2))
          (-> lastclose (.toFixed 2))
          (-> lastclose (- delta-abs) (.toFixed 2))]
         [axis-mask-right-axis
          height
          (-> changeratio-abs (.toFixed 2) (str "%"))
          "0.00%"
          (-> changeratio-abs - (.toFixed 2) (str "%"))]]))))


(defn indexinfo-k-axis-mask
  []
  (let [indexinfo-main-height (rf/subscribe [:indexinfo-main-height])
        kday                  (rf/subscribe [:indexinfo-kday-chart-use-data])
        kweek                 (rf/subscribe [:indexinfo-kweek-chart-use-data])
        kmonth                (rf/subscribe [:indexinfo-kmonth-chart-use-data])
        kdaype                (rf/subscribe [:indexinfo-kdaype-chart-use-data])
        kweekpe               (rf/subscribe [:indexinfo-kweekpe-chart-use-data])
        kmonthpe              (rf/subscribe [:indexinfo-kmonthpe-chart-use-data])
        ktype                 (rf/subscribe [:indexinfo-chart-ktype])
        datazoom              (rf/subscribe [:indexinfo-chart-datazoom])]
    (fn []
      (let [height              (-> @indexinfo-main-height (- (* 4.825 js/rem)))
            {:keys [start-index end-index]} @datazoom
            k-data              (case @ktype
                                  :day     @kday
                                  :week    @kweek
                                  :month   @kmonth
                                  :daype   @kdaype
                                  :weekpe  @kweekpe
                                  :monthpe @kmonthpe
                                  nil)]
        (when (and start-index end-index)
          (when-let [origin (get-in k-data [:k :origin])]
            (let [origin           (subvec origin start-index (min (inc end-index) (count origin)))
                  max-high         (apply max (map #(nth % 3) origin))
                  min-low          (apply min (map #(nth % 4) origin))
                  prices           (:prices k-data) ;; when not pe, nil
                  #_ (prn "prices" prices)
                  max-peprice-high (some->> (some-> prices :max (subvec start-index end-index)) (apply max))
                  min-peprice-low  (some->> (some-> prices :min (subvec start-index end-index)) (apply min))
                  #_ (prn max-high min-low max-peprice-high min-peprice-low)
                  show-high        (apply max (filter identity [max-high max-peprice-high]))
                  show-low         (apply min (filter identity [min-low min-peprice-low]))
                  show-mid         (/ (+ show-high show-low) 2)]
              [:div.axis-mask
               (axis-mask-style "4rem" height)
               [:div.middle-line
                (axis-middle-line-style height)]
               [axis-mask-left-axis
                height
                (some-> show-high (.toFixed 2))
                (some-> show-mid (.toFixed 2))
                (some-> show-low (.toFixed 2))]])))))))

(defn indexinfo-chart-container
  []
  (let [;;data    (rf/subscribe [:indexinfo-line-data])
        k       (rf/subscribe [:indexinfo-data-k])
        ktype   (rf/subscribe [:indexinfo-chart-ktype])
        cate    (rf/subscribe [:indexinfo-chart-cate])
        height  (rf/subscribe [:indexinfo-main-content-height])
        show-y? (rf/subscribe [:indexinfo-chart-show-yaxis])
        _       (rf/dispatch [:indexinfo-set-chart-info-switch-bar-height 0])]
    (fn []
      ;;      (prn "@@@@#####" @cate @ktype js/rem (base-n 40))
      [(keyword (str "div#" @cate "-chart." @cate "-chart"))
       {:style {:height     @height
                :background "#f7f7f7"}}
       [indexinfo-chart-nav]
       (when (and (= :k @cate) @show-y?)
         (case @ktype
           :fenshi [indexinfo-fenshi-axis-mask]
           [indexinfo-k-axis-mask]))
       [:div
        {:style {:clear         "both"
                 :border-bottom "1px solid #ddd"}}
        [indexinfo-chart-info-switch-bar]
        (case @cate
          :pe          [indexinfo-pe-chart]
          :pb          [indexinfo-pb-chart]
          :ps          [indexinfo-ps-chart]
          :totalmv     [indexinfo-totalmv-chart]
          :freefloatmv [indexinfo-freefloatmv-chart]
          :divirate    [indexinfo-dividend-chart]
          :k           (case @ktype
                         :fenshi  [indexinfo-fenshi-chart]
                         :day     [indexinfo-k-day-chart]
                         :week    [indexinfo-k-week-chart]
                         :month   [indexinfo-k-month-chart]
                         :daype   [indexinfo-k-daype-chart]
                         :weekpe  [indexinfo-k-weekpe-chart]
                         :monthpe [indexinfo-k-monthpe-chart]))
        [indexinfo-chart-x-axis-line]]])))

(def indexinfo-fundamental-main
  indexinfo-chart-container)

(def indexinfo-tech-main
  indexinfo-chart-container)

(defn indexinfo-capital-main
  []
  (let [capital-active (rf/subscribe [:indexinfo-capital-active])
        unlocks        (rf/subscribe [:indexinfo-unlocks-info])]
    (fn []
      (case @capital-active
        :unlock (let [{:keys [half_year_ago future_will_unlock]} @unlocks]
                  (if (and (empty? half_year_ago) (empty? future_will_unlock))
                    [no-content]
                    [:div.unlocks.row-container
                     [:div.row
                      [:span.date "解禁日期"]
                      [:span.amount "数量（万股）"]
                      [:span.percent "占流通股比例"]]
                     (doall
                      (for [[index item] (indexed half_year_ago)
                            :let         [{:keys [startdateforfloating
                                                  newmarketableashares
                                                  newmarketablemv
                                                  negotiablemv]} item
                                          percent (some-> newmarketablemv
                                                          (/ negotiablemv)
                                                          (* 100))]]
                        ^{:key (str "hya-" index)}
                        [:div.row
                         [:span.date (some-> startdateforfloating format-date-slash)]
                         [:span.amount newmarketableashares]
                         [:span.percent (some-> percent (.toFixed 2))]]))
                     (when-not (empty? future_will_unlock)
                       [content-separator "即将解禁"])
                     (doall
                      (for [[index item] (indexed future_will_unlock)
                            :let         [{:keys [startdateforfloating
                                                  newmarketableashares
                                                  newmarketablemv
                                                  negotiablemv]} item
                                          percent (some-> newmarketablemv
                                                          (/ negotiablemv)
                                                          (* 100))]]
                        ^{:key (str "fwu-" index)}
                        [:div.row
                         [:span.date (some-> startdateforfloating format-date-slash)]
                         [:span.amount newmarketableashares]
                         [:span.percent (some-> percent (.toFixed 2))]]))]))
        [indexinfo-chart-container]))))

(defn indexinfo-main-content
  []
  (let [active-noodle (rf/subscribe [:indexinfo-active-noodle])
        height        (rf/subscribe [:indexinfo-main-content-height])
        update-ui     (fn [el]
                        (rf/dispatch [:indexinfo-set-main-content-y
                                      (-> el r/dom-node .getBoundingClientRect .-top)]))]
    (r/create-class
     {:display-name         "main-content"
      :component-did-mount  update-ui
      :component-did-update update-ui
      :reagent-render
      (fn []
        [:div.main-content
         {:style {:height @height}}
         (case @active-noodle
           :fundamental [indexinfo-fundamental-main]
           :capital     [indexinfo-capital-main]
           :ingredients [indexinfo-ingredients-main]
           [indexinfo-tech-main])])})))

(defn indexinfo-headline-content
  [cate]
  (let [realtime    (rf/subscribe [:indexinfo-headline-realtime])
        volume      (rf/subscribe [:indexinfo-headline-tech-volume])
        zdf         (rf/subscribe [:indexinfo-headline-tech-zdf])
        newhigh     (rf/subscribe [:indexinfo-headline-tech-newhigh])
        fundamental (rf/subscribe [:indexinfo-fundamental-values])
        capital     (rf/subscribe [:indexinfo-capital-values])]
    (fn [cate]
      (let [{:keys [open turnover mv]}                          @realtime
            {:keys [sixtydays twentydays]}                      @volume
            {:keys [thirty ninety oneyear]}                     @zdf
            {newhigh-thirty  :thirty  newhigh-ninety  :ninety
             newhigh-oneyear :oneyear newhigh-history :history} @newhigh
            sixty                                               (first sixtydays)
            twenty                                              (first twentydays)
            {:keys [pe pb ps totalmv freefloatmv]}              @fundamental
            {:keys [dividend]}                                  @capital]
        [:div
         {:style {:height "1.5rem"}}
         (case cate
           :realtime
           [:div.headline-item
            [:span (str "今开：" (or (some-> open (.toFixed 2)) "--"))]
            [:span (str "换手率：" (or (some-> turnover (.toFixed 2) (str "%")) "--"))]
            [:span (str "市值：" (or (some-> mv (format-number-chn-2 1)) "--"))]]
           :volume
           [:div
            [:div.headline-item
             [:span (str "最新成交额相对于20日均值"
                         (if (some-> twenty pos?) "放" "缩") "量"
                         (or (some-> twenty Math/abs (.toFixed 2)) "--") "%")]]
            [:div.headline-item
             [:span (str "最新成交额相对于60日均值"
                         (if (some-> sixty pos?) "放" "缩") "量"
                         (or (some-> sixty Math/abs (.toFixed 2)) "--") "%")]]]
           :zdf
           [:div
            [:div.headline-item
             [:span (str "30日"
                         (if (some-> thirty pos?) "涨" "跌") "幅"
                         (some-> thirty Math/abs (.toFixed 2)) "%，距30日新高"
                         (some-> newhigh-thirty second))]]
            [:div.headline-item
             [:span (str "90日"
                         (if (some-> ninety pos?) "涨" "跌") "幅"
                         (or (some-> ninety Math/abs (.toFixed 2)) "--") "%，距90日新高"
                         (or (some-> newhigh-ninety second) "--"))]]
            [:div.headline-item
             [:span (str "一年"
                         (if (some-> oneyear pos?) "涨" "跌") "幅"
                         (or (some-> oneyear Math/abs (.toFixed 2)) "--") "%，距一年新高"
                         (or (some-> newhigh-oneyear second (.toFixed 2) (str "%")) "--"))]]
            [:div.headline-item
             [:span (str "距历史新高" (-> newhigh-history second))]]]
           :fundamental
           [:div
            (let [{:keys [val gt-history]} pe
                  #_                       (prn "###pe" val gt-history)]
              [:div.headline-item
               [:span (str "PE: " (or (some-> val (.toFixed 2)) "--"))]
               [:span (str " 高于历史" (or (some-> gt-history (.toFixed 2)) "--") "%的日子")]])
            (let [{:keys [val gt-history]} pb
                  #_                       (prn "###pb" val gt-history)]
              [:div.headline-item
               [:span (str "PB: " (or (some-> val (.toFixed 2)) "--"))]
               [:span (str " 高于历史" (or (some-> gt-history (.toFixed 2)) "--") "%的日子")]])
            (let [{:keys [val]} ps
                  #_            (prn "###ps" val totalmv freefloatmv)]
              [:div.headline-item
               [:span (str "PS: " (or (some-> val (.toFixed 2)) "--"))]])
            [:div.headline-item
             #_[:span (str year "Q" season)]
             [:span (str "总市值：" (some-> totalmv (format-number-chn-2 2)))]
             #_[:span (str "同比增速：" (some-> rate (.toFixed 2)) "%")]]
            [:div.headline-item
             #_[:span (str year "Q" season)]
             [:span (str "自由流通：" (some-> freefloatmv (format-number-chn-2 2)))]
             #_[:span (str "同比增速：" (some-> rate (.toFixed 2)) "%")]]]
           :dividend
           [:div.headline-item [:span (str "股息率：" (some-> dividend (.toFixed 2)) "%")]]
           :ingredients
           [indexinfo-ingredients-nav])]))))

(defn indexinfo-secondary-headline-swiper
  []
  (let [cate  (rf/subscribe [:indexinfo-headline-cate])
        index (rf/subscribe [:indexinfo-headline-current-index])]
    (r/create-class
     {:display-name "headline-swiper"
      :component-will-unmount
      (fn [] (rf/dispatch [:indexinfo-clear-headline-swiper-timer]))
      :reagent-render
      (fn []
        (when @cate
          [:div
           {:style {:background  "#f2f3f3"
                    :height      "1.5rem"
                    :overflow    "hidden"
                    :position    "relative"
                    :line-height "1.5rem"
                    :font-size   ".6rem"
                    :color       "#333"
                    :box-shadow  "0 .5px 2px rgba(0,0,0,.1)"}}
           [:div.headline-animation
            {:style (merge {:position   "absolute"
                            :transition "top .5s"
                            :width      "100%"}
                           (when-let [cnt (headline-cnt @cate)]
                             {:top (str
                                    (* -1.5 (mod @index cnt))
                                    "rem")}))}
            [indexinfo-headline-content @cate]]]))})))


(defn indexinfo-main
  []
  (let [;; screenheight (/ screen-height js/rem)
        ;; height (str (- screenheight 12) "rem")
        ;; ingredients-height (str (- screenheight 9) "rem")
        ;; height        (rf/subscribe [:indexinfo-main-height])
        active-noodle (rf/subscribe [:indexinfo-active-noodle])
        update-ui     (fn [el]
                        (rf/dispatch [:indexinfo-set-indexinfo-main-y
                                      (-> el r/dom-node .getBoundingClientRect .-top)]))]
    (r/create-class
     {:display-name        "indexinfo-main"
      :component-did-mount update-ui
      :reagent-render
      (fn []
        [:div.indexinfo-main
         {:style
          (merge
           {:position "relative"
            :width    "100%"
            :overflow "hidden"}
           (when (not= @active-noodle :ingredients)
             {:padding-bottom "2rem"}))}
         [indexinfo-secondary-nav]
         [indexinfo-secondary-headline-swiper]
         [indexinfo-main-content]])})))

(defn indexinfo-nav
  []
  (let [name        (rf/subscribe [:indexinfo-name])
        id          (rf/subscribe [:indexinfo-id])
        fundamental (rf/subscribe [:indexinfo-fundamental-values])]
    (r/create-class
     {:display-name         "indexinfo-bottom-nav"
      :component-will-mount (fn [])
      :component-did-mount  (fn [el]
                              (rf/dispatch [:indexinfo-set-indexinfo-nav-y
                                            (-> el
                                                r/dom-node
                                                .getBoundingClientRect
                                                .-top)]))
      :reagent-render
      (fn []
        (let [{:keys [pe pb ps totalmv freefloatmv]} @fundamental]
          [:div#indexinfo-nav.secuinfo-nav
           {:style {:position         "fixed"
                    :bottom           0
                    :width            "100%"
                    :height           "2.1rem"
                    :box-shadow       "0 -2px 5px 0 rgba(130,130,130,0.25)"
                    :color            "#666"
                    :background-color "#fff"
                    :z-index          10
                    :text-align       "center"}}
           (doall
            (for [[index item] (indexed indexinfo-nav-data)]
              ^{:key index}
              [:div.bottom-nav-btn
               (deep-merge
                {:style {:position "absolute"
                         :width    (str (/ 100 (count indexinfo-nav-data)) "%")
                         :height   ".85rem"
                         :top      ".15rem"
                         :left     (str (* index (/ 100 (count indexinfo-nav-data))) "%")}}
                (if (= 2 index)
                  {:on-click #(do
                                (rf/dispatch [:share-tip-show])
                                (logger-log "stockIndex.share")
                                (wx/set-share
                                 {:title           (get-stock-title @name @id)
                                  :desc            (str "PE: " (or (some-> pe :value (.toFixed 2)) "--")
                                                        "PB: " (or (some-> pb :value (.toFixed 2)) "--")
                                                        "PS: " (or (some-> ps :value (.toFixed 2)) "--")
                                                        "总市值：" (some-> totalmv (.toFixed 2)) "万亿"
                                                        "自由流通市值：" (some-> freefloatmv (.toFixed 2)) "万亿")
                                  :type            "1"
                                  :timeline-logger "stockIndex.shared.timeline"
                                  :weixin-logger   "stockIndex.shared.appmessage"}))}
                  {})
                (:properties item))
               [:div
                {:style {:line-height "1.1rem"
                         :font-size   ".8rem"}}
                [(icon-key (:icon item))]]
               [:div
                {:style {:line-height ".7rem"
                         :font-size   ".6rem"}}
                (:text item)]]))]))})))

(defn indexinfo-mask-inner
  []
  (let [mask-type    (rf/subscribe [:indexinfo-mask-type])
        mask-content (rf/subscribe [:indexinfo-mask-content])]
    (fn []
      (logger-log (str "stockIndex." (if (keyword? @mask-content)
                                       (name @mask-content)
                                       "main") ".help"))
      (case @mask-type
        :img
        [:div.inner
         [:img
          {:style {:padding "0 .25rem .25rem"
                   :width   "12rem"
                   :height  "auto"}
           :src   (indexinfo-mask-img-data @mask-content)}]]
        :div [:div]
        [:div "no content"]))))

(defn indexinfo-mask
  []
  (let [show? (rf/subscribe [:indexinfo-mask-show])]
    (fn []
      [:div
       (when-not @show? {:style {:display "none"}})
       [:div.viewport-mask
        {:style    {:position         "fixed"
                    :top              0
                    :right            0
                    :bottom           0
                    :left             0
                    :z-index          900
                    :background-color "#000"
                    :opacity          0.5}
         :on-click #(rf/dispatch [:indexinfo-mask-hide])}]
       [:div.mask-content
        {:style {:position    "absolute"
                 :top         "50%"
                 :left        "50%"
                 :transform   "translate(-50%,-50%)"
                 :text-align  "center"
                 :opacity     ".9"
                 :padding     ".5rem"
                 :font-size   ".7rem"
                 :line-height ".8rem"
                 :transition  "margin-top .5s linear"
                 :z-index     1000}}
        [:div.container
         {:style {:border        "1px solid #fff"
                  :position      "relative"
                  :border-radius ".25rem"
                  :border-top    "none"
                  }}
         [:div.close-btn
          {:style    {:height  "1.1rem"
                      :display "flex"}
           :on-click #(rf/dispatch [:indexinfo-mask-hide])}
          [:span.span-1
           {:style {:flex          1
                    :border-top    "1px solid #fff"
                    :border-radius ".25rem 0"}}]
          [:span.span-2
           {:style {:position      "relative"
                    :top           "-1.1rem"
                    :border        ".15rem solid #fff"
                    :border-radius "50%"
                    :display       "inline-block"
                    :width         "1.5rem"
                    :height        "1.5rem"
                    :line-height   "1.6rem"
                    :font-size     "1rem"
                    :color         "#fff"}}
           [:i.icon.icon-times]]
          [:span.span-3
           {:style {:flex          1
                    :border-top    "1px solid #fff"
                    :border-radius "0 .25rem"}}]]
         [indexinfo-mask-inner]]]])))


(defn get-up-pes
  [pe]
  (let [min-pe (-> pe
                   (/ 5)
                   Math/floor
                   inc
                   (* 5))]
    [min-pe (+ 5 min-pe) (+ 10 min-pe)]))

(defn get-down-pes
  [pe]
  (cond
    (> pe 5) (let [max-pe (-> pe
                              (/ 5)
                              Math/ceil
                              dec
                              (* 5))]
               (cond
                 (>= max-pe 15) [(- max-pe 10) (- max-pe 5) max-pe]
                 true           [(precision-round (/ max-pe 4) 1)
                                 (precision-round (/ max-pe 2) 1)
                                 (precision-round max-pe 1)]))
    :else    [0.6 1.3 2.5]))

(defn indexinfo-settings
  []
  (let [changed?       (rf/subscribe [:indexinfo-setting-changed?])
        history-pe     (rf/subscribe [:indexinfo-history-pe])
        name           (rf/subscribe [:indexinfo-name])
        current-pe     (rf/subscribe [:indexinfo-current-pe])
        current-price  (rf/subscribe [:indexinfo-current-price])
        up-pe          (rf/subscribe [:indexinfo-setting-up-pe])
        down-pe        (rf/subscribe [:indexinfo-setting-down-pe])
        up-valid?      (rf/subscribe [:indexinfo-setting-up-valid?])
        down-valid?    (rf/subscribe [:indexinfo-setting-down-valid?])
        up-has-rule?   (rf/subscribe [:indexinfo-setting-up-has-rule?])
        down-has-rule? (rf/subscribe [:indexinfo-setting-down-has-rule?])
        id             (rf/subscribe [:indexinfo-id])
        name           (rf/subscribe [:indexinfo-name])
        intro          (rf/subscribe [:indexinfo-intro])]
    (fn []
      (let [has-valid? (and @changed? (or @up-valid? @down-valid?))
            #_         (prn has-valid? @changed? @up-valid? @down-valid?)]
        [:div.indexinfo-setting
         {:style {:margin-top ".2rem"}}
         [:div.save
          {:style {:background "#fff"
                   :position   "relative"
                   :text-align "center"
                   :height     "2.15rem"
                   :font-size  ".6rem"}}
          [:div.cancel-back-btn.btn
           {:style    (merge {:position      "absolute"
                              :top           ".4rem"
                              :right         "4rem"
                              :height        "1.2rem"
                              :line-height   "1.2rem"
                              :width         "3rem"
                              :border-width  "1px"
                              :border-style  "solid"
                              :border-radius 4}
                             {:color        "#284a7d"
                              :border-color "#284a7d"})
            :on-click (fn [e]
                        (rf/dispatch [:indexinfo-setting-cancel])
                        (logger-log (if has-valid?
                                      "stockIndex.remindPe.cancelChange"
                                      "stockIndex.remindPe.back")))}
           (if has-valid? "取消更改" "返回")]
          [:div.save-btn.btn
           {:style    {:position         "absolute"
                       :top              ".4rem"
                       :right            ".5rem"
                       :height           "1.2rem"
                       :line-height      "1.2rem"
                       :width            "3rem"
                       :border-width     1
                       :border-style     "solid"
                       :border-color     (if has-valid? "#284a7d" "#ccc")
                       :border-radius    4
                       :color            (if has-valid? "#fff" "#ccc")
                       :background-color (if has-valid? "#284a7d" "#fff")}
            :on-click (fn [e]
                        (when has-valid?
                          (rf/dispatch [:indexinfo-setting-save])
                          (logger-log "stockIndex.remindPe.done")
                          (when @up-has-rule?
                            (logger-log "stockIndex.remindPe.done.validup"))
                          (when @down-has-rule?
                            (logger-log "stockIndex.remindPe.done.validdown")))
                        (when (= :setting-save @intro)
                          (rf/dispatch [:indexinfo-set-intro nil])
                          (logger-log "stockIndex.boot.setting-save")
                          #_(set-hash!
                             (routes/indexinfo-url
                              {:id   @id
                               :name @name}))))}
           "保存"]]
         [:div.current
          [:span
           {:style {:font-weight "bold"}}
           @name]
          [:span "当前市盈率(PE) " (or (some-> @current-pe (.toFixed 2)) "--")]
          [:span "当前点位 " (or @current-price "--")]]
         (let [{max-pe :max min-pe :min mid-pe :med} @history-pe]
           [:div.history
            [:span "历史PE："]
            [:span (str "最高 " (some-> max-pe (.toFixed 2)))]
            [:span (str "中位数 " (some-> mid-pe (.toFixed 2)))]
            [:span (str "最低 " (some-> min-pe (.toFixed 2)))]])
         [:div.setting-group
          [:div.setting-line
           [:div.desc
            "市盈率上升到："]
           [:div.input
            [:input
             {:style       (merge {:border-radius    (base 4)
                                   :border           "1px solid #e2e2e2"
                                   :background-color "#f7f7f7"}
                                  (when-not @up-valid?
                                    {:background-color "#dddd33"
                                     :border-color     "#ffab36"}))
              :value       (or @up-pe "")
              :placeholder (str "需>" (some-> @current-pe (.toFixed 2)))
              :on-change   (fn [e]
                             (let [val (-> e .-target .-value)]
                               (rf/dispatch [:indexinfo-setting-set-pe-up val])))
              :on-focus    (fn [e]
                             (logger-log "stockIndex.remindPe.input-focus.up"))}]]
           [:div.multi
            {:style {:float "left"}}
            "倍"]
           [:div.toggle-btns
            [toggle-btn up-has-rule? :indexinfo-setting-clear-pe-up "stockIndex.remindPe.reset.up"]]]
          [:div.setting-line
           [:div.desc
            {:style {:width "5rem"
                     :float "left"}}
            "一键选择："]
           [:div.select-btns
            (doall
             (for [pe (get-up-pes @current-pe)]
               ^{:key pe}
               [:div.select-btn
                {:style    {:background (if (= pe @up-pe) "#aaa" "#eee")
                            :color      (if (= pe @up-pe) "#fff" "#000")}
                 :on-click (fn [e]
                             (rf/dispatch [:indexinfo-setting-set-up-pe-btn pe])
                             (logger-log "stockIndex.remindPe.chooseup")
                             (when (= :setting-choose @intro)
                               (rf/dispatch [:indexinfo-set-intro :setting-save])
                               (logger-log "stockIndex.boot.setting-choose")
                               #_(set-hash!
                                  (routes/indexinfo-url
                                   {:id    @id
                                    :name  @name
                                    :intro "setting-save"}))))}
                pe]))]]
          [:div.setting-line
           [:div.desc "对应点位："]
           [:div.input
            (or (when (and @up-valid? @up-pe @current-price @current-pe)
                  (-> @up-pe
                      (* @current-price)
                      (/ @current-pe)
                      (.toFixed 2)))
                "")]]]
         [:div.setting-group
          [:div.setting-line
           [:div.desc "市盈率下降到："]
           [:div.input
            [:input
             {:style       (merge {:border-radius    (base 4)
                                   :border           "1px solid #e2e2e2"
                                   :background-color "#f7f7f7"
                                   }
                                  (when-not @down-valid?
                                    {:background-color "#dddd33"
                                     :border           "1px solid #ffab36"}))
              :value       (or @down-pe "")
              :placeholder (str "需<" (some-> @current-pe (.toFixed 2)) ", 且>0")
              :on-change   (fn [e]
                             (let [val (-> e .-target .-value)]
                               (rf/dispatch [:indexinfo-setting-set-pe-down val])))
              :on-focus    (fn [e]
                             (logger-log "stockIndex.remindPe.input-focus.down"))}]]
           [:div.multi
            {:style {:float "left"}}
            "倍"]
           [:div.toggle-btns
            [toggle-btn down-has-rule? :indexinfo-setting-clear-pe-down "stockIndex.remindPe.reset.down"]]]
          [:div.setting-line
           [:div.desc
            "一键选择："]
           [:div.select-btns
            (doall
             (for [pe (get-down-pes @current-pe)]
               ^{:key pe}
               [:div.select-btn
                {:style    {:background (if (= pe @down-pe) "#aaa" "#eee")
                            :color      (if (= pe @down-pe) "#fff" "#000")}
                 :on-click (fn [e]
                             (rf/dispatch [:indexinfo-setting-set-down-pe-btn pe])
                             (logger-log "stockIndex.remindPe.choosedown"))}
                pe]))]]
          [:div.setting-line
           [:div.desc "对应点位："]
           #_(prn @down-valid? @down-pe @current-price @current-pe)
           [:div.input
            (or (when (and @down-valid? @down-pe @current-price @current-pe)
                  (-> @down-pe
                      (* @current-price)
                      (/ @current-pe)
                      (.toFixed 2)))
                "")]]]]))))

(defn indexinfo-setting-save-intro
  []
  [:div.indexinfo-intro
   [:div.mask-search]
   [:div.hollow-save
    {:style {:height "1.75rem"}}
    [:div.hollow-save-up
     {:style {:height     "0.25rem"
              :background "rgba(0,0,0,.6)"}}]
    [:div.hollow-save-left
     {:style {:height           "1.5rem"
              :background-color "rgba(0,0,0,.6)"
              :width            "12.25rem"
              :float            "left"
              :pointer-events   "auto"}}]
    [:svg
     {:style
      {:display "block"
       :float   "left"
       :height  "1.5rem"
       :width   "3.4rem"}}
     [:defs
      [:mask
       {:id     "svg-setting-save-mask"
        :x      0
        :y      0
        :width  "100%"
        :height "100%"}
       [:rect
        {:x      0
         :y      0
         :width  "100%"
         :height "100%"
         :fill   "#fff"}]
       [:rect
        {:x      0
         :y      0
         :width  "100%"
         :height "100%"
         :rx     "4px"
         :ry     "4px"}]]]
     [:rect {:x            0
             :y            0
             :width        "100%"
             :height       "100%"
             :mask         "url(#svg-setting-save-mask)"
             :fill         "#000"
             :fill-opacity ".6"}]]
    [:div.hollow-save-right
     {:style {:height           "1.5rem"
              :background-color "rgba(0,0,0,.6)"
              :width            "0.325rem"
              :float            "left"
              :pointer-events   "auto"}}]]
   [:div.guide-save
    {:style
     {:background     "rgba(0,0,0,.6)"
      :height         "100%"
      :pointer-events "auto"}}
    [:div.hand
     {:style
      {:position  "absolute"
       :top       "3.7rem"
       :left      "10rem"
       :font-size "4rem"}}
     [:i.icon.icon-tap-youshang]]
    [:div.text
     {:style
      {:position   "absolute"
       :top        "7.8rem"
       :left       "8.5rem"
       :font-size  ".8rem"
       :text-align "center"}}
     [:div "点这里"]
     [:div "保存设置"]]]])

(defn indexinfo-setting-choose-intro
  []
  [:div.indexinfo-intro
   [:div.mask-search]
   [:div.hollow-pe-up
    {:style {:height "8.65rem"}}
    [:div.hollow-pe-up-up
     {:style {:height     "7.15rem"
              :background "rgba(0,0,0,.6)"}}]
    [:div.hollow-pe-up-left
     {:style {:height           "1.5rem"
              :background-color "rgba(0,0,0,.6)"
              :width            "5.25rem"
              :float            "left"
              :pointer-events   "auto"}}]
    [:svg
     {:style
      {:display "block"
       :float   "left"
       :height  "1.5rem"
       :width   "7rem"}}
     [:defs
      [:mask
       {:id     "svg-setting-choose-mask"
        :x      0
        :y      0
        :width  "100%"
        :height "100%"}
       [:rect
        {:x      0
         :y      0
         :width  "100%"
         :height "100%"
         :fill   "#fff"}]
       [:rect
        {:x      0
         :y      0
         :width  "100%"
         :height "100%"
         :rx     "4px"
         :ry     "4px"}]]]
     [:rect {:x            0
             :y            0
             :width        "100%"
             :height       "100%"
             :mask         "url(#svg-setting-choose-mask)"
             :fill         "#000"
             :fill-opacity ".6"}]]
    [:div.hollow-pe-up-right
     {:style {:height           "1.5rem"
              :background-color "rgba(0,0,0,.6)"
              :width            "3.75rem"
              :float            "left"
              :pointer-events   "auto"}}]]
   [:div.guide-pe-up
    {:style
     {:background     "rgba(0,0,0,.6)"
      :height         "100%"
      :pointer-events "auto"}}
    [:div.hand
     {:style
      {:position  "absolute"
       :top       "10.5rem"
       :left      "7rem"
       :font-size "4rem"}}
     [:i.icon.icon-tap-shang]]
    [:div.text
     {:style
      {:position   "absolute"
       :top        "15rem"
       :left       "7.5rem"
       :font-size  ".8rem"
       :text-align "center"}}
     [:div "点这里"]
     [:div "一键选择上涨倍数"]]]])

(defn indexinfo-clock-intro
  []
  [:div.indexinfo-intro
   [:div.mask-search]
   [:div.mask-clock-up
    {:style {:height         ".25rem"
             :pointer-events "auto"
             :background     "rgba(0,0,0,.6)"}}]
   [:div.hollow-clock
    {:style {:height "1.1rem"
             :width  "15.75rem"
             :float  "left"}}
    [:div.hollow-clock-left
     {:style {:height           "1.1rem"
              :background-color "rgba(0,0,0,.6)"
              :width            "14.65rem"
              :float            "left"
              :pointer-events   "auto"}}]
    [:svg
     {:style
      {:display "block"
       :float   "left"
       :height  "1.1rem"
       :width   "1.1rem"}}
     [:defs
      [:mask
       {:id     "svg-clock-mask"
        :x      0
        :y      0
        :width  "100%"
        :height "100%"}
       [:rect
        {:x      0
         :y      0
         :width  "100%"
         :height "100%"
         :fill   "#fff"}]
       [:rect
        {:x      0
         :y      0
         :width  "100%"
         :height "100%"
         :rx     "4px"
         :ry     "4px"}]]]
     [:rect {:x            0
             :y            0
             :width        "100%"
             :height       "100%"
             :mask         "url(#svg-clock-mask)"
             :fill         "#000"
             :fill-opacity ".6"}]]]
   [:div.mask-clock-right
    {:style
     {:background     "rgba(0,0,0,.6)"
      :height         "1.1rem"
      :width          "0.25rem"
      :float          "right"
      :pointer-events "auto"}}]
   [:div.guide-clock
    {:style
     {:background     "rgba(0,0,0,.6)"
      :height         "100%"
      :pointer-events "auto"
      :position       "relative"
      :top            "1.1rem"
      :left           0
      }}
    [:div.hand
     {:style
      {:position  "absolute"
       :top       "-0.4rem"
       :left      "11rem"
       :font-size "4rem"}}
     [:i.icon.icon-tap-youshang]]
    [:div.text
     {:style
      {:position   "absolute"
       :top        "4.2rem"
       :left       "8.5rem"
       :font-size  ".8rem"
       :text-align "center"}}
     [:div "点这里"]
     [:div "设置指数估值提醒"]]]])

(defn indexinfo-switch-chart-intro
  []
  [:div.indexinfo-intro
   [:div.mask-search]
   [:div.mask-index-up
    {:style {:height         "8.7rem"
             :pointer-events "auto"
             :background     "rgba(0,0,0,.6)"}}]
   [:div.guide-switch-chart
    {:style {:background     "rgba(0,0,0,.6)"
             :height         "100%"
             :width          "12.75rem"
             :float          "left"
             :pointer-events "auto"}}
    [:div.text
     {:style
      {:position   "absolute"
       :top        "10.5rem"
       :left       "5.5rem"
       :font-size  ".8rem"
       :text-align "center"}}
     [:div "点这里"]
     [:div "切换估值线"]]
    [:div.hand
     {:style
      {:position  "absolute"
       :top       "9.5rem"
       :left      "9.5rem"
       :font-size "3rem"
       :transform "rotate(90deg)"}}
     [:i.icon.icon-tap-shang]]]
   [:div.hollow-switch-chart
    {:style {:float  "left"
             :height "1.1rem"}}
    #_[:div.hollow-chart-left
       {:style {:height           "5rem"
                :background-color "rgba(0,0,0,.6)"
                :width            "12.7rem"
                :float            "left"
                :pointer-events   "auto"}}]
    [:svg
     {:style
      {:display "block"
       :float   "left"
       :height  "1.1rem"
       :width   "2.8rem"}}
     [:defs
      [:mask
       {:id     "svg-switch-chart-mask"
        :x      0
        :y      0
        :width  "100%"
        :height "100%"}
       [:rect
        {:x      0
         :y      0
         :width  "100%"
         :height "100%"
         :fill   "#fff"}]
       [:rect
        {:x      0
         :y      0
         :width  "100%"
         :height "100%"
         :rx     "4px"
         :ry     "4px"}]]]
     [:rect {:x            0
             :y            0
             :width        "100%"
             :height       "100%"
             :mask         "url(#svg-switch-chart-mask)"
             :fill         "#000"
             :fill-opacity ".6"}]]
    [:div.hollow-chart-right
     {:style {:height           "1.1rem"
              :background-color "rgba(0,0,0,.6)"
              :width            "0.45rem"
              :float            "left"
              :pointer-events   "auto"}}]]
   [:div.mask-index-right-bottom
    {:style {:background-color "rgba(0,0,0,.6)"
             :height           "100%"
             :width            "3.25rem"
             :float            "right"
             :pointer-events   "auto"}}]])

(defn indexinfo-indexlist
  []
  (let [show?     (rf/subscribe [:indexinfo-show-indexlist?])
        indexlist (rf/subscribe [:indexinfo-index-list])]
    (when @show?
      (logger-log "stockIndex.dropdown")
      [:div.indexlist
       {:style    {:position         "fixed"
                   :top              0
                   :right            0
                   :bottom           0
                   :left             0
                   :z-index          200
                   :background-color "rgba(0,0,0,.5)"}
        :on-click (fn [e]
                    (rf/dispatch [:indexinfo-show-indexlist false])
                    (logger-log "stockIndex.dropdown.back"))}
       [:div.indexlist-title
        {:style {:background    "#fff"
                 :height        (base 50)
                 :width         (base 350)
                 :padding       (combine-base 0 12)
                 :line-height   (base 50)
                 :font-size     (base 20)
                 :color         "#444"
                 :border-bottom "1px solid #eee"}}
        "选择要查看的指数"
        [:div.indexlist-close
         {:style {:float "right"}}
         "收起"
         [:span
          {:style {:display   "inline-block"
                   :min-width (base 30)
                   :transform "rotate(90deg)"
                   :font-size (base 16)}}
          [:i.icon.icon-angle-left]]]]
       (doall
        (for [[index item] (indexed @indexlist)
              :let         [{:keys [indexid indexname]} item]]
          ^{:key index}
          [:div.indexlist-item
           {:on-click (fn [e]
                        (logger-log "stockIndex.dropdown.main")
                        (set-hash!
                         (routes/indexinfo-url
                          {:id   indexid
                           :name indexname})))}
           [:div.index-name indexname]
           [:div.index-id (stockid2secucode indexid)]]))])))

(defn indexinfo-intros
  []
  (let [intro (rf/subscribe [:indexinfo-intro])]
    (case @intro
      :setting-save   [indexinfo-setting-save-intro]
      :setting-choose [indexinfo-setting-choose-intro]
      :clock          [indexinfo-clock-intro]
      :switch-chart   [indexinfo-switch-chart-intro]
      [:div])))

(defn indexinfo
  []
  (let [setting-show? (rf/subscribe [:indexinfo-setting-show])]
    [:div
     [search]
     (if-not @setting-show?
       [:div
        [index-title]
        [index-comment]
        [indexinfo-noodles]
        [indexinfo-main]
        [indexinfo-nav]
        [indexinfo-indexlist]
        [indexinfo-mask]
        [share-tip]]
       [:div
        [indexinfo-settings]
        [indexinfo-mask]
        [indexinfo-nav]])
     [indexinfo-intros]]))
