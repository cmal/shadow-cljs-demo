(ns doumi.loader
  (:require [re-frame.core :as rf]))

#_(defn loader []
    (let [show (rf/subscribe [:loader-show])]
      (fn []
        (when @show
          [:div#loader
           {:style {:width            75
                    :height           75
                    :position         "fixed"
                    :top              "50%"
                    :left             "50%"
                    :transform        "translate(-50%,-50%)"
                    :text-indent      999
                    :overflow         "hidden"
                    :background-image "url(/wx/img/loader.png)"
                    :background-size  600
                    :animation        "loader 1s infinite steps(8)"}}
           "Loading..."]))))

(defn loader []
  [:div#loader
   {:style {:width            75
            :height           75
            :position         "fixed"
            :top              "50%"
            :left             "50%"
            :transform        "translate(-50%,-50%)"
            :text-indent      999
            :overflow         "hidden"
            :background-image "url(/wx/img/loader.png)"
            :background-size  600
            :animation        "loader 1s infinite steps(8)"}}
   "Loading..."])
