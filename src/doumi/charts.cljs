(ns doumi.charts
  (:require
   [goog.string :as gstr]
   [reagent.core :as r]
   [re-frame.core :as rf]
   [clojure.string :as str]
   [doumi.helper :refer [base base-n stockid2secucode js-fmt-date-yyyymmdd]]
   [doumi.log :refer [logger-log]]
   [doumi.pos :refer [screen-width]]
   [doumi.echarts-options :refer [indexinfo-get-option-fn]]
   [doumi.secuinfo-config :refer [indexinfo-line-types]]
   ["echarts" :as echarts]))

(defn secuinfo-dividend-chart-inner-canvas
  []
  (let [stockname (rf/subscribe [:secuinfo-stockname])
        stockid   (rf/subscribe [:secuinfo-stockid])
        option    (rf/subscribe [:secuinfo-divirate-chart-option])
        height    (rf/subscribe [:secuinfo-chart-height])
        chart     (r/atom nil)
        update    (fn [el]
                    ;;                    (prn "@@@@update")
                    (.resize @chart)
                    (if @option
                      (doto @chart
                        .hideLoading
                        (.setOption (clj->js @option) true))
                      (doto @chart
                        .showLoading)))]
    (r/create-class
     {:display-name         "dividend-chart"
      :component-did-mount  (fn [el]
                              (reset! chart (echarts/init (-> el r/dom-node)))
                              (update el))
      :component-did-update update
      :reagent-render       (fn []
                              ;;                              (prn "@@@@render")
                              [:div.chart-1
                               {:style {:width  "100%"
                                        :height @height}}
                               [:canvas
                                {:id "secuinfo-dividend-chart"}]])})))


(defn secuinfo-dividend-chart
  []
  (let [option (rf/subscribe [:secuinfo-divirate-chart-option])]
    (fn []
      [secuinfo-dividend-chart-inner-canvas {:option @option}])))

(defn get-sub-key
  [k]
  (if (contains? (set indexinfo-line-types) k)
    :line
    k))

(defn indexinfo-chart-inner-canvas-builder
  [k]
  (let [cate      (rf/subscribe [:indexinfo-chart-cate])
        active    (rf/subscribe [:indexinfo-chart-active])
        ktype     (rf/subscribe [:indexinfo-chart-ktype])
        stockname (rf/subscribe [:indexinfo-name])
        stockid   (rf/subscribe [:indexinfo-id])
        sub-key   (get-sub-key k)
        use-data  (rf/subscribe [(keyword (str "indexinfo-" (name sub-key) "-chart-use-data"))])
        height    (rf/subscribe [:indexinfo-chart-height])
        chart     (r/atom nil)
        update    (fn [el]
                    (.resize @chart)
                    (if-not (or (nil? @use-data) (empty? @use-data))
                      (do
                        (rf/dispatch [:indexinfo-chart-show-yaxis true])
                        (let [option (clj->js ((indexinfo-get-option-fn k)
                                               (if (contains? (set indexinfo-line-types) @cate)
                                                 (@active @use-data)
                                                 @use-data)))]
                          (doto @chart
                            .hideLoading
                            ;; notMerge
                            (.setOption option))))
                      (do
                        (rf/dispatch [:indexinfo-chart-show-yaxis false])
                        (doto @chart .showLoading))))]
    (r/create-class
     {:display-name         (str (name k) "-chart")
      :component-did-mount  (fn [el]
                              (reset! chart (echarts/init (-> el r/dom-node)))
                              (update el)
                              (let [cnt (count (get-in @use-data [:k :origin]))]
                                (when (pos? cnt)
                                  ;; implies (and (= @cate :k) (not= @ktype :fenshi))
                                  (-> @chart
                                      (.on "datazoom" (fn [^js params]
                                                        (let [{:strs [batch]}     (js->clj params)
                                                              {:strs [start end]} (first batch)
                                                              start-index         (Math/round (* cnt start 0.01))
                                                              end-index           (dec (Math/round (* cnt end 0.01)))]
                                                          (rf/dispatch [:indexinfo-datazoom-set-start-end
                                                                        start end start-index end-index]))
                                                        (logger-log (str "stockIndex.k." (name @ktype) ".Slide"))))))))
      :component-did-update (fn [el]
                              ;;(prn "===> did update" @height)
                              (update el))
      :reagent-render       (fn []
                              (let [info-height (case @cate
                                                  :k (case @ktype
                                                       :fenshi 0
                                                       40)
                                                  0)]
                                [:div.chart-1
                                 {:style {:width  "100%"
                                          :height (- @height (* js/rem (base-n info-height)))}}
                                 [:canvas
                                  {:id (str "indexinfo-" (name k) "-chart")}]]))})))

(defn indexinfo-chart
  [k]
  (let [active  (rf/subscribe [:indexinfo-chart-active])
        cate    (rf/subscribe [:indexinfo-chart-cate])
        ktype   (rf/subscribe [:indexinfo-chart-ktype])
        kswitch (rf/subscribe [:indexinfo-chart-kswitch-show])
        height  (rf/subscribe [:indexinfo-chart-height])
        cnt     (rf/subscribe [:indexinfo-data-cnt])]
    (fn []
      (when @height
        [(fn [] (indexinfo-chart-inner-canvas-builder k))
         {:cate    @cate    :active @active :ktype @ktype
          :kswitch @kswitch :cnt    @cnt}]))))

(def indexinfo-dividend-chart
  (indexinfo-chart :divirate))

(def indexinfo-pe-chart
  (indexinfo-chart :pe))

(def indexinfo-pb-chart
  (indexinfo-chart :pb))

(def indexinfo-ps-chart
  (indexinfo-chart :ps))

(def indexinfo-totalmv-chart
  (indexinfo-chart :totalmv))

(def indexinfo-freefloatmv-chart
  (indexinfo-chart :freefloatmv))

(def indexinfo-fenshi-chart
  (indexinfo-chart :fenshi))

(def indexinfo-k-day-chart
  (indexinfo-chart :kday))

(def indexinfo-k-week-chart
  (indexinfo-chart :kweek))

(def indexinfo-k-month-chart
  (indexinfo-chart :kmonth))

(def indexinfo-k-daype-chart
  (indexinfo-chart :kdaype))

(def indexinfo-k-weekpe-chart
  (indexinfo-chart :kweekpe))

(def indexinfo-k-monthpe-chart
  (indexinfo-chart :kmonthpe))
