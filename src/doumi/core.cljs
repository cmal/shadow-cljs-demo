(ns doumi.core
  (:require [reagent.core :as r]
            [re-frame.core :as rf]
            [ajax.core :refer [GET POST]]
            [doumi.ajax :refer [load-interceptors!]]
            [doumi.events]
            [doumi.history :as hst]
            [doumi.routes :as routes]
            [doumi.errorreporter :refer [install-error-reporter]]
            [doumi.stylesheets :refer [stylesheets]]
            [doumi.bottomnav :refer [bottom-nav]]
            [doumi.loader :refer [loader]]
            [doumi.flash-tip :refer [flash-tip]]
            [doumi.search-bar :refer [search]]
            [doumi.feedback :refer [feedback]]
            [doumi.coupon :refer [coupon]]
            [doumi.doumi :refer [doumi]]
            [doumi.annc :refer [annc]]
            [doumi.secuinfo :refer [secuinfo]]
            [doumi.indexinfo :refer [indexinfo]]))

(def pages
  {:annc      #'annc
   :feedback  #'feedback
   :doumi     #'doumi
   :coupon    #'coupon
   :secuinfo  #'secuinfo
   :indexinfo #'indexinfo})

(defn page []
  [:div
   [stylesheets] ;; works only in html5
   [(pages @(rf/subscribe [:page]))]])

;; -------------------------
;; Initialize app

(defn mount-components []
  (rf/clear-subscription-cache!)
  (r/render [#'page] (.getElementById js/document "app")))

(defn ^:export init! []
  (rf/dispatch-sync [:initialize-db])
  (rf/dispatch [:set-active-page :doumi])
  (load-interceptors!)
  (routes/mount-doumi-routes)
  (hst/hook-browser-navigation!)
  (mount-components)
  (install-error-reporter))

(defn ^:export annc-init []
  (rf/dispatch-sync [:initialize-db])
  (rf/dispatch-sync [:set-active-page :annc])
  (rf/dispatch-sync [:set-annc-active-page :annc])
  (rf/dispatch [:annc-set-display-type :date])
  (load-interceptors!)
  (routes/mount-annc-routes)
  (hst/hook-browser-navigation!)
  (mount-components)
  (install-error-reporter))

(defn ^:export bottom-nav-init [id text]
  (rf/dispatch-sync [:initialize-db])
  (r/render [bottom-nav text]
            (.getElementById js/document id)))

(defn ^:export info-init []
  (rf/dispatch-sync [:initialize-db])
  (load-interceptors!)
  (routes/mount-secuinfo-routes)
  (routes/mount-indexinfo-routes)
  (hst/hook-browser-navigation!)
  (mount-components)
  (install-error-reporter))
