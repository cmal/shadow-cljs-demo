(ns doumi.common
  (:require
   [reagent.core :as r]
   [re-frame.core :as rf]
   [doumi.helper :refer [base format-date-slash]]
   [doumi.log :refer [logger-log]]
   ))

(defn no-content
  []
  [:div.no-content
   [:img
    {:src "/wx/img/no-content-1.png"}]])


(defn secondary-separater
  [left]
  [:div.secondary-separator
   {:style {:position   "absolute"
            :width      1
            :height     "2rem"
            :top        0
            :left       (str left "%")
            :background "#efefef"
            :z-index    1}}])

(defn secondary-decoration
  [width-percent active?]
  [:div.decoration
   {:style {:background (if active? "#ffab35" "#fff")
            :width      (str width-percent "%")}}])

(defn secondary-background-color
  [active?]
  (if active? "#f7f7f7" "#fff"))

(defn share-tip
  []
  (let [show? (rf/subscribe [:share-tip-show])]
    (fn []
      [:div
       {:style    {:display  (if @show? "block" "none")
                   :position "fixed"
                   :width    "100%"
                   :height   "100%"
                   :top      0
                   :left     0
                   :bottom   0
                   :right    0
                   :z-index  100}
        :on-click #(rf/dispatch [:share-tip-hide])}
       [:img
        {:style {:width  "100%"
                 :height "100%"}
         :src   "https://static.joudou.com/m/msg/share.png"}]])))

(defn toggle-btn
  ;; valid? is an atom
  [has-rule? k logger]
  [:div.toggle-container
   {:style
    (merge {:box-sizing    "border-box"
            :height        (base 34)
            :width         (base 68)
            :float         "left"
            :border-radius (base 17)
            :background    "#999"
            :position      "relative"}
           (when @has-rule? {:background "#2b4b7b"}))
    :on-click (fn [e]
                (when @has-rule? (rf/dispatch [k]))
                (logger-log logger))}
   [:div.toggle-circle
    {:style
     (merge {:width         (base 30)
             :height        (base 30)
             :background    "#fff"
             :border-radius "50%"
             :position      "absolute"
             :top           (base 2)
             :left          (base 2)
             :transition    "left .2s"}
            (when @has-rule? {:left (base 36)}))}]])

(defn content-separator
  [text]
  [:div.separator-text
   {:style {:position "relative"
            :height   (base 60)}}
   [:div.text
    {:style {:display          "inline"
             :position         "absolute"
             :left             "50%"
             :transform        "translate(-50%,0)"
             :background-color "#f2f3f3"
             :padding-right    ".25rem"
             :padding-left     ".25rem"}}
    text]
   [:div.separator
    {:style {:position         "relative"
             :height           1
             :z-index          -1
             :background-color "#e2e2e2"
             :top              (base 30)}}]])

(defn msg-capital-row
  [{:keys [date title url unread] :as opts}]
  [:div.row
   [:a (merge {}
              (when url {:href url})
              (when-not unread {:color "#444"}))
    [:span.date (format-date-slash date)]
    [:span.title title]
    (when (false? unread)
      [:span.unreadstatus "已读"])]])

(defn msg-capital-content-rows
  ([nothing? content]
   (msg-capital-content-rows nothing? content nil))
  ([nothing? content-1 content-2]
   (if nothing?
     [no-content]
     [:div.msg-capital-content.row-container
      (doall
       (for [item content-1]
         ^{:key (:id item)}
         [msg-capital-row item]))
      (when (and (not (empty? content-2))
                 (not (empty? content-1)))
        [content-separator "全部公告"])
      (doall
       (for [item content-2]
         ^{:key (:id item)}
         [msg-capital-row item]))])))

(defn chart-legend
  [color]
  [:div.chart-legend
   {:style {:background-color color}}])
