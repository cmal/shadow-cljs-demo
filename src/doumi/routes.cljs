(ns doumi.routes
  (:require [secretary.core :as secretary]
            [reagent.core :as r]
            [re-frame.core :as rf]
            [doumi.log :refer [logger-log]]
            [doumi.helper :refer [redirect-to]]
            [doumi.annc-config :refer [get-dividend-logger
                                       get-nextweek-logger
                                       get-nextweek-jump-code]]))

(secretary/set-config! :prefix "#")

(defn mount-secuinfo-routes
  []
  (secretary/defroute
    secuinfo-path
    "/secuinfo"
    [query-params]
    (let [{:keys [id name jump]} query-params]
      (rf/dispatch-sync [:set-active-page :secuinfo])
      (rf/dispatch-sync [:clear-secuinfo-store])
      (rf/dispatch [:secuinfo-set-stockid-stockname id name])
      (when jump (rf/dispatch [:secuinfo-set-active-page jump]))
      (rf/dispatch [:secuinfo-get-scores-info])
      (rf/dispatch [:secuinfo-clear-get-realtime-info-timer])
      (rf/dispatch [:secuinfo-clear-get-fenshi-info-timer])
      (rf/dispatch [:secuinfo-get-realtime-info])
      (rf/dispatch [:secuinfo-get-fenshi-info])
      (rf/dispatch [:secuinfo-get-realtime-info-timer])
      (rf/dispatch [:secuinfo-get-fenshi-info-timer])
      (rf/dispatch [:secuinfo-get-summary-info])
      (rf/dispatch [:secuinfo-get-unlock-info])
      (rf/dispatch [:secuinfo-get-k-day-info])
      (rf/dispatch [:secuinfo-get-k-week-info])
      (rf/dispatch [:secuinfo-get-k-month-info])
      (rf/dispatch [:secuinfo-get-pepbps-history-info])
      (rf/dispatch [:secuinfo-get-earning-history-info])
      (rf/dispatch [:secuinfo-get-revenue-history-info])
      (rf/dispatch [:secuinfo-get-user-info]) ;; should call when some user component mount
      (rf/dispatch [:secuinfo-get-tech-info])
      (rf/dispatch [:secuinfo-get-annc-info])
      ;; (rf/dispatch [:secuinfo-get-rules-info])
      (rf/dispatch [:secuinfo-get-divirate-info]))))

(defn mount-indexinfo-routes
  []
  (secretary/defroute
    indexinfo-path
    "/indexinfo"
    [query-params]
    (let [{:keys [id name intro jump]} query-params]
      (rf/dispatch-sync [:set-active-page :indexinfo])
      (rf/dispatch-sync [:clear-indexinfo-store])
      (rf/dispatch [:indexinfo-set-id-name id name])
      (when intro (rf/dispatch [:indexinfo-show-intro]))
      #_(when jump (rf/dispatch [:indexinfo-set-active-page jump]))
      (rf/dispatch [:indexinfo-clear-get-realtime-timer])
      (rf/dispatch [:indexinfo-clear-get-fenshi-info-timer])
      (rf/dispatch [:indexinfo-get-realtime])
      (rf/dispatch [:indexinfo-get-fenshi-info])
      (rf/dispatch [:indexinfo-get-realtime-timer])
      (rf/dispatch [:indexinfo-get-fenshi-info-timer])
      (rf/dispatch [:indexinfo-get-summary-info])
      (rf/dispatch [:indexinfo-get-kline-info-d])
      (rf/dispatch [:indexinfo-get-kline-info-w])
      (rf/dispatch [:indexinfo-get-kline-info-m])
      (rf/dispatch [:indexinfo-get-line-info])
      (rf/dispatch [:indexinfo-get-reminder])
      (rf/dispatch [:indexinfo-get-ingredients])
      (rf/dispatch [:indexinfo-get-unlocks-info])
      (rf/dispatch [:indexinfo-get-index-list])
      (logger-log "stockIndex.main"))))

#_(defn secuinfo-url
    ([stockid stockname]
     (secuinfo-url stockid stockname nil))
    ([stockid stockname jump]
     (secuinfo-path {:query-params
                     (merge
                      {:id   stockid
                       :name stockname}
                      (when jump {:jump jump}))})))

(defn indexinfo-url
  [{:keys [id name intro jump] :as params}]
  (indexinfo-path {:query-params params}))

(defn secuinfo-url
  ([stockid stockname]
   (secuinfo-url stockid stockname nil))
  ([stockid stockname jump]
   (str "/new-secuinfo?id=" stockid
        "&name=" stockname
        (when jump (str "&jump=" jump)))))

(defn mount-doumi-routes
  []
  (secretary/defroute doumi-doumi-path "/doumi" []
    (rf/dispatch [:set-active-page :doumi]))
  (secretary/defroute doumi-coupon-path "/coupon" []
    (rf/dispatch [:set-active-page :coupon]))
  (secretary/defroute doumi-feedback-path "/feedback" []
    (rf/dispatch [:set-active-page :feedback])))

(defn get-logger-suffix
  [query-params]
  (when-let [template (:template query-params)]
    (case template
      "true"    "FromMsg"
      "suspend" "FromMsgSuspendResumpt"
      template)))

(defn mount-annc-routes
  []
  (secretary/defroute annc-date-path "/annc/date" [query-params]
    ;; the same as {:keys [query-params]}
    (rf/dispatch [:set-annc-active-page :annc])
    (rf/dispatch [:annc-set-display-type :date])
    (logger-log (str "favornews.list" (get-logger-suffix query-params))))

  (secretary/defroute annc-stock-path "/annc/stock" [query-params]
    (rf/dispatch [:set-annc-active-page :annc])
    (rf/dispatch [:annc-set-display-type :stock])
    (logger-log "favornews.listByStock"))

  (secretary/defroute annc-all-path "/annc/all/:stockid/:stockname" [stockid stockname query-params]
    (rf/dispatch [:set-annc-active-page :annc])
    (rf/dispatch [:annc-show-all stockid stockname])
    (logger-log "favornews.stockNews" {:code stockid}))

  (secretary/defroute annc-performance-path "/performance" [query-params]
    (let [{:keys [orderby stockid]} query-params]
      (when (and (= orderby "stockall") stockid)
        (rf/dispatch [:annc-performance-get-data :stockall stockid]))
      (rf/dispatch [:annc-performance-set-orderby (keyword orderby)]))
    (rf/dispatch [:set-annc-active-page :performance])
    (case (:orderby query-params)
      "date"     (logger-log (str "favornews.income" (get-logger-suffix query-params)))
      "stock"    (logger-log "favornews.incomeByStock")
      "stockall" (logger-log "favornews.incomStockNews")))

  (secretary/defroute annc-dividend-path "/dividend" [query-params]
    (rf/dispatch [:dividend-get-data])
    (rf/dispatch [:set-annc-active-page :dividend])
    (logger-log (str "favornews.bonusdivided" (get-logger-suffix query-params))))

  (secretary/defroute annc-nextweek-path "/nextweek" [query-params]
    (rf/dispatch [:nextweek-get-data])
    (rf/dispatch [:set-annc-active-page :nextweek])
    (logger-log (str "favornews.prospect" (get-logger-suffix query-params)))))

(defn get-annc-nav-path
  [k]
  (case k
    :annc        (annc-date-path)
    :performance (annc-performance-path
                  {:query-params {:orderby "date"}})
    :dividend    (annc-dividend-path)
    :nextweek    (annc-nextweek-path)
    (annc-date-path)))

(defn perf-redirect-to [url]
  (logger-log "secuinfo.fundamental.niZoomFromIncome")
  (redirect-to url))

(defn dividend-redirect-to [jump]
  (fn [stockid stockname]
    (logger-log (get-dividend-logger jump))
    (redirect-to (secuinfo-url stockid stockname jump))))

(defn nextweek-redirect-to [cate]
  (fn [stockid stockname]
    (logger-log (get-nextweek-logger cate))
    (redirect-to (secuinfo-url stockid stockname (get-nextweek-jump-code cate)))))
