(ns doumi.feedback
  (:require [doumi.feedback-config :refer [feedback-desc feedback-svgs]]
            [reagent.core :as r]
            [re-frame.core :as rf]
            [doumi.log :refer [logger-log]]
            [doumi.helper :refer [base]]
            [doumi.search-bar :refer [search]]
            [doumi.bottomnav :refer [bottom-nav]]))

(defn feedback-evaluation-change [kw s]
  (let [timer        (r/atom 0)
        time-updater (atom nil)
        time-outer   (atom nil)]
    (r/create-class
     {:display-name "feedback-evaluation-change"
      :component-will-mount
      (fn [kw s]
        (reset! time-updater
                (js/setInterval
                 #(swap! timer inc) 30))
        (reset! time-outer
                (js/setTimeout
                 #(rf/dispatch [:feedback-change-done]) 2000)))
      :component-will-unmount
      (fn [kw s]
        (js/clearInterval @time-updater)
        (js/clearTimeout @time-outer))
      :reagent-render
      (fn [kw s]
        [:div.change
         {:style {:position  "absolute"
                  :font-size (base 24)
                  :top       (base (- -20 @timer))
                  :opacity   (if (>= @timer 20) 0 (- 1 (/ @timer 20)))
                  :left      "1rem"}}
         s])})))

(defn feedback-evaluation-add [kw]
  [feedback-evaluation-change kw "+1"])

(defn feedback-evaluation-minus [kw]
  [feedback-evaluation-change kw "-1"])

(defn feedback-component []
  (r/create-class
   {:display-name "feedback-component"
    :component-will-mount
    (fn []
      (rf/dispatch [:feedback-get-data])
      (logger-log "feedback.comment"))
    :reagent-render
    (fn []
      (let [counts      {:good      (rf/subscribe [:feedback-good])
                         :bad       (rf/subscribe [:feedback-bad])
                         :confusing (rf/subscribe [:feedback-confusing])}
            current     (rf/subscribe [:feedback-current])
            show-count  (rf/subscribe [:feedback-show-count])
            change      (rf/subscribe [:feedback-change])
            change-done (rf/subscribe [:feedback-change-done])]
        [:div.feedback
         {:style {:background "#fff"
                  :margin-top (base 12)}}
         [:div.desc-top
          {:style {:height         (base 24)
                   :padding-top    (base 180)
                   :padding-bottom (base 50)
                   :font-size      (base 24)
                   :color          "#222"
                   :text-align     "center"}}
          "在九斗转了一圈，给我们一个评价吧"
          [:div
           {:style {:padding-top (base 10)}}
           [:i.icon.icon-arrow-down
            {:style {:color "#ffab35"}}]
           [:i.icon.icon-arrow-down
            {:style {:color "#ffab35"}}]
           "猛戳下面图案评价"
           [:i.icon.icon-arrow-down
            {:style {:color "#ffab35"}}]
           [:i.icon.icon-arrow-down
            {:style {:color "#ffab35"}}]
           ]]
         [:div.evaluation
          {:style {:position     "relative"
                   :border       "1px solid #b5b5b5"
                   :height       (base 170)
                   :margin-top   (base 20)
                   :margin-left  (base 20)
                   :margin-right (base 20)}}
          (doall
           (for [kw [:good :bad :confusing]]
             ^{:key (name kw)}
             [:div.icon-wrapper
              {:style    {:position  "absolute"
                          :top       (base 36)
                          :left      (case kw
                                       :good      (base 67)
                                       :bad       (base 263)
                                       :confusing (base 461))
                          :font-size (base 54)}
               :on-click (fn [e]
                           (rf/dispatch [:feedback-on-click kw]))}
              #_[(keyword (str "i.icon.icon-" (name kw)
                               (when (= kw @current) "-active")
                               ))] ;; icomoon 不支持带颜色填充的icon
              [:div.svg
               {:style {:height           (base 54)
                        :width            (base 54)
                        :background-size  (base 54)
                        :background-image (if (= kw @current)
                                            (get-in feedback-svgs [kw :active])
                                            (get-in feedback-svgs [kw :common]))
                        }}]
              (when @show-count
                [:div.count
                 {:style {:font-size   (base 22)
                          :color       "#444"
                          :position    "absolute"
                          :margin-left (base 64)
                          :top         ".5rem"
                          }}
                 @(kw counts)])
              (when (and (not @change-done) (= kw (:add-one @change)))
                [feedback-evaluation-add kw])
              (when (and (not @change-done) (= kw (:minus-one @change)))
                [feedback-evaluation-minus kw])
              [:div.desc
               {:style {:position    "absolute"
                        :font-size   (base 24)
                        :top         (base 78)
                        :white-space "nowrap"}}
               (kw feedback-desc)]]))]
         [:div.desc-bottom
          {:style {:padding-top    (base 30)
                   :padding-bottom (base 378)
                   :font-size      (base 26)
                   :font-weight    "bold"
                   :color          "#ffab35"
                   :text-align     "center"}}
          (get
           {:good      "感谢您的点赞，小九会继续努力的！"
            :bad       "好的，小九会加油的！"
            :confusing "小九争取更简单高效！"}
           @current)]]))}))

(defn feedback []
  [:div
   [search]
   [feedback-component]
   [bottom-nav "我的"]])
