(ns doumi.bottomnav-comp
  (:require [reagent.core :as r]
            [re-frame.core :as rf]
            [doumi.log :refer [logger-log]]
            [doumi.helper :refer [base]]
            [doumi.bottomnav-config :refer [bottom-nav-data
                                            bottom-nav-data-self]]))

(defn bottom-nav-stack
  [show-stack]
  (r/with-let [handler (fn [e]
                         (logger-log "bottomnav.my")
                         (js/setTimeout
                          (fn [] (reset! show-stack false)) 100))
               _ (-> js/document
                     .-body
                     (.addEventListener "touchend" handler false))]
    [:div.stack {:style {:position        "absolute"
                         :background      "#fff"
                         :left            (base 54)
                         :bottom          (base 90)
                         :width           (base 180)
                         :height          (base 234)
                         :font-size       (base 24)
                         :text-align      "left"
                         :color           "#222"
                         :text-decoration "none"
                         :box-shadow      "rgba(130, 130, 130, 0.25) 0px -2px 4px 0px"}}
     [:a {:style    {:display         "block"
                     :height          (base 53)
                     :border-bottom   "1px solid #dbdbdb"
                     :padding-top     (base 25)
                     :padding-left    (base 28)
                     :color           "#222"
                     :text-decoration "none"}
          :href     "/wx/doumi/#/doumi"
          :on-click #(do (logger-log "bottomnav.my.doumi")
                         (rf/dispatch [:doumi-set-page :strategy]))}
      "斗米"]
     [:a {:style    {:display         "block"
                     :height          (base 53)
                     :border-bottom   "1px solid #dbdbdb"
                     :padding-top     (base 25)
                     :padding-left    (base 28)
                     :color           "#222"
                     :text-decoration "none"}
          :href     "/tally/rank"
          :on-click #(logger-log "bottomnav.my.rank")}
      "交易"]
     [:a {:style    {:display         "block"
                     :height          (base 53)
                     :padding-top     (base 25)
                     :padding-left    (base 28)
                     :background      "#fff"
                     :color           "#222"
                     :text-decoration "none"}
          :href     "/wx/doumi/#/feedback"
          :on-click #(logger-log "bottomnav.my.feedback")}
      "意见反馈"]]
    (finally
      (-> js/document
          .-body
          (.removeEventListener "touchend" handler false)))))

(defn bottom-nav-component
  ;; active = "我的", "消息神器", "自选股"/"自选股列表"
  ;; if in annc page, active = "annc"
  ;; if in stock/self page, active = "self"
  [active]
  (let [data       (case active
                     "self" bottom-nav-data-self
                     bottom-nav-data)
        show-stack (r/atom false)]
    (fn [active]
      [:div.bottom-nav
       {:style {:background      "#fff"
                :position        "fixed"
                :bottom          0
                :height          (base 84)
                :width           "100%"
                :box-shadow      "0 -2px 5px 0 rgba(130,130,130,.25)"
                :text-decoration "none"
                :z-index         30}}
       (when @show-stack
         [bottom-nav-stack show-stack])
       (doall
        (for [item data
              :let [{:keys [href icon text]} item]]
          ^{:key icon}
          [:a
           {:href     (or href "javascript:void(0)")
            :on-click (fn [e]
                        (when-not href
                          (reset! show-stack true)
                          (.stopPropagation e)))}
           [:div.bottom-nav-btn
            {:style {:float "left"
                     :color (if (= text active) "#284a7d" "#666")
                     :width (str (int (/ 100 (count data))) "%")}}
            (when-not href
              [:div
               {:style {:position  "absolute"
                        :font-size (base 22)
                        :left      (base 146)
                        :top       (base 24)
                        :color     "#999"}}
               "☰"])
            [:div.bottom-nav-icon
             {:style {:text-align "center"
                      :font-size  (base 32)
                      :margin-top (base 10)}}
             [(keyword (str "i.icon." icon))]]
            [:div.bottom-nav-text
             {:style {:text-align "center"
                      :font-size  (base 24)
                      :margin-top (base -5)}}
             text]]]))
       #_[logger-iframe]])))
