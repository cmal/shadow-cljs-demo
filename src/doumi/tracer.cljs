(ns doumi.tracer
  (:import goog.debug.Trace))


;; Reset the goog.debug.Trace class to record new data.


(def tracer
  (->
   (doto Trace
     (.reset 0))
   #_(.startTracer "events handlers")))

(defn start-tracer
  []
  (-> tracer (.startTracer "events handlers")))

(start-tracer)

(defn add-trace
  [name]
  #_(-> gdbg/Trace (.addComment "after get filtered data"))
  (-> tracer (.addComment name)))


(defn stop-tracer
  []
  (-> tracer (.stopTracer tracer)))

(defn print-report
  []
  (.log js/console (-> tracer .getFormattedTrace)))
