(ns doumi.log
  (:require [reagent.core :as r]
            [re-frame.core :refer [reg-event-fx reg-fx] :as rf]
            [ajax.core :as ajax]
            [ajax.url :as aurl]
            [day8.re-frame.http-fx]
            [doumi.config :refer [env]]
            [doumi.helper :refer [get-url get-logger-uid
                                  get-ref json-stringify
                                  #_zhuge-track add-logger-info
                                  encode-uri]]))

(defonce logger-url "/p/felog/images/log.jpg")
(def logger-ext-url
  (case env
    "www" "https://felog.joudou.com/images/log.jpg"
    "https://felog.beta.joudou.com/images/log.jpg"))

(reg-event-fx
 :logger-success
 (fn [_ _]
   {}))

(reg-event-fx
 :logger-failed
 (fn [_ _]
   {}))

(reg-event-fx
 :logger-log
 (fn [_ [_ {:keys [target data]}]]
   {:http-xhrio {:method          :post
                 :uri             logger-ext-url
                 :params          {:target target
                                   :userid ""
                                   :Uid    (get-logger-uid)
                                   :ref    (get-ref)
                                   :data   (->> data
                                                add-logger-info
                                                json-stringify
                                                encode-uri)}
                 :format          (aurl/url-request-format)
                 :response-format (ajax/json-response-format {:keywords? true})
                 :on-success      [:logger-success]
                 :on-failure      [:logger-failed]}
    }))

(defn logger-log
  ([target]
   (rf/dispatch [:logger-log {:target target :data nil}]))
  ([target data]
   (rf/dispatch [:logger-log {:target target :data data}])))
