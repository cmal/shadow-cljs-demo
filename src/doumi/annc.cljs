(ns doumi.annc
  #_(:require-macros [doumi.macros :refer-macros [decorate]])
  (:require
   [doumi.feedback-config :refer [feedback-desc feedback-svgs]]
   [clojure.string :as str]
   [reagent.core :as r]
   [re-frame.core :as rf]
   [doumi.log :refer [logger-log]]
   [doumi.helper :refer [base set-hash! redirect-to
                         indexed combine-base
                         icon-key stockid2secucode]]
   [doumi.hof :refer [lazy-load-builder]]
   [doumi.search-bar :refer [search]]
   [doumi.bottomnav :refer [bottom-nav]]
   [doumi.annc-config :refer [annc-nav-titles
                              get-perf-icon-name
                              get-perf-icon-color
                              get-dividend-jump-code
                              get-nextweek-icon-name
                              get-nextweek-jump-code]]
   [doumi.loader :refer [loader]]
   [doumi.routes :as routes :refer [secuinfo-url]]
   [doumi.pos :refer [scroll-top inner-height client-height]]
   [doumi.listener :refer [listen-to unlisten-to
                           win-listen-to win-unlisten-to]]
   [doumi.ua :as ua]
   ))

(defn annc-blank-page
  []
  [:div
   {:style {:text-align  "center"
            :padding-top (base 190)}}
   [:img {:src   "/wx/img/doumi-blank.png"
          :style {:width (base 259)}}]
   [:div
    {:style {:padding-top (base 30)
             :font-size   (base 26)
             :color       "#999"}}
    "暂时没有内容，看看其他页面吧"]])

(defn annc-annc-toggle-bar
  []
  (let [annc-display-type (rf/subscribe [:annc-display-type])]
    (fn []
      [:div
       {:style    {:color            "#ffab35"
                   :font-size        (base 24)
                   :text-align       "right"
                   :line-height      (base 45)
                   :height           (base 45)
                   :padding-right    (base 18)
                   :background-color "#f2f3f3"}
        :on-click (fn [e]
                    (set-hash!
                     (case @annc-display-type
                       :stock (routes/annc-date-path)
                       :date  (routes/annc-stock-path)
                       (routes/annc-stock-path)))
                    #_(rf/dispatch [:annc-toggle-display-type]))}
       (case @annc-display-type
         :stock [:span "按日期查看" [:i.icon.icon-switch]]
         :date  [:span "按股票查看" [:i.icon.icon-switch]]
         "< 返回自选股公告")])))

(defn date-group-header
  [index date]
  [:div.group-header
   (str (when (zero? index) "最新 ")
        (str/replace date "-" "/"))])

(def annc-date-group-header
  date-group-header)

(defn annc-unread-status
  [unread]
  [:div.annc-unread-status
   {:style {:display (if unread "none" "block")}}
   "已读"])

(defn annc-date-group-main
  [anncs]
  (let [read (rf/subscribe [:annc-read])]
    (fn [anncs]
      [:div.group-main.annc-main
       (doall
        (for [[index annc] (indexed anncs)
              :let         [{:keys [announcementlink
                                    id
                                    infopubldate
                                    infotitle
                                    innercode
                                    stockid
                                    stockname
                                    unread]} annc
                            unread (or unread (contains? @read id))
                            title (-> infotitle
                                      (str/replace "：" "")
                                      (str/replace stockname ""))]]
          ^{:key index}
          [:div.annc-date-item
           {:style    {:color (if unread "#666" "#999")}
            :on-click (fn [e]
                        (rf/dispatch [:annc-item-clicked id unread])
                        (logger-log "favornews.pdf"))}
           [annc-unread-status unread]
           [:div.text-line
            [:i.icon.icon-pdf]
            (str stockname "(" stockid "): "
                 (->>
                  title
                  (take 34)
                  str/join)
                 (when (> (count title) 34) "..."))]]))])))

(defn stock-group-header
  "route is a function that takes two params, stockid and stockname
  and return a url for change"
  [title-route stockall-route have-right? stockid stockname]
  [:div.group-header
   [:span
    (when title-route
      ;; when given title-route, performance, otherwise annc
      {:on-click
       #(routes/perf-redirect-to (title-route stockid stockname))})
    (str stockname "(" (stockid2secucode stockid) ")")]
   (when have-right?
     [:span
      {:style    {:float         "right"
                  :padding-right (base 12)}
       :on-click (fn [e]
                   (set-hash! (stockall-route stockid stockname)))}
      "全部"])])

(def annc-stock-group-header
  (partial stock-group-header nil
           (fn [stockid stockname]
             (routes/annc-all-path {:stockid stockid :stockname stockname}))
           true))

(defn annc-stock-group-main
  [anncs stockname]
  (let [read (rf/subscribe [:annc-read])]
    [:div.group-main.annc-main
     (doall
      (for [[index annc] (indexed anncs)
            :let         [{:keys [announcementlink
                                  id
                                  infopubldate
                                  infotitle
                                  innercode
                                  unread]} annc
                          unread (or unread (contains? @read id))
                          title (-> infotitle
                                    (str/replace "：" "")
                                    (str/replace stockname ""))]]
        ^{:key index}
        [:div.annc-date-item
         {:style    {:color (if unread "#666" "#999")}
          :on-click (fn [e]
                      (logger-log "favornews.pdf")
                      (rf/dispatch [:annc-item-clicked id unread]))}
         [annc-unread-status unread]
         [:div.text-line
          [:i.icon.icon-pdf]
          (str (-> infopubldate
                   (str/replace "-" "/"))
               "："
               (->> title
                    (take 34)
                    str/join)
               (when (> (count title) 34) "..."))]]))]))

(defn annc-by-date
  []
  (lazy-load-builder
   [:annc-get-date-data]
   [:annc-get-date-data]
   "annc-by-date"
   (fn []
     [:div#annc-by-date
      {:style {:font-size (base 24)}}
        (doall
       (for [[index item] (indexed @(rf/subscribe [:annc-date-data]))
             :let         [{:keys [date announcements]} item]]
         ^{:key (str date "--" index)}
         [:div
          {:style {:background-color "#fff"
                   :margin-bottom    (base 18)}}
          [annc-date-group-header index date]
          [annc-date-group-main announcements]]))])))

(defn annc-by-stock
  []
  (let [data (rf/subscribe [:annc-stock-data])
        loading (rf/subscribe [:annc-stock-loading])]
    (fn []
      [:div
       (when @loading [loader])
       (doall
        (for [[index item] (indexed @data)
              :let         [{:keys [announcements stockid stockname]} item]
              :when        stockid]
          ^{:key index}
          [:div
           {:style {:background-color "#fff"
                    :margin-bottom    (base 18)}}
           [annc-stock-group-header stockid stockname]
           [annc-stock-group-main announcements stockname]]))])))

(defn annc-all-header
  [stockid stockname]
  (let [favorstock? (rf/subscribe [:annc-all-favorstock?])]
    [:div
     {:style {:color         "#284a7d"
              :height        (base 24)
              :line-height   (base 24)
              :font-size     (base 24)
              :padding       (combine-base 12 18 10)
              :border-bottom "1px solid #f2f3f3"
              :background    "#fff"}}
     [:span
      {:on-click #(redirect-to (secuinfo-url stockid stockname))}
      (str stockname "(" (stockid2secucode stockid) ")")]
     [:span
      {:style {:float         "right"
               :padding-right (base 12)
               :font-size     (base 26)}}
      (if @favorstock?
        [:i.icon.icon-checked
         {:style {:color "#999"}}]
        [:i.icon.icon-add
         {:style    {:color "#284a7d"}
          :on-click (fn [e]
                      (rf/dispatch [:search-add-favorstock (stockid2secucode stockid)]))}])]]))

(defn annc-all-main
  [stockid stockname]
  (let [data (rf/subscribe [:annc-all-data])
        read (rf/subscribe [:annc-read])
        loading (rf/subscribe [:annc-all-loading])]
    (lazy-load-builder
     nil
     [:annc-get-all-data true]
     "annc-all-main"
     (fn [stockid stockname]
       [:div
        {:style {:font-size (base 24)}}
        (when @loading [loader])
        (doall
         (for [[index annc] (indexed @data)
               :let         [{:keys [announcementlink id infopubldate infotitle unread]} annc
                             unread (or unread (contains? @read id))
                             title (-> infotitle
                                       (str/replace "：" "")
                                       (str/replace stockname ""))]]
           ^{:key index}
           [:div.all-main.annc-main
            {:style    {:color (if unread "#666" "#999")}
             :on-click (fn [e]
                         (logger-log "favornews.pdf")
                         (rf/dispatch [:annc-item-clicked id unread]))}
            [:div.text-line
             [:i.icon.icon-pdf]
             (str (-> infopubldate
                      (str/replace "-" "/"))
                  "："
                  (->> title
                       (take 34)
                       str/join)
                  (when (> (count title) 34) "..."))]
            [annc-unread-status unread]]))]))))

(defn annc-single-all
  []
  (let [stockid   (rf/subscribe [:annc-all-stockid])
        stockname (rf/subscribe [:annc-all-stockname])]
    (fn []
      [:div
       [annc-all-header @stockid @stockname]
       [annc-all-main @stockid @stockname]
       #_[annc-bottom-nav @stockid @stockname]])))

(defn annc-nav-bar []
  (let [active (rf/subscribe [:annc-active-page])]
    (fn []
      [:div.annc-nav-bar
       [:div
        {:style {:position   "relative"
                 :font-size  (base 26)
                 :height     (base 62)
                 :text-align "center"
                 :border-top "1px solid #f2f3f3"}}
        (doall
         (for [{:keys [id text]} annc-nav-titles
               :let              [active? (= id @active)]]
           ^{:key id}
           [:div.annc-nav-bar-item
            {:on-click #(set-hash!
                         (routes/get-annc-nav-path id))}
            [:div
             {:style {:color      (if active? "#284a7d" "#222")
                      :background (if active? "#f7f8fa" "#fff")}}
             text]
            [:div.annc-nav-decoration
             {:style {:background (if active? "#284a7d" "#fff")}}]]))]])))

(defn annc-annc []
  (let [annc-display-type (rf/subscribe [:annc-display-type])
        favs              (rf/subscribe [:search-favorstocks])
        init              (rf/subscribe [:annc-annc-init])
        loading (rf/subscribe [:annc-date-loading])]
    (r/create-class
     {:display-name         "annc-annc"
      :component-will-mount (fn []
                              (when @init
                                (rf/dispatch [:annc-get-date-data])
                                (rf/dispatch [:annc-get-stock-data])
                                (rf/dispatch [:annc-annc-init])))
      :reagent-render
      (fn []
        (if (not @favs)
          [loader]
          (if (empty? @favs)
            [annc-blank-page]
            [:div
             (when @loading [loader])
             [annc-annc-toggle-bar]
             (case @annc-display-type
               :stock [annc-by-stock]
               :date  [annc-by-date]
               :all   [annc-single-all])])))})))

(defn annc-performance-toggle-bar []
  (let [orderby       (rf/subscribe [:annc-performance-orderby])
        stockall-data (rf/subscribe [:annc-performance-stockall-data])]
    (fn []
      [:div
       {:style    {:color            "#ffab35"
                   :font-size        (base 24)
                   :text-align       "right"
                   :line-height      (base 45)
                   :height           (base 45)
                   :padding-right    (base 18)
                   :background-color "#f2f3f3"}
        :on-click (fn [e]
                    (prn {:query-params (merge
                                         {:orderby (case @orderby
                                                     :date     "stock"
                                                     :stock    "date"
                                                     :stockall "stock")}
                                         (when (= :stockall @orderby)
                                           {:stockid (:stockid @stockall-data)}))})
                    (set-hash!
                     (routes/annc-performance-path
                      {:query-params (merge
                                      {:orderby (case @orderby
                                                  :date     "stock"
                                                  :stock    "date"
                                                  :stockall "stock")}
                                      (when (= :stockall @orderby)
                                        {:stockid (:stockid @stockall-data)}))})))}
       (case @orderby
         :date     [:span "按股票查看" [:i.icon.icon-switch]]
         :stock    [:span "按日期查看" [:i.icon.icon-switch]]
         :stockall "< 返回最新业绩")])))

(def performance-date-group-header
  date-group-header)

(defn detail-link []
  [:span.detail "详情 >"])

(defn perf-item
  [show-detail-link? stockid stockname type title]
  [:div.perf-item
   {:on-click #(routes/perf-redirect-to (secuinfo-url stockid stockname "egr"))}
   [(icon-key (get-perf-icon-name type))
    {:style {:color (get-perf-icon-color type)}}]
   [:div title]
   (when show-detail-link?
     [detail-link])])

(defn annc-performance-by-date []
  (let [date-data (rf/subscribe [:annc-performance-date-data])
        loading (rf/subscribe [:annc-performance-date-loading])]
    (lazy-load-builder
     nil
     [:annc-performance-get-data :date]
     "annc-performance-by-date"
     (fn []
       [:div
        (when @loading [loader])
        (doall
         (for [[index item] (indexed @date-data)
               :let         [{:keys [title data]} item]]
           ^{:key index}
           [:div.perf-group
            [performance-date-group-header index title]
            (doall
             (for [[i item] (indexed data)
                   :let     [{:keys [type title stockname stockid]} item]]
               ^{:key i}
               [perf-item true stockid stockname type title]))]))]))))

(def performance-stock-group-header
  (partial stock-group-header
           (fn [stockid stockname]
             (secuinfo-url stockid stockname "egr"))
           (fn [stockid stockname]
             (routes/annc-performance-path
              {:query-params
               {:stockid stockid
                :orderby "stockall"}}))
           true))

(defn annc-performance-by-stock []
  (let [stock-data (rf/subscribe [:annc-performance-stock-data])
        loading (rf/subscribe [:annc-performance-stock-loading])]
    (fn []
      [:div
       (when @loading [loader])
       (doall
        (for [[index item] (indexed @stock-data)
              :let         [{:keys [title stockid stockname data]} item]
              :when        stockid]
          ^{:key index}
          [:div.perf-group
           [performance-stock-group-header stockid stockname]
           (doall
            (for [[i  item] (indexed data)
                  :let      [{:keys [type date title]} item]]
              ^{:key i}
              [perf-item false stockid stockname type (str date "：" title)]))]))])))

(def performance-stockall-group-header
  (partial stock-group-header
           (fn [stockid stockname]
             (secuinfo-url stockid stockname "egr"))
           (fn [stockid stockname]
             (routes/annc-performance-path
              {:query-params
               {:stockid stockid
                :orderby "stockall"}}))
           false))

(defn annc-performance-by-stockall []
  ;; get data in routes
  (let [stockall-data (rf/subscribe [:annc-performance-stockall-data])
        loading (rf/subscribe [:annc-performance-stockall-loading])]
    (fn []
      (let [{:keys [stockid stockname title data]} @stockall-data]
        [:div
         (when @loading [loader])
         (when stockid
           [:div.perf-group
            [performance-stockall-group-header stockid stockname]
            (doall
             (for [[i item] (indexed data)
                   :let     [{:keys [type date title]} item]]
               ^{:key i}
               [perf-item false stockid stockname type (str date "：" title)]))])]))))

(defn annc-performance []
  (let [orderby (rf/subscribe [:annc-performance-orderby])]
    (fn []
      [:div
       [annc-performance-toggle-bar]
       (case @orderby
         :date     [annc-performance-by-date]
         :stock    [annc-performance-by-stock]
         :stockall [annc-performance-by-stockall])])))

(def dividend-group-header
  date-group-header)

(defn annc-dividend []
  (let [loading (rf/subscribe [:dividend-loading])]
    (lazy-load-builder
     nil
     [:dividend-get-data false]
     "annc-dividend"
     (fn []
       [:div#annc-dividend
        {:style {:font-size (base 24)}}
        (let [dividend-data @(rf/subscribe [:dividend-data])]        ;; FIXME ??
          (when @loading [loader])
          (if (empty? dividend-data)
            [annc-blank-page]
            (doall
             (for [[index item] (indexed dividend-data)
                   :let         [{:keys [title data]} item]]
               ^{:key index}
               [:div.divid-group
                [dividend-group-header index title]
                (doall
                 (for [{:keys [id title stockname stockid]} data]
                   ^{:key id}
                   [:div.divid-item
                    {:on-click #((routes/dividend-redirect-to
                                  (get-dividend-jump-code title))
                                 stockid stockname)}
                    [:i.icon.icon-dividend]
                    [:div.divid-title title]
                    [detail-link]]))]))))]))))

(defn annc-nextweek []
  (let [nextweek-data  (rf/subscribe [:nextweek-data])
        nextweek-title (rf/subscribe [:nextweek-title])
        loading (rf/subscribe [:nextweek-loading])]
    (fn []
      (let [nwdata @nextweek-data]
        (if (empty? nwdata)
          (if @loading
            [loader]
            [annc-blank-page])
          [:div
           [:div
            {:style {:height      (base 46)
                     :line-height (base 46)
                     :font-size   (base 24)
                     :color       "#666"
                     :text-align  "center"}}
            @nextweek-title]
           (doall
            (for [[index item] (indexed nwdata)
                  :let         [{:keys [title data]} item]]
              ^{:key index}
              [:div.nextweek-group
               [:div.group-header title]
               (doall
                (for [[i item] (indexed data)
                      :let     [{:keys [category title stockname stockid]} item
                                #_ (prn "@@@@@@" category)]]
                  ^{:key i}
                  [:div.nextweek-item
                   {:on-click #((routes/nextweek-redirect-to category)
                                stockid stockname)}
                   [(icon-key (get-nextweek-icon-name category))]
                   [:div title]
                   [detail-link]]))]))])))))

(defn annc []
  (let [page (rf/subscribe [:annc-active-page])
        ;; dividend and nextweek duplicate dispatch with route
        ;; currently works because they replace all response data
        _    (rf/dispatch [:dividend-get-data])
        _    (rf/dispatch [:nextweek-get-data])
        _    (rf/dispatch [:annc-performance-get-data :date])
        _    (rf/dispatch [:annc-performance-get-data :stock])]
    (fn []
      [:div
       [search]
       [annc-nav-bar]
       (case @page
         :annc        [annc-annc]
         :performance [annc-performance]
         :dividend    [annc-dividend]
         :nextweek    [annc-nextweek])
       [bottom-nav ""]
       #_[loader]])))
