(ns doumi.doumi
  (:require [doumi.feedback-config :refer [feedback-desc feedback-svgs]]
            [reagent.core :as r]
            [re-frame.core :as rf]
            [doumi.log :refer [logger-log]]
            [doumi.helper :refer [base set-hash! redirect-to indexed]]
            [doumi.search-bar :refer [search]]
            [doumi.bottomnav :refer [bottom-nav]]
            [doumi.doumi-config :refer [vip-price vip-typename
                                        youzan-vip-url]]
            [doumi.loader :refer [loader]]
            [doumi.routes :as routes]
            [doumi.pos :refer [get-element-left scroll-height]]
            [doumi.listener :refer [listen-to unlisten-to]]
            [doumi.ua :as ua]
            ))

(defn doumi-userinfo []
  (let [headimgurl (rf/subscribe [:doumi-headimgurl])
        username   (rf/subscribe [:doumi-username])
        doumi      (rf/subscribe [:doumi-doumi])]
    [:div
     {:style {:background-color "#fff"
              :height           (base 140)
              :border-bottom    "1px solid #f2f3f3"}}
     [:div
      {:style {:float       "left"
               :margin-left (base 20)
               :margin-top  (base 20)}}
      [:img {:src   @headimgurl
             :style {:height        (base 100)
                     :width         (base 100)
                     :border-radius "50%"}}]]
     [:div
      {:style {:padding-top (base 30)
               :margin-left (base 140)
               :font-size   (base 28)
               :color       "#222"}}
      @username]
     [:div
      {:style {:margin-left (base 140)
               :font-size   (base 24)
               :color       "#444"}}
      [:span "拥有："]
      [:span
       {:style {:font-size (base 32)
                :color     "#ffab35"}}
       @doumi]
      [:span
       {:style {:margin-left (base 8)}}
       "斗"]
      [:span
       {:color "#ffab35"}
       [:i.icon.icon-mi
        {:style {:font-size  (base 35)
                 :position   "absolute"
                 :margin-top (base 5)}}]]]]))

(defn doumi-nav []
  (let [page (rf/subscribe [:doumi-page])]
    [:div
     {:style {:background-color "#fff"
              :height           (base 56)
              :font-size        (base 26)
              :padding-top      (base 20)
              :position         "relative"}}
     [:span
      {:style    {:position    "absolute"
                  :margin-left "5%"
                  :color       (if (= @page :strategy) "#284a7d" "#222")}
       :on-click #(do (rf/dispatch [:doumi-set-page :strategy])
                      (logger-log "dm.dmGuide"))}
      "挖米攻略"]
     [:span
      {:style    {:position    "absolute"
                  :margin-left "35%"
                  :color       (if (= @page :log) "#284a7d" "#222")}
       :on-click #(do (rf/dispatch [:doumi-set-page :log])
                      (logger-log "dm.dmNotes"))}
      "挖米/撒米记录"]
     [:span
      {:style    {:position    "absolute"
                  :margin-left "70%"
                  :color       (if (= @page :use) "#284a7d" "#222")}
       :on-click #(rf/dispatch [:doumi-set-page :use])}
      [:i.icon.icon-point-right]
      [:span
       {:on-click #(logger-log "dm.useDoumi")}
       "使用斗米"]]]))

(defn doumi-strategy []
  [:div.doumi-strategy
   {:style {:padding-bottom (base 80)}}
   [:div.title "1. 成为股票专家:"]
   [:div.desc
    "你的自选股专家排名和涨跌排名同时达到90%以上（打败90%的用户），可换取5斗"
    [:i.icon.icon-mi]
    [:div "注：每月每只股票只能兑换一次"]]
   [:div.desc
    [:img {:src "/wx/img/doumi-expert.png"}]]
   [:div.title
    "2. "
    [:a
     {:on-click #(do (rf/dispatch [:doumi-recommend-user])
                     (rf/dispatch [:flash-tip-doumi-sent-to-term])
                     (logger-log "dm.RecommendNewUsers"))}
     "推荐新用户"]
    ":"]
   [:div.desc
    [:div.desc-line
     "把你的专属二维码发送给好友"]
    [:div.desc-line
     "每推荐1个新用户关注九斗数据，奖励5斗"
     [:i.icon.icon-mi]]
    [:div.desc-line
     "成为活跃用户，再奖15斗"
     [:i.icon.icon-mi]]
    [:div.desc-line
     "好友未来在九斗的所有消费，奖励推荐人10%斗"
     [:i.icon.icon-mi]]]
   [:div.title
    "3. "
    [:a
     {:on-click #(do (rf/dispatch [:doumi-find-bug])
                     (rf/dispatch [:flash-tip-doumi-sent-to-term])
                     (logger-log "dm.findBug"))}
     "为九斗捉虫"]
    ":"]
   [:div.desc
    "在九斗数据公众号每提交1个经客服核实的bug，获得5-10斗"
    [:i.icon.icon-mi]]
   [:div.title
    "4. "
    [:a
     {:on-click #(do (logger-log "dm.stockProfit")
                     (redirect-to "/tally/rank")
                     #_(set! (-> js/window .-location .-href)
                             "/tally/rank"))}
     "使用交易追踪"]
    ":"]
   [:div.desc
    "每上传1次交易记录，奖励1斗"
    [:i.icon.icon-mi]]])

(defn doumi-log []
  (let [logs (rf/subscribe [:doumi-log])]
    (fn []
      [:div.doumi-log
       {:style {:background-color "#fff"
                :margin-top       (base 12)
                :color            "#444"}}
       (if (zero? (count @logs))
         [:div
          {:style {:padding-bottom (base 300)
                   :padding-left   (base 80)}}
          [:img
           {:src   "/wx/img/doumi-blank.png"
            :style {:width (base 480)}}]]
         [:div
          {:style {:min-height (base 800)}}
          [:div.title
           {:style {:height        (base 58)
                    :padding-top   (base 20)
                    :border-bottom "1px solid #eee"
                    :font-size     (base 24)}}
           [:span {:style {:margin-left (base 20)}}
            "日期"]
           [:span {:style {:margin-left (base 98)}}
            "方式"]
           [:span {:style {:margin-left (base 217)}}
            "挖米"]
           [:span {:style {:margin-left (base 22)}}
            "撒米"]
           [:span {:style {:margin-left (base 22)}}
            "结余"]]
          (doall
           (for [[index log] (indexed @logs)
                 :let        [{:keys [date content value balance]} log]]
             ^{:key index}
             [:div.log-item ;; css moved to garden to reduce DOM size
              [:span.date date]
              [:span.content content]
              [:span.wami (if (pos? value) (str "+" value) "")]
              [:span.sami (if (pos? value) "" value)]
              [:span.balance balance]]))])])))

(defn doumi-use-nav []
  (let [use-page (rf/subscribe [:doumi-use-page])]
    (fn []
      (let [border-coupon (if (= @use-page :coupon)
                            "1px solid #ffab35"
                            "1px solid #ccc")
            border-vip    (if (= @use-page :coupon)
                            "1px solid #ccc"
                            "1px solid #ffab35")]
        [:div
         {:style {:height    (base 58)
                  :position  "relative"
                  :font-size (base 24)}}
         [:div.nav-left
          {:style    {:border-top-left-radius    (base 4)
                      :border-bottom-left-radius (base 4)
                      :border-top                border-coupon
                      :border-left               border-coupon
                      :border-right              "1px solid #ffab35"
                      :border-bottom             border-coupon
                      :color                     (if (= @use-page :coupon)
                                                   "#ffab35"
                                                   "#444")
                      :height                    (base 42)
                      :width                     (base 300)
                      :text-align                "center"
                      :padding-top               (base 8)
                      :z-index                   10}
           :on-click #(do (when (= :vip @use-page)
                            (rf/dispatch [:doumi-coupon-set-page :redeem]))
                          (rf/dispatch [:doumi-set-use-page :coupon])
                          (logger-log "dm.exchangeCoupon"))}
          "兑换优惠券"]
         [:div.nav-right
          {:style    {:border-top-right-radius    (base 4)
                      :border-bottom-right-radius (base 4)
                      :border-top                 border-vip
                      :border-left                "none"
                      :border-right               border-vip
                      :border-bottom              border-vip
                      :color                      (if (= @use-page :coupon)
                                                    "#444"
                                                    "#ffab35")
                      :height                     (base 42)
                      :width                      (base 300)
                      :text-align                 "center"
                      :position                   "absolute"
                      :left                       (base 300)
                      :top                        0
                      :padding-top                (base 8)
                      }
           :on-click #(do (when (= :coupon @use-page)
                            (rf/dispatch [:doumi-vip-set-page :redeem-doumi])
                            (rf/dispatch [:doumi-use-vip-set-doumi-page :redeem]))
                          (rf/dispatch [:doumi-set-use-page :vip])
                          (logger-log "dm.exchangeStockVip"))}
          "兑换个股查询VIP"]]))))

(defn doumi-use-no-available []
  (let [doumi-doumi (rf/subscribe [:doumi-doumi])
        doumi-page  (rf/subscribe [:doumi-use-page])]
    (fn []
      [:div
       {:style {:padding-top (base 30)
                :text-align  "center"
                :color       "#444"
                :font-size   (base 24)}}
       [:i.icon.icon-cry
        {:style {:font-size (base 100)
                 :color     "#999"}}]
       [:div
        {:style {:padding-top (base 20)}}
        "抱歉，你当前斗米总数为"
        @doumi-doumi
        (if (= :coupon @doumi-page) "，还不够兑换最小额度" "，无法开通个股查询VIP")
        "~"
        ]
       [:div
        {:style {:padding-top (base 10)}}
        "点击"
        [:a
         {:on-click #(rf/dispatch [:doumi-set-page :strategy])
          :style    {:color           "#284a7d"
                     :text-decoration "underline"}
          }
         "挖米攻略"]
        "，快去挖米攒米吧！"]
       [:div
        {:style {:color           "#284a7d"
                 :text-decoration "underline"
                 :padding-top     (base 40)
                 :padding-bottom  (base 80)
                 }
         }
        (if (= :coupon @doumi-page)
          [:span {:on-click #(do
                               (logger-log "dm.myCouponDmInsufficient")
                               (set-hash! (routes/doumi-coupon-path)))}
           "查看优惠券"]
          [:span {:on-click #(do (rf/dispatch [:doumi-vip-set-page :redeem-rmb])
                                 (logger-log "dm.buyStockVipDmInsufficient"))}
           "我是土豪，直接购买"])]])))

(defn doumi-buy-list [t]
  (let [doumi-doumi            (rf/subscribe [:doumi-doumi])
        doumi-coupon-active    (rf/subscribe [:doumi-coupon-active])
        check-size             (base 40)
        page                   (rf/subscribe [:doumi-use-page])
        vip-page               (rf/subscribe [:doumi-use-vip-page])
        doumi-vip-active-doumi (rf/subscribe [:doumi-vip-active-doumi])
        doumi-vip-active-rmb   (rf/subscribe [:doumi-vip-active-rmb])]
    (fn [d]
      (let [kw        (keyword (str t))
            left      (if (= :coupon @page)
                        (case t
                          10  (base 10)
                          20  (base 210)
                          50  (base 410)
                          100 (base 10)
                          500 (base 210))
                        (case t
                          :month    (base 10)
                          :season   (base 210)
                          :halfyear (base 410)
                          :year     (base 210)))
            top       (if (= :coupon @page)
                        (if (> t 50)
                          (base 116)
                          (base 20))
                        (case t
                          :year (base 116)
                          (base 20)))
            activated (case @page
                        :coupon (= kw @doumi-coupon-active)
                        (case @vip-page
                          :redeem-doumi (= t @doumi-vip-active-doumi)
                          (= t @doumi-vip-active-rmb)))
            border    (str "1px solid "
                           (cond
                             (case @page
                               :coupon (< @doumi-doumi t)
                               (case @vip-page
                                 :redeem-doumi (< @doumi-doumi (t vip-price))
                                 true)) "#999"
                             activated  "#ffab35"
                             :else      "#444"))]
        [:div
         {:style    {:position    "absolute"
                     :padding-top (base 4)
                     :height      (base 72)
                     :width       (base 180)
                     :left        left
                     :top         top
                     :text-align  "center"
                     :border      border}
          :on-click (fn [e]
                      (case @page
                        :coupon (when (>= @doumi-doumi t)
                                  (rf/dispatch [:doumi-coupon-set-active kw]))
                        (case @vip-page
                          :redeem-doumi (when (>= @doumi-doumi (t vip-price))
                                          (rf/dispatch [:doumi-vip-set-active-doumi t]))
                          (rf/dispatch [:doumi-vip-set-active-rmb t]))))}
         [:div
          {:style {:font-size (base 26)
                   :color     (if (or
                                   (and (= :coupon @page) (< @doumi-doumi t))
                                   (and (= :vip @page)
                                        (= :redeem-doumi @vip-page)
                                        (< @doumi-doumi (t vip-price))))
                                "#999"
                                "#444")}}
          (case @page
            :coupon (str "￥" t)
            (str (vip-typename t) "VIP"))]
         [:div
          {:style {:font-size (base 20)
                   :color     "#999"}}
          (case @page
            :coupon (str t "斗米")
            (let [price (case t
                          :month    39
                          :season   109
                          :halfyear 209
                          :year     399)]
              (case @vip-page
                :redeem-doumi (str price "斗米")
                (str "￥" price))))]
         [:div.check
          {:style {:display          (if activated "block" "none")
                   :background-image (str "linear-gradient(45deg,#fff "
                                          (base 32) ;; should ~~ 1.414/2*40
                                          ",#ffab35 0)")
                   :position         "absolute"
                   :width            check-size
                   :height           check-size
                   :top              0
                   :right            0
                   :color            "#fff"}}
          [:i.icon.icon-check-non-border
           {:style {:position "absolute"}}]]
         (when (and (= @page :vip)
                    (= @vip-page :redeem-rmb)
                    (= t :season))
           #_[:div.recommend
              {:style {:display          "block"
                       :background-image (str "linear-gradient(-45deg,#fff "
                                              (base 32) ;; should ~~ 1.414/2*40
                                              ",#ffab35 0)")
                       :position         "absolute"
                       :width            check-size
                       :height           check-size
                       :top              0
                       :left             0
                       :color            "#fff"}}
              [:span {:margin-left (base -6)
                      :font-size   (base 12)
                      :line-height (base 6)}
               "荐"]]
           [:img {:src   "/wx/img/tui-jian.png"
                  :style {:top      0
                          :left     0
                          :position "absolute"
                          :width    check-size
                          :height   check-size}}])]))))

(defn doumi-use-coupon-custom-coupon-input []
  (let [doumi-doumi (rf/subscribe [:doumi-doumi])
        active      (rf/subscribe [:doumi-coupon-active])
        value       (rf/subscribe [:doumi-use-coupon-custom-value])]
    (r/create-class
     {:display-name        "doumi-use-coupon-custom-coupon-input"
      :component-did-mount (fn [e]
                             (.focus (r/dom-node e)))
      :reagent-render
      (fn []
        [:input {:style
                 {:position         "absolute"
                  :background-color "transparent"
                  :border           "none"
                  :top              0
                  :left             0
                  :width            (base 180)
                  :height           (base 58)
                  :outline          "none"
                  :font-size        (base 24)
                  :color            (cond
                                      (not= :custom @active)         "transparent"
                                      (and (>= @value 10)
                                           (>= @doumi-doumi @value)) "#444"
                                      :else                          "#f00")
                  :text-align       "center"}
                 :id       "doumi-custom-input"
                 :type     "number"
                 :step     "1"
                 :min      10
                 :max      @doumi-doumi
                 :on-input (fn [e]
                             (let [val (-> e .-target .-value)]
                               (when-not (empty? val)
                                 (let [val (js/parseInt val)]
                                   (rf/dispatch [:doumi-coupon-set-custom-value val])
                                   (if (and (>= val 10)
                                            (>= @doumi-doumi val))
                                     (rf/dispatch [:doumi-use-coupon-custom-valid true])
                                     (rf/dispatch [:doumi-use-coupon-custom-valid false]))))))}])})))

(defn doumi-use-coupon-custom-coupon []
  (let [doumi-doumi         (rf/subscribe [:doumi-doumi])
        doumi-coupon-active (rf/subscribe [:doumi-coupon-active])
        custom-valid        (rf/subscribe [:doumi-use-coupon-custom-valid])]
    (fn []
      [:div
       {:style    {:position    "absolute"
                   :padding-top (base 4)
                   :font-size   (base 20)
                   :height      (base 72)
                   :width       (base 180)
                   :border      (str "1px solid "
                                     (case @doumi-coupon-active
                                       :custom (if @custom-valid
                                                 "#ffab35""#f00")
                                       "#444"))
                   :left        (base 410)
                   :top         (base 116)}
        :on-click (fn [e]
                    (rf/dispatch [:doumi-coupon-set-active :custom])
                    (.focus (.getElementById js/document "doumi-custom-input")))}
       (when-not (= :custom @doumi-coupon-active)
         [:div
          [:div
           {:style {:font-size (base 26)}}
           "自定义额度"]
          [:div
           {:style {:color     "#999"
                    :font-size (base 20)}}
           (str "(10 ~ " @doumi-doumi "斗米)")]])
       (if ua/isAndroid
         (when (= :custom @doumi-coupon-active)
           [doumi-use-coupon-custom-coupon-input])
         [doumi-use-coupon-custom-coupon-input])])))

(defn doumi-redeem-button []
  (let [custom-valid           (rf/subscribe [:doumi-use-coupon-custom-valid])
        coupon-active          (rf/subscribe [:doumi-coupon-active])
        coupon-disable         (rf/subscribe [:doumi-coupon-redeem-disable])
        page                   (rf/subscribe [:doumi-use-page])
        vip-page               (rf/subscribe [:doumi-use-vip-page])
        doumi-vip-active-doumi (rf/subscribe [:doumi-vip-active-doumi])
        doumi-vip-active-rmb   (rf/subscribe [:doumi-vip-active-rmb])
        vip-disable            (rf/subscribe [:doumi-vip-redeem-disable])
        ]
    (fn []
      (let [disabled (if (= @page :vip)
                       @vip-disable
                       (or @coupon-disable
                           (and
                            (= :custom @coupon-active)
                            (not @custom-valid))))]
        [:div
         {:style    {:margin-top       (base 60)
                     :position         "relative"
                     :padding-top      (base 12)
                     :height           (base 48)
                     :width            (base 200)
                     :background-color (if-not disabled "#ffab35" "#999")
                     :color            "#fff"
                     :font-size        (base 24)
                     :text-align       "center"
                     :left             "50%"
                     :transform        "translate(-50%,0)"
                     }
          :on-click (fn [e]
                      (case @page
                        :coupon (when-not disabled
                                  (rf/dispatch [:doumi-redeem-coupon]))
                        (case @vip-page
                          :redeem-doumi (rf/dispatch [:doumi-redeem-vip-doumi
                                                      @doumi-vip-active-doumi])
                          (-> js/window
                              .-location
                              (.replace (youzan-vip-url @doumi-vip-active-rmb))))))}
         (if (and (= :vip @page)
                  (= :redeem-rmb @vip-page))
           "立即购买"
           "立即兑换")]))))

(defn pay-tip []
  (let [width          (r/atom 0)
        qm-offsetleft  (rf/subscribe [:doumi-style-pay-helper-qm-offsetleft])
        qm-offsetwidth (rf/subscribe [:doumi-style-pay-helper-qm-offsetwidth])]
    (r/create-class
     {:display-name "pay-tip"
      :component-did-mount
      (fn [el]
        (reset! width (-> el r/dom-node .-offsetWidth)))
      :reagent-render
      (fn []
        [:div#pay-tip.tip
         {:style {:position         "relative"
                  :background-color "#000"
                  :border-radius    (base 4)
                  :color            "#fff"
                  :padding          (base 12)
                  :font-size        (base 22)
                  :width            (base 308)
                  :left             (base 210)
                  :margin-left      (base -12)
                  :top              (base -18)}}
         "注意：完成支付后，需点击⎡已完成支付⎦，才能购买成功"])})))

(defn pay-tip-triangle []
  (let [qm-offsetleft  (rf/subscribe [:doumi-style-pay-helper-qm-offsetleft])
        qm-offsetwidth (rf/subscribe [:doumi-style-pay-helper-qm-offsetwidth])]
    [:div.triangle
     {:style {:position     "relative"
              :height       0
              :width        0
              :top          (base -18)
              :border       (str (base 18)
                                 " solid #000")
              :left         @qm-offsetleft
              :margin-left  (base -24)
              :border-color "transparent transparent #000 transparent"
              }}]))

(defn doumi-pay-helper []
  (r/with-let [hide-pay-tip (fn [e] (rf/dispatch [:doumi-use-vip-set-show-help false]))
               show-tip     (rf/subscribe [:doumi-use-vip-rmb-show-help])
               icon-question (atom nil)
               _ (listen-to "touchend" hide-pay-tip)]
    [:span
     {:style    {:position    "relative"
                 :margin-left (base 8)}
      :color    "#000"
      :on-click (fn [e]
                  (let [element (r/dom-node @icon-question)]
                    (rf/dispatch [:doumi-use-vip-set-show-help (not @show-tip)])
                    (rf/dispatch [:doumi-style-set-pay-helper-qm-offsetleft
                                  (-> element get-element-left)])
                    (rf/dispatch [:doumi-style-set-pay-helper-qm-offsetwidth
                                  (-> element .-offsetWidth)])
                    (.stopPropagation ^js e))
                  (js/setTimeout #(-> js/window
                                      (.scrollTo 0 (scroll-height))) 25))}
     [:i#icon-question.icon.icon-question
      {:ref (fn [com] (reset! icon-question com))}]
     (when @show-tip [pay-tip-triangle])
     (when @show-tip [pay-tip])]
    (finally
      (-> js/document
          .-body
          (.removeEventListener "touchend" hide-pay-tip false)))))

(defn doumi-use-available
  []
  (let [page     (rf/subscribe [:doumi-use-page])
        vip-page (rf/subscribe [:doumi-use-vip-page])]
    (fn []
      [:div
       {:style {:position    "relative"
                :padding-top (base 20)
                :text-align  "center"}}
       [:div
        {:style {:padding-top (base 30)
                 :font-size   (base 24)
                 :color       "#444"}}
        (if (= :coupon @page)
          "兑换的优惠券可用于九斗所有付费产品"
          "个股查询VIP可无限次查询个股数据")]
       [:div
        {:style {:font-size   (base 22)
                 :color       "#444"
                 :padding-top (base 20)}}
        (case @page
          :coupon "请选择您要兑换的额度"
          (case @vip-page
            :redeem-doumi "请选择您要兑换的VIP"
            "请选择您要购买的VIP"))]
       [:div
        {:style {:padding-top (base 20)
                 :height      (base 172)
                 :position    "relative"}}
        (doall
         (let [lst    (if (= :coupon @page)
                        [10 20 50 100 500]
                        [:month :season :halfyear :year])
               prefix (when (= :vip @page) @vip-page) ]
           (for [d lst]
             ^{:key (str prefix d)}
             [doumi-buy-list d])))
        (when (= :coupon @page)
          [doumi-use-coupon-custom-coupon])]
       [doumi-redeem-button]
       [:div
        {:style {:color          "#284a7d"
                 :text-align     "center"
                 :font-size      (base 24)
                 :padding-top    (base 20)
                 :padding-bottom (base 80)
                 }
         ;; :on-click #(if (= :coupon @page)
         ;;              (rf/dispatch [:doumi-set-use-page :log])
         ;;              (rf/dispatch [:doumi-vip-set-page :redeem-rmb]))
         }
        (case @page
          :coupon [:span {:style    {:text-decoration "underline"}
                          :on-click #(do
                                       (logger-log "dm.myCoupon")
                                       (set-hash! (routes/doumi-coupon-path)))}
                   "查看优惠券"]
          (case @vip-page
            :redeem-doumi [:span {:style    {:text-decoration "underline"}
                                  :on-click #(do (rf/dispatch [:doumi-vip-set-page :redeem-rmb])
                                                 (logger-log "dm.buyStockVip"))}
                           "我是土豪，直接购买"]
            [:span
             [:span {:style    {:text-decoration "underline"}
                     :on-click #(rf/dispatch [:doumi-verify-redeem-rmb-vip])}
              "已完成支付"]
             [doumi-pay-helper]]))]])))

(defn doumi-use-coupon-redeem []
  (let [doumi-doumi (rf/subscribe [:doumi-doumi])]
    (if (< @doumi-doumi 10)
      [doumi-use-no-available]
      [doumi-use-available])))

(defn doumi-redeem-success []
  (let [doumi-doumi      (rf/subscribe [:doumi-doumi])
        page             (rf/subscribe [:doumi-use-page])
        amount           (rf/subscribe [:doumi-coupon-redeem-amount])
        result           (rf/subscribe [:doumi-coupon-redeem-result])
        vip-page         (rf/subscribe [:doumi-use-vip-page])
        vip-active-doumi (rf/subscribe [:doumi-vip-active-doumi])
        vip-active-rmb   (rf/subscribe [:doumi-vip-active-rmb])
        vip-valid-date   (rf/subscribe [:doumi-use-vip-redeem-valid-date])]
    (fn []
      [:div
       {:style {:position    "relative"
                :padding-top (base 30)
                :text-align  "center"
                :color       "#444"
                :font-size   (base 24)}}
       [:i.icon.icon-check-circle1
        {:style {:font-size (base 100)
                 :color     "#ffab35"}}]
       [:div
        {:style {:padding-top (base 20)}}
        (str
         (case @page
           :coupon (str "兑换" @amount "元优惠券成功！")
           (case @vip-page
             :rmb (str "个股查询" (vip-typename @vip-active-rmb) "VIP开通成功！")
             (str "个股查询" (vip-typename @vip-active-doumi) "VIP开通成功！" "你还剩" @doumi-doumi "斗米"))))
        ]
       (case @page
         :coupon [:div
                  {:style {:padding-top (base 30)
                           :text-align  "center"}}
                  "还差一步，"
                  [:a
                   {:href  @result
                    :style {:color           "#284a7d"
                            :text-decoration "underline"
                            :font-size       (base 30)
                            :font-weight     900}}
                   "领取优惠券"]]
         [:div {:style {:font-size (base 20)}}
          "你的VIP有效期至：" @vip-valid-date])
       (case @page
         :coupon [:div
                  {:style {:padding-top    (base 110)
                           :padding-bottom (base 200)
                           :text-align     "center"}
                   }
                  "已领取，"
                  [:span {:style    {:color           "#284a7d"
                                     :text-decoration "underline"}
                          :on-click #(do
                                       (rf/dispatch [:doumi-coupon-set-page :redeem])
                                       (rf/dispatch [:doumi-coupon-set-active :10]))}
                   "继续兑换"]]
         [:div
          {:style {:position       "relative"
                   :width          (base 400)
                   :padding-top    (base 110)
                   :padding-bottom (base 80)
                   :left           "50%"
                   :transform      "translate(-50%,0)"
                   :font-size      (base 24)
                   :color          "#284a7d"}}
          [:span {:style    {:float           "left"
                             :text-decoration "underline"}
                  :on-click #(do
                               (logger-log "dm.favorstock.search")
                               (.focus (.getElementById js/document "search-bar-input")))}
           "查询个股数据"]
          [:span {:style    {:float           "right"
                             :text-decoration "underline"}
                  :on-click #(rf/dispatch [(case @vip-page
                                             :redeem-doumi :doumi-use-vip-set-doumi-page
                                             :doumi-use-vip-set-rmb-page) :redeem])}
           "继续"
           (case @vip-page
             :redeem-doumi "兑换"
             "购买")]])
       ])))

(defn doumi-use-coupon []
  (let [doumi-use-coupon-page (rf/subscribe [:doumi-use-coupon-page])]
    (case @doumi-use-coupon-page
      :redeem         [doumi-use-coupon-redeem]
      :redeem-success [doumi-redeem-success])))

(defn doumi-phone []
  (let [phone          (r/atom "")
        code           (r/atom "")
        timer          (r/atom 60)
        phone-verified (rf/subscribe [:doumi-phone-verified])
        countdown      (rf/subscribe [:doumi-phone-countdown])]
    (fn []
      [:div.phone-wrapper
       {:style {:position         "fixed"
                :top              0
                :left             0
                :height           "100%"
                :width            "100%"
                :background-color "rgba(50,50,50,.7)"
                :z-index          10}}
       [:div.phone-panel
        {:style {:position         "relative"
                 :top              "50%"
                 :left             "50%"
                 :transform        "translate(-50%,-50%)"
                 :border-radius    (base 8)
                 :background-color "#fff"
                 :width            (base 500)}}
        [:div.phone-panel-header
         {:style {:padding-top    (base 35)
                  :padding-bottom (base 25)
                  :font-size      (base 26)
                  :color          "#222"
                  :text-align     "center"
                  :border-bottom  "1px solid #e2e2e2"}}
         "验证手机号"]
        [:div.phone-panel-main
         {:style {:padding-top    (base 30)
                  :padding-left   (base 40)
                  :padding-right  (base 40)
                  :padding-bottom (base 45)}}
         [:div.phone-row
          [:input
           {:style       {:height           (base 60)
                          :width            (base 400)
                          :border-radius    (base 8)
                          :border           "1px solid #e2e2e2"
                          :background-color "#f7f7f7"
                          :padding          0
                          :padding-left     (base 16)
                          :font-size        (base 24)
                          :color            "#444"}
            :placeholder "请输入您的手机号"
            :type        "number"
            :value       @phone
            :on-input    (fn [e]
                           (let [value (->> e .-target .-value)]
                             (reset! phone value)))}]]
         [:div.verify-code-row
          {:style {:height         (base 60)
                   :padding-top    (base 30)
                   :padding-bottom (base 40)}}
          [:input
           {:style       {:float            "left"
                          :height           (base 60)
                          :width            (base 224)
                          :border-radius    (base 8)
                          :border           "1px solid #e2e2e2"
                          :background-color "#f7f7f7"
                          :padding          0
                          :padding-left     (base 16)
                          :font-size        (base 24)
                          :color            "#444"}
            :placeholder "请输入验证码"
            :type        "number"
            :value       @code
            :on-input    (fn [e]
                           (let [value (->> e .-target .-value)]
                             (reset! code value)))}]
          [:div.get-verify-code-btn
           {:style    {:float            "left"
                       :background-color (if (zero? @countdown)
                                           "#ffab35"
                                           "#999")
                       :color            "#fff"
                       :font-size        (base 24)
                       :width            (base 150)
                       :height           (base 62)
                       :line-height      (base 62)
                       :margin-left      (base 10)
                       :border-radius    (base 8)
                       :text-align       "center"}
            :on-click (fn [e] (if (and (= 11 (count @phone)) (= "1" (first @phone)))
                                (rf/dispatch [:doumi-phone-verify @phone])
                                (js/alert "请输入正确的手机号码")))}
           (if-not (zero? @countdown)
             (str " (" @countdown "s)")
             "获取验证码")]]
         [:div.verify-bottom-row
          {:style    {:background-color (if (= 4 (count @code))
                                          "#ffab35"
                                          "#999")
                      :font-size        (base 24)
                      :color            "#fff"
                      :text-align       "center"
                      :height           (base 60)
                      :width            (base 416)
                      :line-height      (base 60)
                      :border-radius    (base 8)}
           :on-click (fn [e]
                       (when (= 4 (count @code))
                         (rf/dispatch [:doumi-update-user-phone @phone @code])))}
          "立即验证"]]]])))

(defn doumi-use-vip-redeem-rmb-redeem []
  (let [phone          (rf/subscribe [:doumi-phone])
        phone-verified (rf/subscribe [:doumi-phone-verified])
        show-phone     (rf/subscribe [:doumi-use-vip-rmb-show-phone])]
    [:div
     [doumi-use-available]
     (when (and @show-phone (not (or @phone @phone-verified)))
       [doumi-phone])]))

(defn doumi-use-vip-redeem-rmb-failed []
  [:div
   {:style {:text-align "center"}}
   [:div
    {:style {:position    "relative"
             :padding-top (base 50)
             :text-align  "center"
             :color       "#444"
             :font-size   (base 24)}}
    "抱歉！未找到订单"]
   [:div
    {:style {:position       "relative"
             :padding-top    (base 20)
             :padding-bottom (base 20)
             :text-align     "center"
             :color          "#444"
             :font-size      (base 20)}}
    "微信扫码加客服解决"]
   [:img
    {:src   "/wx/img/custom_service.jpeg"
     :style {:height (base 239)
             :width  (base 234)}}]
   [:div
    {:style    {:padding-top     (base 70)
                :padding-bottom  (base 80)
                :color           "#284a7d"
                :font-size       (base 24)
                :text-decoration "underline"}
     :on-click #(rf/dispatch [:doumi-use-vip-set-rmb-page :redeem])}
    "重新购买"]])

(defn doumi-use-vip-redeem-doumi []
  (let [page        (rf/subscribe [:doumi-use-vip-doumi-page])
        doumi-doumi (rf/subscribe [:doumi-doumi])]
    (case @page
      :redeem  (if (< @doumi-doumi 39)
                 [doumi-use-no-available]
                 [doumi-use-available])
      :success [doumi-redeem-success])))

(defn doumi-use-vip-redeem-rmb []
  (let [page (rf/subscribe [:doumi-use-vip-rmb-page])]
    (case @page
      :redeem  [doumi-use-vip-redeem-rmb-redeem]
      :success [doumi-redeem-success]
      :failed  [doumi-use-vip-redeem-rmb-failed])))

(defn doumi-use-vip []
  (let [doumi-use-vip-page (rf/subscribe [:doumi-use-vip-page])]
    (case @doumi-use-vip-page
      :redeem-doumi [doumi-use-vip-redeem-doumi]
      :redeem-rmb   [doumi-use-vip-redeem-rmb])))

(defn doumi-use-main []
  (let [use-page (rf/subscribe [:doumi-use-page])]
    [:div
     (if (= :coupon @use-page)
       [doumi-use-coupon]
       [doumi-use-vip])]))

(defn doumi-use []
  [:div
   {:style {:background-color "#fff"
            :margin-top       (base 12)
            :padding          (base 20)
            }}
   [doumi-use-nav]
   [doumi-use-main]])

(defn doumi-component []
  (r/create-class
   {:display-name "doumi-component"
    :component-will-mount
    (fn []
      (rf/dispatch [:doumi-get-doumi])
      (rf/dispatch [:doumi-get-userinfo]))
    :reagent-render
    (fn []
      (let [doumi-page (rf/subscribe [:doumi-page])]
        [:div
         {:style
          {:margin-top (base 12)}}
         [doumi-userinfo]
         [doumi-nav]
         (case @doumi-page
           :strategy [doumi-strategy]
           :log      [doumi-log]
           :use      [doumi-use])]))}))

(defn doumi []
  (let [loading (rf/subscribe [:doumi-loading])]
    (fn []
      [:div
       [search]
       [doumi-component]
       [bottom-nav "我的"]
       (when @loading [loader])])))
