(ns doumi.secuinfo
  (:require
   #_[rid3.core :as rid3]
   [reagent.core :as r]
   [re-frame.core :as rf]
   [clojure.string :as str]
   [doumi.search-bar :refer [search]]
   [doumi.helper :refer [stockid2secucode icon-key num-name
                         base indexed format-date-slash
                         get-stock-title format-delta-percent]]
   [doumi.pos :refer [screen screen-height doc-width]]
   [doumi.log :refer [logger-log]]
   [doumi.secuinfo-config :refer [secuinfo-nav-data secuinfo-noodles-data
                                  headline-cnt
                                  k-chart-nav-data line-chart-nav-data
                                  get-volume-color get-zdf-color zd-color
                                  secuinfo-mask-img-data pay-urls
                                  noodles-logger tech-nav-logger
                                  fundamental-nav-logger
                                  capital-nav-logger
                                  msg-nav-logger
                                  help-logger]]
   [doumi.common :refer [no-content secondary-separater
                         secondary-decoration secondary-background-color
                         share-tip content-separator
                         msg-capital-content-rows]]
   [doumi.weixin :as wx]
   [doumi.util :refer [deep-merge]]
   #_[doumi.svgcalc :refer [line-chart]]
   [doumi.charts :refer [secuinfo-dividend-chart]]
   ))

(defn stock-title
  []
  (let [stockname     (rf/subscribe [:secuinfo-stockname])
        stockid       (rf/subscribe [:secuinfo-stockid])
        current-price (rf/subscribe [:secuinfo-current-price])
        delta-percent (rf/subscribe [:secuinfo-delta-percent])
        favorstocks   (rf/subscribe [:search-favorstocks])]
    (fn []
      [:div#stock-title.stock-title
       {:style {:position         "relative"
                :font-size        ".65rem"
                :background-color "#fff"
                :padding-left     ".5rem"
                :padding-right    ".5rem"
                :margin-top       ".3rem"
                :border-bottom    "1px solid #e2e2e2"
                :height           (base 58)
                :line-height      (base 58)}}
       [:span.stock-name
        {:style    {:display "inline-block"}
         :on-click #(rf/dispatch [:secuinfo-swiper-set-default])}
        (get-stock-title @stockname @stockid)]
       [:span.current-price
        {:style {:margin-left "1rem"
                 :color       (cond
                                (pos? @delta-percent) (:pos zd-color)
                                (neg? @delta-percent) (:neg zd-color)
                                :else                 (:zero zd-color))}}
        [:span.price (or @current-price "--")]
        [:span.percent
         {:style {:margin-left "1rem"}}
         (format-delta-percent @delta-percent)]]
       (let [in-fav? (contains? @favorstocks @stockid)]
         [:span.stock-title-add-wrapper
          {:style {:float "right"
                   :color (if in-fav? "#666" "#284a7d")}}
          [(icon-key (if in-fav? "checked" "add"))]])])))

(defn secu-balance-minus-one
  []
  (let [timer        (r/atom 0)
        update-timer (atom nil)
        done-timer   (atom nil)]
    (r/create-class
     {:display-name "secu-balance-nimus-one"
      :component-will-mount
      (fn []
        (reset! update-timer (js/setInterval #(swap! timer inc) 30))
        (reset! done-timer (js/setTimeout #(rf/dispatch [:secuinfo-minus-one-done]) 2000)))
      :component-will-unmount
      (fn []
        (js/clearInterval @update-timer)
        (js/clearTimeout @done-timer))
      :reagent-render
      (fn []
        [:div.minus-one
         {:style {:position  "absolute"
                  :font-size (base 24)
                  :top       (base (- -20 @timer))
                  :opacity   (if (>= @timer 20) 0 (- 1 (/ @timer 20)))
                  :left      "1rem"}}])})))

(defn stock-stars
  []
  (let [stock-stars  (rf/subscribe [:secuinfo-stock-stars])
        vip?         (rf/subscribe [:secuinfo-is-vip])
        secu-balance (rf/subscribe [:secuinfo-secu-balance])]
    (fn []
      [:div.stock-stars
       {:style {:position         "relative"
                :padding-left     ".45rem"
                :padding-top      ".45rem"
                :height           (base 58)
                :background-color "#fff"}}
       (into
        [:div.stars
         {:style
          {:float       "left"
           :color       "#fb6969"
           :font-size   ".65rem"
           :line-height ".65rem"}}]
        (when-let [stars @stock-stars]
          (repeat stars [(icon-key "new-star-full")])))
       (when-let [stars @stock-stars]
         [:div.img
          {:style {:position  "absolute"
                   :left      "50%"
                   :transform "translate(-50%, 0)"}}
          [:img {:style {:height ".65rem"}
                 :src   (str "/wx/img/" (num-name stars) "-xing.png")}]])
       [:div.stamp
        {:style    {:position      "relative"
                    :float         "right"
                    :padding-right ".45rem"
                    :font-size     ".65rem"}
         :on-click (fn [e]
                     (rf/dispatch [:secuinfo-show-pay-mask]))}
        (if @vip?
          [:span
           {:style {:color "#ffab35"}}
           [(icon-key "v")]
           [(icon-key "i")]
           [(icon-key "p")]]
          [:span
           {:style {:color "#fb6969"}}
           @secu-balance])]
       (when-not @vip?
         (when (pos? @secu-balance)
           [secu-balance-minus-one]))])))

(defn secuinfo-noodles
  []
  (let [active-noodle    (rf/subscribe [:secuinfo-active-noodle])
        scores           (rf/subscribe [:secuinfo-scores])
        pay-mask-showed? (r/atom false)]
    (fn []
      [:div.noodles
       {:style {:position         "relative"
                :text-align       "center"
                :font-size        ".65rem"
                :color            "#284a7d"
                :background-color "#fff"
                :margin-top       ".3rem"
                :height           "2.4rem"}}
       (doall
        (for [[index item] (indexed secuinfo-noodles-data)
              :let         [noodle (get secuinfo-noodles-data index)
                            noodle-name (:name noodle)]]
          ^{:key index}
          [:div
           (when-not (zero? index)
             [:div.noodle-separator
              {:style {:position   "absolute"
                       :width      1
                       :height     "2.4rem"
                       :top        0
                       :left       (str (* 25 index) "%")
                       :background "#efefef"
                       :z-index    1}}])
           [:div.noodle-item
            {:style    {:position   "relative"
                        :width      "25%"
                        :display    "inline-block"
                        :float      "left"
                        :height     "2.4rem"
                        :background (if (= @active-noodle noodle-name)
                                      "#f3f2f2"
                                      "#fff")}
             :on-click (fn []
                         (when (and (not @pay-mask-showed?)
                                    (not (noodle-name @scores)))
                           (rf/dispatch [:secuinfo-show-pay-mask])
                           (reset! pay-mask-showed? true))
                         (rf/dispatch [:secuinfo-set-active-noodle noodle-name]))}
            [:div.noodle-decoration
             {:style {:position   "relative"
                      :height     ".1rem"
                      :background (if (= @active-noodle noodle-name)
                                    "#ffab35"
                                    "#fff")}}]
            [:div.noodle-name
             {:style {:padding-top ".25rem"}}
             (:text noodle)]
            [:div.noodle-score
             (or (some-> (noodle-name @scores) (.toFixed 0))
                 [:i.icon.icon-lock-empty])]]]))])))

(defn secondary-tech
  []
  (let [tech-info   (rf/subscribe [:secuinfo-secondary-nav-tech])
        tech-active (rf/subscribe [:secuinfo-tech-active])]
    (fn []
      (let [{:keys [volume zdf newhigh]} @tech-info]
        [:div.secondary-tech
         [:div.volume.secondary-item
          {:style    {:background (secondary-background-color (= @tech-active :volume))
                      :color      (get-volume-color (:color volume))}
           :on-click #(rf/dispatch [:secuinfo-set-tech-active :volume])}
          [:div.text
           (:text volume)]
          [:div.value
           (:delta volume)]
          [secondary-decoration 50 (= @tech-active :volume)]]
         [secondary-separater 50]
         [:div.zdf.secondary-item
          {:style    {:background  (secondary-background-color (= @tech-active :zdf))
                      :color       (if (:text newhigh) "#666" (get-zdf-color (:color zdf)))
                      :font-weight (if (:text newhigh) "bold" "normal")}
           :on-click #(rf/dispatch [:secuinfo-set-tech-active :zdf])}
          [:div.text
           (:text zdf)]
          [:div.value
           (:delta zdf)]
          [secondary-decoration 50 (= @tech-active :zdf)]]]))))

(defn secondary-fundamental
  []
  (let [active      (rf/subscribe [:secuinfo-fundamental-active])
        fundamental (rf/subscribe [:secuinfo-fundamental-values])]
    (fn []
      (let [{:keys [pe pb ps rgr egr]} @fundamental]
        [:div.secondary-fundamental
         [:div.pe.secondary-item
          {:style    {:background (secondary-background-color (= @active :pe))}
           :on-click #(rf/dispatch [:secuinfo-set-fundamental-active :pe])}
          [:div.text "PE"]
          [:div.value (or (some-> pe :value (.toFixed 2)) "--")]
          [secondary-decoration 20 (= @active :pe)]]
         [secondary-separater 20]
         [:div.pb.secondary-item
          {:style    {:background (secondary-background-color (= @active :pb))}
           :on-click #(rf/dispatch [:secuinfo-set-fundamental-active :pb])}
          [:div.text "PB"]
          [:div.value (or (some-> pb :value (.toFixed 2)) "--")]
          [secondary-decoration 20 (= @active :pb)]]
         [secondary-separater 40]
         [:div.ps.secondary-item
          {:style    {:background (secondary-background-color (= @active :ps))}
           :on-click #(rf/dispatch [:secuinfo-set-fundamental-active :ps])}
          [:div.text "PS"]
          [:div.value (or (some-> ps :value (.toFixed 2)) "--")]
          [secondary-decoration 20 (= @active :ps)]]
         [secondary-separater 60]
         [:div.rgr.secondary-item
          {:style    {:background (secondary-background-color (= @active :rgr))}
           :on-click #(rf/dispatch [:secuinfo-set-fundamental-active :rgr])}
          [:div.text "营收增速"]
          [:div.value (or (some-> rgr :value (.toFixed 2)) "--")]
          [secondary-decoration 20 (= @active :rgr)]]
         [secondary-separater 80]
         [:div.egr.secondary-item
          {:style    {:background (secondary-background-color (= @active :egr))}
           :on-click #(rf/dispatch [:secuinfo-set-fundamental-active :egr])}
          [:div.text "净利增速"]
          [:div.value (or (some-> egr :value (.toFixed 2)) "--")]
          [secondary-decoration 20 (= @active :egr)]]]))))

(defn secondary-capital
  []
  (let [active  (rf/subscribe [:secuinfo-capital-active])
        capital (rf/subscribe [:secuinfo-capital-values])]
    (fn []
      (let [{:keys [dividend bonus unlock]} @capital]
        [:div.secondary-capital
         [:div.dividend.secondary-item
          {:style    {:background (secondary-background-color (= @active :dividend))}
           :on-click (fn []
                       (rf/dispatch [:secuinfo-set-capital-active :dividend]))}
          [:div.text "股息率"]
          [:div.value (or (some-> dividend (.toFixed 2) (str "%")) "--")]
          [secondary-decoration (/ 100 3) (= @active :dividend)]]
         [secondary-separater (-> 100 (/ 3))]
         [:div.bonus.secondary-item
          {:style    {:background (secondary-background-color (= @active :bonus))}
           :on-click #(rf/dispatch [:secuinfo-set-capital-active :bonus])}
          [:div.text "高送转"]
          [:div.value bonus]
          [secondary-decoration (/ 100 3) (= @active :bonus)]]
         [secondary-separater (-> 100 (/ 3) (* 2))]
         [:div.unlock.secondary-item
          {:style    {:background (secondary-background-color (= @active :unlock))}
           :on-click #(rf/dispatch [:secuinfo-set-capital-active :unlock])}
          [:div.text "6个月解禁"]
          [:div.value (or (some-> unlock (.toFixed 2) (str "%")) "--")]]]))))

(defn secondary-msg
  []
  (let [active (rf/subscribe [:secuinfo-msg-active])]
    (fn []
      [:div
       [:div.secondary-msg
        [:div.dividend.secondary-item
         {:style    {:background (secondary-background-color (= @active :merger))}
          :on-click #(rf/dispatch [:secuinfo-set-msg-active :merger])}
         [:div.one-line-text "并购"]
         [secondary-decoration (/ 100 3) (= @active :merger)]]
        [secondary-separater (-> 100 (/ 3))]
        [:div.dividend.secondary-item
         {:style    {:background (secondary-background-color (= @active :pp))}
          :on-click #(rf/dispatch [:secuinfo-set-msg-active :pp])}
         [:div.one-line-text "定增"]
         [secondary-decoration (/ 100 3) (= @active :pp)]]
        [secondary-separater (-> 100 (/ 3) (* 2))]
        [:div.dividend.secondary-item
         {:style    {:background (secondary-background-color (= @active :jl))}
          :on-click #(rf/dispatch [:secuinfo-set-msg-active :jl])}
         [:div.one-line-text "激励"]
         [secondary-decoration (/ 100 3) (= @active :jl)]]]])))

(defn secuinfo-headline-content
  [cate]
  (let [realtime    (rf/subscribe [:secuinfo-headline-realtime])
        volume      (rf/subscribe [:secuinfo-headline-tech-volume])
        zdf         (rf/subscribe [:secuinfo-headline-tech-zdf])
        newhigh     (rf/subscribe [:secuinfo-headline-tech-newhigh])
        fundamental (rf/subscribe [:secuinfo-fundamental-values])
        capital     (rf/subscribe [:secuinfo-capital-values])]
    (fn [cate]
      (let [{:keys [open turnover mv]}                          @realtime
            {:keys [sixtydays twentydays]}                      @volume
            {:keys [thirty ninety oneyear]}                     @zdf
            {newhigh-thirty  :thirty  newhigh-ninety  :ninety
             newhigh-oneyear :oneyear newhigh-history :history} @newhigh
            sixty                                               (first sixtydays)
            twenty                                              (first twentydays)
            {:keys [pe pb ps rgr egr]}                          @fundamental
            {:keys [dividend]}                                  @capital]
        [:div
         {:style {:height "1.5rem"}}
         (case cate
           :realtime
           [:div.headline-item
            [:span (str "今开：" (or (some-> open (.toFixed 2)) "--"))]
            [:span (str "换手率：" (or (some-> turnover (.toFixed 1) (str "%")) "--"))]
            [:span (str "市值（亿）：" (or (some-> mv (.toFixed 2)) "--"))]]
           :volume
           [:div
            [:div.headline-item
             [:span (str "最新成交额相对于20日均值"
                         (if (some-> twenty pos?) "放" "缩") "量"
                         (or (some-> twenty Math/abs (.toFixed 2)) "--") "%")]]
            [:div.headline-item
             [:span (str "最新成交额相对于60日均值"
                         (if (some-> sixty pos?) "放" "缩") "量"
                         (or (some-> sixty Math/abs (.toFixed 2)) "--") "%")]]]
           :zdf
           [:div
            [:div.headline-item
             [:span (str "30日"
                         (if (some-> thirty pos?) "涨" "跌") "幅"
                         (some-> thirty Math/abs (.toFixed 2)) "%，距30日新高"
                         (some-> newhigh-thirty second) "元")]]
            [:div.headline-item
             [:span (str "90日"
                         (if (some-> ninety pos?) "涨" "跌") "幅"
                         (or (some-> ninety Math/abs (.toFixed 2)) "--") "%，距90日新高"
                         (or (some-> newhigh-ninety second) "--") "元")]]
            [:div.headline-item
             [:span (str "一年"
                         (if (some-> oneyear pos?) "涨" "跌") "幅"
                         (or (some-> oneyear Math/abs (.toFixed 2)) "--") "%，距一年新高"
                         (or (some-> newhigh-oneyear second) "--") "元")]]
            [:div.headline-item
             [:span (str "距历史新高" (-> newhigh-history second) "元")]]]
           :fundamental
           [:div
            (let [{:keys [value gt_history]} pe]
              [:div.headline-item
               [:span (str "PE: " (or (some-> value (.toFixed 2)) "--"))]
               [:span (str " 高于历史" (or (some-> gt_history (.toFixed 2)) "--") "%的日子")]])
            (let [{:keys [value gt_history]} pb]
              [:div.headline-item
               [:span (str "PB: " (or (some-> value (.toFixed 2)) "--"))]
               [:span (str " 高于历史" (or (some-> gt_history (.toFixed 2)) "--") "%的日子")]])
            (let [{:keys [value]} ps]
              [:div.headline-item
               [:span (str "PS: " (or (some-> value (.toFixed 2)) "--"))]])
            (let [{:keys [year season value rate]} rgr]
              [:div.headline-item
               [:span (str year "Q" season)]
               [:span (str "营收：" (some-> value (.toFixed 2)) "亿,")]
               [:span (str "同比增速：" (some-> rate (.toFixed 2)) "%")]])
            (let [{:keys [year season value rate]} egr]
              [:div.headline-item
               [:span (str year "Q" season)]
               [:span (str "净利润" (some-> value (.toFixed 2)) "亿,")]
               [:span (str "同比增速：" (some-> rate (.toFixed 2)) "%")]])]
           :dividend
           [:div.headline-item [:span (str "股息率：" (some-> dividend (.toFixed 2)) "%")]])]))))

(defn secuinfo-secondary-headline-swiper
  []
  (let [cate  (rf/subscribe [:secuinfo-headline-cate])
        index (rf/subscribe [:secuinfo-headline-current-index])]
    (r/create-class
     {:display-name "headline-swiper"
      :component-will-unmount
      (fn [] (rf/dispatch [:secuinfo-clear-headline-swiper-timer]))
      :reagent-render
      (fn []
        (when @cate
          [:div
           {:style {:background  "#eaeaea"
                    :height      "1.5rem"
                    :overflow    "hidden"
                    :position    "relative"
                    :line-height "1.5rem"
                    :font-size   ".6rem"
                    :color       "#333"}}
           [:div.headline-animation
            {:style (merge {:position   "absolute"
                            :transition "top .5s"}
                           (when-let [cnt (headline-cnt @cate)]
                             {:top (str
                                    (* -1.5 (mod @index cnt))
                                    "rem")}))}
            [secuinfo-headline-content @cate]]]))})))

(defn secuinfo-secondary-nav
  []
  (let [active-noodle  (rf/subscribe [:secuinfo-active-noodle])
        update-ui      (fn [el]
                         (prn "secuinfo-secondary-nav set secondary-nav y"
                              (-> el r/dom-node .getBoundingClientRect .-top))
                         (rf/dispatch [:secuinfo-set-secondary-nav-y
                                       (-> el r/dom-node .getBoundingClientRect .-top)]))
        secuinfo-nav-y (rf/subscribe [:secuinfo-secuinfo-nav-y])]
    (r/create-class
     {:display-name         "secondary-nav"
      :component-did-mount  update-ui
      :component-did-update update-ui
      :reagent-render
      (fn []
        [:div.secondary-nav
         {:style {:position    "fixed"
                  :top         (some-> @secuinfo-nav-y
                                       (/ js/rem)
                                       (- 2)
                                       (str "rem"))
                  :height      "2rem"
                  :background  "#fff"
                  :width       "100%"
                  :text-align  "center"
                  :line-height (base 24)
                  :font-size   (base 24)
                  :color       "#666"}}
         (case @active-noodle
           :fundamental [secondary-fundamental]
           :capital     [secondary-capital]
           :msg         [secondary-msg]
           [secondary-tech])])})))

(defn secuinfo-tech-content
  []
  (let [tech-active (rf/subscribe [:secuinfo-tech-active])]
    [:div]))

(defn secuinfo-fundamental-content
  []
  (let [fundamental-active (rf/subscribe [:secuinfo-fundamental-active])]
    [:div]))

(defn secuinfo-dividend-chart-info-switch-bar
  []
  [:div
   {:style {:height     (base 0)
            :background "yellow"}}])

(defn secuinfo-dividend-chart-x-axis-line
  []
  (let [secuinfo-main-height (rf/subscribe [:secuinfo-main-height])
        ;;height               (rf/subscribe [:secuinfo-chart-x-axis-line-height])
        divirate             (rf/subscribe [:secuinfo-divirate-data])
        ;;update-ui
        #_                   (fn [el]
                               (rf/dispatch [:secuinfo-set-chart-x-axis-line-height
                                             (-> el
                                                 r/dom-node
                                                 .getBoundingClientRect
                                                 .-height)]))]
    (fn []
      [:div.dividend-chart-x-axis-line
       {:style {:position    "absolute"
                :top         (- @secuinfo-main-height (* 0.875 js/rem))
                :height      ".875rem" #_ (base 35)
                :line-height ".875rem" #_ (base 35)
                :width       "100%"
                :font-size   (base 20)
                :color       "#999"
                :background  "transparent"}}
       [:div.dividend-x-axis-left
        {:style {:width      "33.333%"
                 :text-align "left"
                 :float      "left"}}
        [:span
         {:style {:padding-left "4px"}}
         (-> @divirate
             first
             :day
             format-date-slash)]]
       [:div.dividend-x-axis-middle
        {:style {:width      "33.333%"
                 :text-align "center"
                 :float      "left"}}
        (-> @divirate
            (nth (quot (count @divirate) 2))
            :day
            format-date-slash)]
       [:div.dividend-x-axis-right
        {:style {:width      "33.333%"
                 :text-align "right"
                 :float      "left"}}
        [:span
         {:style {:padding-right "4px"}}
         (-> @divirate
             last
             :day
             format-date-slash)]]])))

(defn secuinfo-chart-nav
  []
  (let [dividend-active (rf/subscribe [:secuinfo-chart-active])]
    (r/create-class
     {:display-name        "secuinfo-chart-nav"
      :component-did-mount (fn [el]
                             (rf/dispatch [:secuinfo-set-chart-nav-height
                                           (-> el
                                               r/dom-node
                                               .getBoundingClientRect
                                               .-height)]))
      :reagent-render
      (fn []
        [:div.chart-nav
         (doall
          (for [item line-chart-nav-data
                :let [{:keys [k name]} item
                      active? (= k @dividend-active)]]
            ^{:key (str k)}
            [:div.type-item
             {:on-click #(rf/dispatch [:secuinfo-chart-set-active k])
              :style    (merge
                         {:border-bottom (str "1px solid " (if active? "#fdaa43" "#ddd"))}
                         (when active? {:color "#fdaa43"}))}
             name]))])})))

(defn secuinfo-dividend-chart-container
  []
  (let [height (rf/subscribe [:secuinfo-main-content-height])
        _      (rf/dispatch [:secuinfo-set-chart-info-switch-bar-height 0])]
    (fn []
      [:div#dividend-chart.dividend-chart
       {:style {:height     @height
                :background "#f7f7f7"}}
       [secuinfo-chart-nav]
       [:div
        {:style {:clear         "both"
                 :border-bottom "1px solid #ddd"}}
        [secuinfo-dividend-chart-info-switch-bar]
        [secuinfo-dividend-chart]
        [secuinfo-dividend-chart-x-axis-line]]])))

(defn secuinfo-capital-content
  []
  (let [capital-active (rf/subscribe [:secuinfo-capital-active])
        annc           (rf/subscribe [:secuinfo-annc-data])
        unlock         (rf/subscribe [:secuinfo-unlock-data])]
    (fn []
      (case @capital-active
        nil       (let [{:keys [fund]} @annc]
                    [msg-capital-content-rows (empty? fund) fund])
        :dividend [secuinfo-dividend-chart-container]
        :bonus    (let [{:keys [fund]} @annc
                        bonus          (filter #(= "dividend" (:category %)) fund)]
                    [msg-capital-content-rows (empty? bonus) bonus])
        :unlock   (let [{:keys [half_year_ago future_will_unlock]} @unlock]
                    (if (and (empty? half_year_ago) (empty? future_will_unlock))
                      [no-content]
                      [:div.unlocks.row-container
                       [:div.row
                        [:span.date "解禁日期"]
                        [:span.amount "数量（万股）"]
                        [:span.percent "占流通股比例"]]
                       (doall
                        (for [[index item] (indexed half_year_ago)
                              :let         [{:keys [startdateforfloating
                                                    newmarketableashares
                                                    newmarketablemv
                                                    negotiablemv]} item
                                            percent (some-> newmarketablemv
                                                            (/ negotiablemv)
                                                            (* 100))]]
                          ^{:key (str "hya-" index)}
                          [:div.row
                           [:span.date (format-date-slash startdateforfloating)]
                           [:span.amount newmarketableashares]
                           [:span.percent (some-> percent (.toFixed 2))]]))
                       (when-not (empty? future_will_unlock)
                         [content-separator "即将解禁"])
                       (doall
                        (for [[index item] (indexed future_will_unlock)
                              :let         [{:keys [startdateforfloating
                                                    newmarketableashares
                                                    newmarketablemv
                                                    negotiablemv]} item
                                            percent (some-> newmarketablemv
                                                            (/ negotiablemv)
                                                            (* 100))]]
                          ^{:key (str "fwu-" index)}
                          [:div.row
                           [:span.date (format-date-slash startdateforfloating)]
                           [:span.amount newmarketableashares]
                           [:span.percent (some-> percent (.toFixed 2))]]))]))))))

(defn secuinfo-msg-content
  []
  (let [msg-active (rf/subscribe [:secuinfo-msg-active])
        annc       (rf/subscribe [:secuinfo-annc-data])]
    (fn []
      (let [{:keys [merger private incentive announcements]} @annc
            events                                           (concat
                                                              (take 1 merger)
                                                              (take 1 private)
                                                              (take 1 incentive))]
        (case @msg-active
          nil     [msg-capital-content-rows
                   (and (empty? announcements) (empty? events))
                   (sort-by :date events)
                   announcements]
          :merger [msg-capital-content-rows (empty? merger) merger]
          :pp     [msg-capital-content-rows (empty? private) private]
          :jl     [msg-capital-content-rows (empty? incentive) incentive])))))

(defn secuinfo-main-content
  []
  (let [active-noodle (rf/subscribe [:secuinfo-active-noodle])
        height        (rf/subscribe [:secuinfo-main-content-height])
        update-ui     (fn [el]
                        (prn "secuinfo-main-content set main-content-y"
                             (-> el r/dom-node .getBoundingClientRect .-top))
                        (rf/dispatch [:secuinfo-set-main-content-y
                                      (-> el r/dom-node .getBoundingClientRect .-top)]))]
    (r/create-class
     {:display-name         "main-content"
      :component-did-mount  update-ui
      :component-did-update update-ui
      :reagent-render
      (fn []
        [:div.main-content
         {:style {:height                     @height
                  :overflow-y                 "scroll"
                  :-webkit-overflow-scrolling "touch"}}
         (case @active-noodle
           :fundamental [secuinfo-fundamental-content]
           :capital     [secuinfo-capital-content]
           :msg         [secuinfo-msg-content]
           [secuinfo-tech-content])])})))

(defn secuinfo-main
  []
  (let [height    (rf/subscribe [:secuinfo-main-height])
        update-ui (fn [el]
                    (rf/dispatch [:secuinfo-set-secuinfo-main-y
                                  (-> el r/dom-node .getBoundingClientRect .-top)]))]
    (r/create-class
     {:display-name        "secuinfo-main"
      :component-did-mount update-ui
      :reagent-render      (fn []
                             [:div.secuinfo-main
                              {:style
                               {:position       "relative"
                                :width          "100%"
                                :padding-bottom "2rem"
                                :overflow       "hidden"}}
                              [secuinfo-secondary-nav]
                              [secuinfo-secondary-headline-swiper]
                              [secuinfo-main-content]])})))

(defn secuinfo-nav
  []
  (let [stockname    (rf/subscribe [:secuinfo-stockname])
        stockid      (rf/subscribe [:secuinfo-stockid])
        stock-stars  (rf/subscribe [:secuinfo-stock-stars])
        vip?         (rf/subscribe [:secuinfo-is-vip])
        secu-balance (rf/subscribe [:secuinfo-secu-balance])
        scores       (rf/subscribe [:secuinfo-scores])
        ;;        active-noodle (rf/subscribe [:secuinfo-active-noodle])
        ]
    (r/create-class
     {:display-name         "secuinfo-bottom-nav"
      :component-will-mount (fn [])
      :component-did-mount  (fn [el]
                              (logger-log "secuinfo.main" {:code @stockid})
                              (rf/dispatch [:secuinfo-set-secuinfo-nav-y
                                            (-> el
                                                r/dom-node
                                                .getBoundingClientRect
                                                .-top)]))
      :reagent-render
      (fn []
        [:div#secuinfo-nav.secuinfo-nav
         {:style {:position         "fixed"
                  :bottom           0
                  :width            "100%"
                  :height           "2.1rem"
                  :box-shadow       "0 -2px 5px 0 rgba(130,130,130,0.25)"
                  :color            "#666"
                  :background-color "#fff"
                  :z-index          10
                  :text-align       "center"}}
         (doall
          (for [[index item] (indexed secuinfo-nav-data)]
            ^{:key index}
            [:div.bottom-nav-btn
             (deep-merge
              {:style {:position "absolute"
                       :width    (str (/ 100 (count secuinfo-nav-data)) "%")
                       :height   ".85rem"
                       :top      ".15rem"
                       :left     (str (* index (/ 100 (count secuinfo-nav-data))) "%")}}
              (if (= 2 index)
                {:on-click #(do
                              (wx/set-share
                               {:title
                                (str (get-stock-title @stockname @stockid)
                                     (if (or @vip? @secu-balance)
                                       (str " | " (some-> @stock-stars (.toFixed 0)) "星"))
                                     "好想看星级啊，帮我点点链接吧")
                                :desc
                                (if (or @vip? @secu-balance)
                                  (str "技术面" (:tech scores)
                                       " 基本面" (:fundamental scores)
                                       " 资金面" (:capital scores)
                                       " 消息面" (:msg scores))
                                  "点击解锁星级评分")
                                :type "1"})
                              (.log js/console "share-tip-show")
                              (rf/dispatch [:share-tip-show]))}
                {})
              (:properties item))
             [:div
              {:style {:line-height "1.1rem"
                       :font-size   ".8rem"}}
              [(icon-key (:icon item))]]
             [:div
              {:style {:line-height ".7rem"
                       :font-size   ".6rem"}}
              (:text item)]]))])})))

(defn pay-kaitong
  [{:keys [id rec name price url]}]
  [:a
   {:href url}
   [:div.kt-item
    {:id id}
    (when rec
      [:div.decorate
       [:img {:src "/wx/img/tui-jian.png"}]])
    [:div.left
     [:div.name name]
     [:div.price price]]
    [:div.right
     [:div.button "开通"]]]])

(defn pay-window
  []
  (let [vip?           (rf/subscribe [:secuinfo-is-vip])
        secu-balance   (rf/subscribe [:secuinfo-secu-balance])
        available-days (rf/subscribe [:secuinfo-vip-available-days])
        val            (r/atom "")
        verify-status  (rf/subscribe [:secuinfo-verify-status])]
    (fn []
      [:div.pay-dialog
       [:div.logo
        [:img {:src "http://static.joudou.com/m/common/logo.png"}]]
       [:div.counts-desc
        (if @vip?
          [:div
           "你的VIP权限剩余时间"
           [:span.account-info
            (some-> @available-days (.toFixed 0)) "天"]]
          (if (some->> @secu-balance
                       zero?
                       not)
            "太遗憾了，本月查看评级次数已用完。赶紧分享专属链接，让小伙伴帮你增加次数吧"
            [:span
             "你的查看次数还剩"
             [:span.account-info @secu-balance]
             "次。赶紧分享专属链接，让小伙伴帮你增加次数吧"]))]
       [:div.share
        [:span.text
         "戳右上角分享："
         [:span.share-page-icon
          [:i.icon.icon-share-page]]
         [:img {:src "/wx/img/moments.png"}]]]
       [:div.kai-tong
        [:div.text "土豪用户简单粗暴方案："]
        [pay-kaitong {:id    "season"
                      :name  "季度VIP"
                      :price "¥ 109"
                      :rec   true
                      :url   (pay-urls :season)}]
        [pay-kaitong {:id    "month"
                      :name  "月度VIP"
                      :price "¥ 39"
                      :rec   false
                      :url   (pay-urls :month)}]
        [pay-kaitong {:id    "halfyear"
                      :name  "半年VIP"
                      :price "¥ 209"
                      :rec   false
                      :url   (pay-urls :halfyear)}]
        [pay-kaitong {:id    "year"
                      :name  "年度VIP"
                      :price "¥ 399"
                      :rec   false
                      :url   (pay-urls :year)}]]
       [:div.tel-form
        [:div.text
         "付款成功后，请在下方输入你在有赞商城购买商品时填写的手机号，开通VIP"]
        [:div.tel-input
         "手机号："
         [:input#phone-number-input
          {:type        "number"
           :name        "tel-number-input"
           :placeholder "请输入手机号"
           :value       @val
           :on-input    (fn [e]
                          (reset! val (-> e .-target .-value)))}]
         [:div.button-submit
          {:on-click #(rf/dispatch [:secuinfo-verify-phone @val])}
          "开通"]]
        [:div.tel-check
         (cond
           (true? @verify-status)  "验证成功"
           (false? @verify-status) "验证失败，请核对你输入的手机号"
           :else                   "")]]])))

(defn mask-inner
  []
  (let [mask-type    (rf/subscribe [:secuinfo-mask-type])
        mask-content (rf/subscribe [:secuinfo-mask-content])]
    (fn []
      [:div.inner
       (case @mask-type
         :img [:img
               {:style {:width      "12rem"
                        :min-height "16rem"
                        :height     "auto"}
                :src   (secuinfo-mask-img-data @mask-content)}]
         :div [pay-window])])))

(defn secuinfo-mask
  []
  (let [show? (rf/subscribe [:secuinfo-mask-show])]
    (fn []
      [:div
       (when-not @show? {:style {:display "none"}})
       [:div.viewport-mask
        {:style    {:position         "fixed"
                    :top              0
                    :right            0
                    :bottom           0
                    :left             0
                    :z-index          90
                    :background-color "#000"
                    :opacity          0.5}
         :on-click #(rf/dispatch [:secuinfo-mask-hide])}]
       [:div.mask-content
        {:style {:position    "absolute"
                 :top         "50%"
                 :left        "50%"
                 :transform   "translate(-50%,-50%)"
                 :text-align  "center"
                 :opacity     ".9"
                 :padding     ".5rem"
                 :font-size   ".7rem"
                 :line-height ".8rem"
                 :transition  "margin-top .5s linear"
                 :z-index     100}}
        [:div.container
         {:style {:border        "1px solid #fff"
                  :position      "relative"
                  :border-radius ".25rem"
                  :border-top    "none"
                  }}
         [:div.close-btn
          {:style    {:height  "1.1rem"
                      :display "flex"}
           :on-click #(rf/dispatch [:secuinfo-mask-hide])}
          [:span.span-1
           {:style {:flex          1
                    :border-top    "1px solid #fff"
                    :border-radius ".25rem 0"}}]
          [:span.span-2
           {:style {:position      "relative"
                    :top           "-1.1rem"
                    :border        ".15rem solid #fff"
                    :border-radius "50%"
                    :display       "inline-block"
                    :width         "1.5rem"
                    :height        "1.5rem"
                    :line-height   "1.6rem"
                    :font-size     "1rem"
                    :color         "#fff"}}
           [:i.icon.icon-times]]
          [:span.span-3
           {:style {:flex          1
                    :border-top    "1px solid #fff"
                    :border-radius "0 .25rem"}}]]
         [mask-inner]]]])))

(defn intro
  []
  [:div])

(defn secuinfo
  []
  (let [stockid (rf/subscribe [:secuinfo-stockid])]
    (fn []
     [:div
      [search]
      [stock-title]
      [stock-stars]
      [secuinfo-noodles]
      [secuinfo-main]
      [secuinfo-nav]
      [secuinfo-mask]
      [share-tip]
      [intro]])))
