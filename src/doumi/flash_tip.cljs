(ns doumi.flash-tip
  (:require [reagent.core :as r]
            [re-frame.core :as rf]
            [doumi.doumi-config :refer [flash-tip-data]]
            [doumi.helper :refer [base icon-key]]
            [doumi.csshelper :refer [transition-css]]
            ))

(defn flash-tip
  []
  (let [show (rf/subscribe [:flash-tip-show])
        kw   (rf/subscribe [:flash-tip-kw])]
    (fn []
      (let [[icon-name text-line-1 text-line-2] (get flash-tip-data @kw)]
        [:div.flash-tip
         {:style (merge {:display          (if @show "block" "none")
                         :position         "absolute"
                         :top              "50%"
                         :left             "50%"
                         :background-color "rgba(51,51,51,.75)"
                         :width            (base 160)
                         :height           (base 160)
                         :border-radius    (base 16)
                         :transform        "translate(-50%,-50%)"
                         :color            "#fff"
                         :z-index          10000
                         :padding          0
                         :text-align       "center"
                         :opacity          (if @show 1 0)}
                        (transition-css "opacity 1s ease-out"))}
         [:div.icon-wrapper
          {:style {:padding-top    (base 25)
                   :padding-bottom (base 15)
                   :font-size      (base 42)
                   :width          "100%"}}
          [(icon-key icon-name)]]
         [:div.text-wrapper
          {:style {:font-size (base 24)
                   :width     "100%"}}
          [:div.text-line-1 text-line-1]
          [:div.text-line-2 text-line-2]]]))))
