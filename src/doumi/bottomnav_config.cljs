(ns doumi.bottomnav-config)

(defonce bottom-nav-data
  [{:href nil
    :icon "mine"
    :text "我的"}
   {:href "/msgList.html"
    :icon "msg"
    :text "消息神器"}
   {:href "/stock/self"
    :icon "favorstock"
    :text "自选股"}])

;; 自选股页面的底部导航
(defonce bottom-nav-data-self
  [{:href nil
    :icon "mine"
    :text "我的"}
   {:href "/msgList.html"
    :icon "msg"
    :text "消息神器"}
   {:href "/stock/annc/#/annc/date"
    :icon "pdf-o"
    :text "自选股公告"}])
