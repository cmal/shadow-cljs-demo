(ns doumi.bottomnav-subpage
  (:require [re-frame.core :as rf]
            [doumi.helper :refer [set-hash! stockid2secucode]]))

(defn bottom-nav-subpage-data-annc
  [stockid stockname in-fav]
  [{:props {:style    {:color "#666"}
            :on-click (fn [e]
                        (set-hash! "/date")
                        (rf/dispatch [:annc-set-display-type :date]))}
    :text  "自选股公告"
    :icon  "icon-pdf-o"}
   {:props {:style {:color "#666"}
            :href  (str "/new-secuinfo?id=" stockid "&name=" stockname)}
    :text  "个股详情"
    :icon  "icon-stock-main"}
   {:props (merge {:style {:color (if in-fav "#999" "#666")}}
                  (when in-fav {:on-click #(rf/dispatch [:search-add-favorstock stockid])}))
    :text  "加自选"
    :icon  (if in-fav "icon-checked" "icon-add-block")}
   {:props {:style    {:color "#666"}
            :on-click #(rf/dispatch [:share-tip-show true])}
    :text  "分享"
    :icon  "icon-share"}])
