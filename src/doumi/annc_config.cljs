(ns doumi.annc-config
  (:require [clojure.string :as str]))

(defonce annc-nav-titles
  [{:text "最新公告"
    :id   :annc}
   {:text "最新业绩"
    :id   :performance}
   {:text "分红送转"
    :id   :dividend}
   {:text "下周前瞻"
    :id   :nextweek}])

(defonce perf-icon-conf
  {"forecast" {:name "yu" :color "#6680a9"}
   "fast"     {:name "kuai" :color "#33568c"}
   "income"   {:name "cai" :color "#284a7d"}})

(defn get-perf-icon-name
  [cate]
  (get-in perf-icon-conf [cate :name]))

(defn get-perf-icon-color
  [cate]
  (get-in perf-icon-conf [cate :color]))

(defn get-dividend-logger
  [jump]
  (str "secuinfo.capital." jump "FromBonusDividend"))

(defn get-dividend-jump-code [title]
  (if (str/index-of title "派")
    "rate"
    "bonus"))

(defonce nextweek-conf
  {"reserve"    {:jump   "egr"
                 :logger "secuinfo.fundamental.niZoomFromProspect"
                 :icon   "pi"}
   "floating"   {:jump   "unlock"
                 :logger "secuinfo.capital.unlockFromProspect"
                 :icon   "jie"}
   "exdividend" {:jump   "bonus"
                 :logger "secuinfo.capital.bonusFromProspect"
                 :icon   "chu"}})

(defn get-nextweek-jump-code
  [cate]
  (get-in nextweek-conf [cate :jump]))

(defn get-nextweek-icon-name
  [cate]
  (get-in nextweek-conf [cate :icon]))

(defn get-nextweek-logger
  [cate]
  (get-in nextweek-conf [cate :logger]))
