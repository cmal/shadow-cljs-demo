(ns doumi.hof
  (:require [reagent.core :as r]
            [re-frame.core :as rf]
            [clojure.string :as str]
            [doumi.pos :refer [scroll-top scroll-left
                               inner-height client-height scroll-height]]
            [doumi.listener :refer [listen-to win-listen-to unlisten-to win-unlisten-to]]
            ))


(defn lazy-load-builder
  "data-sub key used for main data for renderer, data-dispatch key
  used for lazy load new data, display-name is a string for
  :display-name, render-generator is a higher order function who takes
  a parameter of main data and returns a rengent-render function."
  [init-dispatch lazy-dispatch display-name render-f]
  (let [this-touch-loaded (r/atom true)
        set-flag          #(reset! this-touch-loaded true)
        clear-flag        #(reset! this-touch-loaded false)
        init-load         (fn []
                            (when init-dispatch
                              (when (>= (+ (scroll-top) (inner-height)) (client-height))
                                (rf/dispatch init-dispatch))))
        lazy-load         (fn []
                            (when
                                (and
                                 (>= (+ (scroll-top) (inner-height)) (client-height))
                                 (not @this-touch-loaded))
                              (set-flag)
                              (rf/dispatch lazy-dispatch)))]
    (r/create-class
     {:display-name           display-name
      :component-did-update   init-load
      :component-did-mount    (fn [e]
                                (win-listen-to "scroll" lazy-load)
                                (listen-to "touchstart" clear-flag)
                                (init-load))
      :component-will-unmount (fn [e]
                                (win-unlisten-to "scroll" lazy-load)
                                (unlisten-to "touchstart" clear-flag))
      :reagent-render         render-f})))
