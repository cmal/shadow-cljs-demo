(ns doumi.doumi-config)

(defonce flash-tip-data
  {:favorstock-add-success ["check-circle" "已添加" "至自选"]
   :favorstock-add-failed  ["times" "添加自选" "失败"]
   :doumi-sent-to-term     ["check-circle" "已推送" "至公众号"]
   :indexinfo-reminder-set ["check-circle" "已设置" "PE倍数提醒"]})

(defonce vip-price
  {:month    39
   :season   109
   :halfyear 209
   :year     399})

(defonce vip-typename
  {:month    "月度"
   :season   "季度"
   :halfyear "半年"
   :year     "年度"})

#_(defonce youzan-vip-url
    {:month    "https://detail.youzan.com/show/goods?alias=2fz1ktfbsocgp&redirect_count=1&sf=wx_sm"
     :season   "https://detail.youzan.com/show/goods?alias=1ycomou0e7dcp&sf=wx_sm"
     :halfyear "https://detail.youzan.com/show/goods?alias=26xt4rmp64ukp&sf=wx_sm"
     :year     "https://detail.youzan.com/show/goods?alias=35wq633humg9l&sf=wx_sm"})


(defonce youzan-vip-url
  {:month    "https://h5.youzan.com/v2/goods/2fz1ktfbsocgp"
   :season   "https://h5.youzan.com/v2/goods/1ycomou0e7dcp"
   :halfyear "https://h5.youzan.com/v2/goods/26xt4rmp64ukp"
   :year     "https://h5.youzan.com/v2/goods/35wq633humg9l"})
