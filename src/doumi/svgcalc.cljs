(ns doumi.svgcalc
  (:require
   [clojure.string :as str]
   [cljs-time.core :as ct]
   [doumi.helper :refer [indexed]]
   ))

(defn line-chart
  [{:keys [date data]} opts]
  (let [{:keys [x-max y-max margin]} opts
        indexed-data                 (indexed data)
        data-max                     (apply max data)
        data-min                     (apply min data)
        cnt                          (count data)
        x-left-axis-margin           (get-in margin [:x :left :axis])
        ;;        x-left-graph-margin          (get-in margin [:x :left :graph])
        x-right-axis-margin          (get-in margin [:x :right :axis])
        x-right-graph-margin         (get-in margin [:x :right :axis])
        y-top-axis-margin            (get-in margin [:y :top :axis])
        y-top-graph-margin           (get-in margin [:x :top :graph])
        y-bottom-axis-margin         (get-in margin [:y :bottom :axis])
        ;;        y-bottom-graph-margin        (get-in margin [:x :bottom :graph])
        chart-x                      (* x-max (- 1 (+ x-left-axis-margin x-right-axis-margin)))
        graph-x                      (* x-max (- 1 (+ x-left-axis-margin
                                                      x-right-axis-margin
                                                      x-right-graph-margin)))
        x-ratio                      (/ graph-x cnt)
        chart-y                      (* y-max (- 1 (+ y-top-axis-margin
                                                      y-bottom-axis-margin)))
        graph-y                      (* y-max (- 1 (+ y-top-axis-margin
                                                      y-bottom-axis-margin
                                                      y-top-graph-margin)))
        y-ratio                      (/ graph-y (- data-max data-min))]
    (prn (last indexed-data))
    [:g {:transform (str "translate("
                         (* x-max x-left-axis-margin)
                         ","
                         (* y-max (- 1 y-bottom-axis-margin))
                         ") scale(1,-1)")}
     [:g.x-axis
      [:path
       {:stroke       "#999"
        :stroke-width 1
        :d            (str "M0,0H" chart-x)}]
      #_      [:line
               {:x1    0
                :y1    0
                :x2    chart-x
                :y2    0
                :style {:stroke "#333"
                        }}]]
     [:g.y-axis
      [:path
       {:stroke       "#999"
        :stroke-width 0.5
        :d            (str "M0,0V" chart-y)}]
      #_[:line
         {:x1    0
          :y1    0
          :x2    0
          :y2    chart-y
          :style {:stroke "#333"
                  }}
         ]]
     [:polyline
      {:points (str/join " " (map (fn [[x y]] (str (* x-ratio x) "," (* y-ratio y))) indexed-data))
       :style  {:stroke          "#ffab36"
                :stroke-width    1.5
                :stroke-linejoin "round"
                :stroke-linecap  "round"
                :shape-rendering "crispEdges"
                :fill            "none"}}]]))
