(ns doumi.listener
  )

(defn win-listen-to
  ([event callback]
   (win-listen-to event callback false))
  ([event callback use-capture]
   (-> js/window
       (.addEventListener event callback use-capture))))

(defn win-unlisten-to
  ([event callback]
   (win-unlisten-to event callback false))
  ([event callback use-capture]
   (-> js/window
       (.removeEventListener event callback use-capture))))

(defn listen-to
  ([event callback]
   (listen-to event callback false))
  ([event callback use-capture]
   (-> js/document
       .-body
       (.addEventListener event callback use-capture))))

(defn unlisten-to
  ([event callback]
   (unlisten-to event callback false))
  ([event callback use-capture]
   (-> js/document
       .-body
       (.removeEventListener event callback use-capture)))  )
