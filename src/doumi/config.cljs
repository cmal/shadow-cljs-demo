(ns doumi.config)

(def env
  (case (-> js/location .-host)
    "m.joudou.com"      "www"
    "m.beta.joudou.com" "beta"
    "www"))
